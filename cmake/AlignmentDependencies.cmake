###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
if(NOT COMMAND lhcb_find_package)
  # Look for LHCb find_package wrapper
  find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
  if(LHCbFindPackage_FILE)
      include(${LHCbFindPackage_FILE})
  else()
      # if not found, use the standard find_package
      macro(lhcb_find_package)
          find_package(${ARGV})
      endmacro()
  endif()
endif()

# -- Public dependencies
lhcb_find_package(Moore REQUIRED)

find_package(Boost REQUIRED
    headers
    regex
)
find_package(Eigen3 REQUIRED)
find_package(GSL REQUIRED)
find_package(ROOT 6.20 REQUIRED
    Core
    GenVector
    Gpad
    Graf
    Hist
    MathCore
    RIO
    RooFit
    Tree
)

if(USE_DD4HEP)
  find_package(DD4hep REQUIRED)
endif()

find_data_package(AppConfig)
find_data_package(FieldMap)
find_data_package(ParamFiles)

# -- Private dependencies
if(WITH_Alignment_PRIVATE_DEPENDENCIES)
    find_package(AIDA REQUIRED)
    find_package(Boost REQUIRED filesystem)
    find_package(CLHEP REQUIRED)
    find_package(fmt REQUIRED)
    find_package(ROOT 6.20 REQUIRED
        Matrix
        Minuit
        Postscript
        Spectrum
    )
    find_package(Python REQUIRED Interpreter)
endif()
