# Alignment constants
Improved alignments for 2022 data are being made available to be used with DD4hep. Please note that a dedicated half alignment is necessary to account for the drift of the right VELO half. If there is no half alignment for a run  you are interested in (or at least a run shortly before), please get in touch. Otherwise the relative VELO half misalignment could be significant.

## Overview
The table below gives a quick overview of which constants were updated in a tag. For a detailed summary of changes, see below. The "X" indicates that the corresponding constants have been updated, otherwise they are the same as in the previous tags.

| sub-system             | v2          | v4          | 2022_12_HLT2           | v5                | v6          | v7                    | v9                | v10 |
| ---------------------- | ----------- | ----------- | ---------------------- | ----------------- | ----------- | --------------------- | ----------------- | --- |
| VELO module and sensor | X           |             | X                      |                   |             |                       |                   |     |
| VELO half              | two runs    | more runs   | fills 8489, 8491, 8496 | for 4 runs of VdM |             |                       |                   |     |
| SciFi                  | X           |             | X                      |                   |             |                       |                   |  X  |
| Muon                   |             |             |                        |                   |             |                       |                   |     |
| RICH mirror            |             |             | X                      |                   |             |                       |                   |  X  |
| RICH panel             | X           |             | X                      |                   | X           |                       |                   |     |
| RICH calibration       |             |             | for some runs          | for other runs    |             |                       |                   |     |
| Other                  |             |             |                        |                   |             | Scifi mat contraction | Tell40 conditions |     |
| Magnet                 | MagUp wrong | MagUp wrong | MagUp wrong            | MagUp wrong       | MagUp wrong | MagUp wrong           | MagUp correct     |     |

## Usage
To run with an improved alignment

```
options.conditions_version = TAGNAME
options.simulation = False
# make sure that you're using the git database
# from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
# dd4hep = DD4hepSvc()
# dd4hep.ConditionsLocation = 'git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git'
```

## Available tags

* latest tag  [AlignmentV10_2023_05_09_LHCP](AlignmentV10_2023_05_09_LHCP)
* previous tags
    * [AlignmentV9_2023_03_16_VPSciFiRich](AlignmentV9_2023_03_16_VPSciFiRich)
    * [AlignmentV8_2023_03_15_VPSciFiRich](AlignmentV8_2023_03_15_VPSciFiRich) (mainly for testing, please don't use)
    * [AlignmentV7_2023_02_16_VPSciFiRich](AlignmentV7_2023_02_16_VPSciFiRich)
    * [AlignmentV6_2023_02_07_VPSciFiRich](AlignmentV6_2023_02_07_VPSciFiRich)
    * [AlignmentV5_2023_01_31_VPSciFiRich](AlignmentV5_2023_01_31_VPSciFiRich)
    * [2022_12_HLT2Processing](2022_12_HLT2Processing)
    * [AlignmentV4_2022_11_24_VPSciFiRichPanel](AlignmentV4_2022_11_24_VPSciFiRichPanel)
    * [AlignmentV2_2022_11_22_VPSciFiRichPanel](AlignmentV2_2022_11_22_VPSciFiRichPanel)
### AlignmentV2_2022_11_22_VPSciFiRichPanel
First tag containing VELO, SciFi and RICH panels alignment. Note that a dedicated VELO alignment is only included for runs 252975 and  254869

| TAGNAME | 
|-------------------------------|
| AlignmentV2_2022_11_22_VPSciFiRichPanel |
| **VELO**  |
| - v1 module and sensor alignment    [elog](https://lblogbook.cern.ch/Alignment+and+calibration/5)
| - half alignment: starting from velo open alignment, fix the A-left side and align only for the C-right side:
|   - Run 0 - 252974 Design position
|    - Run 252975-254868 alignment evaluated on run 252975
|    - Run 254869-...... alignment evaluated on run 254869
| **SciFi** |
| - v1 Modules Tx Rz  [elog](https://lblogbook.cern.ch/Alignment+and+calibration/6) |
|    - valid for all runs|
| **RICH mirror** | 
| - Design position |
| **RICH panel** |
| - v1 [git](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/merge_requests/44)
|   - Run 200000 - ....|
| - Design position |
| **Muon** |
| - Design position |

### AlignmentV4_2022_11_24_VPSciFiRichPanel
Same as `AlignmentV2_2022_11_22_VPSciFiRichPanel` with the addition of VELO half alignment for more runs listed [here](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/tree/AlignmentV4_2022_11_24_VPSciFiRichPanel/Conditions/VP/Alignment/Global.yml)

| TAGNAME | 
|-------------------------------|
| AlignmentV4_2022_11_24_VPSciFiRichPanel |
| **VELO**  |
| - v1 module and sensor alignment    [elog](https://lblogbook.cern.ch/Alignment+and+calibration/5) valid for all runs
| - half alignment: starting from velo open alignment, fix the A-left side and align only for the C-right side valid for [runs](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/tree/AlignmentV4_2022_11_24_VPSciFiRichPanel/Conditions/VP/Alignment/Global.yml) 253433  253449  253460  253474  253495  253503  253513  253529  255293 251342  253435  253450  253468  253475  253496  253504  253516  253530  255344 252975  253444  253452  253471  253476  253497  253507  253518  253531  255362 253430  253445  253453  253472  253478  253500  253511  253519  254869  255366 253432  253448  253456  253473  253479  253501  253512  253522  255265  255402
| **SciFi** |
| - v1 Modules Tx Rz  [elog](https://lblogbook.cern.ch/Alignment+and+calibration/6) |
|    - valid for all runs|
| **RICH mirror** | 
| - Design position |
| **RICH panel** |
| - v1 [git](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/merge_requests/44)
|   - Run 200000 - ....|
| **Muon** |
| - Design position |


### 2022_12_HLT2Processing
see [log book](https://lblogbook.cern.ch/Alignment+and+calibration/28)

| TAGNAME                                                                                                                                                                                                                                                                                                                                                |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 2022_12_HLT2Processing                                                                                                                                                                                                                                                                                                                                 |
| **VELO**                                                                                                                                                                                                                                                                                                                                               |
| - v2 module and sensor alignment    [elog](https://lblogbook.cern.ch/Alignment+and+calibration/27) valid for all runs                                                                                                                                                                                                                                  |
| - half alignment: starting from velo open alignment, fix the A-left side and align only for the C-right side valid for [runs listed here](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/tree/2022_12_HLT2Processing/Conditions/VP/Alignment/Global.yml). In particular for all runs of fills 8489, 8491, 8496 where data was available. |
| **SciFi**                                                                                                                                                                                                                                                                                                                                              |
| - v2 Modules Tx Rz  [elog](https://lblogbook.cern.ch/Alignment+and+calibration/26)                                                                                                                                                                                                                                                                     |
| - valid for all runs                                                                                                                                                                                                                                                                                                                                   |
| **RICH mirror**                                                                                                                                                                                                                                                                                                                                        |
| - central mirror alignment                                                                                                                                                                                                                                                                                                                             |
| **RICH panel**                                                                                                                                                                                                                                                                                                                                         |
| - Run 200000 - ....                                                                                                                                                                                                                                                                                                                                    |
| [commit](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/commit/1560dd8cdbcb6d94e98398c050db41acf504564f)                                                                                                                                                                                                                                                                                                                                                       |
| **RICH calibration**                                                                                                                                                                                                                                                                                                                                   |
| [commit](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/commit/e8cf22f3ecd3ac2c6d03129a6ff11cb319144215)                                                                                                                                                                                                                          |
| **Muon**                                                                                                                                                                                                                                                                                                                                               |
| - Design position                                                                                                                                                                                                                                                                                                                                      |




### AlignmentV5_2023_01_31_VPSciFiRich
* based on `2022_12_HLT2Processing`
* updated RICH refractive index calibration for more runs
* VELO right half alignment for runs 253026, 253082, 253116, 253211 (VdM scan)


| TAGNAME                                                                                                                                                                                                                                                                                                                                                |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| AlignmentV5_2023_01_31_VPSciFiRich                                                                                                                                                                                                                                                                                                                                |
| **VELO**                                                                                                                                                                                                                                                                                                                                               |
| - v2 module and sensor alignment    [elog](https://lblogbook.cern.ch/Alignment+and+calibration/27) valid for all runs                                                                                                                                                                                                                                  |
| - half alignment: starting from velo open alignment, fix the A-left side and align only for the C-right side valid for [runs listed here](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/tree/2022_12_HLT2Processing/Conditions/VP/Alignment/Global.yml). In particular for all runs of fills 8489, 8491, 8496 where data was available.  Right half alignment for VdM runs 253026, 253082, 253116, 253211 |
| **SciFi**                                                                                                                                                                                                                                                                                                                                              |
| - v2 Modules Tx Rz  [elog](https://lblogbook.cern.ch/Alignment+and+calibration/26)                                                                                                                                                                                                                                                                     |
| - valid for all runs                                                                                                                                                                                                                                                                                                                                   |
| **RICH mirror**                                                                                                                                                                                                                                                                                                                                        |
| - central mirror alignment                                                                                                                                                                                                                                                                                                                             |
| **RICH panel**                                                                                                                                                                                                                                                                                                                                         |
| - Run 200000 - ....                                                                                                                                                                                                                                                                                                                                    |
| [commit](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/commit/1560dd8cdbcb6d94e98398c050db41acf504564f)                                                                                                                                                                                                                                                                                                                                                       |
| **RICH calibration**                                                                                                                                                                                                                                                                                                                                   |
| [commit](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/commit/e8cf22f3ecd3ac2c6d03129a6ff11cb319144215)   [update](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/commit/7f18fa125398e0a8a79e27007b4f186894340a14)                                                                                                                                                                                                                                    |
| **Muon**                                                                                                                                                                                                                                                                                                                                               |
| - Design position                                                                                                                                                                                                                                                                                                                                      |



### AlignmentV6_2023_02_07_VPSciFiRich
* based on `AlignmentV5_2023_01_31_VPSciFiRich`
* only difference between this tag and `AlignmentV5_2023_01_31_VPSciFiRich` are updated RICH panel conditions based off translation scan in xy ([see study here](https://indico.cern.ch/event/1252457/))

| TAGNAME                                                                                                                                                                                                                                                                                                                                                |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| AlignmentV6_2023_02_07_VPSciFiRich                                                                                                                                                                                                                                                                                                                               |
| **VELO**                                                                                                                                                                                                                                                                                                                                               |
| - v2 module and sensor alignment    [elog](https://lblogbook.cern.ch/Alignment+and+calibration/27) valid for all runs                                                                                                                                                                                                                                  |
| - half alignment: starting from velo open alignment, fix the A-left side and align only for the C-right side valid for [runs listed here](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/tree/2022_12_HLT2Processing/Conditions/VP/Alignment/Global.yml). In particular for all runs of fills 8489, 8491, 8496 where data was available.  Right half alignment for VdM runs 253026, 253082, 253116, 253211 |
| **SciFi**                                                                                                                                                                                                                                                                                                                                              |
| - v2 Modules Tx Rz  [elog](https://lblogbook.cern.ch/Alignment+and+calibration/26)                                                                                                                                                                                                                                                                     |
| - valid for all runs                                                                                                                                                                                                                                                                                                                                   |
| **RICH mirror**                                                                                                                                                                                                                                                                                                                                        |
| - central mirror alignment                                                                                                                                                                                                                                                                                                                             |
| **RICH panel**                                                                                                                                                                                                                                                                                                                                         |
| - Run 200000 - ....                                                                                                                                                                                                                                                                                                                                    |
| [commit](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/commit/8a00f0f8c871a0131a59d34f21e788c2fbe979b7)                                                                                                                                                                                                                                                                                                                                                       |
| **RICH calibration**                                                                                                                                                                                                                                                                                                                                   |
| [commit](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/commit/e8cf22f3ecd3ac2c6d03129a6ff11cb319144215)   [update](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/commit/7f18fa125398e0a8a79e27007b4f186894340a14)                                                                                                                                                                                                                                    |
| **Muon**                                                                                                                                                                                                                                                                                                                                               |
| - Design position

### AlignmentV7_2023_02_16_VPSciFiRich
* same as previous tag, with the addition of conditions for SciFi mat contraction, see [here](https://gitlab.cern.ch/lhcb/LHCb/-/merge_requests/3591) and related MRs

### AlignmentV8_2023_03_15_VPSciFiRich
* test tag to check inclusion of magnet up polarity. Please do not use

### AlignmentV9_2023_03_16_VPSciFiRich 
* based on `AlignmentV7_2023_02_16_VPSciFiRich`
* add missing MagUp polarity for fill 8496
* update Tell40 conditions

### AlignmentV10_2023_05_09_LHCP
- RICH1 full mirror alignment
- SciFi alignment v3

## VELO right half alignment
A significant drift (consistent with a rotation around the y-axis) was observed for the right VELO half during 2022 data-taking. To account for this, a dedicated right half alignment is required per run. The list of runs where this half alignment was evaluated can be found [here](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/tree/alignment2022/Conditions/VP/Alignment/Global.yml). Please note that depending on the tag some runs might not have a dedicated half alignment.
### History 
* [AlignmentV2_2022_11_22_VPSciFiRichPanel](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/tree/AlignmentV2_2022_11_22_VPSciFiRichPanel/Conditions/VP/Alignment/Global.yml): half alignments only for runs 252975 and 254869
* [AlignmentV4_2022_11_24_VPSciFiRichPanel](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/tree/AlignmentV4_2022_11_24_VPSciFiRichPanel/Conditions/VP/Alignment/Global.yml): more runs included
* [2022_12_HLT2Processing](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/tree/2022_12_HLT2Processing/Conditions/VP/Alignment/Global.yml): all runs for fills 8489, 8491, 8496 (where data for alignment  was available)
* [AlignmentV5_2023_01_31_VPSciFiRich](https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/tree/AlignmentV5_2023_01_31_VPSciFiRich/Conditions/VP/Alignment/Global.yml): VdM runs 253026, 253082, 253116, 253211

No further half alignments were added since `AlignmentV5_2023_01_31_VPSciFiRich`. Note that the HLT2 processing started with `2022_12_HLT2Processing` and continued with `AlignmentV5_2023_01_31_VPSciFiRich`, so HLT2-processed data is guaranteed to include the half alignment listed in `2022_12_HLT2Processing` tag, in particular for the fills 8489, 8491, 8496


## Requesting a new tag
Please follow the procedure outlined [here](https://indico.cern.ch/event/1252457/contributions/5261637/attachments/2590991/4471192/WP4_9_02_2023.pdf):
* ask a slot at a RTA-WP4/5 meeting to present the updates
* contact Florian Reiss who is the responsible for tags
* committing the updates to the branch `alignment2022` (without creating a new tag) should be done by the responsible of the subsystem
* if the tag is urgent the presentation can be skipped but the RTA-WP4/5 coordinators need to be informed
* update the documenation with the changes included in the tag. Ideally also create an entry in the alignment and calibration log book

## Contact persons
* Silvia Borghi
* Florian Reiss


Thanks to the many people who provided the various alignment constants or otherwise contributed!


