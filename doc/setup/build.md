# Building options
There are a few ways to setup the alignment software. For developing, using the stack setup is recommended.

## Using the stack

Follow the instructions on [setting up the stack](https://gitlab.cern.ch/rmatev/lb-stack-setup/). These are general for any project. By default, a DD4hep build will be used.

## Building from the nightlies
- set up lb-dev for nightlies head
- checkout alignment package and build
```bash
lb-set-platform x86_64_v2-centos7-gcc11-opt
lb-dev --nightly lhcb-master Alignment/HEAD
cd ./AlignmentDev_HEAD
git lb-use Alignment
git lb-checkout Alignment/<branch> Alignment
```

## Cloning and compiling from Alignment master
- clone Alignment from the git repository
- init the project
- make
- run
```bash
git clone ssh://git@gitlab.cern.ch:7999/lhcb/Alignment.git
cd Alignment
lb-project-init
make -j8
./build-something/run gaudirun.py options.py
```

## Setting up a tagged version 
You can check the releases of the Alignment in https://gitlab.cern.ch/lhcb/Alignment/-/releases . Setting up the version is with using the lb-dev commands
```bash
lb-set-platform x86_64_v2-centos7-gcc11-opt
lb-dev Alignment/<versionnumber>
cd ./AlignmentDev_versionnum
git lb-use Alignment
git lb-checkout Alignment/versionnum Alignment
make
```


