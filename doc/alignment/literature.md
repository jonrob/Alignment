# Literature
## Presentations and Proceedings
* *F.Reiss* **Real-time alignment procedure at the LHCb experiment for Run 3** [[LHCb-TALK-2022-299]](https://cds.cern.ch/record/2841343) [[LHCb-PROC-2023-001]](http://cds.cern.ch/record/2846414)

## Publications

## Legacy documentation
### Twiki
* [TAlignmentManual](https://twiki.cern.ch/twiki/bin/view/LHCb/TAlignmentManual)
* [AlignmentIntro](https://twiki.cern.ch/twiki/bin/view/LHCb/AlignmentIntro)
* [AlignIntro AlignLog](https://twiki.cern.ch/twiki/bin/view/LHCb/AlignmentIntroAlignlog)
* [Conference plots](https://twiki.cern.ch/twiki/bin/view/LHCb/ConferencePlots)
* [LHCbDetectorAlignment](https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbDetectorAlignment)
* [Online Alignment HowTo](https://twiki.cern.ch/twiki/bin/view/LHCb/OnlineAlignmentHowTo)

### CodiMD
* [Alignment constants](https://codimd.web.cern.ch/j8PNz5kCRbuz5eoA7QHXDA?view)
* [Run 3 Alignment instructions](https://codimd.web.cern.ch/JSseMIRKR3eLjMArdrzw-Q?view)

