Welcome to the Alignment documentation!
=======================================

The Alignment software package is designed to manage tools and scripts related to the alignment of the LHCb detector.

.. toctree::
   :caption: User Guide
   :maxdepth: 2

   setup/general
   setup/build
   setup/input_files
   setup/run_humboldt
   constants/constants
   alignment/piquet
   alignment/literature

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
