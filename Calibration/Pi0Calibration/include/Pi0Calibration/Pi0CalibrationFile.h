/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * =====================================================================================
 *
 *       Filename:  Pi0CalibrationFile.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  02/15/2017 05:28:23 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (), Zhirui.Xu@cern.ch
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef CALIBRATION_PI0CALIBRATION_PI0CALIBRATIONFILE_H
#define CALIBRATION_PI0CALIBRATION_PI0CALIBRATIONFILE_H

#include <map>
#include <string>
#include <utility>
#include <vector>

#include "Math/SMatrix.h"
#include "TTree.h"

namespace Calibration {
  namespace Pi0Calibration {
    struct Candidate {
      int            bkg;
      unsigned short ind1;
      unsigned short ind2;
      double         m12;
      double         prs1;
      double         prs2;
      double         g1E;
      double         g2E;
    };
    class Pi0CalibrationFile {
    public:
      enum { maxIdx = 16384 };
      template <class SEL>
      Pi0CalibrationFile( const std::string& filename, const std::string& tuplename, const std::string& outfilename,
                          const SEL& sel );
#ifndef __CLING__
      static auto make_indices_selector( const std::vector<unsigned int>& idx ) {
        std::vector<bool> bitmap( maxIdx );
        for ( auto i : idx ) bitmap[i] = true;
        return [bitmap]( unsigned i ) { return ( i < maxIdx && bitmap[i] ); };
      }
#endif
    public:
      Pi0CalibrationFile( const std::string& filename, const std::string& tuplename, const std::string& outfilename );
      Pi0CalibrationFile( const std::string& filename, const std::string& tuplename, const std::string& outfilename,
                          const std::vector<unsigned int>& indices );
      virtual ~Pi0CalibrationFile();
    };
  } // namespace Pi0Calibration
} // namespace Calibration

#endif
