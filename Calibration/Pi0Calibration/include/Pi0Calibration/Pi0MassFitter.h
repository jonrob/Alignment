/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * =====================================================================================
 *
 *       Filename:  Pi0MassFitter.h
 *
 *    Description:  (1) create the pi0 mass histogram;
 *                  (2) fit the histogram;
 *                  (3) save and return the results
 *
 *        Version:  1.0
 *        Created:  11/21/2016 04:32:16 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef CALIBRATION_PI0CALIBRATION_PI0MASSFITTER_H
#define CALIBRATION_PI0CALIBRATION_PI0MASSFITTER_H 1

#include <iostream>
#include <string>
#include <utility>

#include "TH1.h"

namespace Calibration {
  namespace Pi0Calibration {
    class Pi0MassFitter {
    public:
      Pi0MassFitter( TH1* hist, double mpi0, double sigma );
      virtual ~Pi0MassFitter() {}

      static Double_t backgroundSB( Double_t* x, Double_t* par );
      static Double_t polyn( Double_t* x, Double_t* par );
      static Double_t polynSB( Double_t* x, Double_t* par );
      static Double_t signal( Double_t* x, Double_t* par );
      static Double_t fitFunctionPol( Double_t* x, Double_t* par );

      void                      chi2fit( const std::string& outputdir, const std::string& outfilename, bool verbose );
      std::pair<double, double> getMean() { return m_mean; }
      std::pair<double, double> getSigma() { return m_sigma; }
      std::pair<double, double> getNSignal() { return m_nsignal; }
      int                       getStatus() { return m_status; }

    private:
      TH1*   m_hist;
      double m_mpi0;
      double m_sigma0;

      std::pair<double, double> m_mean;
      std::pair<double, double> m_sigma;
      std::pair<double, double> m_nsignal;
      int                       m_status;
    };
  } // namespace Pi0Calibration
} // namespace Calibration

#endif
