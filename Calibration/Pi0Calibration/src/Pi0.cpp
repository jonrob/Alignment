/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <memory>
#include <set>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/SmartDataPtr.h"
// ============================================================================
//  LHCbKrrnel
// ============================================================================
#include "Kernel/Counters.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
#include "Event/CaloHypo.h"
#include "Event/Vertex.h"
// ============================================================================
#include "Event/CaloDataFunctor.h"
// ============================================================================
#include "Event/ProtoParticle.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Algo.h"
#include "LoKi/BasicFunctors.h"
#include "LoKi/IHybridFactory.h"
#include "LoKi/ParticleCuts.h"
#include "LoKi/Photons.h"
// ============================================================================
// CaloUtils
// ============================================================================
#include "CaloFutureUtils/ClusterFunctors.h"
// ============================================================================
// DeCalorimeter
// ============================================================================
#include "CaloDet/DeCalorimeter.h"
// ============================================================================
// LHCbMath
// ============================================================================
#include "LHCbMath/LHCbMath.h"
// ============================================================================

// ==========================================================================
/** @class Pi0
 *  Simple algorithm for Calorimeter Cailbration using pi0 peak
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2009-09-28
 *
 *                    $Revision$
 *  Last modification $Date$
 *                 by $Author$
 */
class Pi0 : public LoKi::Algo {
  // ========================================================================
public:
  // ========================================================================
  StatusCode initialize() override; // the proper tinitialzation
  StatusCode analyse() override;    //  the main 'execution' method
  // ========================================================================
  // protected:
  // ========================================================================
  /** standard constructor
   *  @param name (INPUT) the algorithm instance name
   *  @param pSvc (INPUT) the pointer to Service Locator
   */
  Pi0( const std::string& name, //    the algorithm instance name
       ISvcLocator*       pSvc )      // the pointer to Service Locator
      : LoKi::Algo( name, pSvc )
      //
      , m_mirror( false )
      , m_veto_dm( -1 * Gaudi::Units::MeV )
      , m_veto_chi2( -1 )
      , m_numbers( "Counters/Kali" )
      , m_counters()
      , m_pi0CutExp( "PT > 200*MeV*(7-ETA)" )
      , m_pi0Cut( LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( false ) )
      , m_ecal( 0 )
      // histograms
      , m_h1( 0 ) {
    declareProperty( "Mirror", m_mirror, "Flag to activate Albert's trick with backroung estimation" )
        ->declareUpdateHandler( &Pi0::mirrorHandler, this );
    declareProperty( "Pi0VetoDeltaMass", m_veto_dm, "Delta-Mass for pi0-veto" )
        ->declareUpdateHandler( &Pi0::vetoHandler, this );
    declareProperty( "Pi0VetoChi2", m_veto_chi2, "Chi2 for pi0-veto" )->declareUpdateHandler( &Pi0::vetoHandler, this );
    declareProperty( "Pi0Cut", m_pi0CutExp, "Predicate for Pi0 (LoKi/Bender expression)" );
    declareProperty( "CounterTES", m_numbers, "TES location of Gaudi::Numbers object for global event actovity" );
    //
    m_counters.push_back( "nVelo" );
    m_counters.push_back( "nLong" );
    m_counters.push_back( "nPV" );
    m_counters.push_back( "nOT" );
    m_counters.push_back( "nITClusters" );
    m_counters.push_back( "nTTClusters" );
    m_counters.push_back( "nVeloClusters" );
    m_counters.push_back( "nEcalClusters" );
    m_counters.push_back( "nEcalDigits" );
    declareProperty( "Counters", m_counters, "List of counters" );
    //
    auto* histos = Gaudi::Utils::getProperty( this, "HistoProduce" );
    Assert( 0 != histos, "Unable to get property 'HistoProduce'" );
    if ( 0 != histos && 0 == histos->updateCallBack() ) { histos->declareUpdateHandler( &Pi0::histosHandler, this ); }
    //
    setProperty( "HistoProduce", false ).ignore();
    //
    ToolMap _combiners;
    _combiners[""] = "MomentumCombiner";
    StatusCode sc  = setProperty( "ParticleCombiners", _combiners );
    Assert( sc.isSuccess(), "Unable to set the proper ParticleCombiner" );
    //
  }
  // ========================================================================
private:
  // ========================================================================
  Pi0();                        //  the default constructor is disabled
  Pi0( const Pi0& );            //     the copy constructor is disabled
  Pi0& operator=( const Pi0& ); // the assignement operator is disabled
  /// fill tuple method
  void fillTuple( const Tuple&, const Gaudi::LorentzVector&, const Gaudi::LorentzVector&, const Gaudi::LorentzVector&,
                  const LHCb::Detector::Calo::CellID&, const LHCb::Detector::Calo::CellID&, const Gaudi::XYZPoint&,
                  const Gaudi::XYZPoint&, const LHCb::CaloCluster*, const LHCb::CaloCluster*, const int );
  // ========================================================================
  double getSeedCellEnergy( const LHCb::CaloCluster* );
  // ========================================================================
private:
  // ========================================================================
  /// setup monitoringhistograms
  void setupHistos();
  // ========================================================================
public:
  // ========================================================================
  void mirrorHandler( Gaudi::Details::PropertyBase& p ); // update handler for 'Mirror' property
  void vetoHandler( Gaudi::Details::PropertyBase& p );   // update handler for 'Veto' properties
  void histosHandler( Gaudi::Details::PropertyBase& p ); // update handler for property
  // ========================================================================
private:
  // ========================================================================
  bool   m_mirror;    // use Albert's trick?
  double m_veto_dm;   // Delta-mass for pi0-veto
  double m_veto_chi2; // Delta-mass for pi0-veto
  /// TES-location of counters for Global Event Activity
  std::string              m_numbers;  // counters for Global Event Activity
  std::vector<std::string> m_counters; // names for the counters
  /// pi0-Cut functor
  std::string      m_pi0CutExp; //       Pi0-Cut functor
  LoKi::Types::Cut m_pi0Cut;    // pi0-Cut functor expression
  DeCalorimeter*   m_ecal;      //      DeCalorimeter object for ECAL
  // ========================================================================
private:
  // ========================================================================
  AIDA::IHistogram1D* m_h1; // histogram with all pi0s
  // ========================================================================
};
// ============================================================================
// update handler for 'Mirror' property
// ============================================================================
void Pi0::mirrorHandler( Gaudi::Details::PropertyBase& /* p */ ) {
  // no action if not initialized yet:
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  //
  if ( m_mirror ) {
    Warning( "Albert's trick for background evaluation is   activated!", StatusCode::SUCCESS ).ignore();
  } else {
    Warning( "Albert's trick for background evaluation is deactivated!", StatusCode::SUCCESS ).ignore();
  }
  //
}
// ============================================================================
// update handler for 'Veto' properties
// ============================================================================
void Pi0::vetoHandler( Gaudi::Details::PropertyBase& p ) {
  // no action if not initialized yet:
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  //
  info() << "Pi0 Veto is : " << p << endmsg;
  //
}
// ============================================================================
// update handler for 'HistoProduce' property
// ============================================================================
void Pi0::histosHandler( Gaudi::Details::PropertyBase& /* p */ ) {
  // no action if not initialized yet:
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  //
  setupHistos();
  //
}
// ============================================================================
// setup monitoring histograms
// ============================================================================
void Pi0::setupHistos() {
  using Gaudi::Units::MeV;

  if ( produceHistos() ) {
    Warning( "Monitoring histograms are   activated", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_h1 ) { m_h1 = book( "mpi0", 0, 250 * MeV, 250 ); }
  } else {
    Warning( "Monitoring histograms are deactivated!", StatusCode::SUCCESS ).ignore();
    m_h1 = 0;
  }
}
// ============================================================================
// tuple fill helper function
// ============================================================================
void Pi0::fillTuple( const Tuple& tuple, const Gaudi::LorentzVector& p1, const Gaudi::LorentzVector& p2,
                     const Gaudi::LorentzVector& p12, const LHCb::Detector::Calo::CellID& cell1,
                     const LHCb::Detector::Calo::CellID& cell2, const Gaudi::XYZPoint& point1,
                     const Gaudi::XYZPoint& point2, const LHCb::CaloCluster* clus1, const LHCb::CaloCluster* clus2,
                     const int bkg ) {

  using Gaudi::Units::GeV;

  // fill N-tuple
  tuple->column( "m12", p12.M() ).ignore();

  tuple->column( "p0", p12 ).ignore();
  tuple->column( "g1", p1 ).ignore();
  tuple->column( "g2", p2 ).ignore();

  // here 1 MeV precision is OK for us...
  const unsigned int ipt  = LHCb::Math::round( std::max( 0.0, std::min( p12.Pt(), 5 * GeV ) ) );
  const unsigned int ipt1 = LHCb::Math::round( std::max( 0.0, std::min( p1.Pt(), 5 * GeV ) ) );
  const unsigned int ipt2 = LHCb::Math::round( std::max( 0.0, std::min( p2.Pt(), 5 * GeV ) ) );
  tuple->column( "pt", ipt ).ignore();
  tuple->column( "pt1", ipt1 ).ignore();
  tuple->column( "pt2", ipt2 ).ignore();

  const unsigned short _indx1 = cell1.index();
  const unsigned short _indx2 = cell2.index();
  tuple->column( "ind1", _indx1 ).ignore();
  tuple->column( "ind2", _indx2 ).ignore();

  tuple->column( "bkg", bkg, 0, 2 ).ignore();

  Gaudi::XYZVector vec   = point2 - point1;
  double           cSize = std::max( m_ecal->cellSize( cell1 ), m_ecal->cellSize( cell2 ) );
  double           dist  = ( cSize > 0 ) ? vec.Rho() / cSize : 0;
  tuple->column( "dist", dist ).ignore();

  // Energies
  const double eRaw1 = clus1->position().e();
  const double eRaw2 = clus2->position().e();
  tuple->column( "eClus1", eRaw1 ).ignore();
  tuple->column( "eClus2", eRaw2 ).ignore();

  const int nClus1 = clus1->entries().size();
  const int nClus2 = clus2->entries().size();
  tuple->column( "nClus1", nClus1 ).ignore();
  tuple->column( "nClus2", nClus2 ).ignore();

  const double eSeed1 = getSeedCellEnergy( clus1 );
  const double eSeed2 = getSeedCellEnergy( clus2 );
  tuple->column( "eSeed1", eSeed1 ).ignore();
  tuple->column( "eSeed2", eSeed2 ).ignore();

  tuple->write().ignore();
}
// ============================================================================
// Energy of the seed cell of a CaloCluster
// ============================================================================
double Pi0::getSeedCellEnergy( const LHCb::CaloCluster* cluster ) {
  const LHCb::CaloCluster::Entries&          entries = cluster->entries();
  LHCb::CaloCluster::Entries::const_iterator iseed =
      LHCb::ClusterFunctors::locateDigit( entries.begin(), entries.end(), LHCb::CaloDigitStatus::Mask::SeedCell );
  //
  if ( entries.end() == iseed ) return 0.0;
  //
  return iseed->digit()->e();
}
// ============================================================================
// the proper initialization
// ============================================================================
StatusCode Pi0::initialize() // the proper initialzation
{
  //
  StatusCode sc = LoKi::Algo::initialize();
  if ( sc.isFailure() ) { return sc; }
  //
  if ( m_mirror ) {
    Warning( "Albert's trick         is   activated!", StatusCode::SUCCESS ).ignore();
  } else {
    Warning( "Albert's trick         is deactivated!", StatusCode::SUCCESS ).ignore();
  }
  //
  if ( 0 < m_veto_dm || 0 < m_veto_chi2 ) {
    Warning( "Pi0-Veto               is   activated!", StatusCode::SUCCESS ).ignore();
  }
  //
  setupHistos();
  //
  LoKi::IHybridFactory* factory = tool<LoKi::IHybridFactory>( "LoKi::Hybrid::Tool/HybridFactory:PUBLIC" );
  sc                            = factory->get( m_pi0CutExp, m_pi0Cut );
  if ( sc.isFailure() ) { return Error( "Unable to compile Pi0-Cut predicate", sc ); }
  //
  m_ecal = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
// the only one essential method
// ============================================================================
StatusCode Pi0::analyse() // the only one essential method
{
  using namespace LoKi;
  using namespace LoKi::Types;
  using namespace LoKi::Cuts;
  using namespace LoKi::Photons;

  using Gaudi::Units::MeV;

  const double ptCut_Gamma = cutValue( "PtGamma" );

  // get all photons with
  Range all   = select( "all_g", "gamma" == ID );
  Range gamma = select( "g", all, ( PT > ptCut_Gamma ) );

  counter( "#gamma_all" ) += all.size();
  counter( "#gamma" ) += gamma.size();

  Tuple tuple = nTuple( "Pi0-Tuple" );

  const bool make_tuples = produceNTuples();

  LHCb::CaloDigit::Set digits;

  typedef std::set<const LHCb::Particle*> Photons;
  Photons                                 photons;

  const Gaudi::Numbers* numbers = 0;
  if ( exist<Gaudi::Numbers>( m_numbers ) ) { numbers = get<Gaudi::Numbers>( m_numbers ); }
  //
  // global event activity:
  //
  typedef std::map<std::string, double> GecMap;
  GecMap                                gec;
  for ( std::vector<std::string>::const_iterator item = m_counters.begin(); m_counters.end() != item; ++item ) {
    gec[*item] = -1;
    if ( 0 == numbers ) { continue; }
    //
    const Gaudi::Numbers::Map&          m     = numbers->numbers();
    Gaudi::Numbers::Map::const_iterator ifind = m.find( *item );
    if ( m.end() == ifind ) {
      Warning( "Gaudi::Numbers does nto contain item: " + ( *item ) ).ignore();
      continue;
    }
    gec[*item] = ifind->second;
  }
  //
  // statistics:
  for ( GecMap::const_iterator igec = gec.begin(); gec.end() != igec; ++igec ) {
    counter( igec->first ) += igec->second;
  }
  //
  // the major loop
  //
  for ( Loop pi0 = loop( "g g", "pi0" ); pi0; ++pi0 ) {

    const double              m12 = pi0->mass( 1, 2 );
    const LoKi::LorentzVector p12 = pi0->p( 1, 2 );

    const LHCb::Particle* g1 = pi0( 1 );
    if ( 0 == g1 ) { continue; } // CONTINUE

    const LHCb::Particle* g2 = pi0( 2 );
    if ( 0 == g2 ) { continue; } // CONTINUE

    // trick with "mirror-background" by Albert Puig

    // invert the first photon :
    Gaudi::LorentzVector _p1 = g1->momentum();
    _p1.SetPx( -_p1.Px() );
    _p1.SetPy( -_p1.Py() );
    const Gaudi::LorentzVector fake = ( _p1 + g2->momentum() );

    // create the fake pi0
    auto fakePi0 = std::make_unique<LHCb::Particle>( *pi0.particle() );
    fakePi0->setMomentum( fake );

    bool good    = ( m12 < 335 * MeV );
    bool goodBkg = m_mirror && ( fake.M() < 335 * MeV );

    if ( ( !good ) && ( !goodBkg ) ) { continue; } // CONTINUE!!!

    const LHCb::CaloHypo* hypo1 = hypo( g1 );
    if ( 0 == hypo1 ) { continue; } // CONTINUE
    const LHCb::CaloHypo* hypo2 = hypo( g2 );
    if ( 0 == hypo2 ) { continue; } // CONTINUE

    const LHCb::CaloCluster* cluster1 = cluster( g1 );
    if ( 0 == cluster1 ) { continue; } // CONITNUE
    const LHCb::CaloCluster* cluster2 = cluster( g2 );
    if ( 0 == cluster2 ) { continue; } // CONTINUE

    // apply pi0-cut:
    // if  ( !m_pi0Cut ( pi0 ) ) { continue ; }               // CONTINUE
    good    = good && m_pi0Cut( pi0 );
    goodBkg = goodBkg && m_pi0Cut( fakePi0.get() );
    if ( ( !good ) && ( !goodBkg ) ) { continue; } // CONTINUE

    const Gaudi::LorentzVector mom1 = g1->momentum();
    const Gaudi::LorentzVector mom2 = g2->momentum();

    // pi0-veto ?
    bool veto = true;
    if ( 0 < m_veto_dm || 0 < m_veto_chi2 ) { veto = pi0Veto( g1, g2, all, m_veto_dm, m_veto_chi2 ); }
    if ( !veto ) { continue; } // CONTINUE

    if ( good && m12 < 250 * MeV ) {
      if ( 0 != m_h1 ) { m_h1->fill( m12 ); }
    }

    // finally save good photons:
    photons.insert( g1 );
    photons.insert( g2 );

    if ( !make_tuples ) { continue; } // CONTINUE

    const LHCb::Detector::Calo::CellID cell1 = cellID( g1 );
    const LHCb::Detector::Calo::CellID cell2 = cellID( g2 );

    Gaudi::XYZPoint point1( hypo1->position()->x(), hypo1->position()->y(), hypo1->position()->z() );
    //
    if ( good || goodBkg ) {
      for ( GecMap::const_iterator igec = gec.begin(); gec.end() != igec; ++igec ) {
        tuple->column( igec->first, igec->second ).ignore();
      }
    }
    // fill N-tuples
    if ( good ) {
      const LHCb::CaloPosition* p2 = hypo2->position();
      const Gaudi::XYZPoint     point2( p2->x(), p2->y(), p2->z() );
      fillTuple( tuple, mom1, mom2, p12, cell1, cell2, point1, point2, cluster1, cluster2, 0 );
    }
    if ( goodBkg ) {
      const LHCb::CaloPosition* p2 = hypo2->position();
      const Gaudi::XYZPoint     point2Sym( -p2->x(), -p2->y(), p2->z() );
      fillTuple( tuple, mom1, mom2, fake, cell1, cell2, point1, point2Sym, cluster1, cluster2, 1 );
    }
    //
  }

  for ( Photons::const_iterator iphoton = photons.begin(); photons.end() != iphoton; ++iphoton ) {
    // keep these photons
    LHCb::Particle ph( **iphoton );
    this->markTree( &ph );
  }

  counter( "#photons" ) += photons.size();

  setFilterPassed( !photons.empty() );

  return StatusCode::SUCCESS;
}
// ============================================================================
// The factory:
// ============================================================================
DECLARE_COMPONENT( Pi0 )
// ============================================================================
// The END
// ============================================================================
