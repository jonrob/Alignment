/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * =====================================================================================
 *
 *       Filename:  Pi0MassFiller.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  11/21/2016 04:40:34 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:
 *
 * =====================================================================================
 */

#include <iostream>

// from ROOT
#include "TCanvas.h"
#include "TChain.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
// from local
#include "Pi0Calibration/Pi0MassFiller.h"
// from boost
#include <boost/filesystem.hpp>

using namespace boost;
using namespace boost::filesystem;
using namespace Calibration::Pi0Calibration;

Pi0MassFiller::Pi0MassFiller() { m_hists = new TH2D(); }

Pi0MassFiller::Pi0MassFiller( const std::string& filename ) {

  TFile* file = new TFile( filename.c_str() );
  m_hists     = (TH2D*)file->Get( "hists" );
  m_hists->SetDirectory( 0 );
  file->Close();
}

Pi0MassFiller::Pi0MassFiller( const std::string& filename, const std::string& tuplename,
                              const std::vector<unsigned int>& indices, const Pi0LambdaMap& lambdamap,
                              const std::vector<double>* vecBeta, const std::vector<double>* vecGamma ) {
  auto min_max = std::minmax_element( indices.begin(), indices.end() );
  int  xmax    = min_max.second - indices.begin();
  m_hists      = new TH2D( "hists", "hists", indices.at( xmax ), 0, indices.at( xmax ), 100, 0.0, 250.0 );

  if ( !exists( filename ) ) {
    std::cout << "ERROR: file not exist! " << filename << std::endl;
    return;
  }
  TFile* file = new TFile( filename.c_str() );
  TTree* tree = (TTree*)file->Get( tuplename.c_str() );
  tree->SetBranchStatus( "*", 0 );
  for ( auto name : {"bkg", "m12", "ind1", "ind2", "prs1", "prs2", "g1E", "g2E"} ) tree->SetBranchStatus( name, 1 );

  Candidate cand;
  // have tree use cand as storage
  tree->SetBranchAddress( "bkg", &cand.bkg );
  tree->SetBranchAddress( "ind1", &cand.ind1 );
  tree->SetBranchAddress( "ind2", &cand.ind2 );
  tree->SetBranchAddress( "m12", &cand.m12 );
  tree->SetBranchAddress( "prs1", &cand.prs1 );
  tree->SetBranchAddress( "prs2", &cand.prs2 );
  tree->SetBranchAddress( "g1E", &cand.g1E );
  tree->SetBranchAddress( "g2E", &cand.g2E );

  Long64_t nEntries = tree->GetEntries();
  for ( Long64_t i = 0; i < nEntries; i++ ) {
    tree->GetEntry( i );
    auto bkg  = cand.bkg;
    auto m12  = cand.m12;
    auto ind1 = cand.ind1;
    auto ind2 = cand.ind2;

    //
    // auto prs1 = cand.prs1;
    // auto prs2 = cand.prs2;
    //

    // correct the energy and re-computer the pi0 invariant mass
    auto scale = scale_factor( cand, lambdamap, vecBeta, vecGamma );
    if ( bkg == 0 ) {
      // if(bkg==0 && prs1<10 && prs2<10){
      m_hists->Fill( ind1, m12 * scale );
      m_hists->Fill( ind2, m12 * scale );
    }
  }

  if ( file->IsOpen() ) file->Close();
}

Pi0MassFiller::Pi0MassFiller( const std::string& filename, const std::vector<unsigned int>& indices,
                              const Pi0LambdaMap& lambdamap, const std::vector<double>* vecBeta,
                              const std::vector<double>* vecGamma ) {

  auto min_max = std::minmax_element( indices.begin(), indices.end() );
  int  xmax    = min_max.second - indices.begin();
  m_hists      = new TH2D( "hists", "hists", indices.at( xmax ), 0, indices.at( xmax ), 100, 0.0, 250.0 );

  if ( !exists( filename ) ) {
    std::cout << "ERROR: file not exist! " << filename << std::endl;
    return;
  }

  MMapVector<Candidate> v( filename.c_str(), MMapVector<Candidate>::ReadOnly );
  for ( auto& c : v ) {
    auto bkg  = c.bkg;
    auto m12  = c.m12;
    auto ind1 = c.ind1;
    auto ind2 = c.ind2;
    // correct the energy and re-computer the pi0 invariant mass
    auto scale = scale_factor( c, lambdamap, vecBeta, vecGamma );
    if ( bkg == 0 ) {
      m_hists->Fill( ind1, m12 * scale );
      m_hists->Fill( ind2, m12 * scale );
    }
  }
}
