/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 *    Description:  main algorithm for pi0 calibration at LHCb online
 *        Version:  1.0
 *        Created:  11/21/2016 01:53:44 PM
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 */
#include "Pi0Calibration/Pi0LambdaMap.h"
#include "Pi0Calibration/Pi0MassFiller.h"
#include "Pi0Calibration/Pi0MassFitter.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "CaloDet/DeCalorimeter.h"
#include "CaloKernel/CaloVector.h"
#include "Detector/Calo/CaloCellCode.h"
#include "Detector/Calo/CaloCellID.h"
#include "LHCbAlgs/Consumer.h"

#include "GaudiKernel/Point3DTypes.h"

#include "TBox.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TROOT.h"
#include "TStyle.h"

#include "TH1.h"
#include "TH2.h"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

namespace {
  struct Conditions {
    std::vector<unsigned int>                                      indices;
    std::vector<LHCb::Detector::Calo::CellID>                      cells;
    std::map<std::string, std::map<unsigned int, Gaudi::XYZPoint>> map;
    std::map<std::string, double>                                  cellsize;
  };
} // namespace

class Pi0CalibrationMonitor
    : public LHCb::Algorithm::Consumer<void( Conditions const& ), LHCb::DetDesc::usesConditions<Conditions>> {

public:
  Pi0CalibrationMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( Conditions const& ) const override;

private:
  Gaudi::Property<std::string> m_tupleFileName{this, "tupleFileName", ""};
  Gaudi::Property<std::string> m_tupleName{this, "tupleName", ""};
  Gaudi::Property<std::string> m_outputDir{this, "outputDir", ""};
  Gaudi::Property<std::string> m_inputDir{this, "inputDir", ""};

  ServiceHandle<LHCb::IParticlePropertySvc> m_ppsvc{this, "ParticlePropertySvc", "LHCb::ParticlePropertySvc"};
  double                                    m_pdgPi0;

  std::vector<LHCb::Detector::Calo::CellID> m_skippedCells;
  void                                      readCells( std::vector<LHCb::Detector::Calo::CellID>& cells );
  void readValues( std::string varname, std::map<unsigned int, std::pair<double, double>>& values ) const;
  void monitorPlots( std::string name, double centreVal, const std::map<unsigned int, double>& values,
                     Conditions const& conds ) const;
  void monitorPlots( std::string name, double centreVal,
                     const std::map<unsigned int, std::pair<double, double>>& values, Conditions const& conds ) const;
};

using namespace Calibration::Pi0Calibration;

DECLARE_COMPONENT( Pi0CalibrationMonitor )

Pi0CalibrationMonitor::Pi0CalibrationMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, {"Conditions", name + "_Conditions"} ) {}

StatusCode Pi0CalibrationMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    // find the pi0 mass value
    const LHCb::ParticleProperty* pi0 = m_ppsvc->find( "pi0" );
    if ( 0 == pi0 ) { return StatusCode::SUCCESS; }
    m_pdgPi0 = pi0->mass();
    // import a list of calo cells
    addConditionDerivation( {DeCalorimeterLocation::Ecal}, inputLocation<Conditions>(),
                            [&]( DeCalorimeter const& calo ) -> Conditions {
                              Conditions result;
                              for ( auto& cell : calo.cellParams() ) {
                                LHCb::Detector::Calo::CellID id = cell.cellID();
                                if ( !calo.valid( id ) || id.isPin() ) continue;
                                if ( calo.hasQuality( id, CaloCellQuality::OfflineMask ) ) continue;
                                result.cells.push_back( id );
                                result.indices.push_back( id.index() );
                                result.map[id.areaName()][id.index()] = cell.center();
                                result.cellsize[id.areaName()]        = cell.size();
                              }
                              return result;
                            } );
    readCells( m_skippedCells );
    return StatusCode::SUCCESS;
  } );
}

void Pi0CalibrationMonitor::readCells( std::vector<LHCb::Detector::Calo::CellID>& cells ) {
  boost::filesystem::path filename( m_inputDir + "/" + "skipped_cells.txt" );
  std::string             line;
  std::ifstream           ifile( filename.string() );
  if ( ifile.is_open() ) {
    while ( std::getline( ifile, line ) ) {
      std::vector<std::string> values;
      values.clear();
      boost::split( values, line, boost::is_any_of( "\t " ), boost::token_compress_on );
      if ( values.size() != 5 || values[0].find( "#" ) != std::string::npos ) continue;
      if ( values.size() == 5 ) {
        auto calo = std::stoul( values[0] );
        auto area = std::stoul( values[1] );
        auto row  = std::stoul( values[2] );
        auto col  = std::stoul( values[3] );
        cells.emplace_back( static_cast<LHCb::Detector::Calo::CellCode::Index>( calo ), area, row, col );
      }
    }
    ifile.close();
  }
}

void Pi0CalibrationMonitor::operator()( Conditions const& conds ) const {
  std::map<std::string, double> expects = {{"mean", m_pdgPi0}, {"sigma", 10.0}, {"lambda", 1.0}, {"npi0", 5000}};
  for ( auto varname : {"mean", "sigma", "lambda", "npi0"} ) {
    std::map<unsigned int, std::pair<double, double>> Values;
    readValues( varname, Values );
    monitorPlots( varname, expects[varname], Values, conds );
  }
}

void Pi0CalibrationMonitor::readValues( std::string                                        varname,
                                        std::map<unsigned int, std::pair<double, double>>& varvalues ) const {

  boost::filesystem::path filename( m_inputDir + "/" + varname + ".txt" );
  std::string             line;
  std::ifstream           ifile( filename.string() );
  if ( ifile.is_open() ) {
    while ( std::getline( ifile, line ) ) {
      std::vector<std::string> values;
      values.clear();
      boost::split( values, line, boost::is_any_of( "\t " ), boost::token_compress_on );
      if ( values.size() != 3 || values[0].find( "#" ) != std::string::npos ) continue;
      if ( values.size() == 3 ) {
        auto index       = std::stoul( values[0] );
        auto value       = std::stod( values[1] );
        auto error       = std::stod( values[2] );
        varvalues[index] = std::make_pair( value, error );
      }
    }
    ifile.close();
  }
}

void Pi0CalibrationMonitor::monitorPlots( std::string name, double centreVal,
                                          const std::map<unsigned int, std::pair<double, double>>& values,
                                          Conditions const&                                        conds ) const {
  std::map<unsigned int, double> newValues;
  for ( auto& value : values ) newValues[value.first] = value.second.first;
  monitorPlots( name, centreVal, newValues, conds );
  newValues.clear();
}

void Pi0CalibrationMonitor::monitorPlots( std::string name, double centreVal,
                                          const std::map<unsigned int, double>& values,
                                          Conditions const&                     conds ) const {
  std::map<std::string, std::vector<double>> xvalues;
  std::map<std::string, std::vector<double>> yvalues;
  for ( auto const& igrid : conds.map ) {
    auto area = igrid.first;
    for ( auto value : igrid.second ) {
      xvalues[area].push_back( value.second.x() - 0.5 * conds.cellsize.at( area ) );
      yvalues[area].push_back( value.second.y() - 0.5 * conds.cellsize.at( area ) );
    }
  }
  std::vector<std::string>    areas = {"Inner", "Middle", "Outer"};
  std::map<std::string, TH2*> hist_calo;
  std::map<std::string, TH1*> hist_calo_value;
  // find the minimum and maximum of the values in all the areas
  auto it = std::minmax_element(
      values.begin(), values.end(),
      []( const std::map<unsigned int, double>::value_type& l,
          const std::map<unsigned int, double>::value_type& r ) -> bool { return l.second < r.second; } );
  double deltaVal = 0.5 * ( it.second->second - it.first->second );
  if ( centreVal < it.first->second ) centreVal = it.first->second + deltaVal;
  if ( centreVal > it.second->second ) centreVal = it.second->second - deltaVal;

  for ( auto area : areas ) {
    std::sort( xvalues[area].begin(), xvalues[area].end() );
    std::sort( yvalues[area].begin(), yvalues[area].end() );
    xvalues[area].push_back( xvalues[area].back() + conds.cellsize.at( area ) );
    yvalues[area].push_back( yvalues[area].back() + conds.cellsize.at( area ) );
    xvalues[area].erase( std::unique( xvalues[area].begin(), xvalues[area].end() ), xvalues[area].end() );
    yvalues[area].erase( std::unique( yvalues[area].begin(), yvalues[area].end() ), yvalues[area].end() );
    hist_calo[area] =
        new TH2D( ( "hist_calo_" + name + "_" + area ).c_str(), ( "hist_calo_" + name + "_" + area ).c_str(),
                  xvalues[area].size() - 1, xvalues[area].data(), yvalues[area].size() - 1, yvalues[area].data() );
    hist_calo_value[area] = new TH1D(
        ( "hist_calo_value_" + name + "_" + area ).c_str(), ( "hist_calo_value_" + name + "_" + area ).c_str(),
        ( name == "mean" ) ? 800 : ( ( name == "lambda" ) ? 400 : 100 ), it.first->second, it.second->second );
  }
  std::map<std::string, TH2*> grid_calo;
  for ( auto area : areas ) {
    std::vector<double> cols;
    std::vector<double> rows;
    for ( auto& cell : conds.cells ) {
      if ( cell.areaName() != area ) continue;
      cols.push_back( cell.col() - 0.5 );
      rows.push_back( cell.row() - 0.5 );
    }
    std::sort( cols.begin(), cols.end() );
    std::sort( rows.begin(), rows.end() );
    cols.push_back( cols.back() + 1.0 );
    rows.push_back( rows.back() + 1.0 );
    grid_calo[area] =
        new TH2D( ( "grid_calo_" + name + "_" + area ).c_str(), ( "grid_calo_" + name + "_" + area ).c_str(),
                  cols.size() - 1, cols.data(), rows.size() - 1, rows.data() );
  }

  for ( auto& cell : conds.cells ) {
    unsigned int index = cell.index();
    if ( values.end() == values.find( index ) ) continue;
    hist_calo[cell.areaName()]->Fill( conds.map.at( cell.areaName() ).at( index ).x(),
                                      conds.map.at( cell.areaName() ).at( index ).y(), values.find( index )->second );
    hist_calo_value[cell.areaName()]->Fill( values.find( index )->second );
    grid_calo[cell.areaName()]->Fill( cell.col(), cell.row(), values.find( index )->second );
  }
  hist_calo_value["ALL"] =
      new TH1D( ( "hist_calo_value_" + name + "_ALL" ).c_str(), ( "hist_calo_value_" + name + "_ALL" ).c_str(),
                ( name == "mean" ) ? 800 : ( ( name == "lambda" ) ? 400 : 100 ), it.first->second, it.second->second );
  for ( auto& v : values ) hist_calo_value["ALL"]->Fill( v.second );

  // build 1D histogram to visualize the parameters
  gStyle->SetPadRightMargin( 0.25 );
  gStyle->SetTitleX( 0.5 );
  gStyle->SetTitleAlign( 23 );
  gStyle->SetTitleBorderSize( 0 );
  gStyle->SetPaintTextFormat( "5.0f" );
  gStyle->SetStatFormat( "5.5f" );
  gStyle->SetTitleFontSize( 0.07 );
  gStyle->SetPadTickY( 1 );
  gStyle->SetPadTickX( 1 );
  gStyle->SetOptStat( 1111110 );
  gStyle->SetOptFit( 1111110 );
  gROOT->ForceStyle();

  TCanvas* cdata = new TCanvas( "cdata", "cdata", 1600, 1200 );
  cdata->Divide( 2, 2 );
  cdata->cd( 1 );
  float xmin = -9999, xmax = 9999;
  if ( name == "lambda" ) {
    xmin = 0.92;
    xmax = 1.08;
  }
  if ( name == "mean" ) {
    xmin = 120;
    xmax = 150;
  }
  if ( name == "sigma" ) {
    xmin = 0;
    xmax = 20;
  }
  hist_calo_value["ALL"]->SetNameTitle( ( name + "_ALL" ).c_str(), ( name + "_ALL" ).c_str() );
  hist_calo_value["ALL"]->GetXaxis()->SetTitle( name.c_str() );
  hist_calo_value["ALL"]->GetXaxis()->SetRangeUser( xmin, xmax );
  hist_calo_value["ALL"]->Draw();
  cdata->cd( 2 );
  hist_calo_value["Inner"]->SetNameTitle( ( name + "_Inner" ).c_str(), ( name + "_Inner" ).c_str() );
  hist_calo_value["Inner"]->GetXaxis()->SetTitle( name.c_str() );
  hist_calo_value["Inner"]->GetXaxis()->SetRangeUser( xmin, xmax );
  hist_calo_value["Inner"]->Draw();
  cdata->cd( 3 );
  hist_calo_value["Middle"]->SetNameTitle( ( name + "_Middle" ).c_str(), ( name + "_Middle" ).c_str() );
  hist_calo_value["Middle"]->GetXaxis()->SetTitle( name.c_str() );
  hist_calo_value["Middle"]->GetXaxis()->SetRangeUser( xmin, xmax );
  hist_calo_value["Middle"]->Draw();
  cdata->cd( 4 );
  hist_calo_value["Outer"]->SetNameTitle( ( name + "_Outer" ).c_str(), ( name + "_Outer" ).c_str() );
  hist_calo_value["Outer"]->GetXaxis()->SetTitle( name.c_str() );
  hist_calo_value["Outer"]->GetXaxis()->SetRangeUser( xmin, xmax );
  hist_calo_value["Outer"]->Draw();
  boost::filesystem::path outfile_histvalue( m_outputDir + "/" + "calo_" + name + "_1d.pdf" );
  cdata->SaveAs( outfile_histvalue.c_str() );
  cdata->Close();
  for ( auto hist : hist_calo_value ) { delete hist.second; }

  // build 2D histogram to visualize the mean and sigma
  gStyle->SetOptStat( 0 );
  gStyle->SetOptFit( 0 );
  unsigned int nColors = 52;
  Int_t        MyPalette[52];
  Double_t     s[3] = {0.00, 0.50, 1.00};
  Double_t     b[3] = {0.80, 1.00, 0.00};
  Double_t     g[3] = {0.00, 1.00, 0.00};
  Double_t     r[3] = {0.00, 1.00, 0.80};
  Int_t        FI   = TColor::CreateGradientColorTable( 3, s, r, g, b, nColors );
  for ( unsigned int k( 0 ); k < nColors; k++ ) { MyPalette[k] = FI + k; }
  gStyle->SetNumberContours( nColors );
  gStyle->SetPalette( nColors, MyPalette );
  gROOT->ForceStyle();

  TCanvas* cc = new TCanvas( "cc", "cc", 1600, 1200 );
  for ( auto& hist : hist_calo ) {
    hist.second->SetMaximum( centreVal + deltaVal );
    if ( centreVal - deltaVal > 0. )
      hist.second->SetMinimum( centreVal - deltaVal );
    else if ( it.first->second > 0. )
      hist.second->SetMinimum( it.first->second );
    else
      hist.second->SetMinimum( 0. );
    hist.second->SetTitle( "ECAL cells X vs Y" );
    hist.second->GetXaxis()->SetTitle( "X [mm]" );
    hist.second->GetYaxis()->SetTitle( "Y [mm]" );
  }
  hist_calo.find( "Outer" )->second->Draw( "COLZ" );
  hist_calo.find( "Middle" )->second->Draw( "COLZ,SAME" );
  hist_calo.find( "Inner" )->second->Draw( "COLZ,SAME" );
  // plot boxes
  TBox* box = new TBox();
  box->SetFillColor( kWhite );
  box->SetFillStyle( 0 );
  box->SetLineStyle( 1 );
  box->SetLineColor( kBlack ); // 14
  for ( auto const& igrid : conds.map ) {
    auto area = igrid.first;
    for ( auto value : igrid.second ) {
      box->DrawBox(
          value.second.x() - 0.5 * conds.cellsize.at( area ), value.second.y() - 0.5 * conds.cellsize.at( area ),
          value.second.x() + 0.5 * conds.cellsize.at( area ), value.second.y() + 0.5 * conds.cellsize.at( area ) );
    }
  }
  boost::filesystem::path outfile_calomean( m_outputDir + "/" + "calo_" + name + ".pdf" );
  cc->SaveAs( outfile_calomean.c_str() );
  cc->Close();
  for ( auto& hist : grid_calo ) {
    auto     area = hist.first;
    TCanvas* cc   = new TCanvas( "cc", "cc", 1600, 1200 );
    hist.second->SetMaximum( centreVal + deltaVal );
    if ( centreVal - deltaVal > 0. )
      hist.second->SetMinimum( centreVal - deltaVal );
    else if ( it.first->second > 0. )
      hist.second->SetMinimum( it.first->second );
    else
      hist.second->SetMinimum( 0. );
    hist.second->SetTitle( area.c_str() );
    hist.second->Draw( "COLZ" );
    hist.second->GetXaxis()->SetTitle( "COLUMN" );
    hist.second->GetYaxis()->SetTitle( "ROW" );
    // plot boxes
    TBox* box = new TBox();
    box->SetFillColor( kWhite );
    box->SetFillStyle( 0 );
    box->SetLineStyle( 3 );
    box->SetLineColor( kBlack ); // 14
    for ( auto& cell : conds.cells ) {
      if ( cell.areaName() == area )
        box->DrawBox( cell.col() - 0.5, cell.row() - 0.5, cell.col() + 0.5, cell.row() + 0.5 );
    }
    delete box;
    TBox* boxempty = new TBox();
    boxempty->SetFillColor( 14 );
    boxempty->SetFillStyle( 3254 );
    boxempty->SetLineStyle( 3 );
    boxempty->SetLineColor( 14 );
    for ( auto& cell : m_skippedCells ) {
      if ( cell.areaName() == area )
        boxempty->DrawBox( cell.col() - 0.5, cell.row() - 0.5, cell.col() + 0.5, cell.row() + 0.5 );
    }
    delete boxempty;
    boost::filesystem::path outfile_calogrid( m_outputDir + "/" + "calo_" + name + "_grid_" + area + ".pdf" );
    cc->SaveAs( outfile_calogrid.c_str() );
    cc->Close();
  }
  for ( auto hist : hist_calo ) { delete hist.second; }
  for ( auto hist : grid_calo ) { delete hist.second; }
  gStyle->SetOptStat( 1111110 );
  gStyle->SetOptFit( 1111110 );
  gROOT->ForceStyle();
}
