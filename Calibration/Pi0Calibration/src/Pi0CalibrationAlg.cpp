/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 *    Description:  main algorithm for pi0 calibration at LHCb online
 *        Version:  1.0
 *        Created:  11/21/2016 01:53:44 PM
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 */
#include "Pi0Calibration/Pi0LambdaMap.h"
#include "Pi0Calibration/Pi0MassFiller.h"
#include "Pi0Calibration/Pi0MassFitter.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "CaloDet/DeCalorimeter.h"
#include "Detector/Calo/CaloCellID.h"
#include "LHCbAlgs/Consumer.h"

#include "TH1.h"
#include "TH2.h"

#include <boost/filesystem.hpp>

#include <yaml-cpp/yaml.h>

#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

namespace {
  struct Conditions {
    std::vector<double>                       beta;
    std::vector<double>                       gamma;
    std::vector<unsigned int>                 indices;
    std::vector<LHCb::Detector::Calo::CellID> cells;
  };
} // namespace

class Pi0CalibrationAlg
    : public LHCb::Algorithm::Consumer<void( Conditions const& ), LHCb::DetDesc::usesConditions<Conditions>> {

public:
  Pi0CalibrationAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( Conditions const& ) const override;

private:
  Gaudi::Property<std::string> m_tupleFileName{this, "tupleFileName", ""};
  Gaudi::Property<std::string> m_tupleName{this, "tupleName", ""};
  Gaudi::Property<std::string> m_outputDir{this, "outputDir", ""};
  Gaudi::Property<std::string> m_lambdaFileName{this, "lambdaFileName", ""};
  Gaudi::Property<std::string> m_saveLambdaFile{this, "saveLambdaFile", ""};
  Gaudi::Property<std::string> m_saveLambdaDBFile{this, "saveLambdaDBFile", ""};
  Gaudi::Property<std::string> m_fitMethod{this, "fitMethod", "CHI2"};
  Gaudi::Property<std::string> m_filetype{this, "filetype", "ROOT"};

  ServiceHandle<LHCb::IParticlePropertySvc> m_ppsvc{this, "ParticlePropertySvc", "LHCb::ParticlePropertySvc"};
  double                                    m_pdgPi0;

  void saveCells( const std::vector<LHCb::Detector::Calo::CellID>& cells ) const;
  void saveValues( const std::map<unsigned int, std::pair<double, double>>& values, std::string varname ) const;
};

using namespace Calibration::Pi0Calibration;

DECLARE_COMPONENT( Pi0CalibrationAlg )

Pi0CalibrationAlg::Pi0CalibrationAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, {"Conditions", name + "_Conditions"} ) {}

StatusCode Pi0CalibrationAlg::initialize() {
  return Consumer::initialize().andThen( [&] {
    // find the pi0 mass value
    const LHCb::ParticleProperty* pi0 = m_ppsvc->find( "pi0" );
    if ( 0 == pi0 ) { return StatusCode::SUCCESS; }
    m_pdgPi0 = pi0->mass();
    // Get beta and gamma parameters from the DB, for ECorrections and import a list of calo cells
    addConditionDerivation( {"Conditions/Reco/Calo/PhotonECorrection", DeCalorimeterLocation::Ecal},
                            inputLocation<Conditions>(),
                            [&]( YAML::Node const& cond, DeCalorimeter const& calo ) -> Conditions {
                              Conditions result;
                              result.beta  = cond["beta"].as<std::vector<double>>();
                              result.gamma = cond["offset"].as<std::vector<double>>();
                              for ( auto& cell : calo.cellParams() ) {
                                LHCb::Detector::Calo::CellID id = cell.cellID();
                                if ( !calo.valid( id ) || id.isPin() ) continue;
                                if ( calo.hasQuality( id, CaloCellQuality::OfflineMask ) ) continue;
                                result.cells.push_back( id );
                                result.indices.push_back( id.index() );
                              }
                              return result;
                            } );
    return StatusCode::SUCCESS;
  } );
}

void Pi0CalibrationAlg::operator()( Conditions const& conds ) const {
  if ( !boost::filesystem::exists( m_tupleFileName.value() ) ) {
    throw GaudiException( "Pi0CalibrationAlg::operator()", "no m_tupleFileName", StatusCode::FAILURE );
  }
  // load the lambda map
  Pi0LambdaMap lMap( conds.indices );
  if ( boost::filesystem::exists( m_lambdaFileName.value() ) ) { lMap.loadMapFromFile( m_lambdaFileName ); }
  if ( msgLevel( MSG::INFO ) ) info() << " the lambda is loaded from file " << m_lambdaFileName << endmsg;
  // load the histogram
  Pi0MassFiller filler;
  if ( m_filetype == "MMAP" ) {
    if ( msgLevel( MSG::INFO ) ) info() << " read files created by MMapVector" << endmsg;
    filler = Pi0MassFiller( m_tupleFileName, conds.indices, lMap, &conds.beta, &conds.gamma );
  } else if ( m_filetype == "ROOT" ) {
    if ( msgLevel( MSG::INFO ) ) info() << " read normal ROOT files" << endmsg;
    filler = Pi0MassFiller( m_tupleFileName, m_tupleName, conds.indices, lMap, &conds.beta, &conds.gamma );
  } else if ( m_filetype == "TH2D" ) {
    if ( msgLevel( MSG::INFO ) ) info() << " read histo files" << endmsg;
    filler = Pi0MassFiller( m_tupleFileName );
  } else {
    throw GaudiException( "Pi0CalibrationAlg::operator()", fmt::format( "File type not recognised {}", m_filetype ),
                          StatusCode::FAILURE );
  }
  TH2* hists = filler.hists();

  std::map<unsigned int, std::pair<double, double>> meanValues;
  std::map<unsigned int, std::pair<double, double>> sigmaValues;
  std::map<unsigned int, std::pair<double, double>> npi0Values;
  std::vector<LHCb::Detector::Calo::CellID>         skippedCells;

  if ( msgLevel( MSG::INFO ) )
    info() << " fit the pi0 mass distribution to get the initial lambda values and the resolutions in each region "
           << endmsg;

  Pi0LambdaMap newMap( conds.indices );
  for ( auto& cell : conds.cells ) {
    unsigned int seedindex = cell.index();

    TH1D* hist = hists->ProjectionY( ( "S_ID" + std::to_string( seedindex ) + "_ROW" + std::to_string( cell.row() ) +
                                       "_COL" + std::to_string( cell.col() ) )
                                         .c_str(),
                                     seedindex + 1, seedindex + 1 );
    if ( hist->GetEntries() == 0 ) {
      skippedCells.push_back( cell );
      continue;
    }

    Pi0MassFitter fitter( hist, m_pdgPi0, 10.0 );
    std::string   outfileName( "Calibration_Pi0_massfit" );
    outfileName += "_" + cell.areaName();
    outfileName += "_ROW" + std::to_string( cell.row() );
    outfileName += "_COL" + std::to_string( cell.col() );
    outfileName += "_INDEX" + std::to_string( cell.index() );
    outfileName += ".pdf";
    if ( m_fitMethod == "CHI2" )
      fitter.chi2fit( m_outputDir + "/" + cell.areaName(), outfileName, msgLevel( MSG::VERBOSE ) );

    meanValues[cell.index()]  = fitter.getMean();
    sigmaValues[cell.index()] = fitter.getSigma();
    npi0Values[cell.index()]  = fitter.getNSignal();

    auto lold = lMap.get_lambda( cell.index() );
    if ( fitter.getStatus() == 0 ) {
      if ( lold.first < 0.0 ) lold = std::make_pair( 1.0, 0.0 );
      auto lnew_value   = m_pdgPi0 / fitter.getMean().first;
      auto lnew_error   = lnew_value * fitter.getMean().second / fitter.getMean().first;
      auto lambda_value = lold.first * lnew_value;
      auto lambda_error = lambda_value * std::sqrt( lnew_error / lnew_value * lnew_error / lnew_value +
                                                    lold.second / lold.first * lold.second / lold.first );
      newMap.setLambda( cell.index(), std::make_pair( lambda_value, lambda_error ) );
    } else {
      skippedCells.push_back( cell );
      if ( !( lold.first < 0.0 || lold.second < 0.0 ) ) newMap.setLambda( cell.index(), lold );
    }
  }

  if ( msgLevel( MSG::INFO ) ) info() << " save the updated lambda to the file " << m_saveLambdaFile << endmsg;
  newMap.saveMapToFile( m_saveLambdaFile );
  newMap.saveMapToDBFile( m_saveLambdaDBFile );

  saveCells( skippedCells );
  saveValues( meanValues, "mean" );
  saveValues( sigmaValues, "sigma" );
  saveValues( npi0Values, "npi0" );
}

void Pi0CalibrationAlg::saveCells( const std::vector<LHCb::Detector::Calo::CellID>& cells ) const {
  boost::filesystem::path filename( m_outputDir + "/" + "skipped_cells.txt" );
  std::ofstream           ofile;
  ofile.open( filename.string(), std::ios::out | std::ios::trunc );
  for ( auto& cell : cells )
    ofile << cell.calo() << " " << cell.area() << " " << cell.row() << " " << cell.col() << " " << cell.index() << "\n";
  ofile.close();
}

void Pi0CalibrationAlg::saveValues( const std::map<unsigned int, std::pair<double, double>>& values,
                                    std::string                                              varname ) const {
  boost::filesystem::path filename( m_outputDir + "/" + varname + ".txt" );
  std::ofstream           ofile;
  ofile.open( filename.string(), std::ios::out | std::ios::trunc );
  for ( auto& value : values )
    ofile << value.first << "  " << value.second.first << "  " << value.second.second << "\n";
  ofile.close();
}
