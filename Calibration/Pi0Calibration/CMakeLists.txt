###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Calibration/Pi0Calibration
--------------------------
#]=======================================================================]

gaudi_add_library(Pi0CalibrationLib
    SOURCES
        src/MMapVector.cpp
        src/Pi0CalibrationFile.cpp
        src/Pi0LambdaMap.cpp
        src/Pi0MassFiller.cpp
        src/Pi0MassFitter.cpp
    LINK
        PUBLIC
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
            ROOT::Hist
            ROOT::MathCore
            ROOT::Tree
        PRIVATE
            Boost::filesystem
            Boost::headers
            Boost::regex
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::CaloFutureUtils
            LHCb::CaloFutureInterfaces
            LHCb::CaloKernel
            LHCb::DetDescLib
            LHCb::DigiEvent
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::LoKiCoreLib
            LHCb::PartPropLib
            LHCb::PhysEvent
            LHCb::RecEvent
            Rec::LoKiAlgo
            Rec::LoKiArrayFunctorsLib
            Rec::LoKiPhysLib
            ROOT::Core
            ROOT::Gpad
            ROOT::Graf
            ROOT::Hist
            ROOT::MathCore
            ROOT::Postscript
            ROOT::RIO
            ROOT::RooFit
            ROOT::Spectrum
            ROOT::Tree
)

gaudi_add_module(Pi0Calibration
    SOURCES
        src/Pi0CalibrationAlg.cpp
        src/Pi0CalibrationMonitor.cpp
        src/Pi0MMap2Histo.cpp
        src/Pi0.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::CaloDetLib
        LHCb::CaloFutureUtils
        LHCb::CaloFutureInterfaces
        LHCb::DigiEvent
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::LoKiCoreLib
        LHCb::PhysEvent
        LHCb::RecEvent
        Rec::LoKiAlgo
        Rec::LoKiArrayFunctorsLib
        Rec::LoKiPhysLib
        Pi0CalibrationLib
        ROOT::Core
        ROOT::Gpad
        ROOT::Graf
        ROOT::Hist
        ROOT::MathCore
        ROOT::Postscript
        ROOT::RIO
        ROOT::RooFit
        ROOT::Spectrum
        ROOT::Tree
)

gaudi_add_dictionary(Pi0CalibrationDict
    HEADERFILES dict/Pi0CalibrationDict.h
    SELECTION dict/Pi0CalibrationDict.xml
    LINK Pi0CalibrationLib
)

gaudi_install(PYTHON)
gaudi_generate_confuserdb()
lhcb_add_confuser_dependencies(
    Kernel/LHCbKernel
)
