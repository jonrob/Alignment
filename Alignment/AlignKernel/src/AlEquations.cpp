/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AlignKernel/AlEquations.h"
#include "AlignKernel/AlFileIO.h"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>

namespace LHCb::Alignment {

  namespace FileIO {

    template <>
    std::ofstream& operator<<( std::ofstream& file, const ElementData& data ) {
      // std::cout << "Writing ElementData to file." << std::endl ;
      file << data.m_alignframe << data.m_alpha << data.m_dChi2DAlpha << data.m_d2Chi2DAlpha2
           << data.m_d2Chi2DAlphaDBeta << data.m_dStateDAlpha << data.m_dVertexDAlpha << data.m_numHits
           << data.m_numOutliers << data.m_numTracks << data.m_weightV << data.m_weightR << data.m_alphaIsSet
           << data.m_sumX << data.m_sumY << data.m_sumZ;
      return file;
    }

    template <>
    std::ifstream& operator>>( std::ifstream& file, ElementData& data ) {
      // std::cout << "Reading ElementData from file." << std::endl ;
      file >> data.m_alignframe >> data.m_alpha >> data.m_dChi2DAlpha >> data.m_d2Chi2DAlpha2 >>
          data.m_d2Chi2DAlphaDBeta >> data.m_dStateDAlpha >> data.m_dVertexDAlpha >> data.m_numHits >>
          data.m_numOutliers >> data.m_numTracks >> data.m_weightV >> data.m_weightR >> data.m_alphaIsSet >>
          data.m_sumX >> data.m_sumY >> data.m_sumZ;
      return file;
    }

  } // namespace FileIO

  void ElementData::add( const ElementData& rhs ) {
    if ( !m_alphaIsSet && rhs.m_alphaIsSet ) {
      // if this is the first one and the parameters are not set, copy the parameters
      m_alpha      = rhs.m_alpha;
      m_alphaIsSet = rhs.m_alphaIsSet;
      m_alignframe = rhs.m_alignframe;
    }
    m_dChi2DAlpha += rhs.m_dChi2DAlpha;
    m_d2Chi2DAlpha2 += rhs.m_d2Chi2DAlpha2;
    m_numHits += rhs.m_numHits;
    m_numOutliers += rhs.m_numOutliers;
    m_numTracks += rhs.m_numTracks;
    m_weightV += rhs.m_weightV;
    m_weightR += rhs.m_weightR;
    m_dStateDAlpha += rhs.m_dStateDAlpha;
    m_dVertexDAlpha += rhs.m_dVertexDAlpha;
    for ( OffDiagonalContainer::const_iterator rhsit = rhs.m_d2Chi2DAlphaDBeta.begin();
          rhsit != rhs.m_d2Chi2DAlphaDBeta.end(); ++rhsit ) {
      OffDiagonalContainer::iterator it = m_d2Chi2DAlphaDBeta.find( rhsit->first );
      if ( it == m_d2Chi2DAlphaDBeta.end() )
        m_d2Chi2DAlphaDBeta.insert( *rhsit );
      else
        it->second += rhsit->second;
    }
    m_sumX += rhs.m_sumX;
    m_sumY += rhs.m_sumY;
    m_sumZ += rhs.m_sumZ;
  }

  void ElementData::transform( const Gaudi::Matrix6x6& jacobian ) {
    ElementData tmp = *this;
    m_dChi2DAlpha   = jacobian * tmp.m_dChi2DAlpha;
    m_d2Chi2DAlpha2 = ROOT::Math::Similarity( jacobian, tmp.m_d2Chi2DAlpha2 );
    m_dStateDAlpha  = jacobian * tmp.m_dStateDAlpha;
    m_dVertexDAlpha = jacobian * tmp.m_dVertexDAlpha;
    m_d2Chi2DAlphaDBeta.clear();
    for ( OffDiagonalContainer::const_iterator it = tmp.m_d2Chi2DAlphaDBeta.begin();
          it != tmp.m_d2Chi2DAlphaDBeta.end(); ++it )
      m_d2Chi2DAlphaDBeta[it->first].matrix() = jacobian * it->second.matrix();
  }

  Equations::Equations( size_t nElem, Gaudi::Time initTime ) : m_elements{nElem}, m_initTime{initTime} {}

  void Equations::clear() {
    m_elements.clear();
    m_numEvents            = 0u;
    m_numTracks            = 0u;
    m_numVertices          = 0u;
    m_numParticles         = 0u;
    m_totalChiSquare       = 0.0;
    m_totalNumDofs         = 0u;
    m_numExternalHits      = 0u;
    m_totalVertexChiSquare = 0.0;
    m_totalVertexNumDofs   = 0u;
    m_firstTime            = Gaudi::Time( Gaudi::Time::max() );
    m_lastTime             = Gaudi::Time( Gaudi::Time::epoch() );
    m_initTime             = 0;
    m_minIOV               = std::numeric_limits<size_t>::max();
    m_maxIOV               = 0;
  }

  void Equations::writeToBuffer( std::ofstream& buffer ) const {
    using namespace FileIO;
    buffer << m_elements << m_numEvents << m_numTracks << m_numVertices << m_numParticles << m_totalChiSquare
           << m_totalNumDofs << m_numExternalHits << m_totalVertexChiSquare << m_totalVertexNumDofs << m_firstTime
           << m_lastTime << m_initTime << m_firstRun << m_lastRun << m_minIOV << m_maxIOV;
  }

  void Equations::readFromBuffer( std::ifstream& buffer ) {
    using namespace FileIO;
    buffer >> m_elements >> m_numEvents >> m_numTracks >> m_numVertices >> m_numParticles >> m_totalChiSquare >>
        m_totalNumDofs >> m_numExternalHits >> m_totalVertexChiSquare >> m_totalVertexNumDofs >> m_firstTime >>
        m_lastTime >> m_initTime >> m_firstRun >> m_lastRun;
  }

  std::tuple<size_t, long> Equations::writeToFile( const char* filename ) const {
    std::ofstream file( filename, std::ios::out | std::ios::binary );
    writeToBuffer( file );
    long fileBytes = file.tellp();
    file.close();
    return std::make_tuple( m_numEvents, fileBytes );
  }

  std::tuple<StatusCode, std::string> Equations::readFromFile( const char* filename ) {
    std::ifstream file( filename, std::ios::in | std::ios::binary );
    if ( file.is_open() ) {
      /*
      auto fileSize = std::filesystem::file_size( filename );

      if ( fileSize < 8 * 1024 ) {
        return std::make_tuple( StatusCode::FAILURE, "Equations::readFromFile: derivative file too small!" );
      }
      */
      readFromBuffer( file );
      size_t      fileBytes = file.tellg();
      std::string infoString =
          format( "Equations::readFromFile: read  %u events in %u bytes from file ", m_numEvents, fileBytes ) +
          filename;
      file.close();
      return std::make_tuple( StatusCode::SUCCESS, infoString );
    } else {
      return std::make_tuple( StatusCode::FAILURE, "Equations::readFromFile: Cannot open input file" );
    }
  }

  std::map<std::string, std::vector<std::string>> Equations::add( const Equations& rhs, bool correctDeltaAlpha ) {
    std::map<std::string, std::vector<std::string>> msgMap;
    // if this if the first one, we just copy
    if ( m_elements.empty() ) {
      m_elements = rhs.m_elements;
      m_initTime = rhs.m_initTime;
    } else {
      if ( m_elements.size() != rhs.m_elements.size() ) {
        std::string tmpMsg = format( "Equations::add: adding up derivatives with different sizes: This: %d RHS: %d ",
                                     m_elements.size(), rhs.m_elements.size() );
        msgMap["error"].push_back( tmpMsg );
        return msgMap;
      }
      if ( m_initTime != rhs.m_initTime ) {
        std::string tmpMsg = format( "Equations::add: adding up Equations with different initTime: %s and %s. Make "
                                     "sure that you know what you are doing",
                                     m_initTime, rhs.m_initTime );
        msgMap["warning"].push_back( tmpMsg );
      }

      // this is a tricky one. if we add up derivatievs that were
      // actually computed around a different point, then we need to
      // update all first derivatives. we correct the rhs before doing
      // the addition. Note the signs. I think that they are correct now.
      if ( correctDeltaAlpha ) {
        Equations ncrhs = rhs;
        size_t    index( 0 );
        for ( ElementContainer::iterator rhsit = ncrhs.m_elements.begin(); rhsit != ncrhs.m_elements.end();
              ++rhsit, ++index ) {
          // compute delta-alpha (usign the 'old' rhs)
          Gaudi::Vector6 deltaalpha = rhs.m_elements[index].m_alpha - m_elements[index].m_alpha;
          if ( ROOT::Math::Dot( deltaalpha, deltaalpha ) > 1e-12 ) {
            std::string tmpMsg =
                format( "Equations::add: INFO:: Correcting for shifted alignment: %f %f %f %f %f %f ", deltaalpha[0],
                        deltaalpha[1], deltaalpha[2], deltaalpha[3], deltaalpha[4], deltaalpha[5] );
            msgMap["info"].push_back( tmpMsg );
          }
          // first the diagonal
          rhsit->m_dChi2DAlpha += rhsit->m_d2Chi2DAlpha2 * deltaalpha;
          // now all the correlated elements
          for ( ElementData::OffDiagonalContainer::const_iterator offdiagit = rhsit->m_d2Chi2DAlphaDBeta.begin();
                offdiagit != rhsit->m_d2Chi2DAlphaDBeta.end(); ++offdiagit ) {
            // the upper diagonal
            ncrhs.m_elements[offdiagit->first].m_dChi2DAlpha +=
                ROOT::Math::Transpose( offdiagit->second.matrix() ) * deltaalpha;
            // but also the lower diagonal. for this we need the delta-alpha of the other element
            Gaudi::Vector6 deltaalphaj =
                rhs.m_elements[offdiagit->first].m_alpha - m_elements[offdiagit->first].m_alpha;
            rhsit->m_dChi2DAlpha += offdiagit->second.matrix() * deltaalphaj;
          }
          // update alpha itself
          rhsit->m_alpha = m_elements[index].m_alpha;
        }

        // only now add the right-hand-side
        ElementContainer::iterator it = m_elements.begin();
        for ( ElementContainer::const_iterator rhsit = ncrhs.m_elements.begin(); rhsit != ncrhs.m_elements.end();
              ++rhsit, ++it )
          ( *it ).add( *rhsit );
      } else {
        ElementContainer::iterator it = m_elements.begin();
        for ( ElementContainer::const_iterator rhsit = rhs.m_elements.begin(); rhsit != rhs.m_elements.end();
              ++rhsit, ++it )
          ( *it ).add( *rhsit );
      }
    }

    m_numEvents += rhs.m_numEvents;
    m_numTracks += rhs.m_numTracks;
    m_numVertices += rhs.m_numVertices;
    m_numParticles += rhs.m_numParticles;
    m_totalChiSquare += rhs.m_totalChiSquare;
    m_totalNumDofs += rhs.m_totalNumDofs;
    m_numExternalHits += rhs.m_numExternalHits;
    m_totalVertexChiSquare += rhs.m_totalVertexChiSquare;
    m_totalVertexNumDofs += rhs.m_totalVertexNumDofs;
    if ( m_firstTime.ns() > rhs.m_firstTime.ns() ) m_firstTime = rhs.m_firstTime;
    if ( m_lastTime.ns() < rhs.m_lastTime.ns() ) m_lastTime = rhs.m_lastTime;
    if ( m_firstRun > rhs.m_firstRun ) m_firstRun = rhs.m_firstRun;
    if ( m_lastRun < rhs.m_lastRun ) m_lastRun = rhs.m_lastRun;
    if ( m_minIOV > rhs.m_minIOV ) m_minIOV = rhs.m_minIOV;
    if ( m_maxIOV < rhs.m_maxIOV ) m_maxIOV = rhs.m_maxIOV;
    return msgMap;
  }

  size_t Equations::numHits() const {
    return std::accumulate( begin( m_elements ), end( m_elements ), 0,
                            []( size_t sum, const ElementData& a ) { return sum + a.m_numHits; } );
  }

  std::ostream& Equations::fillStream( std::ostream& os, int verbosity ) const {
    // Let's dump some stuff in a human readible form
    // std::stringstream stream;
    auto& stream = os;
    stream << "NumEvents = " << numEvents() << std::endl
           << "NumTracks = " << m_numTracks << std::endl
           << "NumVertices = " << m_numVertices << std::endl
           << "NumParticles = " << m_numParticles << std::endl
           << "NumExternalHits = " << m_numExternalHits << std::endl;
    if ( verbosity > 1 ) {
      stream << "NumElements = " << m_elements.size() << std::endl;
      int index{0};
      for ( const auto& ielem : m_elements ) {
        stream << "Element: " << index << " numhits= " << ielem.m_numHits << " numoutlier= " << ielem.m_numOutliers
               << " numtracks=" << ielem.m_numTracks << " related elements: " << ielem.d2Chi2DAlphaDBeta().size();
        if ( verbosity > 2 ) {
          stream << " : " << std::flush;
          for ( const auto& jelem : ielem.d2Chi2DAlphaDBeta() ) stream << jelem.first << " " << std::flush;
        }
        stream << std::endl;
        ++index;
      }
    }
    return os;
  }

} // namespace LHCb::Alignment
