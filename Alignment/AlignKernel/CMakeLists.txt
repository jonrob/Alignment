###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Alignment/AlignKernel
---------------------
#]=======================================================================]

gaudi_add_library(AlignKernel
    SOURCES
        src/AlEquations.cpp
        src/AlSymMat.cpp
        src/OTMonoLayerAlignData.cpp
    LINK
        PUBLIC
            Eigen3::Eigen
            Gaudi::GaudiKernel
            LHCb::DetDescLib
        PRIVATE
            GSL::gsl
)
if(USE_DD4HEP)
    target_link_libraries(AlignKernel PUBLIC Detector::DetectorLib)
endif()

gaudi_add_dictionary(AlignKernelDict
    HEADERFILES dict/AlignKernelDict.h
    SELECTION dict/AlignKernelDict.xml
    LINK AlignKernel
)
