/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// from STD
#include <iomanip>
#include <iostream>
#include <vector>

#include "AlignKernel/AlMat.h"
#include "AlignKernel/AlVec.h"

class AlSymMat {

public:
  AlSymMat( size_t N ) { reSize( N ); }
  AlSymMat()                    = default;
  AlSymMat( const AlSymMat& m ) = default;
  AlSymMat( AlSymMat&& m )      = default;
  AlSymMat& operator=( const AlSymMat& m ) = default;
  AlSymMat& operator=( AlSymMat&& m ) = default;

  // access method that requires irow>=icol without checking
  double  fast( size_t irow, size_t icol ) const { return m_data[( irow + 1 ) * irow / 2 + icol]; }
  double& fast( size_t irow, size_t icol ) { return m_data[( irow + 1 ) * irow / 2 + icol]; }
  double operator()( size_t irow, size_t icol ) const { return irow >= icol ? fast( irow, icol ) : fast( icol, irow ); }
  double& operator()( size_t irow, size_t icol ) { return irow >= icol ? fast( irow, icol ) : fast( icol, irow ); }

  AlSymMat& operator+=( const AlSymMat& m );
  AlSymMat& operator-=( const AlSymMat& m );
  AlSymMat& operator*=( const double& d );
  AlSymMat  operator-( const AlSymMat& m ) const;
  AlSymMat  operator+( const AlSymMat& m ) const;

  // insert block from SMatrix types
  template <typename T>
  void insert( unsigned col, unsigned row, const T& rhs ) {
    for ( unsigned i = 0; i < T::kCols; ++i )
      for ( unsigned j = 0; j < T::kRows; ++j ) ( *this )( col + i, row + j ) = rhs( i, j );
  }

  // reSize method
  void reSize( size_t N ) {
    m_N = N;
    m_data.resize( N * ( N + 1 ) / 2, 0.0 );
  }
  size_t size() const { return m_N; }
  size_t rows() const { return m_N; }
  size_t cols() const { return m_N; }

  int          diagonalize_GSL( AlVec& egval, AlMat& egvec ) const;
  double       determinant() const;
  inline AlMat toMatrix() const;
  // explicit operator AlMat() const { return toMatrix() ; }

private:
  std::vector<double> m_data;
  size_t              m_N{0};
};

// inline operators
inline std::ostream& operator<<( std::ostream& lhs, const AlSymMat& rhs ) {
  lhs << "[ ";
  for ( size_t i = 0u; i < rhs.size(); ++i ) {
    for ( size_t j = 0u; j < rhs.size(); ++j ) { lhs << " " << rhs( i, j ) << " "; }
    lhs << std::endl;
  }
  lhs << " ]" << std::endl;
  return lhs;
}

inline AlMat AlSymMat::toMatrix() const {
  const auto&                                           matrix = *this;
  const auto                                            N      = m_N;
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> rc{N, N};
  for ( size_t irow = 0; irow < N; ++irow )
    for ( size_t icol = 0; icol <= irow; ++icol ) rc( irow, icol ) = rc( icol, irow ) = matrix.fast( irow, icol );
  return rc;
}

inline AlMat operator*( const AlSymMat& lhs, const AlSymMat& rhs ) { return lhs.toMatrix() * rhs.toMatrix(); }

inline AlMat operator*( const AlMat& lhs, const AlSymMat& rhs ) { return lhs * rhs.toMatrix(); }

inline AlMat operator*( const AlSymMat& lhs, const AlMat& rhs ) { return lhs.toMatrix() * rhs; }

inline AlSymMat operator*( double d, const AlSymMat& m ) {
  AlSymMat rc = m;
  rc *= d;
  return rc;
}
