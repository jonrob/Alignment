/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/Time.h"
#include <fstream>
#include <istream>
#include <map>
#include <ostream>
#include <vector>

namespace LHCb::Alignment {
  class OffDiagonalData {
  public:
    Gaudi::Matrix6x6 m_matrix;
    size_t           m_numTracks{0};
    size_t           m_numHits{0};

  public:
    Gaudi::Matrix6x6&       matrix() { return m_matrix; }
    const Gaudi::Matrix6x6& matrix() const { return m_matrix; }
    size_t                  numTracks() const { return m_numTracks; }
    auto&                   numTracks() { return m_numTracks; }
    size_t                  numHits() const { return m_numHits; }
    template <class T>
    void add( const T& m ) {
      m_matrix += m;
      ++m_numHits;
    }
    OffDiagonalData& operator+=( const OffDiagonalData& rhs ) {
      m_matrix += rhs.m_matrix;
      m_numTracks += rhs.m_numTracks;
      m_numHits += rhs.m_numHits;
      return *this;
    }
  };

  class ElementData {
  public:
    typedef std::map<size_t, OffDiagonalData> OffDiagonalContainer;
    typedef ROOT::Math::SMatrix<double, 6, 5> TrackDerivatives;
    typedef ROOT::Math::SMatrix<double, 6, 3> VertexDerivatives;

    void                        add( const ElementData& rhs );
    void                        transform( const Gaudi::Matrix6x6& jacobian );
    const Gaudi::Vector6&       dChi2DAlpha() const { return m_dChi2DAlpha; }
    const Gaudi::SymMatrix6x6&  d2Chi2DAlpha2() const { return m_d2Chi2DAlpha2; }
    const OffDiagonalContainer& d2Chi2DAlphaDBeta() const { return m_d2Chi2DAlphaDBeta; }
    const TrackDerivatives&     dStateDAlpha() const { return m_dStateDAlpha; }
    const VertexDerivatives&    dVertexDAlpha() const { return m_dVertexDAlpha; }
    const Gaudi::Vector6&       alpha() const { return m_alpha; }
    void                        setAlpha( const Gaudi::Vector6& alpha ) {
      m_alpha      = alpha;
      m_alphaIsSet = true;
    }
    auto                  alphaIsSet() const { return m_alphaIsSet; }
    void                  setAlignFrame( const Gaudi::Vector6& alignframe ) { m_alignframe = alignframe; }
    const Gaudi::Vector6& alignFrame() const { return m_alignframe; }

    double fracNonOutlier() const { return 1 - ( m_numHits > 0 ? m_numOutliers / double( m_numHits ) : 0 ); }
    double weightV() const { return m_weightV; }
    double weightR() const { return m_weightR; }
    size_t numHits() const { return m_numHits; }
    size_t numOutliers() const { return m_numOutliers; }
    size_t numTracks() const { return m_numTracks; }
    void   addHitSummary( double V, double R, const Gaudi::XYZPoint& hitpos ) {
      m_numHits += 1;
      m_weightV += 1 / V;
      m_weightR += R / ( V * V );
      m_sumX += hitpos.x();
      m_sumY += hitpos.y();
      m_sumZ += hitpos.z();
    }
    Gaudi::XYZPoint averageHitPosition() const {
      return m_numHits > 0 ? Gaudi::XYZPoint( m_sumX / m_numHits, m_sumY / m_numHits, m_sumZ / m_numHits )
                           : Gaudi::XYZPoint();
    }
    auto& numTracks() { return m_numTracks; }

    Gaudi::Vector6       m_alignframe;        // parameters of the alignment frame
    Gaudi::Vector6       m_alpha;             // the set of parameters at which the derivatives are computed
    Gaudi::Vector6       m_dChi2DAlpha;       // (half) 1st derivative
    Gaudi::SymMatrix6x6  m_d2Chi2DAlpha2;     // (half) 2nd derivative diagonal ('this-module')
    OffDiagonalContainer m_d2Chi2DAlphaDBeta; // (half) 2nd derivative off-diagonal ('module-to-module')
    TrackDerivatives     m_dStateDAlpha;      // derivative of track parameter to alpha
    VertexDerivatives    m_dVertexDAlpha;     // derivative of vertex position to alpha
    size_t               m_numHits{0};
    size_t               m_numOutliers{0};
    size_t               m_numTracks{0};
    double               m_weightV{0}; // sum V^{-1}          --> weight of 1st derivative
    double               m_weightR{0}; // sum V^{-1} R V^{-1} --> weight of 2nd derivative
    bool                 m_alphaIsSet{false};
    double               m_sumX{0};
    double               m_sumY{0};
    double               m_sumZ{0};
  };

  class Equations {
  public:
    typedef std::vector<ElementData> ElementContainer;
    Equations( size_t nElem = 0, Gaudi::Time time = Gaudi::Time::epoch() );
    void               clear();
    size_t             nElem() const { return m_elements.size(); }
    ElementData&       element( size_t i ) { return m_elements[i]; }
    const ElementData& element( size_t i ) const { return m_elements[i]; }

    void addChi2Summary( double chisq, size_t ndof, size_t nexternal ) {
      m_totalChiSquare += chisq;
      m_totalNumDofs += ndof;
      m_numExternalHits += nexternal;
    }

    void addVertexChi2Summary( double chisq, size_t ndof ) {
      m_totalVertexChiSquare += chisq;
      m_totalVertexNumDofs += ndof;
    }

    void addEventSummary( size_t numtracks, size_t numvertices, size_t numparticles, Gaudi::Time time, size_t runnr,
                          size_t iov ) {
      ++m_numEvents;
      m_numTracks += numtracks;
      m_numVertices += numvertices;
      m_numParticles += numparticles;
      if ( m_firstTime.ns() > time.ns() ) m_firstTime = time;
      if ( m_lastTime.ns() < time.ns() ) m_lastTime = time;
      if ( m_firstRun > runnr ) m_firstRun = runnr;
      if ( m_lastRun < runnr ) m_lastRun = runnr;
      if ( m_minIOV > iov ) m_minIOV = iov;
      if ( m_maxIOV < iov ) m_maxIOV = iov;
    }

    size_t      numTracks() const { return m_numTracks; }
    size_t      numVertices() const { return m_numVertices; }
    size_t      numParticles() const { return m_numParticles; }
    size_t      numEvents() const { return m_numEvents; }
    Gaudi::Time firstTime() const { return m_firstTime; }
    Gaudi::Time lastTime() const { return m_lastTime; }
    Gaudi::Time initTime() const { return m_initTime; }
    size_t      firstRun() const { return m_firstRun; }
    size_t      lastRun() const { return m_lastRun; }
    auto        minIOV() const { return m_minIOV; }
    auto        maxIOV() const { return m_maxIOV; }
    size_t      numHits() const;
    double      totalChiSquare() const { return m_totalChiSquare; }
    size_t      totalNumDofs() const { return m_totalNumDofs; }
    double      totalVertexChiSquare() const { return m_totalVertexChiSquare; }
    size_t      totalVertexNumDofs() const { return m_totalVertexNumDofs; }
    double      totalTrackChiSquare() const { return totalChiSquare() - m_totalVertexChiSquare; }
    size_t      totalTrackNumDofs() const { return totalNumDofs() - m_totalVertexNumDofs; }
    size_t      numExternalHits() const { return m_numExternalHits; }

    std::tuple<size_t, long>            writeToFile( const char* filename ) const;
    std::tuple<StatusCode, std::string> readFromFile( const char* filename );
    void                                writeToBuffer( std::ofstream& buffer ) const;
    void                                readFromBuffer( std::ifstream& buffer );

    /// Add the data from another Equations object. If correctDeltaAlpha is true, it will check that the other
    /// object was computed around the same parameter set, and if not, correct for the difference.
    std::map<std::string, std::vector<std::string>> add( const Equations&, bool correctDeltaAlpha = false );

    std::ostream& fillStream( std::ostream& os, int verbosity = 0 ) const;

    const ElementContainer& elements() const { return m_elements; }

  private:
    ElementContainer m_elements;
    size_t           m_numEvents{0};
    size_t           m_numTracks{0};
    size_t           m_numVertices{0};
    size_t           m_numParticles{0};
    double           m_totalChiSquare{0.};
    size_t           m_totalNumDofs{0};
    size_t           m_numExternalHits{0};
    double           m_totalVertexChiSquare{0.};
    size_t           m_totalVertexNumDofs{0};
    Gaudi::Time      m_firstTime{Gaudi::Time::max()};
    Gaudi::Time      m_lastTime{Gaudi::Time::epoch()};
    Gaudi::Time      m_initTime{0};
    size_t           m_firstRun{std::numeric_limits<size_t>::max()};
    size_t           m_lastRun{0};
    size_t           m_minIOV{std::numeric_limits<size_t>::max()};
    size_t           m_maxIOV{0};
  };

} // namespace LHCb::Alignment
