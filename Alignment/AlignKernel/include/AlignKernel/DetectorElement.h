/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/DetectorElement.h"
#ifndef USE_DD4HEP
#  include "DetDesc/GlobalToLocalDelta.h"
#endif

#include "GaudiKernel/GaudiException.h"

namespace LHCb::Alignment {

  namespace details {

#ifdef USE_DD4HEP
#  define DELEGATE( CALL )                                                                                             \
    auto& de = m_data;                                                                                                 \
    return de.CALL;
#  define DELEGATE2( EXPR1, EXPR2 )                                                                                    \
    auto& de = m_data;                                                                                                 \
    return EXPR2;
#  define DELEGATEVOID( EXPR1, EXPR2 )                                                                                 \
    auto& de = m_data;                                                                                                 \
    EXPR2
#else
#  define DELEGATE( CALL )                                                                                             \
    auto& de = m_data;                                                                                                 \
    return de->CALL;
#  define DELEGATE2( EXPR1, EXPR2 )                                                                                    \
    auto& de = m_data;                                                                                                 \
    return EXPR1;
#  define DELEGATEVOID( EXPR1, EXPR2 )                                                                                 \
    auto& de = m_data;                                                                                                 \
    EXPR1;
#endif
  } // namespace details

  /**
   * Wrapper around DetectorElement and/or DeIOV, hiding the differences between DetDesc and DD4Hep
   * To be dropped one DetDesc is dropped.
   */
  class DetectorElement {
  public:
#ifdef USE_DD4HEP
    template <typename T>
    DetectorElement( const T& de ) : m_data( de ) {}
#else
    DetectorElement( const DetDesc::DetectorElementPlus& de ) : m_data( &de ) {}
    DetectorElement( const DetDesc::DetectorElementPlus* de ) : m_data( de ) {}
#endif
    const std::string       name() const { DELEGATE2( de->name(), std::string( de.detector().path() ) ); }
    ROOT::Math::Transform3D toLocalMatrix() const { DELEGATE( toLocalMatrix() ); }
    ROOT::Math::Transform3D toLocalMatrixNominal() const { DELEGATE( toLocalMatrixNominal() ); }
    ROOT::Math::XYZPoint    toLocal( const ROOT::Math::XYZPoint& globalPoint ) const {
      DELEGATE( toLocal( globalPoint ) );
    }
    ROOT::Math::XYZVector toLocal( const ROOT::Math::XYZVector& globalVector ) const {
      DELEGATE( toLocal( globalVector ) );
    }
    ROOT::Math::Transform3D toLocal( const ROOT::Math::Transform3D& globalTransform ) const {
      DELEGATE( toLocal( globalTransform ) );
    }
    ROOT::Math::Transform3D toGlobalMatrix() const { DELEGATE( toGlobalMatrix() ); }
    ROOT::Math::Transform3D toGlobalMatrixNominal() const { DELEGATE( toGlobalMatrixNominal() ); }
    ROOT::Math::XYZPoint    toGlobal( const ROOT::Math::XYZPoint& localPoint ) const {
      DELEGATE( toGlobal( localPoint ) );
    }
    ROOT::Math::XYZVector toGlobal( const ROOT::Math::XYZVector& localVector ) const {
      DELEGATE( toGlobal( localVector ) );
    }
    ROOT::Math::Transform3D toGlobal( const ROOT::Math::Transform3D& localTransform ) const {
      DELEGATE( toGlobal( localTransform ) );
    }
    ROOT::Math::Transform3D ownToOffNominalMatrix() const { DELEGATE( ownToOffNominalMatrix() ); }
#ifdef USE_DD4HEP
    dd4hep::Delta      delta() const { return m_data.delta(); }
    dd4hep::DetElement detector() const { return m_data.detector(); }
    size_t             iov() const { return m_data->iovData()->key().first; }
#else
    bool ownToOffNominalMatrix( const ROOT::Math::Transform3D& newDelta ) const {
      return m_data->ownToOffNominalMatrix( newDelta );
    }
#endif
    std::vector<double>                    elemDeltaTranslations() const { DELEGATE( elemDeltaTranslations() ); }
    std::vector<double>                    elemDeltaRotations() const { DELEGATE( elemDeltaRotations() ); }
    std::optional<ROOT::Math::Transform3D> motionSystemTransform() const { DELEGATE( motionSystemTransform() ); }
    const ROOT::Math::Transform3D          localDeltaMatrix( const ROOT::Math::Transform3D& globalDelta ) const {
      DELEGATE2( ::DetDesc::localDeltaMatrix( de, globalDelta ), de.localDeltaMatrix( globalDelta ) );
    }
#ifdef USE_DD4HEP
    const std::string            alignmentConditionName() const { return std::string( m_data.detector().path() ); }
    const LHCb::Detector::DeIOV* alignmentCondition() const { return &m_data; }
#else
    const std::string alignmentConditionName() const {
      return alignmentCondition() ? alignmentCondition()->name() : "";
    }
    const AlignmentCondition* alignmentCondition() const {
      return m_data->geometryPlus() ? m_data->geometryPlus()->alignmentCondition() : nullptr;
    }
#endif

    void applyToAllChildren( std::function<void( const LHCb::Alignment::DetectorElement& )> func ) const {
      DELEGATEVOID(
          const auto& children = de->childIDetectorElements();
          std::for_each( children.begin(), children.end(),
                         [&func]( const ::IDetectorElement* item ) {
                           std::invoke( func, LHCb::Alignment::DetectorElement{
                                                  dynamic_cast<const DetDesc::DetectorElementPlus*>( item )} );
                         } ),
          if ( !de.detector().children().empty() ) de.applyToAllChildren( [&func]( LHCb::Detector::DeIOV item ) {
            std::invoke( func, LHCb::Alignment::DetectorElement{item} );
          } ); );
    }

    LHCb::Alignment::DetectorElement getChild( const std::string& part ) {
#ifdef USE_DD4HEP
      LHCb::Detector::detail::DeIOVObject* det{nullptr};
      m_data.applyToAllChildren( [&det, &part]( LHCb::Detector::DeIOV item ) {
        if ( item.name() == part ) det = item.access();
      } );
      if ( det ) return LHCb::Detector::DeIOV( det );
#else
      const auto& children = m_data->childIDetectorElements();
      auto it = std::find_if( children.begin(), children.end(), [&part]( const DetDesc::IDetectorElementPlus* item ) {
        auto& name = item->name();
        return name.length() > part.length() && name.substr( name.length() - part.length() ) == part &&
               name[name.length() - part.length() - 1] == '/';
      } );
      if ( it != children.end() ) return dynamic_cast<const DetDesc::DetectorElementPlus*>( *it );
#endif
      throw GaudiException( "WriteAlignmentConditions : unable to find requested element : no child named " + part +
                                " was found in " + name(),
                            "GetElementsToBeAligned:", StatusCode::FAILURE );
    }

#ifdef USE_DD4HEP
    const std::optional<DetectorElement> findElemForPoint( const ROOT::Math::XYZPoint& point ) const {
      DetectorElement myele = m_data.findChildForPoint( point );
      return myele;
    }
#else
    DetDesc::IDetectorElementPlus const* _findElemForPoint( DetDesc::IDetectorElementPlus const* elem,
                                                            const ROOT::Math::XYZPoint&          xyz ) const {
      auto child = elem ? elem->childDEWithPoint( xyz ) : nullptr;
      if ( !child ) return elem;
      return _findElemForPoint( child, xyz );
    }
    const std::optional<DetectorElement> findElemForPoint( const ROOT::Math::XYZPoint& xyz ) const {
      auto* d = _findElemForPoint( m_data, xyz );
      return d ? DetectorElement{dynamic_cast<const DetDesc::DetectorElementPlus*>( d )}
               : std::optional<DetectorElement>{};
    }
#endif

    auto size() const { DELEGATE2( de->childIDetectorElements().size(), de.size() ); }

    bool operator==( const DetectorElement& other ) const { return m_data == other.m_data; }
    bool operator<( const DetectorElement& other ) const { return m_data < other.m_data; }

#ifdef USE_DD4HEP
    template <typename T>
    std::optional<const T> castTo() const {
      const T handle{m_data};
      return handle.isValid() ? std::optional<const T>{handle} : std::nullopt;
    }
#else
    template <typename T>
    const T* castTo() const {
      return dynamic_cast<const T*>( m_data );
    }
#endif

  private:
#ifdef USE_DD4HEP
    ::DetectorElement m_data;
#else
    const DetDesc::DetectorElementPlus* m_data;
#endif
  };

} // namespace LHCb::Alignment
