/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef NAMEITSECTOR_H
#define NAMEITSECTOR_H

#include <string>

/** @class  NameITSector
 *  @author Zhirui Xu
 *  @date   2015.10.16
 */

namespace Alignment {
  namespace AlignmentMonitoring {
    class NameITSector {
    public:
      // standard constructor
      NameITSector( const std::string name );
      virtual ~NameITSector(){};

      std::string GetBoxName();
      std::string GetLayerName();
      int         GetUniqueSector();
      int         GetUniqueLayer();
      int         GetUniqueBox();
      int         GetNumberOfSensors();
      int         GetSectorID();
      int         GetStationID();
      int         GetChannel();

    protected:
    private:
      std::string m_station;
      std::string m_box;
      std::string m_sector;
      std::string m_layer;

      int m_uniqueBox;
      int m_uniqueLayer;
      int m_uniqueSector;
      int m_sectorID;
      int m_stationID;
      int m_numberOfSensors;

      int  m_channel;
      void ConvertName2Channel();
    };

  } // namespace AlignmentMonitoring
} // namespace Alignment

#endif
