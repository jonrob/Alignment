#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from builtins import str
import os, re, fnmatch


def findLastRun(activity):
    #-->activity=Velo/Tracker/...
    rootDir = '/group/online/AligWork/{0}/'.format(activity)
    ll = sorted([(re.findall('run([0-9]+)$', i)[0],
                  os.stat(os.path.join(rootDir, i)).st_mtime)
                 for i in os.listdir(rootDir) if re.match('run([0-9]+)$', i)],
                key=lambda x: x[-1])
    return ll[-1][0]


def findAlignlog(activity, run):
    rootDir = '/group/online/AligWork/{0}/'.format(activity)
    inDir = os.path.join(rootDir, 'run{0}'.format(run))
    alignlog_path = os.path.join(inDir, 'alignlog.txt')
    return alignlog_path


def findHistosAlignlog(activity, run):
    rootDir = '/group/online/AligWork/{0}/'.format(activity)
    inDir = os.path.join(rootDir, 'run{0}'.format(run))
    try:
        last = sorted([
            i for i in os.listdir(inDir)
            if 'AligHistos{0}_run'.format(activity) in i
        ])[-1]
    except IndexError:
        raise IOError('No alignlog histo root file in ' + inDir)
    halignlog_path = os.path.join(inDir, last)
    return halignlog_path


def findHistos(activity, run):
    histDir = '/hist/Savesets/2017/LHCbA/AligWrk_{0}'.format(activity)
    matches = []
    for root, dirs, files in os.walk(histDir):
        for file in files:
            #runplus = str(int(run)+2) #temporary hack
            #match = re.findall(r'AligWrk_{activity}-{runplus}(.*?)-(.*?)-EOR.root'.format(**locals()), file) #temporary hack
            run_start = int(str(run)[:-2])  #temporary hack
            run_end = int(str(run)[-2:])  #temporary hack
            match = re.findall(
                r'AligWrk_{activity}-{run_start}(?:{run_end:02}|{run_end1:02}|{run_end2:02})(.*?)-(.*?)-EOR.root'
                .format(
                    run_end1=run_end + 1, run_end2=run_end + 2,
                    **locals()), file)  #temporary hack
            #match = re.findall(r'AligWrk_{activity}-{run}(.*?)-(.*?)-EOR.root'.format(**locals()), file) #correct
            if match:
                matches.append(list(match[0]) + [os.path.join(root, file)])

    matches = sorted(matches, key=lambda x: x[1], reverse=True)

    if len(matches) == 0:
        raise IOError('No histogram file found for run {0}'.format(run))

    files_histo = {}
    #####################################################
    # Commented all, do not trust iteration number at all to avoid
    # cases like run 192212 where second iteration has iteration number 01 :(
    # N.B. Obviously causes problems if run alignment multiple times on a run
    #####################################################
    #if matches[0][0] == '01': # only one histogram file
    #    files_histo['old'] = matches[0][2]
    #else:
    #    for match in matches:
    #        if match[0] == '01':
    #            files_histo['old'] = match[2]
    #            files_histo['new'] = matches[0][2]
    #            break
    #####################################################
    if not len(
            files_histo
    ):  # temporary patch if first run has not 01, use only timestamp info, assumes each fill run only once
        files_histo['old'] = matches[-1][2]
        files_histo['new'] = matches[0][2]
    return files_histo
