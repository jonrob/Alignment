#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
import os, sys, re, time, subprocess, shlex
import urllib.request, urllib.parse, urllib.error, json

AligWork_dir = '/group/online/AligWork'
Moni_dir = '/group/online/AligWork/MoniPlots'
alignment_dir = '/group/online/alignment'
activity = 'Velo'
minRun = 169620  # 160140 # 168233 #
oldtime = {'Velo': 0, 'Tracker': 0, 'Muon': 0}

this_file_dir = os.path.dirname(os.path.realpath(__file__))

sys.path.append('/group/online/rundb/RunDatabase/python/')


def getFillNumber_old(runNumber):
    import rundb
    db = rundb.RunDB()
    return db.getrun(runNumber)[0]['fillID']


def getFillNumber(runNumber):

    url = 'http://lbrundb.cern.ch/api/run/{run_number}/'
    url = url.format(run_number=runNumber)

    data = None
    for n_attempt in range(5):
        response = urllib.request.urlopen(url)
        if response.getcode() == 200:
            data = json.loads(response.read())
            break
        else:
            time.sleep(1)

    if data is None:
        raise RuntimeError('Failed to get fill number from ' + url,
                           response.getcode())

    return data['fillid']


def getRunsAnalised(activity='Velo'):
    alignsDir = os.path.join(Moni_dir, activity)
    runs = []
    for folder in os.listdir(alignsDir):
        if re.match('^(?:v[0-9]*_)?([0-9]*).pdf$', folder):
            runs.append(
                int(re.findall('^(?:v[0-9]*_)?([0-9]*).pdf$', folder)[0]))
    return sorted([i for i in runs])


def getRunsAligned(activity='Velo', minRun=0):
    alignsDir = os.path.join(AligWork_dir, activity)
    runs = []
    for folder in os.listdir(alignsDir):
        if re.match('^run([0-9]*)$', folder):
            runs.append(int(re.findall('^run([0-9]*)$', folder)[0]))
    return sorted([i for i in runs if i >= minRun])


def getRunFromXml(xml_file):
    text = open(xml_file).read()
    try:
        run = re.findall('<!-- Version:: run([0-9]*) -->', text)[0]
        return int(run)
    except IndexError:
        return None


def getRunsUpdated(activity='Velo', minRun=0):
    if activity == 'Tracker': activity = 'IT'
    xmlDir = os.path.join(alignment_dir, '{0}/{0}Global'.format(activity))
    runs = {}
    for xml_file in os.listdir(xmlDir):
        alignVersion = int(re.findall('.*v([0-9]*).xml', xml_file)[0])
        run = getRunFromXml(os.path.join(xmlDir, xml_file))
        if run != None and run >= minRun:
            runs[run] = alignVersion
    return runs


def getRuns2Analise(activity='Velo', minRun=0):
    return sorted([
        i for i in getRunsAligned(activity, minRun)
        if i not in getRunsAnalised(activity)
    ])


def hasNewAlignment(activity='Velo'):
    global oldtime
    newtime = os.path.getmtime(os.path.join(AligWork_dir, activity))
    if newtime > oldtime[activity]:
        oldtime[activity] = newtime
        return True
    else:
        return False


def printTime():
    return time.strftime('%d/%m/%y %H:%M:%S |')


def writeInLogbook(Fill,
                   activity,
                   updated=False,
                   file2upload=None,
                   version=None,
                   bigVariations=None):
    host = 'lblogbook.cern.ch'  #'localhost'
    port = 8080
    username = 'common Common\\!'
    logbook = 'Alignment monitoring'
    author = 'monibot'
    Type = 'Convergence'
    instruction_file = 'https://lbgroups.cern.ch/online/Shifts/alignMonitoring.pdf'
    if activity == 'Tracker':
        instruction_file = 'https://lbgroups.cern.ch/online/Shifts/alignMonitoring_Tracker.pdf'
    if updated:
        status = 'Unchecked'
        subject = 'Monitoring plots'
        textVersion = ' (v{0}),'.format(version) if version else ','
        text = "Updated {activity} alignment for fill {Fill}{textVersion} monitoring plots in the attachment; shifter's instructions can be found at {instruction_file}".format(
            **locals())
    else:
        if bigVariations:
            status = 'Bad'
            subject = 'Variation too big'
            if activity == 'Muon':
                text = "Constant's variations too big for {activity} alignment for fill {Fill}.\n {bigVariations}".format(
                    **locals())
            else:
                text = "{activity} alignment not updated for fill {Fill} because constants' variation too big.\n {bigVariations}".format(
                    **locals())
        else:
            status = 'Good'
            subject = 'No alignment update'
            text = "No {activity} alignment update needed for fill {Fill}.".format(
                **locals())

    command = 'elog -h "{host}" -p {port} -l "{logbook}" -u {username} -a "Author={author}" -a "Fill={Fill}" -a "System={activity}" -a "Type={Type}" -a "Status={status}" -a "Subject={subject}" "{text}"'.format(
        **locals())

    if file2upload:
        command += ' -f "{0}"'.format(file2upload)

    cmd = shlex.split(command)
    #FNULL = open(os.devnull, 'w')
    FNULL = open(
        '/group/online/AligWork/MoniPlots/moniLog_Elog.log'.format(activity),
        'w')

    subprocess.call(cmd, stdout=FNULL, stderr=subprocess.STDOUT)
    return 0


def sendEmail(text, receivers=['gdujany@cern.ch']):
    import smtplib
    sender = 'lhcb-onlinealignmentcalibration@cern.ch'
    message = 'From: Alignment Monitor <lhcb-onlinealignmentcalibration@cern.ch>\n'
    message += 'To: <lhcb-onlinealignmentcalibration@cern.ch>\n'
    message += 'Subject: Warning from Alignment monitor\n\n'
    message += '\n{0}\n\n'.format(text)

    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receivers, message)
        print("Successfully sent email")
    except smtplib.SMTPException:
        print("Error: unable to send email")


if __name__ == '__main__':
    moniScript = {
        'Velo': 'moniPlots.py',
        'Tracker': 'moniPlots_Tracker.py',
        'Muon': 'moniPlots_Muon.py'
    }

    #################################################################
    # Initialize monitoring Job to send plots to the presenter
    #################################################################
    from Configurables import MonitoringJob, Hlt2SaverSvc
    # Try to have log less verbose
    MonitoringJob.OutputLevel = 'Fatal'
    #Hlt2SaverSvc.OutputLevel = 'Fatal'
    from Monitoring.MonitoringJob import start
    mj = MonitoringJob()
    # The JobName should be the same as the Senders
    mj.JobName = "MoniOnlineAlig"
    # If I want to use directory different from JobName
    mj.HistogramDirectories = [
        mj.JobName + "Velo", mj.JobName + "Tracker", mj.JobName + "Muon",
        mj.JobName + "Rich1", mj.JobName + "Rich2"
    ]
    # mj.SavePath = "/tmp/histograms"
    # If the histograms need to be available to an analysis task, this
    # property should be set
    # mj.RegistrarConnection = "tcp://%s:31360" % socket.gethostname()
    mj.Sender = False
    mj.Saver = True
    # Start our main job
    gaudi, monSvc = start()
    #################################################################

    # Logging

    logFile_name = '/group/online/AligWork/MoniPlots/log.log'
    if os.path.exists(logFile_name) and os.path.getsize(logFile_name) > 1e6:
        os.remove(logFile_name)

    import logging
    logger = logging.getLogger('myapp')
    hdlr = logging.FileHandler(logFile_name)
    formatter = logging.Formatter('%(asctime)s | %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)

    while True:
        for activity in ['Velo', 'Tracker', 'Muon']:
            if hasNewAlignment(activity):
                if not os.path.exists(os.path.join(Moni_dir, activity)):
                    os.mkdir(os.path.join(Moni_dir, activity))
                    os.chmod(os.path.join(Moni_dir, activity), 0o1775)
                toAnalise = getRuns2Analise(activity, minRun)
                # if activity == 'Velo': toAnalise = [192704]
                #if activity == 'Tracker': toAnalise = [192704]
                #if activity == 'Muon': toAnalise = [187178]
                runsUpdated = getRunsUpdated(activity, minRun)
                print(printTime(), 'Alignments, runs to analise for ',
                      activity, ': ', toAnalise)
                sys.stdout.flush()
                logger.info('Alignments, runs to analise for {0} : {1}'.format(
                    activity, toAnalise))
                if not toAnalise:  # Make and send to presenter plots last alignment
                    #logger.info('Making plots for {0} anyway'.format(activity))
                    # Get path savesets used in previous run:
                    try:
                        paths = open(
                            '/group/online/AligWork/{}/lastHistos.txt'.format(
                                activity)).readlines()
                        histoFiles = '--histoFiles {0} {1}'.format(
                            paths[2].strip(), paths[1].strip())
                        if not (os.path.exists(paths[2].strip())
                                and os.path.exists(paths[1].strip())):
                            sendEmail(
                                'Files {0} and/or {1} needed to make plot for run {2} disappeared'
                                .format(paths[2].strip(), paths[1].strip(),
                                        activity))
                            raise IOError(
                                'Files {0} and/or {1} needed to make plot for run {2} disappeared'
                                .format(paths[2].strip(), paths[1].strip(),
                                        activity))
                    except IOError:
                        histoFiles = ''
                    command = '{0} --online -o {1} {2}'.format(
                        os.path.join(this_file_dir, moniScript[activity]),
                        '/tmp/alignment.pdf', histoFiles)
                    #if activity == 'Velo': command += ' -r 192704'
                    #if activity == 'Tracker': command += ' -r 192704'
                    #if activity == 'Muon': command += ' -r 192715'
                    cmd = shlex.split(command)
                    FNULL = open(
                        '/group/online/AligWork/MoniPlots/moniLog_{0}.log'.
                        format(activity), 'w')  #open(os.devnull, 'w')
                    success = subprocess.call(
                        cmd, stdout=FNULL,
                        stderr=subprocess.STDOUT)  # if succes should be 0
                for run in toAnalise:
                    if run in runsUpdated:
                        version = runsUpdated[run]
                        outFile_name = os.path.join(
                            Moni_dir, '{0}/v{1}_{2}.pdf'.format(
                                activity, version, run))
                    else:
                        version = None
                        outFile_name = os.path.join(
                            Moni_dir, '{0}/{1}.pdf'.format(activity, run))
                    #outFile_name='test{0}.pdf'.format(activity)
                    print(
                        printTime(), 'Analising run {0}'.format(run) +
                        (', alignment version v{0}'.format(version)
                         if version else ''))
                    sys.stdout.flush()
                    logger.info('Analising run {0}'.format(run) +
                                (', alignment version v{0}'.
                                 format(version) if version else ''))
                    # Making pdf with plots
                    command = '{0} --online -r {1} -o {2}'.format(
                        os.path.join(this_file_dir, moniScript[activity]), run,
                        outFile_name)
                    cmd = shlex.split(command)
                    FNULL = open(
                        '/group/online/AligWork/MoniPlots/moniLog_{0}.log'.
                        format(activity), 'w')  #open(os.devnull, 'w')
                    success = subprocess.call(
                        cmd, stdout=FNULL,
                        stderr=subprocess.STDOUT)  # if succes should be 0
                    # success = 1 # to avoid spamming the loogbook with tests

                    if not version:
                        # check if it did not update because too big a movement
                        command = '{0} {1}/Iter1/ {1}/xml/ -l 3'.format(
                            os.path.join(this_file_dir, 'diffXML.py'),
                            os.path.join(AligWork_dir, activity,
                                         'run{0}'.format(run)))
                        cmd = shlex.split(command)
                        pgr = subprocess.Popen(cmd, stdout=subprocess.PIPE)
                        bigVariations = pgr.communicate()[0]
                        rc = pgr.returncode
                        if rc != 27 and rc != 0:
                            sendEmail(
                                'Unable to make check if costats movement was too large for {0} run {1}, please have a look'
                                .format(activity, run))

                    if success == 0:
                        if version:
                            if activity != 'Muon':
                                writeInLogbook(
                                    Fill=getFillNumber(run),
                                    activity=activity,
                                    file2upload=outFile_name,
                                    version=version,
                                    updated=True)
                        else:
                            if rc == 27:
                                writeInLogbook(
                                    Fill=getFillNumber(run),
                                    activity=activity,
                                    updated=False,
                                    bigVariations=bigVariations,
                                    file2upload=outFile_name)
                            else:
                                if activity != 'Muon':
                                    writeInLogbook(
                                        Fill=getFillNumber(run),
                                        activity=activity,
                                        updated=False)

                        os.chmod(outFile_name, 0o444)
                    else:
                        sendEmail(
                            'Unable to make pdf for {0} run {1}, please have a look'
                            .format(activity, run))

                    # Making root file with convergence plots
                    command = '{0} -r {1} -a {2}'.format(
                        os.path.join(this_file_dir, 'alignlog2root.py'), run,
                        activity)
                    cmd = shlex.split(command)
                    success = subprocess.call(
                        cmd, stdout=FNULL
                    )  #, stderr=subprocess.STDOUT) # if succes should be 0
                    if success != 0:
                        sendEmail(
                            'Unable to make root file for {0} run {1}, please have a look'
                            .format(activity, run))

                    # Making file with reference to last histos
                    command = '{0} -r {1} -a {2} -o {3}'.format(
                        os.path.join(this_file_dir, 'lastHistos.py'), run,
                        activity, '/group/online/AligWork/{0}/lastHistos.txt'.
                        format(activity))
                    cmd = shlex.split(command)
                    success = subprocess.call(
                        cmd, stdout=FNULL
                    )  #, stderr=subprocess.STDOUT) # if succes should be 0
                    if success != 0:
                        sendEmail(
                            'Unable to make file with references to histos for run {0}, please have a look'
                            .format(run))

                print(printTime(), 'Done for now')
                sys.stdout.flush()
                logger.info('Done for now')

        # Make nice trend plots from time to time
        log_trend_plots = '/group/online/AligWork/MoniPlots/moniLog_Trends.log'

        try:
            if (time.time() - os.stat(log_trend_plots).st_mtime) > 604800:
                raise OSError
        except OSError:  # Goes here both if file does not exists or if it is older than a week
            # command = 'bash /group/online/dataflow/cmtuser/AlignmentRelease/Alignment/AlignmentMonitoring/examples/makePublicityPlots.sh -o /group/online/www/Alignment/publicityPlots_latest'
            command = 'bash {0} -o /group/online/www/Alignment/publicityPlots_latest'.format(
                os.path.join(this_file_dir,
                             '../examples/makePublicityPlots.sh'))
            cmd = shlex.split(command)
            FNULL = open(log_trend_plots, 'w')
            success = subprocess.call(
                cmd, stdout=FNULL,
                stderr=subprocess.STDOUT)  # if succes should be 0

        time.sleep(5)
