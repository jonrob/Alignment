#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from __future__ import print_function
std_AligWork_dir = '/group/online/AligWork/'

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description=
        "Macro to make new xml files with alignment updates needed according to the thresholds set"
    )
    parser.add_argument(
        '-a',
        '--activity',
        help='choose between Velo, Tracker; default is Velo',
        choices=['Velo', 'Tracker', 'Muon'],
        default='Tracker')
    parser.add_argument(
        '--AligWork_dir',
        help='folder with alignment',
        default=std_AligWork_dir)
    parser.add_argument('-r', '--run', help='run number to compare')
    parser.add_argument(
        '-v', '--version', help='version number to compare', default='0')
    args = parser.parse_args()
##########################

import os, sys, re, shutil, pickle


def getListRuns(activity, AligWork_dir=std_AligWork_dir):
    runs = []
    for directory in os.listdir(os.path.join(AligWork_dir, activity)):
        match = re.findall('^run([0-9]+)$', directory)
        if match: runs.append(int(match[0]))
    return sorted(runs)


def std_vector(list, kind='std::string'):
    vec = gbl.std.vector(kind)()
    for i in list:
        vec.push_back(i)
    return vec


if __name__ == '__main__':

    from GaudiPython import gbl
    AlMon = gbl.Alignment.AlignmentMonitoring

    run = getListRuns(args.activity)[-1] if args.run == None else args.run

    print('Analising run', run)

    if args.version == '0':
        aligDir = os.path.join(args.AligWork_dir, args.activity,
                               'run{0}'.format(run))
    else:
        aligDir = os.path.join(args.AligWork_dir, args.activity,
                               'run{0}_v{1}'.format(run, args.version))

    xml_path = os.path.join(aligDir, 'xml/{detector}/{part}.xml')
    Iter1_path = os.path.join(aligDir, 'Iter1/{detector}/{part}.xml')

    if args.activity == 'Velo':
        detectors = {'Velo': ['VeloGlobal', 'VeloModules']}
    elif args.activity == 'Tracker':
        detectors = {
            'TT': ['TTGlobal', 'TTModules'],
            'IT': ['ITGlobal', 'ITModules'],
            'OT': ['OTGlobal', 'OTModules'],
        }
    elif args.activity == 'Muon':
        detectors = {'Muon': ['MuonGlobal', 'MuonModules']}

    cc = AlMon.CompareConstants()
    cc.Compare(
        std_vector([
            Iter1_path.format(detector=detector, part=part)
            for detector in detectors for part in detectors[detector]
        ]),
        std_vector([
            xml_path.format(detector=detector, part=part)
            for detector in detectors for part in detectors[detector]
        ]))

    print('*' * 70)
    print('WARNINGS:\n')
    cc.PrintWarnings(2)

    print('*' * 70)
    print('SEVERE:\n')
    cc.PrintWarnings(3)
    print('*' * 70)

    #thresholds = {alignable: [i for i in cc.GetLimits(alignable)] for alignable in cc.GetAlignablesWithWarnings(2).split(';')}
    #for key, val in thresholds.items():
    #    print key, val

    #cc.PrintWarnings(1)

    #[i for i in cc.GetLimits('TTbVLayerR2Module3B.Tx')]
