#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

##########################
###   Options parser   ###
from __future__ import print_function
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="Macro to find position of the last root files with plots")
    parser.add_argument(
        '-r', '--run', help='run number, default is the last one')
    parser.add_argument(
        '-a',
        '--activity',
        help='choose between Velo, Tracker, Muon; default is Velo',
        choices=['Velo', 'Tracker', 'Muon'],
        default='Velo')
    parser.add_argument(
        '-o', '--outFile', help='output file name, default stdout')
    args = parser.parse_args()

##########################

from AlignmentMonitoring.OnlineUtils import findLastRun, findHistos, findHistosAlignlog
import sys

if __name__ == '__main__':

    run = args.run if args.run else findLastRun(args.activity)

    histos = findHistos(args.activity, run)
    halignlog = findHistosAlignlog(args.activity, run)

    if args.outFile:
        sys.stdout = open(args.outFile, 'w')

    print(halignlog)
    print(histos['new'])
    print(histos['old'])
