#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from __future__ import print_function
std_AligWork_dir = '/group/online/AligWork/'

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description=
        "Macro to compare 2 alignments printing warnings elements that ")
    parser.add_argument('align1', help='First file or folder')
    parser.add_argument('align2', help='Second file or folder')
    parser.add_argument(
        '-l', '--level', help='level', choices=[1, 2, 3], type=int, default=2)
    parser.add_argument(
        '-s',
        '--strict',
        help='Only message for the level chosen',
        action='store_true')
    parser.add_argument(
        '-f',
        '--file',
        help="File with alignment constants' references constants")

    args = parser.parse_args()
##########################

import os, sys, re, fnmatch

dd = '/group/online/AligWork/Velo/run173044/xml/'
ff = '/group/online/AligWork/Velo/run173044/xml/Velo/VeloGlobal.xml'


def findPaths(file_names, startingPath):
    if isinstance(file_names, str):
        file_name = file_names
        for root, dirs, files in os.walk(startingPath):
            for file in files:
                if fnmatch.fnmatch(file, file_name):
                    return root + '/' + file
    else:
        for file_name in file_names:
            for root, dirs, files in os.walk(startingPath):
                for file in files:
                    if fnmatch.fnmatch(file, file_name):
                        return root + '/' + file


def std_vector(list, kind='std::string'):
    vec = gbl.std.vector(kind)()
    for i in list:
        vec.push_back(i)
    return vec


if __name__ == '__main__':

    from GaudiPython import gbl
    AlMon = gbl.Alignment.AlignmentMonitoring

    xmls = [
        'VeloGlobal.xml', 'VeloModules.xml', 'TTGlobal.xml', 'TTModules.xml',
        'ITGlobal.xml', 'ITModules.xml', 'OTGlobal.xml', 'OTModules.xml',
        'MuonGlobal.xml', 'MuonModules.xml'
    ]

    if args.align1[-4:] == '.xml':
        xmls_1 = [args.align1]
    else:
        xmls_1 = [
            i for i in [findPaths(xml, args.align1) for xml in xmls] if i
        ]
    if args.align1[-4:] == '.xml':
        xmls_2 = [args.align2]
    else:
        xmls_2 = [
            i for i in [findPaths(xml, args.align2) for xml in xmls] if i
        ]

    # print xmls_1

    assert len(xmls_1) == len(xmls_2)
    if len(xmls_1) < 1:
        raise IOError('Xml Files no founds, have you given the correct path?')

    cc = AlMon.CompareConstants()

    if args.file:
        cc.SetRanges(os.path.expanduser(os.path.expandvars(args.file)))

    cc.Compare(std_vector(xmls_1), std_vector(xmls_2))

    titles = {1: 'OK', 2: 'WARNINGS', 3: 'SEVERE'}

    exit_code = 0

    for level in [1, 2, 3]:
        if (not args.strict
                and level >= args.level) or (args.strict
                                             and level == args.level):
            numWarnings = cc.GetNumWarnings(level)
            if numWarnings:
                exit_code = 27
                print('*' * 70)
                print('{0}:\n'.format(titles[level]))
                print(cc.GetWarningMessages(level))
                print('*' * 70)

    sys.exit(exit_code)
