###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import TrackEventFitter
from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
from Configurables import (
    TrackEventFitter, TrackMasterFitter, TrackKalmanFilter,
    TrackProjectorSelector, TrackMasterExtrapolator, TrackSimpleExtraSelector,
    TrackDistanceExtraSelector, TrackHerabExtrapolator,
    SimplifiedMaterialLocator, DetailedMaterialLocator, MeasurementProvider,
    StateDetailedBetheBlochEnergyCorrectionTool, StateThickMSCorrectionTool)
from TrackSys.Configuration import TrackSys


def MyConfiguredForwardStraightLineEventFitter(Name, TracksInContainer,
                                               TracksOutContainer):
    eventfitter = TrackEventFitter(Name)
    eventfitter.TracksInContainer = TracksInContainer
    eventfitter.TracksOutContainer = TracksOutContainer
    fittername = Name + ".Fitter"
    eventfitter.addTool(
        MyConfiguredForwardStraightLineFitter(Name=fittername), name="Fitter")
    return eventfitter


def MyConfiguredForwardStraightLineFitter(Name):
    fitter = MyConfiguredForwardFitter(Name, FieldOff=True)
    fitter.ApplyMaterialCorrections = False
    return fitter


def MyConfiguredForwardFitter(Name,
                              FieldOff=None,
                              LiteClusters=True,
                              ForceUseDriftTime=True):
    fitter = MyConfiguredMasterFitter(
        Name,
        FieldOff=FieldOff,
        SimplifiedGeometry=True,
        LiteClusters=LiteClusters)
    fitter.NumberFitIterations = 1
    fitter.MaxNumberOutliers = 0
    fitter.AddDefaultReferenceNodes = False
    fitter.NodeFitter.ForceBiDirectionalFit = False
    fitter.FillExtraInfo = False
    fitter.UpdateReferenceInOutlierIterations = False
    return fitter


def MyConfiguredMasterFitter(Name,
                             FieldOff=None,
                             SimplifiedGeometry=None,
                             NoDriftTimes=None,
                             KalmanSmoother=None,
                             LiteClusters=False,
                             ApplyMaterialCorrections=None,
                             StateAtBeamLine=True,
                             MaxNumberOutliers=2,
                             FastOutlierIteration=False,
                             MSRossiAndGreisen=False):
    from GaudiKernel.Configurable import ConfigurableAlgTool

    # set the mutable default arguments
    if FieldOff is None: FieldOff = TrackSys().fieldOff()
    if SimplifiedGeometry is None:
        SimplifiedGeometry = TrackSys().simplifiedGeometry()
    if NoDriftTimes is None: NoDriftTimes = TrackSys().noDrifttimes()
    if KalmanSmoother is None: KalmanSmoother = TrackSys().kalmanSmoother()
    if ApplyMaterialCorrections is None:
        ApplyMaterialCorrections = not TrackSys().noMaterialCorrections()

    fitter = TrackMasterFitter(Name)

    # apply material corrections
    if not ApplyMaterialCorrections:
        fitter.ApplyMaterialCorrections = False
        fitter.Extrapolator.ApplyMultScattCorr = False
        fitter.Extrapolator.ApplyEnergyLossCorr = False
        fitter.Extrapolator.ApplyElectronEnergyLossCorr = False
    # Using 'RossiAndGreisen' will omit the log term in the multiple scattering calculation (which should give better pulls).
    if SimplifiedGeometry:
        fitter.addTool(SimplifiedMaterialLocator, name="MaterialLocator")
        fitter.Extrapolator.addTool(
            SimplifiedMaterialLocator, name="MaterialLocator")
    else:
        fitter.addTool(DetailedMaterialLocator, name="MaterialLocator")
        fitter.Extrapolator.addTool(
            DetailedMaterialLocator, name="MaterialLocator")

    fitter.MaterialLocator.addTool(StateThickMSCorrectionTool,
                                   "StateMSCorrectionTool")
    fitter.MaterialLocator.StateMSCorrectionTool.UseRossiAndGreisen = MSRossiAndGreisen
    fitter.Extrapolator.MaterialLocator.addTool(StateThickMSCorrectionTool,
                                                "StateMSCorrectionTool")
    fitter.Extrapolator.MaterialLocator.StateMSCorrectionTool.UseRossiAndGreisen = MSRossiAndGreisen
    # special settings for field-off
    if FieldOff:
        fitter.Extrapolator.addTool(
            TrackSimpleExtraSelector, name="ExtraSelector")
        fitter.Extrapolator.ExtraSelector.ExtrapolatorName = "TrackLinearExtrapolator"
        fitter.Extrapolator.ApplyEnergyLossCorr = False
        fitter.Extrapolator.ApplyElectronEnergyLossCorr = False
        fitter.ApplyEnergyLossCorr = False
        fitter.NodeFitter.DoF = 4

    # change the smoother
    if KalmanSmoother:
        fitter.NodeFitter.ForceBiDirectionalFit = False

    if FastOutlierIteration:
        fitter.UpdateReferenceInOutlierIterations = False

    return fitter
