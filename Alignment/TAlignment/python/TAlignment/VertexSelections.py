from __future__ import print_function

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


class VertexSelection(object):
    def __init__(self, Name, Location, Algorithm=None):
        self._name = Name
        self._location = Location
        self._algorithm = Algorithm

    def algorithm(self):
        return self._algorithm

    def location(self):
        return self._location

    def name(self):
        return self._name


def configuredPVSelection():
    # we assume that the PV reconstruction has already been performed.
    # (can always add later)
    from Configurables import TrackPVRefitter, VertexListRefiner, GaudiSequencer
    pvrefitter = TrackPVRefitter(
        "AlignPVRefitter",
        PVContainer="Rec/Vertex/Primary",
        OutPVContainer="Rec/Vertex/PrimaryRefitted",
        StandardGeometryTop="/dd/Structure/LHCb")
    pvselector = VertexListRefiner(
        "AlignPVSelector",
        InputLocation=pvrefitter.OutPVContainer,
        OutputLocation="Rec/Vertex/AlignPrimaryVertices",
        MaxChi2PerDoF=5,
        MinNumTracks=15,
        MinNumLongTracks=0)
    seq = GaudiSequencer("AlignPVSelSeq")
    seq.Members += [pvrefitter, pvselector]
    sel = VertexSelection(
        Name="DefaultPVSelection",
        Location="Rec/Vertex/AlignPrimaryVertices",
        Algorithm=seq)
    return sel


def configuredLoosePVSelection():
    # we assume that the PV reconstruction has already been performed.
    # (can always add later)
    from Configurables import TrackPVRefitter, VertexListRefiner, GaudiSequencer
    pvrefitter = TrackPVRefitter(
        "AlignPVRefitter",
        PVContainer="Rec/Vertex/Primary",
        OutPVContainer="Rec/Vertex/PrimaryRefitted",
        StandardGeometryTop="/dd/Structure/LHCb")
    pvselector = VertexListRefiner(
        "AlignPVSelector",
        InputLocation=pvrefitter.OutPVContainer,
        OutputLocation="Rec/Vertex/AlignPrimaryVertices",
        MaxChi2PerDoF=5,
        MinNumTracks=4,
        MinNumLongTracks=0)
    seq = GaudiSequencer("AlignPVSelSeq")
    seq.Members += [pvrefitter, pvselector]
    sel = VertexSelection(
        Name="DefaultPVSelection",
        Location="Rec/Vertex/AlignPrimaryVertices",
        Algorithm=seq)
    return sel


def configuredPVSelectionFromDST():
    print("I still need to implement this. "\
        "Use the PV Refitter. It can also refit the tracks")
    return None
