###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##################################################################
# Helper function to create a sequence to fit the tracks and run the hitadder
##################################################################
def configuredFitAndHitAdderSequence(Name, InputLocation, OutputLocation):
    from TrackFitter.ConfiguredFitters import ConfiguredEventFitter
    from Configurables import (TrackHitAdder, TrackContainerCopy,
                               TrackSelector, GaudiSequencer,
                               TrackStateInitAlg)
    # create the sequence
    if isinstance(Name, GaudiSequencer):
        seq = Sequence
        Name = seq.name()
        Name.replace('Sequence', '')
        Name.replace('Seq', '')
    else:
        seq = GaudiSequencer(Name + 'Seq')

    # I am lazy: use the DOD to get the decoded clusters
    #importOption( "$STDOPTS/DecodeRawEvent.py" )
    # now setup the fitters
    seq.Members += [
        TrackStateInitAlg(Name + 'FitInit', TrackLocation=InputLocation),
        ConfiguredEventFitter(
            Name + 'FitBeforeHitAdder', TracksInContainer=InputLocation),
        TrackHitAdder(Name + 'HitAdder', TrackLocation=InputLocation),
        ConfiguredEventFitter(
            Name + 'FitAfterHitAdder', TracksInContainer=InputLocation)
    ]
    tracksel = TrackContainerCopy(
        Name + 'CopyAndSelect',
        inputLocations=[InputLocation],
        outputLocation=OutputLocation,
        Selector=TrackSelector())
    # also apply a selection
    tracksel.Selector.MaxChi2Cut = 5
    tracksel.Selector.MaxChi2PerDoFMatch = 5
    tracksel.Selector.MaxChi2PerDoFVelo = 5
    tracksel.Selector.MaxChi2PerDoFDownstream = 5
    seq.Members.append(tracksel)
    return seq
