###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# @package TAlignment
#  Configurable for TAlignment package
#  @author Johan Blouw <johan.blouw@physi.uni-heidelberg.de>
#  @date   29/01/2009

from __future__ import print_function
from __future__ import absolute_import
from os import environ
from os.path import expandvars

from Gaudi.Configuration import *
import GaudiKernel.ProcessJobOptions
from Configurables import (LHCbConfigurableUser, LHCbApp, GaudiSequencer)
from TAlignment.SurveyConstraints import SurveyConstraints
from TAlignment.TrackSelections import GoodLongTracks
from DDDB.CheckDD4Hep import UseDD4Hep
# Workaround for ROOT-10769
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import cppyy


class WriteMultiAlignmentConditions:
    """Allows to write back conditions to XML"""
    xmlWriters = []

    def addXmlWriter(self, talign, subdet, condname, depths, online):
        if online:
            outputFile = talign.getProp(
                'OnlineAligWorkDir') + '/' + self.getProp(
                    'CondFilePrefix'
                ) + subdet + '/' + subdet + '/Alignment/' + condname + '.xml'
        else:
            subdetprefix = subdet if subdet not in ['TT'
                                                    ] else 'Online/' + subdet
            outputFile = talign.getProp(
                'CondFilePrefix'
            ) + 'Conditions/' + subdetprefix + '/Alignment/' + condname + '.xml'
        name = 'Write' + subdet + condname + 'ToXML'
        topElement = talign.getProp(subdet + 'TopLevelElement')
        precision = talign.getProp("Precision")
        print("XMLWriter : topElement=%s outputFile=%s precision=%d" %
              (topElement, outputFile, precision))
        self.xmlWriters.append((outputFile, topElement, precision, depths,
                                online))

    def addXmlWriters(self, talign, online=False):
        print(
            '================ Configuring XML writer for offline alignment ====================='
        )
        listOfCondToWrite = talign.getProp("WriteCondSubDetList")
        if 'VP' in listOfCondToWrite:
            self.addXmlWriter(talign, 'VP', 'Global', [0, 1], False)
            self.addXmlWriter(talign, 'VP', 'Modules', [2], False)
        if 'TT' in listOfCondToWrite:
            self.addXmlWriter(talign, 'TT', 'Global', [0, 1, 2], online)
            self.addXmlWriter(talign, 'TT', 'Modules', [3, 4], online)
        if 'FT' in listOfCondToWrite:
            self.addXmlWriter(talign, 'FT', 'FTSystem', [0, 1, 2, 3], False)
            self.addXmlWriter(talign, 'FT', 'Modules', [4], False)
            self.addXmlWriter(talign, 'FT', 'Mats', [5], False)
        if 'Muon' in listOfCondToWrite:
            self.addXmlWriter(talign, 'Muon', 'Global', [0, 1, 2], online)
            if online:
                self.addXmlWriter(talign, 'Muon', 'Modules', [2], online)
        if 'Ecal' in listOfCondToWrite:
            self.addXmlWriter(talign, 'Ecal', 'alignment', [], online)

    def xmlWriterAsString(self, outputFile, topElement, precision, depths,
                          onlineMode):
        return ':'.join([
            outputFile, topElement,
            str(precision), "/".join([str(d) for d in depths]),
            "1" if onlineMode else "0", "1"
        ])

    def xmlWritersAsStrings(self):
        return [
            self.xmlWriterAsString(outputFile, topElement, precision, depths,
                                   onlineMode) for outputFile, topElement,
            precision, depths, onlineMode in self.xmlWriters
        ]


class TAlignment(LHCbConfigurableUser):
    INFO = 3

    __used_configurables__ = []
    __queried_configurables__ = [SurveyConstraints]
    __version__ = "$Id: Configuration.py"
    __author__ = "Johan Blouw <Johan.Blouw@physi.uni-heidelberg.de>"

    __slots__ = {
        "Sequencer":
        GaudiSequencer(
            "TAlignmentSequencer")  # the sequencer to add algorithms to
        ,
        "TrackSelections":
        [GoodLongTracks()]  # input track selections for alignment
        ,
        "ParticleSelections": []  # input particles for alignment
        ,
        "PVSelection":
        None  # primary vertices for alignment
        ,
        "ElementsToAlign": []  # Elements to align
        ,
        "UseLocalFrame":
        True  # Use local frame?
        ,
        "NumIterations":
        1  # Number of iterations
        ,
        "TrackLocation":
        ""  # track container to be used for alignment
        ,
        "VertexLocation":
        ""  # Location of input vertex list
        ,
        "ParticleLocation":
        ""  # Location of input vertex list
        ,
        "UseCorrelations":
        True  # Correlations
        ,
        "Constraints": []  # Specifies 'exact' (lagrange) constraints
        ,
        "UseWeightedAverageConstraint":
        False  # Weighted average constraint
        ,
        "MinNumberOfHits":
        10  # Min number of hits per element
        ,
        "Chi2Outlier":
        10000  # Chi2 cut for outliers
        ,
        "UsePreconditioning":
        True  # Pre-conditioning
        ,
        "SolvTool":
        "EigenDiagSolvTool"  # Solver to use
        ,
        "EigenValueThreshold":
        -1  # Eigenvalue threshold for cutting out weak modes
        ,
        "WriteCondSubDetList": [
        ]  # List of sub-detectors for which to write out the conditions
        ,
        "CondFilePrefix":
        "xml/"  # Prefix for xml file names
        ,
        "VPTopLevelElement":
        "/dd/Structure/LHCb/BeforeMagnetRegion/VP"
        if not UseDD4Hep else "/world/BeforeMagnetRegion/VP",
        "TTTopLevelElement":
        "/dd/Structure/LHCb/BeforeMagnetRegion/TT",
        "FTTopLevelElement":
        "/dd/Structure/LHCb/AfterMagnetRegion/T/FT",
        "MuonTopLevelElement":
        "/dd/Structure/LHCb/DownstreamRegion/Muon",
        "EcalTopLevelElement":
        "/dd/Structure/LHCb/DownstreamRegion/Ecal",
        "Precision":
        10  # Set precision for conditions
        ,
        "OutputLevel":
        INFO  # Output level
        ,
        "LogFile":
        "alignlog.txt"  # log file for kalman type alignment
        ,
        "UpdateInFinalize":
        True,
        "OutputDataFile":
        "",
        "DatasetName":
        "Unknown",
        "OnlineMode":
        False,
        "OnlineAligWorkDir":
        "/group/online/AligWork/running",
        "RunList": [],
        "xmlWriters":
        None
    }

    def __apply_configuration__(self):
        print("******* calling ", self.name())
        mainseq = self.getProp("Sequencer")
        mainseq.getProperties()
        if mainseq.name() == "":
            mainseq = GaudiSequencer("AlignSequence")
        mainseq.MeasureTime = True
        self.sequencers()
        sys.stdout.flush()  # avoid mixing the output of python and Gaudi

    def getProp(self, name):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            return self.getDefaultProperties()[name]

    # set up the sequence that create the track selections
    def inputSeq(self, outputLevel=INFO):
        if not allConfigurables.get("AlignInputSeq"):
            if outputLevel == VERBOSE:
                print("VERBOSE: Filter Sequencer not defined! Defining!")

            inputSequencer = GaudiSequencer("AlignInputSeq")
            inputSequencer.MeasureTime = True
            trackselections = self.getProp("TrackSelections")
            if len(trackselections) > 0:
                trackInputSeq = GaudiSequencer(
                    "AlignTrackSelSeq", IgnoreFilterPassed=True)
                inputSequencer.Members.append(trackInputSeq)
                # add the algorithms for the track selections to the sequence.
                # also merge the tracks lists into one list
                from Configurables import TrackContainerCopy

                trackmerger = TrackContainerCopy("AlignTracks")
                trackmerger.inputLocations = []
                trackmerger.outputLocation = "Rec/Track/AlignTracks"
                self.setProp("TrackLocation", trackmerger.outputLocation)

                for i in trackselections:
                    alg = i.algorithm()
                    if alg: trackInputSeq.Members.append(alg)
                    trackmerger.inputLocations.append(i.location())
                trackInputSeq.Members.append(trackmerger)

            # add all particle selections
            if len(self.getProp("ParticleSelections")) > 0:
                particleInputSeq = GaudiSequencer(
                    "AlignParticleSelSeq", IgnoreFilterPassed=True)
                inputSequencer.Members.append(particleInputSeq)
                from Configurables import FilterDesktop
                particlemerger = FilterDesktop("AlignParticles", Code="ALL")
                particlemerger.Code = "ALL"
                particlemerger.CloneFilteredParticles = False
                for i in self.getProp("ParticleSelections"):
                    alg = i.algorithm()
                    if alg:
                        particleInputSeq.Members.append(alg)
                        print("adding particleinputsel to sequence: ",
                              i.name(), i.algorithm(), i.location())
                    particlemerger.Inputs.append(i.location())
                particleInputSeq.Members.append(particlemerger)
                self.setProp("ParticleLocation",
                             '/Event/Phys/AlignParticles/Particles')

            # add the PV selection
            if hasattr(self, "PVSelection"):
                inputSequencer.Members.append(
                    self.getProp("PVSelection").algorithm())
                self.setProp("VertexLocation",
                             self.getProp("PVSelection").location())

            return inputSequencer
        else:
            if outputLevel == VERBOSE:
                print("VERBOSE: AlignInputSeq already defined!")
            return allConfigurables.get("AlignInputSeq")

    # set up the monitoring sequence
    def createTrackMonitors(self, name, location):
        from Configurables import (TrackMonitor, TrackFitMatchMonitor)
        seq = [TrackMonitor(name + "TrackMonitor", TracksInContainer=location)]
        #if not self.getProp("Upgrade"):
        seq += [
            TrackFitMatchMonitor(
                name + "FitMatchMonitor", TrackContainer=location)
        ]
        #    ]
        return seq

    def monitorSeq(self):
        from Configurables import (TrackVertexMonitor,
                                   ParticleToTrackContainer)
        from Configurables import LoKi__VoidFilter as Filter
        monitorSeq = GaudiSequencer("AlignMonitorSeq", IgnoreFilterPassed=True)
        for i in self.getProp("TrackSelections"):
            if isinstance(i, str): i = TrackSelection(i)
            name = i.name()
            location = i.location()
            monitorSeq.Members += self.createTrackMonitors(name, location)
        for i in self.getProp("ParticleSelections"):
            location = i.location()
            name = i.name()
            pmonseq = GaudiSequencer("AlignMonitorSeq" + name)
            # add a filter on the alignment particles
            fltr = Filter(
                'Seq' + name + 'Output',
                Code=" 0 < CONTAINS ( '%s') " % location)
            pmonseq.Members += [fltr]
            pmonseq.Members += [
                ParticleToTrackContainer(
                    name + "ParticleToTrackForMonitoring",
                    ParticleLocation=location,
                    TrackLocation=location + "Tracks")
            ]
            pmonseq.Members += self.createTrackMonitors(
                name, location + "Tracks")
            monitorSeq.Members.append(pmonseq)

        if self.getProp("VertexLocation") != "":
            MyTrackVertexMonitor = TrackVertexMonitor(
                "AlignVertexMonitor",
                PVContainer=self.getProp("VertexLocation"))
            MyTrackVertexMonitor.NumTracksPV = 7
            monitorSeq.Members.append(MyTrackVertexMonitor)

        return monitorSeq

    def postMonitorSeq(self):
        from Configurables import (TrackMonitor, TrackVertexMonitor)
        monitorSeq = GaudiSequencer("AlignMonitorSeq")
        postMonitorSeq = GaudiSequencer("AlignPostMonitorSeq")
        for m in monitorSeq.Members:
            copy = m.clone(m.name() + "Post")
            postMonitorSeq.Members.append(copy)
        return postMonitorSeq

    def alignmentSeq(self, outputLevel=INFO):
        if not allConfigurables.get("AlignmentSeq"):
            if outputLevel == VERBOSE:
                print("VERBOSE: Alignment Sequencer not defined! Defining!")

            alignSequencer = GaudiSequencer("AlignmentAlgSeq")
            alignSequencer.MeasureTime = True

            from Configurables import (
                AlignAlgorithm, gslSVDsolver, CLHEPSolver, SparseSolver,
                EigenDiagSolvTool, GslDiagSolvTool, AlignUpdateTool)

            if not UseDD4Hep:
                alignAlg = AlignAlgorithm(
                    "AlignAlgorithm", DELHCbPath="/dd/Structure/LHCb")
            else:
                alignAlg = AlignAlgorithm(
                    "AlignAlgorithm",
                    DELHCbPath=
                    "/world/BeforeMagnetRegion/VP:DetElement-Info-IOV",
                    ODINLocation='/Event/DAQ/ODIN',
                    TracksLocation="Rec/Track/Best")
            alignAlg.OutputLevel = outputLevel
            alignAlg.NumberOfIterations = self.getProp("NumIterations")
            alignAlg.TracksLocation = self.getProp("TrackLocation")
            alignAlg.VertexLocation = self.getProp("VertexLocation")
            alignAlg.ParticleLocation = self.getProp("ParticleLocation")
            alignAlg.UseCorrelations = self.getProp("UseCorrelations")
            alignAlg.Chi2Outlier = self.getProp("Chi2Outlier")
            alignAlg.UpdateInFinalize = self.getProp("UpdateInFinalize")
            alignAlg.OutputDataFile = self.getProp("OutputDataFile")
            alignAlg.OnlineMode = self.getProp("OnlineMode")
            alignAlg.RunList = self.getProp("RunList")
            alignAlg.Elements = self.getProp("ElementsToAlign")
            alignAlg.UseLocalFrame = self.getProp("UseLocalFrame")

            #print "Error",alignAlg
            # and also the update tool is in the toolsvc
            updatetool = AlignUpdateTool("AlignUpdateTool")
            updatetool.MinNumberOfHits = self.getProp("MinNumberOfHits")
            updatetool.UsePreconditioning = self.getProp("UsePreconditioning")
            updatetool.LogFile = self.getProp("LogFile")
            updatetool.addTool(gslSVDsolver)
            updatetool.MatrixSolverTool = self.getProp("SolvTool")
            # for now we do it like this
            SurveyConstraints().configureTool(updatetool.SurveyConstraintTool)

            updatetool.Constraints = self.getProp("Constraints")
            updatetool.UseWeightedAverage = self.getProp(
                "UseWeightedAverageConstraint")

            # Setup XML writer, also a public tool
            self.xmlWriters = WriteMultiAlignmentConditions()
            self.xmlWriters.addXmlWriters(self, self.getProp("OnlineMode"))
            if self.getProp("OnlineMode"):
                updatetool.LogFile = self.getProp(
                    'OnlineAligWorkDir') + '/' + self.getProp('LogFile')
            print('=================== OnlineMode = ',
                  self.getProp("OnlineMode"))
            alignAlg.XmlWriters = self.xmlWriters.xmlWritersAsStrings()

            # and these too
            gslSVDsolver().EigenValueThreshold = self.getProp(
                "EigenValueThreshold")
            EigenDiagSolvTool().EigenValueThreshold = self.getProp(
                "EigenValueThreshold")
            GslDiagSolvTool().EigenValueThreshold = self.getProp(
                "EigenValueThreshold")

            alignSequencer.Members.append(alignAlg)

            return alignSequencer
        else:
            if outputLevel == VERBOSE:
                print("VERBOSE: Alignment Sequencer already defined!")
            return allConfigurables.get("AlignmentSeq")

    def sequencers(self):
        print("**** setting the main kalman aligment sequence!")
        ## The main sequence
        mainSeq = self.getProp("Sequencer")
        mainSeq.MeasureTime = True

        # Add the sequence that creates input to the alignment (e.g. trackselections)
        mainSeq.Members.append(self.inputSeq(self.getProp("OutputLevel")))
        # Add the accumulator
        mainSeq.Members.append(self.alignmentSeq(self.getProp("OutputLevel")))
        # Add the monitoring
        #if not self.getProp("OnlineMode"):
        mainSeq.Members.append(self.monitorSeq())
        if self.getProp("NumIterations") > 1:
            mainSeq.Members.append(self.postMonitorSeq())

    def xmlWritersAsString(self):
        if self.xmlWriters:
            return self.xmlWriters.xmlWritersAsString()
        return "[]"
