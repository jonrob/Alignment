/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "AlignKernel/AlEquations.h"
#include "AlignKernel/AlSymMat.h"
#include "AlignKernel/AlVec.h"
#include "TAlignment/AlParameters.h"
#include "TAlignment/AlignmentElement.h"

#include "Event/ChiSquare.h"

#include "GaudiKernel/IAlgTool.h"

namespace LHCb::Alignment {
  struct IAlignChisqConstraintTool : public extend_interfaces<IAlgTool> {
    DeclareInterfaceID( IAlignChisqConstraintTool, 2, 0 );

    virtual ChiSquare addConstraints( const Alignment::Elements& elements, AlVec& halfDChi2DAlpha,
                                      AlSymMat& halfD2Chi2DAlpha2, std::ostream& logmessage ) const = 0;
    virtual ChiSquare addConstraints( const Alignment::Elements& inputelements, Equations& equations,
                                      std::ostream& logmessage ) const                              = 0;
    virtual ChiSquare chiSquare( const Alignment::Element& element, bool activeonly = true ) const  = 0;
    // this returns the survey in the AlignmentFrame of the element, taking correlations into account.
    virtual const AlParameters* surveyParameters( const Alignment::Element& element ) const = 0;
  };
} // namespace LHCb::Alignment
