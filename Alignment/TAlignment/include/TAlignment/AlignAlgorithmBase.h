/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/SynchronizedValue.h"

#include "TAlignment/AlElementHistos.h"
#include "TAlignment/GetElementsToBeAligned.h"
#include "TAlignment/IVertexResidualTool.h"

#include "AlignKernel/AlEquations.h"
#include "AlignmentInterfaces/IAlignUpdateTool.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/ODIN.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "TrackInterfaces/ITrackProjectorSelector.h"

#include "Gaudi/Accumulators.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/IUpdateable.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/ToolHandle.h"

#include <memory>
#include <mutex>
#include <string>
#include <utility>
#include <vector>

//-----------------------------------------------------------------------------
// Implementation file for class : AlignAlgorithmBase
//
// Original author of AlignAlgorithm:
// 2007-03-05 : Jan Amoraal
//-----------------------------------------------------------------------------

namespace LHCb {
  class FitNode;
  namespace Al {
    class Equations;
    template <typename TNode>
    class Residuals;
  } // namespace Al
  namespace Pr::Tracks::Fit {
    struct Node;
  }
} // namespace LHCb

/** @class AlignAlgorithmBase AlignAlgorithmBase.h
 *
 *  @author Jan Amoraal
 *  @date   2007-03-05
 */

namespace LHCb::Alignment {

  class HistoUpdater {
  public:
    unsigned long m_Reference;
    IMonitorSvc*  m_MonSvc;
    void          setMonitorService( IMonitorSvc* ms ) { m_MonSvc = ms; }
    HistoUpdater() : m_Reference( 0 ), m_MonSvc( 0 ) {}
    unsigned long readReference( const std::string& refFileName ) {
      FILE* f = fopen( refFileName.c_str(), "r" );
      fscanf( f, "%lu", &m_Reference );
      fclose( f );
      printf( "[INFO]==================> Read Reference %ld from file %s\n", m_Reference, refFileName.c_str() );
      fflush( ::stdout );
      return m_Reference;
    }
    unsigned long getReference() { return m_Reference; }
    void          Update( int runnr, const std::string& refFileName ) {
      unsigned long ref = readReference( refFileName );
      if ( m_MonSvc != 0 ) {
        SmartIF<IUpdateableIF> aaa( m_MonSvc );
        if ( aaa.isValid() ) aaa->update( runnr * 100 + ref ).ignore();
      }
    }
  };

  // helper using to design the base detectorElementClass, as it's different
  // for DDDB and DD4hep
#ifdef USE_DD4HEP
  using GenericDetElem = LHCb::Detector::DeIOV;
#else
  using GenericDetElem = ::DetectorElementPlus;
#endif

  /**
   * Algorithm handling alignment
   *
   * Note that although it looks fully functional, the alignment can change tracks in place, so it shouldn't be
   * considered to be functional at this point Also, there are a bunch of handles used on top of the inputs of
   * operator() : odin, tracks, vertices and particles. They exist because these inputs may be missing. So declaring
   * them as parameter of the operator() would thus break backward compatibility This should anyway be revisited at a
   * later stage -> FIXME
   */
  class AlignAlgorithmBase : public Gaudi::Functional::Consumer<void( const EventContext&, const GenericDetElem& ),
                                                                LHCb::DetDesc::usesConditions<GenericDetElem>> {
  public:
    /// Some handy typedefs
    typedef std::vector<double>   AlignConstants;
    typedef std::vector<FitNode*> Nodes;
    typedef Gaudi::Matrix1x6      Derivatives;
    typedef Gaudi::Matrix6x6      HMatrix;
    enum { CanonicalConstraintOff = 0, CanonicalConstraintOn, CanonicalConstraintAuto } CanonicalConstraintStrategy;

    AlignAlgorithmBase( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;
    StatusCode finalize() override;
    // Note that operator() has several other "inputs" via external Handles, see comment at the top of the class
    // void operator()( const EventContext& evtContext, const GenericDetElem& lhcb ) const override;
    StatusCode stop() override;
    StatusCode start() override;
    long long  initialTime() const { return m_initialTime; }
    void       updateGeometry( size_t iter, size_t maxiter );
    void       clearEquationsAndReloadGeometry();

#ifdef USE_DD4HEP
    void writeAlignmentConditions( std::string );
    void loadAlignmentConditions( std::string in_dir );
#else
    void writeAlignmentConditions() const;
#endif
    void reset();

  protected:
    void resetHistos() const;

    typedef std::vector<RecVertex>    VertexContainer;
    typedef std::vector<const Track*> TrackContainer;
    void                              selectVertexTracks( const RecVertex& vertex, const TrackContainer& tracks,
                                                          TrackContainer& tracksinvertex ) const;
    void                              removeVertexTracks( const RecVertex& vertex, TrackContainer& tracks ) const;
    void                              removeParticleTracks( const Particle& p, TrackContainer& tracks ) const;
    void splitVertex( const RecVertex& vertex, const TrackContainer& tracks, VertexContainer& splitvertices ) const;
    RecVertex* cloneVertex( const RecVertex& vertex, const TrackContainer& selectedtracks ) const;

    void update( size_t iter, size_t maxiter );
    void update();

    DataObjectReadHandle<ODIN>             m_odin{this, "ODINLocation", ODINLocation::Default};
    DataObjectReadHandle<Track::Range>     m_tracks{this, "TracksLocation", TrackLocation::Default};
    DataObjectReadHandle<RecVertex::Range> m_vertices{this, "VertexLocation", "", "Vertices for alignment"};
    DataObjectReadHandle<Particle::Range>  m_particles{this, "ParticleLocation", "",
                                                      "particles with mass constraint for alignment"};
    // properties
    Gaudi::Property<size_t>      m_nIterations{this, "NumberOfIterations", 10u,
                                          "Number of iterations used on histogram axes"};
    Gaudi::Property<std::string> m_histoPartitionName{this, "PartitionName", "LHCbA"};
    Gaudi::Property<std::string> m_histoRefFileName{this, "ReferenceFile", ""};

    ToolHandle<ITrackProjectorSelector>   m_projSelector{this, "ProjectorSelector", "TrackProjectorSelector"};
    PublicToolHandle<IVertexResidualTool> m_vertexresidualtool{this, "VertexResidualTool", "VertexResidualTool"};
    PublicToolHandle<IAlignUpdateTool>    m_updatetool{this, "UpdateTool", "AlignUpdateTool"};
    Gaudi::Property<bool>                 m_fillHistos{this, "FillHistos", false};

    Gaudi::Property<bool>        m_correlation{this, "UseCorrelations", true,
                                        "Do we take into account correlations between residuals?"};
    Gaudi::Property<bool>        m_updateInFinalize{this, "UpdateInFinalize", false, "Call update from finalize"};
    Gaudi::Property<double>      m_chi2Outlier{this, "Chi2Outlier", 1e4, "Chi2 threshold for outlier rejection"};
    Gaudi::Property<size_t>      m_minTracksPerVertex{this, "MinTracksPerVertex", 2};
    Gaudi::Property<size_t>      m_maxTracksPerVertex{this, "MaxTracksPerVertex", 8};
    Gaudi::Property<std::string> m_outputDataFileName{this, "OutputDataFile"};
    Gaudi::Property<std::vector<std::string>> m_inputDataFileNames{this, "InputDataFiles"};
    Gaudi::Property<long long>                m_initialTime{this, "ForcedInitialTime", 0,
                                             "initial time to be used for alignment geometry. Defaults to epoch"};
    Gaudi::Property<bool>                     m_Online{this, "OnlineMode", false};
    Gaudi::Property<std::vector<std::string>> m_RunList{this, "RunList"};

    Gaudi::Property<bool> m_useLocalFrame{this, "UseLocalFrame", true, "Use local frame as alignmentframe"};
    Gaudi::Property<std::vector<std::string>> m_elemsToBeAligned{this, "Elements", {}, "Path to elements"};

    Gaudi::Property<std::vector<XmlWriter>> m_xmlWriters{
        this, "XmlWriters", {}, "Definitions of the XmlWriters handling the output"};

    mutable Gaudi::Accumulators::Counter<>    m_iteration{this, "Iteration"};
    mutable Gaudi::Accumulators::Counter<>    m_nTracks{this, "Number of tracks used"};
    mutable Gaudi::Accumulators::Counter<>    m_nTracksRejected{this, "Number of rejected tracks"};
    mutable Gaudi::Accumulators::Counter<>    m_covFailure{this, "Number of covariance calculation failures"};
    mutable Gaudi::Accumulators::Histogram<1> m_numTracksPerPVH1{
        this, "NumTracksPerPV", "NumTracksPerPV", {31, -0.5, 30.5}};

    mutable bool         m_resetHistos{false}; // reset histos on next event processing
    mutable HistoUpdater m_HistoUpdater;

    struct Data {
      Equations                               equations;       ///< Equations to solve
      std::unique_ptr<GetElementsToBeAligned> elementProvider; /// The GetElementsToBeAligned instance holding alignment
                                                               /// data
    };

    mutable LHCb::cxx::SynchronizedValue<Data, std::shared_mutex> m_data;

    /// initializes m_elementProvider. Take care, this method is not thread safe !
    void initializeGetElementsToBeAligned( const GenericDetElem& lhcb ) const;

    /// Monitoring
    // @todo: Move this to a monitoring tool
    mutable std::vector<std::unique_ptr<AlElementHistos>> m_elemHistos;
  };
} // namespace LHCb::Alignment
