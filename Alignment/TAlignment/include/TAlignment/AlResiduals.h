/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "TAlignment/AlignmentElement.h"
// from TrackEvent
#include "Event/FitNode.h"
#include "Event/Track.h"

#include "Kernel/LHCbID.h"
#include <vector>

// forward declarations
namespace LHCb {
  class FitNode;
  class KalmanFitResult;
  struct PrKalmanFitResult;
  namespace Pr::Tracks::Fit {
    struct Node;
  }
} // namespace LHCb

namespace LHCb::Alignment {
  class Element;

  struct TrackResidualCreator;

  template <typename TNode>
  inline const Gaudi::TrackProjectionMatrix1D& projectionMatrix( const TNode* node ) {
    // const Gaudi::TrackProjectionMatrix1D& Hk;
    if constexpr ( std::is_same<TNode, LHCb::Pr::Tracks::Fit::Node>::value ) { return node->projectionMatrix(); }
    if constexpr ( std::is_same<TNode, LHCb::FitNode>::value ) {
      return node->template visit_r<const Gaudi::TrackProjectionMatrix1D&>(
          []( const LHCb::FitNode::DimInfos<LHCb::Enum::nDim::Type::one>& d ) -> const Gaudi::TrackProjectionMatrix1D& {
            return d.projectionMatrix();
          },
          []( ... ) -> const Gaudi::TrackProjectionMatrix1D& {
            throw std::logic_error( "Alignment not implemented for 2D measurements" );
          } );
    }
  }

  class Residual1D {
  public:
    using TrackProjectionMatrix = Gaudi::TrackProjectionMatrix1D;
    using ResidualVertexMatrix  = ROOT::Math::SMatrix<double, 1, 3>;
    using AlignDerivativeMatrix = Gaudi::Matrix1x6;

    // templated constructor
    template <typename TNode>
    Residual1D( const TNode& n, const Element& e );
    const auto&        node() const { return m_node; }
    const Element&     element() const { return *m_element; }
    double             r() const { return m_r; }
    double             V() const { return m_V; }
    double             HCH() const { return m_HCH; }
    double             R() const { return V() - HCH(); }
    double             chi2() const { return m_r * m_r / R(); }
    void               setHCH( double hch ) { m_HCH = hch; }
    void               setR( double r ) { m_r = r; }
    const auto&        H() const { return m_H; }
    const LHCb::State& state() const { return m_state; }

    // TrackProjectionMatrix& residualStateCov() { return m_residualStateCov; }
    const TrackProjectionMatrix& residualStateCov() const { return m_residualStateCov; }
    void                         setResidualStateCov( const TrackProjectionMatrix& c ) { m_residualStateCov = c; }

    // ResidualVertexMatrix& residualVertexCov() { return m_residualVertexCov; }
    const ResidualVertexMatrix& residualVertexCov() const { return m_residualVertexCov; }
    void                        setResidualVertexCov( const ResidualVertexMatrix& c ) { m_residualVertexCov = c; }

    const auto& derivative() const { return m_derivative; }
    void        setDerivative( const AlignDerivativeMatrix& c ) { m_derivative = c; }

  private:
    std::variant<const LHCb::FitNode*, const Pr::Tracks::Fit::Node*> m_node;
    // const TNode*          m_node;
    const Element*              m_element{0};
    double                      m_r{0};
    double                      m_V{1};
    double                      m_HCH{1};
    const LHCb::State           m_state;
    const TrackProjectionMatrix m_H;
    TrackProjectionMatrix       m_residualStateCov;  // correlation between residual and reference state
    ResidualVertexMatrix        m_residualVertexCov; // correlation between residual and vertex
    AlignDerivativeMatrix       m_derivative;
  };

  template <typename TNode>
  Residual1D::Residual1D( const TNode& n, const Element& e )
      : m_node( &n )
      , m_element{&e}
      , m_r{n.residual()}
      , m_V{n.errMeasure2()}
      , m_HCH{n.errMeasure2() - n.errResidual2()}
      , m_state{smoothedState( n )}
      , m_H{projectionMatrix( &n )} {}

  struct CovarianceElement {
    CovarianceElement( size_t r, size_t c, double v ) : row( r ), col( c ), val( v ) {}
    size_t row;
    size_t col;
    double val;
    double operator*( double x ) const { return val * x; }
    double operator+=( double x ) { return val += x; }
  };

  class Residuals {
  public:
    typedef std::vector<Residual1D>        ResidualContainer;
    typedef std::vector<CovarianceElement> CovarianceContainer;

    Residuals( double chisq, int ndof, size_t numexthits = 0 );

    size_t                     size() const { return m_residuals.size(); }
    double                     chi2() const { return m_chi2; }
    int                        nDoF() const { return m_nDoF; }
    size_t                     nExternalHits() const { return m_nExternalHits; }
    const CovarianceContainer& HCHElements() const { return m_HCHElements; }
    const CovarianceContainer& hch() const { return m_HCHElements; }
    const ResidualContainer&   residuals() const { return m_residuals; }
    const Residual1D&          residual( size_t i ) const { return m_residuals[i]; }
    bool                       testHCH( std::string& message ) const;
    size_t                     nAlignables() const;

  private:
    friend struct TrackResidualCreator;
    friend class TrackResidualHelper;
    friend class VertexResidualTool;
    ResidualContainer   m_residuals;
    CovarianceContainer m_HCHElements; // off-diagonal elements of matrix HCH
    double              m_chi2;
    int                 m_nDoF;
    size_t              m_nExternalHits;
  };

  inline Residuals::Residuals( double chi2, int ndof, size_t numexthits )
      : m_chi2( chi2 ), m_nDoF( ndof ), m_nExternalHits( numexthits ) {}

  inline size_t Residuals::nAlignables() const {
    std::set<size_t> alignables;
    for ( auto inode = m_residuals.begin(); inode != m_residuals.end(); ++inode )
      alignables.insert( inode->element().index() );
    return alignables.size();
  }

  inline bool Residuals::testHCH( std::string& message ) const {

    // Let's check the result. Keep track of the roots.
    std::stringstream   warningmsg;
    const double        tol = 1e-3;
    std::vector<double> HCHdiagroots;
    std::vector<double> Rdiagroots;
    bool                error( false );
    for ( size_t irow = 0; irow < size() && !error; ++irow ) {
      double HCH = m_residuals[irow].HCH();
      double V   = m_residuals[irow].V();
      error      = !( 0 <= HCH );
      if ( error ) warningmsg << "found negative element on diagonal: " << HCH << std::endl;
      error = !( HCH < V );
      if ( error ) { warningmsg << "found too large element on diagonal: " << HCH / V << std::endl; }
      HCHdiagroots.push_back( std::sqrt( HCH ) );
      Rdiagroots.push_back( std::sqrt( V - HCH ) );
    }
    bool offdiagerror( false );
    for ( Residuals::CovarianceContainer::const_iterator it = m_HCHElements.begin();
          it != m_HCHElements.end() && !error; ++it ) {
      size_t irow       = it->row;
      size_t icol       = it->col;
      double c          = it->val / ( HCHdiagroots[irow] * HCHdiagroots[icol] );
      bool   thiserror  = !( -1 - tol < c && c < 1 + tol );
      double d          = it->val / ( Rdiagroots[irow] * Rdiagroots[icol] );
      bool   thisderror = !( -1 - tol < d && d < 1 + tol );
      if ( thiserror || thisderror ) {
        warningmsg << "found bad element on offdiagonal: " << irow << " " << icol << " " << c << " " << d << std::endl;
      }
      offdiagerror |= thiserror || thisderror;
    }
    error = error || offdiagerror;
    if ( error ) message = warningmsg.str();
    return error;
  }

  class TrackResiduals : public Residuals {
  public:
    // typedefs, enums
    typedef std::vector<Gaudi::TrackProjectionMatrix1D> ProjectionMatrix;

    /// Standard constructor
    TrackResiduals( const LHCb::Track& track, const LHCb::State& state )
        : Residuals{track.chi2(), track.nDoF()}, m_state{state}, m_track{&track} {}
    const LHCb::State& state() const { return m_state; }
    const LHCb::Track& track() const { return *m_track; }

  private:
    friend struct TrackResidualCreator;
    LHCb::State        m_state; // most upstream state
    const LHCb::Track* m_track;
  };

  class MultiTrackResiduals : public Residuals {
  public:
    /// Standard constructor
    MultiTrackResiduals( double chi2, int ndof, size_t numexthits, std::vector<const TrackResiduals*>& tracks,
                         double vertexchi2, int vertexndof )
        : Residuals( chi2, ndof, numexthits )
        , m_tracks( tracks )
        , m_vertexchi2( vertexchi2 )
        , m_vertexndof( vertexndof ) {}
    ~MultiTrackResiduals(){}; ///< Destructor
    double vertexChi2() const { return m_vertexchi2; }
    int    vertexNDoF() const { return m_vertexndof; }
    size_t numTracks() const { return m_tracks.size(); }

  private:
    friend class VertexResidualTool;
    std::vector<const TrackResiduals*> m_tracks;
    double                             m_vertexchi2;
    int                                m_vertexndof;
  };

} // namespace LHCb::Alignment
