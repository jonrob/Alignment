/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "AlignKernel/AlEquations.h"
#include "TAlignment/AlignmentElement.h"
#include "TAlignment/XmlWriter.h"

#include "Kernel/LHCbID.h"
#include "Kernel/STLExtensions.h"

#ifdef USE_DD4HEP
#  include <DD4hep/GrammarUnparsed.h>
#endif
#include "VPDet/DeVP.h"

#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SmartIF.h"

#include "boost/regex.hpp"
#include "boost/tokenizer.hpp"

#include <list>
#include <map>
#include <string>
#include <utility>
#include <vector>

/** @class GetElementsToBeAligned GetElementsToBeAligned.h
 *
 *
 *  @author Jan Amoraal
 *  @date   2007-10-08
 */

namespace LHCb {
  class FitNode;
  namespace Pr::Tracks::Fit {
    struct Node;
  }
} // namespace LHCb

namespace LHCb::Alignment {

  class GetElementsToBeAligned {
  public:
    GetElementsToBeAligned( LHCb::span<const std::string> elemsToBeAligned, const DetectorElement& topOfGeometry,
                            bool useLocalFrame, MsgStream& warningStream, SmartIF<IDataProviderSvc>& detSvc );
    GetElementsToBeAligned( const GetElementsToBeAligned& ) = delete;
    GetElementsToBeAligned( GetElementsToBeAligned&& )      = default;
    GetElementsToBeAligned& operator=( const GetElementsToBeAligned& ) = delete;
    GetElementsToBeAligned& operator=( GetElementsToBeAligned&& ) = default;
    ~GetElementsToBeAligned();

    // Return pair of begin iter and end iter
    const Elements& elements() const { return m_elements; }

    // Return method that finds an alignment element for a given DetectorElement
    const Element* findElement( const DetectorElement& de ) const;

    // Return method that finds an alignment element for a given Measuerment
    const Element* findElement( const LHCb::FitNode& node ) const;
    const Element* findElement( const LHCb::Pr::Tracks::Fit::Node& node ) const;
    const Element* findElement( const LHCb::LHCbID id ) const;

    // Find the list of elements corresponding to a path (which can be a regular expression)
    Elements findElements( const std::string& path ) const;

    // initialize the alignment frames now. if time=0, it will use the
    // current time.
    void initAlignmentFrame();

    // initialize an Equations object
    void initEquations( Equations& ) const;

    /// Write alignment conditions to file
    void writeAlignmentConditions( const std::vector<XmlWriter>& xmlWriters, MsgStream& warningStream ) const;

    std::map<std::string, double> getAlignmentConstants() const;
#ifdef USE_DD4HEP
    size_t iov() const { return m_topOfGeometry.iov(); }
#else
    size_t iov() const { return 0; }
#endif
    //
    bool isInitialized() const { return m_isInitialized; }
    void resetInitialized() { m_isInitialized = false; }

  private:
    enum e_DoFs { Tx, Ty, Tz, Rx, Ry, Rz };

    void            writeAlignmentConditions( const XmlWriter& writer, MsgStream& warningStream ) const;
    DetectorElement getDet( std::string const& path ) const;

  private:
    SmartIF<IDataProviderSvc>                 m_detSvc{nullptr};
    DetectorElement                           m_topOfGeometry; /// Current top of geometry
    Elements                                  m_elements;      /// Flat vector of alignment elements
    std::map<DetectorElement, const Element*> m_elementMap;    /// Map of detector elements to alignable
    std::array<const Element*, 208>           m_vpidmap{};     // map from vp sensor id to alignable
    std::map<int, const Element*> m_utidmap; // map from ut sensor id to alignable FIXME: invent new unique UT ID, then
                                             // replace by array
    std::array<const Element*, 258> m_ftidmap{};   // map from ft module id to alignable
    std::array<const Element*, 192> m_muonidmap{}; // map from muon chamber id to alignable
    bool                            m_isInitialized{false};
  };

} // namespace LHCb::Alignment
