/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cstddef>

#include "Gaudi/Accumulators/Histogram.h"
#include "TAlignment/AlignmentElement.h"

// FIXME: temporarily disable a non-understood warning on gcc11
#ifndef __clang__
#  pragma GCC diagnostic ignored "-Wsubobject-linkage"
#endif

namespace LHCb::Alignment {

  struct AlElementHistos {
    using Histogram2D = Gaudi::Accumulators::Histogram<2>;
    using Histogram1D = Gaudi::Accumulators::Histogram<1>;

    std::string m_name;
    size_t      m_index;
    std::string m_dir;
    double      m_resmax;
    Histogram2D m_resHisto;
    Histogram2D m_unbiasedResHisto;
    Histogram2D m_pullHisto;
    Histogram1D m_nHitsHisto;
    Histogram2D m_autoCorrHisto;
    Histogram1D m_residualPullHistos[6];

    template <typename OWNER>
    AlElementHistos( const OWNER* owner, const Alignment::Element& elem, unsigned int numiter )
        : m_name{elem.name()}
        , m_index{elem.index()}
        , m_dir{"element" + std::to_string( elem.index() ) + "/"}
        , m_resmax{elem.histoResidualMax()}
        , m_resHisto{owner,
                     m_dir + std::to_string( 1000u ),
                     "Residual vs iteration for " + m_name,
                     {numiter, -0.5, numiter - 0.5},
                     {100, -m_resmax, +m_resmax}}
        , m_unbiasedResHisto{owner,
                             m_dir + std::to_string( 1001u ),
                             "Unbiased Residual vs iteration for " + m_name,
                             {numiter, -0.5, numiter - 0.5},
                             {100, -m_resmax, +m_resmax}}
        , m_pullHisto{owner,
                      m_dir + std::to_string( 2000u ),
                      "Pull vs iteration for " + m_name,
                      {numiter, -0.5, numiter - 0.5},
                      {100, -5.0, 5.0}}
        , m_nHitsHisto{owner,
                       m_dir + std::to_string( 3000u ),
                       "Number of hits vs iteration for " + m_name,
                       {numiter, -0.5, numiter - 0.5}}
        , m_autoCorrHisto{owner,
                          m_dir + std::to_string( 4000u ),
                          "hit residual scale factor in " + m_name + "  vs iteration ",
                          {numiter, -0.5, numiter - 0.5},
                          {100, -1, 1}}
        , m_residualPullHistos{{owner, m_dir + "ResPullTx", "Residual pull Tx", {50, -5.0, 5.0}},
                               {owner, m_dir + "ResPullTy", "Residual pull Ty", {50, -5.0, 5.0}},
                               {owner, m_dir + "ResPullTz", "Residual pull Tz", {50, -5.0, 5.0}},
                               {owner, m_dir + "ResPullRx", "Residual pull Rx", {50, -5.0, 5.0}},
                               {owner, m_dir + "ResPullRy", "Residual pull Ry", {50, -5.0, 5.0}},
                               {owner, m_dir + "ResPullRz", "Residual pull Rz", {50, -5.0, 5.0}}} {}

    void reset() {
      // only reset histograms that don't go by 'iteration'
      for ( size_t ipar = 0; ipar < 6; ++ipar ) m_residualPullHistos[ipar].reset();
    }
  };

} // namespace LHCb::Alignment
