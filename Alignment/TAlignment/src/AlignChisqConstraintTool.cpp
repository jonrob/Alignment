/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TAlignment/AlignmentElement.h"
#include "TAlignment/IAlignChisqConstraintTool.h"

#include "DetDesc/3DTransformationFunctions.h"
#include "DetDesc/AlignmentCondition.h"
#include "DetDesc/IGeometryInfo.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include <boost/assign/list_of.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>

#include <fstream>
#include <string>

namespace LHCb::Alignment {
  struct ConfiguredSurveyData {
    ConfiguredSurveyData() {}
    std::string    name;
    Gaudi::Vector6 par;
    Gaudi::Vector6 err;
    double         pivot[3] = {0, 0, 0};
  };

  struct XmlSurveyData {
    XmlSurveyData() { pivot[0] = pivot[1] = pivot[2] = 0; }
    std::string    name;
    Gaudi::Vector6 par;
    double         pivot[3];
  };

  struct NamedXmlUncertainty {
    std::string name;
    double      err[6]   = {1, 1, 1, 1, 1, 1};
    double      pivot[3] = {0, 0, 0};
  };

  class AlignChisqConstraintTool : public extends<GaudiTool, IAlignChisqConstraintTool> {
  public:
    using extends::extends;
    StatusCode          initialize() override;
    LHCb::ChiSquare     addConstraints( const Elements& elements, AlVec& halfDChi2DAlpha, AlSymMat& halfD2Chi2DAlpha2,
                                        std::ostream& stream ) const override;
    LHCb::ChiSquare     addConstraints( const Elements& inputelements, Equations& equations,
                                        std::ostream& logmessage ) const override;
    LHCb::ChiSquare     chiSquare( const Element& element, bool activeonly ) const override;
    const AlParameters* surveyParameters( const Element& element ) const override;
    LHCb::ChiSquare     addElementJoints( const Elements& inputelements, Equations& equations,
                                          std::ostream& logmessage ) const;

  private:
    StatusCode                 parseXmlFile( const std::string& filename );
    StatusCode                 parseXmlUncertainties( const std::string& patterm );
    StatusCode                 parseElement( const std::string& element );
    const NamedXmlUncertainty* findXmlUncertainty( const std::string& name ) const;

  private:
    Gaudi::Property<std::vector<std::string>>      m_constraintNames{this, "Constraints"};
    Gaudi::Property<std::vector<std::string>>      m_xmlFiles{this, "XmlFiles"};
    Gaudi::Property<std::vector<std::string>>      m_xmlUncertainties{this, "XmlUncertainties"};
    Gaudi::Property<std::vector<std::string>>      m_joints{this, "ElementJoints"};
    typedef std::map<std::string, XmlSurveyData>   XmlData;
    XmlData                                        m_xmldata;
    typedef std::vector<ConfiguredSurveyData>      ConstraintData;
    ConstraintData                                 m_configuredSurvey;
    std::vector<NamedXmlUncertainty>               m_xmlUncertaintyMap;
    typedef std::map<const Element*, AlParameters> SurveyParameters;
    mutable SurveyParameters                       m_cachedSurvey;
  };

  DECLARE_COMPONENT_WITH_ID( AlignChisqConstraintTool, "AlignChisqConstraintTool" )

  StatusCode AlignChisqConstraintTool::initialize() {
    StatusCode sc = GaudiTool::initialize();
    for ( std::vector<std::string>::const_iterator ifile = m_constraintNames.begin();
          ifile != m_constraintNames.end() && sc.isSuccess(); ++ifile )
      sc = parseElement( *ifile );

    for ( std::vector<std::string>::const_iterator ifile = m_xmlFiles.begin();
          ifile != m_xmlFiles.end() && sc.isSuccess(); ++ifile )
      sc = parseXmlFile( *ifile );

    for ( std::vector<std::string>::const_iterator ipattern = m_xmlUncertainties.begin();
          ipattern != m_xmlUncertainties.end() && sc.isSuccess(); ++ipattern ) {
      sc = parseXmlUncertainties( *ipattern );
      if ( !sc.isSuccess() ) error() << "Cannot parse XML uncertainty pattern \'" << *ipattern << "\'" << endmsg;
    }

    info() << "Number of entries in xml data: " << m_xmldata.size() << endmsg;
    info() << "Number of entries specified by job-option: " << m_configuredSurvey.size() << endmsg;

    return sc;
  }

  namespace {
    // collection of static functions used in below

    std::string assembleConditionLine( std::ifstream& file ) {
      // creates a single line for a condition that may span several lines
      bool        conditionopen = false;
      std::string rc;
      while ( file && ( conditionopen || rc.empty() ) ) {
        char line[2048];
        file.getline( line, 2048 );
        std::string linestr( line );
        size_t      ipos( 0 ), jpos( 0 );
        try {
          if ( ( ipos = linestr.find( "<condition", 0 ) ) != std::string::npos ) {
            conditionopen = true;
          } else {
            ipos = 0;
          }
          if ( ( jpos = linestr.find( "/condition>", 0 ) ) != std::string::npos ) {
            conditionopen = false;
            rc += linestr.substr( ipos, jpos - ipos + 11 );
          } else if ( conditionopen )
            rc += linestr.substr( ipos, linestr.size() - ipos );
        } catch ( const std::exception& e ) {
          std::cout << "problem parsing line: " << linestr << std::endl;
          throw e;
        }
      }
      return rc;
    }

    std::string removechars( const std::string& s, const std::string& chars ) {
      std::string rc;
      for ( std::string::const_iterator it = s.begin(); it != s.end(); ++it )
        if ( std::find( chars.begin(), chars.end(), *it ) == chars.end() ) rc.push_back( *it );
      return rc;
    }

    std::vector<std::string> tokenize( const std::string& s, const char* separator ) {
      typedef boost::char_separator<char> Separator;
      typedef boost::tokenizer<Separator> Tokenizer;
      Tokenizer                           split( s, Separator( separator ) );
      std::vector<std::string>            rc;
      rc.insert( rc.end(), split.begin(), split.end() );
      return rc;
    }

    double extractdouble( const std::string& str ) {
      std::string input = removechars( str, " \t" );
      double      rc    = 1;
      size_t      ipos;
      if ( ( ipos = input.find( "*mrad" ) ) != std::string::npos ) {
        rc = 0.001;
        input.erase( ipos, 5 );
      } else if ( ( ipos = input.find( "*mm" ) ) != std::string::npos ) {
        input.erase( ipos, 3 );
      }
      try {
        rc *= boost::lexical_cast<double>( input );
      } catch ( const std::exception& e ) {
        std::cout << "error parsing input string: \'" << str << "\'" << std::endl;
        throw e;
      }
      return rc;
    }

    inline bool match( const std::string& name, const std::string& pattern ) {
      boost::regex ex;
      bool         rc = false;
      try {
        ex.assign( pattern, boost::regex_constants::icase );
        rc = boost::regex_match( name, ex );
      } catch ( ... ) { std::cout << "Problem with pattern: " << pattern << std::endl; }
      return rc;
    }
  } // namespace

  StatusCode AlignChisqConstraintTool::parseXmlFile( const std::string& filename ) {
    // I tried to use the standard xml parser, but single xml files
    // are not self-contaiend, so that will never work. Terefore, I
    // have written soemthing very stupid that should work for xml
    // files with alignment conditions.
    std::ifstream ifs( filename.c_str() );
    if ( !ifs.is_open() ) {
      error() << "Cannot open file: " << filename << endmsg;
      return StatusCode::FAILURE;
    }

    while ( ifs ) {
      std::string linestr = assembleConditionLine( ifs );
      if ( !linestr.empty() ) {
        size_t ipos( 0 ), jpos( 0 );
        ipos                      = linestr.find( "name", 0 );
        ipos                      = linestr.find_first_of( "\"", ipos + 1 );
        jpos                      = linestr.find_first_of( "\"", ipos + 1 );
        std::string conditionname = linestr.substr( ipos + 1, jpos - ipos - 1 );

        ipos                             = linestr.find( "dPosXYZ", jpos + 1 );
        ipos                             = linestr.find_first_of( ">", ipos );
        jpos                             = linestr.find_first_of( "<", ipos + 1 );
        std::vector<std::string> numbers = tokenize( linestr.substr( ipos + 1, jpos - ipos - 1 ), " " );
        assert( numbers.size() == 3 );
        std::vector<double> pospars( 3, 0 );
        for ( size_t i = 0; i < 3; ++i ) pospars[i] = extractdouble( numbers[i] );

        // std::cout << "position string: "
        //<< std::endl << numbers << std::endl ;
        // sscanf( numbers.c_str(),"%g %g %g", &dx,&dy,&dz) ;

        ipos    = linestr.find( "dRotXYZ", jpos + 1 );
        ipos    = linestr.find_first_of( ">", ipos );
        jpos    = linestr.find_first_of( "<", ipos + 1 );
        numbers = tokenize( linestr.substr( ipos + 1, jpos - ipos - 1 ), " " );
        assert( numbers.size() == 3 );
        std::vector<double> rotpars( 3, 0 );
        for ( size_t i = 0; i < 3; ++i ) rotpars[i] = extractdouble( numbers[i] );

        ipos = linestr.find( "pivotXYZ", jpos + 1 );
        std::vector<double> pivotpars( 3, 0 );
        if ( ipos != std::string::npos ) {
          ipos    = linestr.find_first_of( ">", ipos );
          jpos    = linestr.find_first_of( "<", ipos + 1 );
          numbers = tokenize( linestr.substr( ipos + 1, jpos - ipos - 1 ), " " );
          assert( numbers.size() == 3 );
          for ( size_t i = 0; i < 3; ++i ) pivotpars[i] = extractdouble( numbers[i] );
        }
        // now put the result in the dictionary
        XmlSurveyData& entry = m_xmldata[conditionname];
        entry.name           = conditionname;
        for ( int i = 0; i < 3; ++i ) {
          entry.par[i]     = pospars[i];
          entry.par[i + 3] = rotpars[i];
          entry.pivot[i]   = pivotpars[i];
        }
      }
    }
    return StatusCode::SUCCESS;
  }

  StatusCode AlignChisqConstraintTool::parseElement( const std::string& element ) {
    // there are two types of definitions, namely those that look like
    //   " pattern : valTx valTy valTz valRx valRy valRz : errTx errTy errTz errRx errRy errRz" --> 6 values and 6
    //   errors
    // and the old type
    //   " pattern : Tx : val +/- err " or even " pattern : Tx : err "
    // new pattern type with a pivot point, where the constraint is defined in the element frame
    //    " pattern : valTx valTy valTz valRx valRy valRz : errTx errTy errTz errRx errRy errRz : pivotx pivoty pivotz"
    //    --> 6 values and 6 and 3 values
    StatusCode sc = StatusCode::SUCCESS;
    debug() << "Parsing constraint: " << element << endmsg;
    std::vector<std::string> tokens = tokenize( element, ":" );
    debug() << "Number of tokens: " << tokens.size() << endmsg;
    if ( tokens.size() != 3 && tokens.size() != 4 ) {
      error() << "constraint has wrong number of tokens: " << element << endmsg;
      sc = StatusCode::FAILURE;
    } else {
      std::string name = removechars( tokens[0], " " );
      m_configuredSurvey.push_back( ConfiguredSurveyData() );
      ConfiguredSurveyData& entry = m_configuredSurvey.back();
      entry.name                  = name;

      // first see if definition is of the old type
      const std::vector<std::string> dofnames = boost::assign::list_of( "Tx" )( "Ty" )( "Tz" )( "Rx" )( "Ry" )( "Rz" );
      std::string                    dofstr   = removechars( tokens[1], " ," );
      size_t dof = std::distance( dofnames.begin(), std::find( dofnames.begin(), dofnames.end(), dofstr ) );
      if ( dof < dofnames.size() ) {
        error() << "constraint value has wrong number of tokens: " << element << endmsg;
        sc = StatusCode::FAILURE;
      } else {
        std::vector<std::string> numbers = tokenize( tokens[1], " " );
        if ( numbers.size() != 6 ) {
          error() << "constraint has wrong number of deltas: " << tokens[1] << endmsg;
          sc = StatusCode::FAILURE;
        } else {
          for ( size_t i = 0; i < 6; ++i ) entry.par[i] = boost::lexical_cast<double>( numbers[i] );
        }

        numbers = tokenize( tokens[2], " " );
        if ( numbers.size() != 6 ) {
          error() << "constraint has wrong number of errors: " << tokens[2] << endmsg;
          sc = StatusCode::FAILURE;
        } else {
          for ( size_t i = 0; i < 6; ++i ) entry.err[i] = boost::lexical_cast<double>( numbers[i] );
        }

        if ( tokens.size() == 4 ) {
          numbers = tokenize( tokens[3], " " );
          if ( numbers.size() != 3 ) {
            error() << "constraint has wrong number of pivot coordinates: \'" << tokens[3] << "\', " << numbers
                    << endmsg;
            sc = StatusCode::FAILURE;
          } else {
            for ( size_t i = 0; i < 3; ++i ) entry.pivot[i] = boost::lexical_cast<double>( numbers[i] );
          }
        }
      }
    }
    return sc;
  }

  StatusCode AlignChisqConstraintTool::parseXmlUncertainties( const std::string& ipattern ) {
    // now still parse the patterns with XML uncertainties
    StatusCode               sc     = StatusCode::SUCCESS;
    std::vector<std::string> tokens = tokenize( ipattern, ":" );
    if ( tokens.size() != 2 ) {
      error() << "xml uncertainty pattern has wrong number of tokens: " << ipattern << endmsg;
      sc = StatusCode::FAILURE;
    }
    NamedXmlUncertainty data;
    data.name              = removechars( tokens[0], " " );
    const auto valuetokens = tokenize( tokens[1], " " );
    if ( valuetokens.size() != 6 ) {
      error() << "xml uncertainty pattern has wrong number of errors: " << tokens[1] << endmsg;
      sc = StatusCode::FAILURE;
    }
    for ( int i = 0; i < 6; ++i ) data.err[i] = boost::lexical_cast<double>( valuetokens[i] );

    if ( tokens.size() == 3 ) {
      warning() << "pivot point for xml uncertainties not yet implemented" << endmsg;
      const auto valuetokens = tokenize( tokens[2], " " );
      if ( valuetokens.size() != 6 ) {
        error() << "xml uncertainty pattern has wrong number of pivot values: " << tokens[2] << endmsg;
        sc = StatusCode::FAILURE;
      }
      for ( int i = 0; i < 3; ++i ) data.pivot[i] = boost::lexical_cast<double>( valuetokens[i] );
    }

    m_xmlUncertaintyMap.emplace_back( data );
    return sc;
  }

  const NamedXmlUncertainty* AlignChisqConstraintTool::findXmlUncertainty( const std::string& name ) const {
    // always take the last one:
    const NamedXmlUncertainty* rc{0};
    for ( const auto& entry : m_xmlUncertaintyMap ) {
      if ( match( name, entry.name ) ) rc = &entry;
    }
    if ( rc ) { debug() << "Matched " << name << " to " << rc->name << " " << rc->err << endmsg; }
    return rc;
  }

  const AlParameters* AlignChisqConstraintTool::surveyParameters( const Element& element ) const {
    const AlParameters*              rc( 0 );
    SurveyParameters::const_iterator it = m_cachedSurvey.find( &element );
    if ( it != m_cachedSurvey.end() ) {
      rc = &( it->second );
    } else {

      AlParameters newsurvey;

      // did we add information for this alignable explicitely? note
      // that we take the last one that matches.
      bool found = false;
      for ( ConstraintData::const_iterator it = m_configuredSurvey.begin(); it != m_configuredSurvey.end(); ++it )
        // match the name of the alignable, or, if there is only one element, match the name of the condition
        if ( element.name() == it->name || match( element.name(), it->name ) ) {
          debug() << "Matched element to configured survey" << element.name() << endmsg;
          const ConfiguredSurveyData&       survey     = *it;
          AlParameters::TransformParameters parameters = survey.par;
          AlParameters::TransformCovariance covmatrix;
          for ( int i = 0; i < 6; ++i ) covmatrix( i, i ) = survey.err[i] * survey.err[i];
          newsurvey = AlParameters{parameters, covmatrix};
          found     = true;

          // if a pivot point is specified, make sure to transform survey to the right frame
          if ( survey.pivot[0] != 0 || survey.pivot[1] != 0 || survey.pivot[2] != 0 ) {
            debug() << "Correcting for survey pivot point for element: " << element.name() << " " << survey.pivot[0]
                    << " " << survey.pivot[1] << " " << survey.pivot[2] << endmsg;
            Gaudi::Transform3D nominalFrame = Element::toGlobalMatrixMinusDelta( element.detelements().front() );
            const ROOT::Math::Translation3D pivot( survey.pivot[0], survey.pivot[1], survey.pivot[2] );
            Gaudi::Transform3D              surveyFrame = nominalFrame * pivot;
            // get the transform from survey to alignment and transform the survey
            Gaudi::Transform3D fromSurveyToAlignment = element.alignmentFrame().Inverse() * surveyFrame;
            newsurvey                                = newsurvey.transformTo( fromSurveyToAlignment );
            debug() << "Survey covariance matrix in alignment frame: " << newsurvey.covariance() << endmsg;
          }
        }

      if ( !found ) {
        debug() << "Did not find any matching configured survey for alignable: " << element.name() << endmsg;

        // OK. Here is the new strategy which should also work for
        // groups of elements. Make an average of the xml found for all
        // daughters. Assign the errors based on the xml-error
        // patterns. Let's first look for that pattern.

        // for the error pattern we have the following rule:
        // - first we match the name of the alignable
        // - if there is only a single element, we then try to match the name of the condition
        // - if there is a single element, we then try to match the name of the element
        // extract the condition name, but only if this alignable has only one detector element

        // decltype needed for compatibility with DetDEsc and DD4hep
        // To be dropped when DetDesc is gone
        // The resulting type is basically AlignmentCondition*
        const auto* errors = findXmlUncertainty( element.name() );
        if ( errors == 0 && element.detelements().size() == 1 ) {
          if ( ( element.detelements().front().alignmentCondition() ) ) {
            std::string condname = element.detelements().front().alignmentConditionName();
#ifndef USE_DD4HEP
            // need to remove a leading slash
            condname = condname.substr( 1 );
#endif
            errors = findXmlUncertainty( condname );
          }
          if ( !errors ) errors = findXmlUncertainty( element.detelements().front().name() );
        }

        if ( !errors ) {
          warning() << "Cannot find XML uncertainties for alignable: " << element.name() << endmsg;
        } else {
          AlParameters::TransformCovariance localcovmatrix;
          for ( int i = 0; i < 6; ++i ) localcovmatrix( i, i ) = ( errors->err )[i] * ( errors->err )[i];

          AlParameters::TransformParameters sumparameters;
          AlParameters::TransformCovariance sumcovmatrix;

          size_t numfound( 0 );

          // now loop over all daughter elements
          for ( const auto& detelem : element.detelements() ) {
            // find this element in the XML catalogue
            std::string condname;
            if ( ( detelem.alignmentCondition() ) ) {
              condname = detelem.alignmentConditionName();
#ifndef USE_DD4HEP
              // need to remove a leading slash
              condname = condname.substr( 1 );
#endif
            }
            XmlData::const_iterator it = m_xmldata.find( condname );
            if ( it == m_xmldata.end() ) {
              error() << "Cannot find condition \"" << condname << "\" for alignable " << element.name()
                      << " in survey dictionary." << endmsg;
            } else {
              const XmlSurveyData& survey = it->second;

              Gaudi::Transform3D              nominalFrame = Element::toGlobalMatrixMinusDelta( detelem );
              AlParameters                    surveypars( survey.par, localcovmatrix );
              const ROOT::Math::Translation3D pivot( survey.pivot[0], survey.pivot[1], survey.pivot[2] );
              Gaudi::Transform3D              surveyFrame = nominalFrame * pivot;
              // get the transform from survey to alignment and transform the survey
              Gaudi::Transform3D fromSurveyToAlignment = element.alignmentFrame().Inverse() * surveyFrame;
              AlParameters       surveyParameters      = surveypars.transformTo( fromSurveyToAlignment );

              sumparameters += surveyParameters.transformParameters();
              sumcovmatrix += surveyParameters.transformCovariance();
              ++numfound;
            }
          }

          if ( numfound == element.detelements().size() ) {
            // now solve a peculiar problem: if we share xml parameters
            // with the daughters (because there is no xml for this
            // alignable) set the survey for these parameters to
            // 0. otherwise, it cannot converge.
            std::vector<int> shareddofs = element.redundantDofs();
            for ( auto idof : shareddofs ) {
              sumparameters( idof ) = 0;
              for ( int jdof = 0; jdof < 6; ++jdof ) sumcovmatrix( idof, jdof ) = 0;
              sumcovmatrix( idof, idof ) = numfound * localcovmatrix( idof, idof );
            }

            newsurvey = AlParameters( sumparameters / numfound, Gaudi::SymMatrix6x6( sumcovmatrix / numfound ) );
            found     = true;
          }
        }
      }
      if ( !found ) {
        warning() << "Haven't been able to construct survey for " << element.name() << endmsg;
      } else {
        m_cachedSurvey[&element] = newsurvey;
        rc                       = &( m_cachedSurvey[&element] );
      }
    }

    return rc;
  }

  LHCb::ChiSquare AlignChisqConstraintTool::addConstraints( const Elements& inputelements, AlVec& halfDChi2DAlpha,
                                                            AlSymMat&     halfD2Chi2DAlpha2,
                                                            std::ostream& logmessage ) const {
    size_t totalnumconstraints( 0 );
    double totalchisq( 0 );
    for ( auto& elem : inputelements ) {
      const AlParameters* psurvey = surveyParameters( *elem );
      if ( psurvey ) {
        const AlParameters&               surveypars   = *psurvey;
        AlParameters                      currentdelta = elem->currentDelta();
        AlParameters::TransformParameters residual =
            surveypars.transformParameters() - currentdelta.transformParameters();
        AlParameters::TransformCovariance weight = surveypars.transformCovariance();
        weight.Invert();
        // FIX ME: take only active dofs when evaluating survey chisq
        totalchisq += ROOT::Math::Similarity( residual, weight );
        totalnumconstraints += 6;
        Gaudi::Vector6      dChi2DAlpha   = weight * residual;
        Gaudi::SymMatrix6x6 d2Chi2DAlpha2 = weight;
        for ( int idof = 0; idof < 6; ++idof ) {
          int ipar = elem->activeParIndex( idof );
          if ( ipar >= 0 ) {
            halfDChi2DAlpha( ipar ) += dChi2DAlpha( idof );
            for ( int jdof = 0; jdof <= idof; ++jdof ) {
              int jpar = elem->activeParIndex( jdof );
              if ( jpar >= 0 ) halfD2Chi2DAlpha2.fast( ipar, jpar ) += d2Chi2DAlpha2( idof, jdof );
            }
          }
        }
      }
    }
    logmessage << "Total chisquare of survey constraints: " << totalchisq << " / " << totalnumconstraints << std::endl;
    return LHCb::ChiSquare( totalchisq, totalnumconstraints );
  }

  LHCb::ChiSquare AlignChisqConstraintTool::addConstraints( const Elements& inputelements, Equations& equations,
                                                            std::ostream& logmessage ) const {
    size_t totalnumconstraints( 0 );
    double totalchisq( 0 );
    for ( auto& elem : inputelements ) {
      const AlParameters* psurvey = surveyParameters( *elem );
      if ( psurvey ) {
        const AlParameters&               surveypars   = *psurvey;
        ElementData&                      elemdata     = equations.element( elem->index() );
        AlParameters                      currentdelta = elem->currentDelta();
        AlParameters::TransformParameters residual =
            surveypars.transformParameters() - currentdelta.transformParameters();
        AlParameters::TransformCovariance weight = surveypars.transformCovariance();
        weight.InvertChol();
        totalchisq += ROOT::Math::Similarity( residual, weight );
        elemdata.m_dChi2DAlpha += weight * residual;
        elemdata.m_d2Chi2DAlpha2 += weight;
        totalnumconstraints += 6;
      } // end if elem.isActive
    }
    logmessage << "Total chisquare of survey constraints: " << totalchisq << " / " << totalnumconstraints << std::endl;
    if ( !m_joints.empty() ) addElementJoints( inputelements, equations, logmessage );

    return LHCb::ChiSquare( totalchisq, totalnumconstraints );
  }

  LHCb::ChiSquare AlignChisqConstraintTool::chiSquare( const Element& element, bool activeonly ) const {
    size_t              totalnumconstraints( 0 );
    double              totalchisq( 0 );
    const AlParameters* psurvey = surveyParameters( element );
    if ( psurvey ) {
      const AlParameters&               surveypars   = *psurvey;
      AlParameters                      currentdelta = element.currentDelta();
      AlParameters::TransformParameters residual =
          surveypars.transformParameters() - currentdelta.transformParameters();
      AlParameters::TransformCovariance weight = surveypars.transformCovariance();
      totalnumconstraints += 6;
      if ( activeonly ) {
        // this is extremely lazy: just set errors on inactive parameters 'big'
        const double largeerrors[6] = {1000, 1000, 1000, 1, 1, 1};
        for ( int idof = 0; idof < 6; ++idof )
          if ( !element.dofMask().isActive( idof ) ) {
            weight( idof, idof ) += largeerrors[idof] * largeerrors[idof];
            --totalnumconstraints;
          }
      }
      weight.InvertChol();
      totalchisq += ROOT::Math::Similarity( residual, weight );
    }
    return LHCb::ChiSquare( totalchisq, totalnumconstraints );
  }

  LHCb::ChiSquare AlignChisqConstraintTool::addElementJoints( const Elements& inputelements, Equations& equations,
                                                              std::ostream& logmessage ) const {
    size_t totalnumconstraints( 0 );
    double totalchisq( 0 );
    for ( const auto& configuredjoint : m_joints ) {
      // first decode (this could go elsewhere)
      debug() << "Parsing joint constraint: " << configuredjoint << endmsg;
      std::vector<std::string> tokens = tokenize( configuredjoint, ":" );
      if ( tokens.size() != 4 && tokens.size() != 3 ) {
        error() << "Joint constraint has wrong number of tokens: " << configuredjoint << endmsg;
      } else {
        const auto elementnameA = removechars( tokens[0], " " );
        const auto elementnameB = removechars( tokens[1], " " );
        double     pivotA[3]    = {0, 0, 0};
        double     errors[6]    = {0, 0, 0, 0, 0, 0};
        {
          auto errorsasstring = tokenize( tokens[2], " " );
          for ( size_t i = 0; i < 6; ++i ) errors[i] = boost::lexical_cast<double>( errorsasstring[i] );
        }
        if ( tokens.size() == 4 ) {
          auto pivotAstring = tokenize( tokens[3], " " );
          for ( size_t i = 0; i < 3; ++i ) pivotA[i] = boost::lexical_cast<double>( pivotAstring[i] );
        }
        // find the two elements
        const LHCb::Alignment::Element* elementA{0};
        const LHCb::Alignment::Element* elementB{0};
        for ( const auto* element : inputelements ) {
          // match the name of the alignable, or, if there is only one element, match the name of the condition
          if ( !elementA && ( element->name() == elementnameA || match( element->name(), elementnameA ) ) )
            elementA = element;
          if ( !elementB && ( element->name() == elementnameB || match( element->name(), elementnameB ) ) )
            elementB = element;
        }
        if ( !elementA ) error() << "Cannot find element for joint constraint A: \'" << elementnameA << "\'" << endmsg;
        if ( !elementB ) error() << "Cannot find element for joint constraint B: \'" << elementnameB << "\'" << endmsg;
        if ( elementA && elementB ) {

          // add the 'joint' constraints
          // - each constraint has two elements
          // - for each element we have defined a pivot point in the local frame
          // - we need to derivatives of the alignment constants at this point in the global frame to the alignment
          // constants in the alignment frame (jacobian)
          // - we have also defined an 'uncertainty' for each of the 6 dofs
          // - we then derive the 6-dimensional residual in the global frame (will this be a problem if modules are
          // tilted? should we rather choose the local frame of the first module?)
          // - then add contributions to the chi2
          const auto frameA = elementA->alignmentFrame();
          const auto frameB = elementB->alignmentFrame();

          // create the transform for the frame where we compare the parameters
          Gaudi::Transform3D nominalA           = Element::toGlobalMatrixMinusDelta( elementA->detelements().front() );
          const auto         commonframe        = nominalA * ROOT::Math::Translation3D{pivotA[0], pivotA[1], pivotA[2]};
          const auto         fromGlobalToCommon = commonframe.Inverse();

          // now we need the deltas in this frame, and the jacobians
          const auto         fromAlignmentAToCommon = fromGlobalToCommon * frameA;
          const auto         fromAlignmentBToCommon = fromGlobalToCommon * frameB;
          const AlParameters deltaA                 = elementA->currentDelta(); // in alignment frame A
          const AlParameters deltaB                 = elementB->currentDelta(); // in alignment frame B
          const auto         deltaA_common          = deltaA.transformTo( fromAlignmentAToCommon ); // in common frame
          const auto         deltaB_common          = deltaB.transformTo( fromAlignmentBToCommon ); // in common frame
          debug() << "Delta A (align,common): " << deltaA.transformParameters() << " "
                  << deltaA_common.transformParameters() << endmsg;
          debug() << "Delta B (align,common): " << deltaB.transformParameters() << " "
                  << deltaB_common.transformParameters() << endmsg;

          // Now create the chi2 and the derivatives and add.
          // There are serious issues with 'auto' and 'S-matrix expressions', so make sure to assign temporaries to
          // concrete types.
          using Matrix6x6        = AlParameters::Matrix6x6;
          using Vector6          = AlParameters::TransformParameters;
          const Vector6 residual = deltaB_common.transformParameters() - deltaA_common.transformParameters();
          debug() << "Joint residual: " << residual << endmsg;
          AlParameters::TransformCovariance W;
          for ( int i = 0; i < 6; ++i ) W( i, i ) = 1 / ( errors[i] * errors[i] );
          const Vector6 Wresidual = W * residual;
          totalchisq += ROOT::Math::Similarity( residual, W );

          totalnumconstraints += 6;
          const Matrix6x6 jacobianA  = AlParameters::jacobian( fromAlignmentAToCommon );
          const Matrix6x6 jacobianAT = ROOT::Math::Transpose( jacobianA );
          const Matrix6x6 jacobianB  = AlParameters::jacobian( fromAlignmentBToCommon );
          const Matrix6x6 jacobianBT = ROOT::Math::Transpose( jacobianB );
          debug() << "Jacobian A: " << std::endl << jacobianA << endmsg;
          debug() << "Jacobian B: " << std::endl << jacobianB << endmsg;

          // pay attention: 'jacobian' is not the derivative of the
          // residual. for that you need to add an extra minus sign to
          // 'A'. however, for some reason all over the alignment coee
          // we have defined the first derivative witht he wrong
          // sign. (that's the FIXME below.)
          ElementData& elemdataA = equations.element( elementA->index() );
          // FIX ME: sign is exactly opposite of what you expect. See also AlignAlgorithm.
          elemdataA.m_dChi2DAlpha += jacobianAT * Wresidual;
          elemdataA.m_d2Chi2DAlpha2 += ROOT::Math::Similarity( jacobianAT, W );
          ElementData& elemdataB = equations.element( elementB->index() );
          // FIX ME: sign is exactly opposite of what you expect. See also AlignAlgorithm.
          elemdataB.m_dChi2DAlpha -= jacobianBT * Wresidual;
          elemdataB.m_d2Chi2DAlpha2 += ROOT::Math::Similarity( jacobianBT, W );
          // don't forget the off-diagonal element.
          const auto rowindex = elementA->index();
          const auto colindex = elementB->index();
          if ( rowindex < colindex ) {
            elemdataA.m_d2Chi2DAlphaDBeta[colindex].add( -1 * jacobianAT * W * jacobianB );
          } else if ( rowindex > colindex ) {
            elemdataB.m_d2Chi2DAlphaDBeta[rowindex].add( -1 * jacobianBT * W * jacobianA );
          }
        }
      }
    }
    logmessage << "Total chi2 of joint constraints: " << totalchisq << "/" << totalnumconstraints << std::endl;
    return LHCb::ChiSquare( totalchisq, totalnumconstraints );
  }
} // namespace LHCb::Alignment
