/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Track.h"

#include "TAlignment/AlResiduals.h"
#include "TAlignment/GetElementsToBeAligned.h"
#include "TAlignment/TrackResidualHelper.h"

#include "PrKalmanFilter/KF.h"
#include "TrackInterfaces/ITrackProjector.h"
#include "TrackInterfaces/ITrackProjectorSelector.h"

#include "Event/KalmanFitResult.h"
#include "Event/PrKalmanFitResult.h"
#include "Event/TrackFitResult.h"
#include "LHCbMath/MatrixInversion.h"
#include "TrackKernel/TrackFunctors.h"

#include <iostream>
#include <map>
#include <vector>

namespace LHCb {
  class FitNode;
}

namespace {
  template <class Matrix>
  Matrix correlationMatrix( const Matrix& M ) {
    Matrix N;
    for ( int i = 0; i < Matrix::kCols; ++i ) {
      N( i, i ) = 1;
      for ( int j = 0; j < i; ++j ) N( i, j ) = M( i, j ) / std::sqrt( M( i, i ) * M( j, j ) );
    }
    return N;
  }

  template <int N>
  ROOT::Math::SMatrix<double, 2 * N, 2 * N, ROOT::Math::MatRepSym<double, 2 * N>>
  constructNxN( const Gaudi::SymMatrix5x5& MAA, const Gaudi::SymMatrix5x5& MBB, const Gaudi::Matrix5x5& MAB ) {
    ROOT::Math::SMatrix<double, 2 * N, 2 * N, ROOT::Math::MatRepSym<double, 2 * N>> M;
    for ( int i = 0; i < N; ++i ) {
      for ( int j = 0; j <= i; ++j ) {
        M( i, j )         = MAA( i, j );
        M( i + N, j + N ) = MBB( i, j );
      }
      for ( int j = 0; j < N; ++j ) { M( i, j + N ) = MAB( i, j ); }
    }
    return M;
  }

  const Gaudi::TrackMatrix& getOffDiagCov( size_t l, size_t k,
                                           std::vector<std::vector<Gaudi::TrackMatrix*>>& offdiagcov,
                                           const std::vector<Gaudi::TrackMatrix>&         smoothergainmatrix ) {
    assert( k < l );
    if ( offdiagcov[l][k] == 0 )
      offdiagcov[l][k] = new Gaudi::TrackMatrix( getOffDiagCov( l, k + 1, offdiagcov, smoothergainmatrix ) *
                                                 Transpose( smoothergainmatrix[k] ) );
    return *offdiagcov[l][k];
  }

  using namespace LHCb;
  using namespace LHCb::Alignment;

  inline Gaudi::TrackMatrix smootherGainMatrix( const LHCb::KalmanFitResult& fr, size_t idx ) {
    return fr.nodes().at( idx )->smootherGainMatrix();
  }

  inline Gaudi::TrackMatrix smootherGainMatrix( const LHCb::PrKalmanFitResult& fr, size_t idx ) {
    return fr.gain_matrices[idx];
  }

  inline void setClassicalSmoother( const LHCb::KalmanFitResult& fr ) {

    const_cast<LHCb::KalmanFitResult&>( fr ).setBiDirectionnalSmoother( false );
  }

  inline void setClassicalSmoother( const LHCb::PrKalmanFitResult& ) { return; }

  inline bool errorInFitResult( const KalmanFitResult& fr, MsgStream& warnLog ) {
    bool error = fr.inError();
    if ( error ) warnLog << "Error in KalmanFitResult: " << fr.getError() << endmsg;
    return error;
  }

  inline bool errorInFitResult( const PrKalmanFitResult&, MsgStream& ) { return false; }

  template <typename TFitResult>
  bool testNodes( const TFitResult* fr, MsgStream& warnLog ) {
    if ( nodes( *fr ).size() <= 0 ) return false;
    bool success = true;
    for ( auto& node : nodes( *fr ) ) {
      if ( node.hasMeasurement() && node.isHitOnTrack() ) {
        if ( !( node.errResidual2() > TrackParameters::lowTolerance ) ) {
          warnLog << "Found node with zero errror on residual: " << node.errResidual2() << endmsg;
          success = false;
        }
        if ( !( node.errResidual2() < node.errMeasure2() ) ) {
          warnLog << "Found node with R2 > V2: " << node.errResidual2() << " " << node.errMeasure2() << endmsg;
          success = false;
        }
        if ( !( node.errMeasure2() > 1e-6 ) ) {
          warnLog << "Found node with very small error on measurement: " << node.errMeasure2() << endmsg;
          success = false;
        }
        if ( node.errResidual2() < 1e-3 * node.errMeasure2() ) {
          warnLog << "Found node with negligible weight: " << node.errResidual2() << " " << node.errMeasure2()
                  << endmsg;
          success = false;
        }
      }
    }
    return success;
  }

  // set the derivative in a residual. once this is short enough we move it somewhere else
  void setDerivative( LHCb::Alignment::Residual1D& res, const ITrackProjectorSelector* projselector ) {

    std::visit(
        [&]( const auto& node ) {
          using vtype = std::decay_t<decltype( node )>;
          if constexpr ( std::is_same_v<vtype, const LHCb::FitNode*> ) {
            // Project measurement
            const LHCb::Measurement& meas = std::get<const LHCb::FitNode*>( res.node() )->measurement();
            const ITrackProjector*   proj = projselector->projector( meas );
            if ( !proj ) {
              throw GaudiException( "Could not get projector for selected measurement", "AlignAlgorithm",
                                    StatusCode::FAILURE );
            } else {
              LHCb::StateVector state{res.state().stateVector(), res.state().z()};
              res.setDerivative( proj->alignmentDerivatives( state, meas, Gaudi::XYZPoint( 0, 0, 0 ) ) *
                                 res.element().jacobian() );
            }
          } else if constexpr ( std::is_same_v<vtype, const LHCb::Pr::Tracks::Fit::Node*> ) {
            res.setDerivative(
                LHCb::Pr::Tracks::Fit::KF::alignmentDerivatives(
                    *std::get<const LHCb::Pr::Tracks::Fit::Node*>( res.node() ), Gaudi::XYZPoint( 0, 0, 0 ) ) *
                res.element().jacobian() );
          } else {
            throw GaudiException( "Using invalid fit node type", "AlignAlgorithm", StatusCode::FAILURE );
          }
        },
        res.node() );
  }
} // namespace

namespace LHCb::Alignment {

  // ugly: put this inside a class, such that we can access the private data members of TrackResidual
  struct TrackResidualCreator {

    template <typename NodesWithIndex>
    std::unique_ptr<TrackResiduals> create( const LHCb::Track& track, const NodesWithIndex& nodes,
                                            std::vector<size_t>& alignablenodeindices, size_t& refnodeindex,
                                            const GetElementsToBeAligned&  elementProvider,
                                            const ITrackProjectorSelector* projselector ) {
      NodesWithIndex nodeswithindex;
      nodeswithindex.reserve( nodes.size() );
      size_t index( 0 );
      // make pairs of node and its index in original list
      // use only nodes with measurement that is not outlier
      for ( const auto& it : nodes ) {
        if ( it.first->hasMeasurement() && !it.first->isOutlier() ) nodeswithindex.emplace_back( it.first, index );
        index++;
      }

      // now make sure the order is right for eventual vertexing
      bool backward    = track.isVeloBackward();
      bool increasingz = nodes.front().first->z() < nodes.back().first->z();
      if ( ( backward && increasingz ) || ( !backward && !increasingz ) )
        std::reverse( nodeswithindex.begin(), nodeswithindex.end() );

      // the first node is the reference for vertexing
      refnodeindex = nodeswithindex.front().second;

      // now select the subset of nodes associated to an alignable object
      alignablenodeindices.clear();
      alignablenodeindices.reserve( nodeswithindex.size() );
      auto rc = std::unique_ptr<TrackResiduals>{
          new TrackResiduals{track, smoothedState( *nodeswithindex.front().first /*, true*/ )}};
      rc->m_residuals.reserve( nodeswithindex.size() );
      size_t idx{0};
      for ( const auto& it : nodeswithindex ) {
        const auto node = it.first;
        const auto elem = elementProvider.findElement( *node );
        // Wouter's change and test
        if ( elem ) {
          rc->m_residuals.push_back( Residual1D{*node, *elem} );
          setDerivative( rc->m_residuals.back(), projselector );
          alignablenodeindices.push_back( it.second );
        } else {
          rc->m_nExternalHits++;
        }
        idx++;
      }
      return rc;
    }

    template <typename TFitResult>
    std::unique_ptr<TrackResiduals> compute( const LHCb::Track& track, const TFitResult& fr,
                                             const GetElementsToBeAligned&  elementProvider,
                                             const ITrackProjectorSelector* projselector, MsgStream& warnLog ) {
      // This test used to be in AlignAlgorithm but better fits here
      bool ok = testNodes( &fr, warnLog );
      if ( !ok ) return std::unique_ptr<TrackResiduals>{};

      // Note that setting classical smoother for fit result makes difference only for KalmanFitResult.
      // This is because of the "on demand" calculations in TrackMasterFitter and LHCb::FitNode.
      // PrKalmanFilter has to be configured to use classical smoother from the beginning
      // and there is nothing that can change the smoother afterwards.
      setClassicalSmoother( fr );

      // make pairs of node and its index in original list
      using TNode          = typename TFitResult::NodeType;
      using NodeWithIndex  = std::pair<const TNode*, size_t>;
      using NodesWithIndex = std::vector<NodeWithIndex>;
      NodesWithIndex allnodeswithindex;
      allnodeswithindex.reserve( nodes( fr ).size() );
      size_t index( 0 );
      for ( auto it = nodes( fr ).begin(); it != nodes( fr ).end(); ++it, ++index )
        allnodeswithindex.emplace_back( &( *it ), index );

      // find first and last node with measurement that is not outlier
      auto is_measurement_not_outlier = []( const NodeWithIndex& n ) {
        return n.first->hasMeasurement() && !n.first->isOutlier();
      };
      auto begin = std::find_if( allnodeswithindex.begin(), allnodeswithindex.end(), is_measurement_not_outlier );
      auto end =
          std::find_if( allnodeswithindex.rbegin(), allnodeswithindex.rend(), is_measurement_not_outlier ).base();
      // copy all nodes between the first and last node with measurement to make things easier later
      std::vector<NodeWithIndex> nodeswithindex;
      nodeswithindex.reserve( end - begin );
      std::transform( begin, end, std::back_inserter( nodeswithindex ), []( const auto& i ) { return i; } );

      size_t numnodes = nodeswithindex.size();
      // These are the diagonal elements. At first we just copy these
      std::vector<const Gaudi::TrackSymMatrix*> diagcov( numnodes, 0 );
      for ( size_t irow = 0; irow < numnodes; ++irow ) {
        // FIXME for LHCb::FitNode it is classical by default but can be switched with second input parameter,
        // for PrFitNode the smoother is set already when configuring the track fit and it cannot be switched now
        diagcov[irow] = &( smoothedStateCovariance( *nodeswithindex[irow].first /*, true*/ ) );
      }

      // These are the off-diagonal elements, which are not symmetric. We
      // need to choose whether we store lower or upper triangle. We are
      // going to define now that row >= col (lower triangle), which means
      // that we store the correlation Cov{k,k-1} etc.

      // To make things extra confusing, we also store the row with zero
      // elements. We do not need to store the diagonal.
      std::vector<std::vector<Gaudi::TrackMatrix*>> offdiagcov( numnodes );
      for ( size_t irow = 0; irow < numnodes; ++irow ) {
        offdiagcov[irow] = std::vector<Gaudi::TrackMatrix*>( irow, 0 );
      }

      // We first compute the smoother gain matrix on every (but the
      // last) node. Once we have the gain matrices, the rest is
      // trivial, though still CPU intensive.
      std::vector<Gaudi::TrackMatrix> smoothergainmatrix( numnodes - 1 );
      for ( size_t k = numnodes - 1; k > 0; --k ) {
        const TNode* node = dynamic_cast<const TNode*>( nodeswithindex[k].first );
        assert( node );
        // retrieve smoother gain matrix from PREVIOUS node
        smoothergainmatrix[k - 1] = smootherGainMatrix( fr, nodeswithindex[k - 1].second );

        // FIXME for LHCb::FitNode smoothed state covariance is classical by default but can be switched with second
        // input parameter for PrFitNode the smoother is set already when configuring the track fit and it cannot be
        // switched now
        Gaudi::TrackMatrix C_km1_k = smoothergainmatrix[k - 1] * smoothedStateCovariance( *node /*, true*/ );
        offdiagcov[k][k - 1]       = new Gaudi::TrackMatrix( Transpose( C_km1_k ) );
      }

      bool error = errorInFitResult( fr, warnLog );

      std::unique_ptr<TrackResiduals> residuals;

      if ( !error ) {
        // now it is time to calculate the covariance matrix for the
        // residuals. We already know which nodes correspond to
        // measurements so let's fill the matrix.

        // create the Residuals object
        std::vector<size_t> alignable_node_indices;
        size_t              refnodeindex;
        residuals =
            create( track, nodeswithindex, alignable_node_indices, refnodeindex, elementProvider, projselector );

        // now create the 'sparce' matrix HCH
        size_t numalignables = alignable_node_indices.size();
        residuals->m_HCHElements.reserve( numalignables * ( numalignables - 1 ) / 2 );
        for ( size_t irow = 0; irow < numalignables; ++irow ) {
          size_t k = alignable_node_indices[irow];
          assert( std::get<const TNode*>( residuals->m_residuals[irow].node() ) == nodeswithindex[k].first );
          const Gaudi::TrackProjectionMatrix1D& Hk = projectionMatrix( nodeswithindex[k].first );
          // first the off-diagonal elements
          for ( size_t icol = 0; icol < irow; ++icol ) {
            size_t l = alignable_node_indices[icol];
            assert( std::get<const TNode*>( residuals->m_residuals[icol].node() ) == nodeswithindex[l].first );
            const Gaudi::TrackProjectionMatrix1D& Hl  = projectionMatrix( nodeswithindex[l].first );
            double                                hch = l < k
                             ? ( Hk * getOffDiagCov( k, l, offdiagcov, smoothergainmatrix ) * Transpose( Hl ) )( 0, 0 )
                             : ( Hl * getOffDiagCov( l, k, offdiagcov, smoothergainmatrix ) * Transpose( Hk ) )( 0, 0 );
            residuals->m_HCHElements.push_back( CovarianceElement( irow, icol, hch ) );
          }
          // now the diagonal element. we recompute this, just to make
          // sure it is consistent. (this turns out not to be necessary,
          // but anyway.)
          residuals->m_residuals[irow].setHCH( ROOT::Math::Similarity( Hk, *( diagcov[k] ) )( 0, 0 ) );

          // and finally the column for the correlation with the reference state
          Gaudi::Matrix1x5 correlationmatrix;
          if ( refnodeindex == k )
            correlationmatrix = Hk * *( diagcov[k] );
          else if ( refnodeindex < k )
            correlationmatrix = Hk * getOffDiagCov( k, refnodeindex, offdiagcov, smoothergainmatrix );
          else
            correlationmatrix = Hk * Transpose( getOffDiagCov( refnodeindex, k, offdiagcov, smoothergainmatrix ) );
          // now copy it into the trackresidual. need to be very carefull
          // with signs here. use H = - dres/dstate. in fact, this the
          // only place we need to know what the sign of the residual is.
          residuals->m_residuals[irow].setResidualStateCov( -correlationmatrix );
        }

        // For PRKalmanFitter tracks we need to do a trick to get the right reference state. The code above takes one of
        // the nodes, but for PrKalmanFilter there is no node corresponding to the state at the beamline. Therefore, if
        // the reference node is not the ClosestToBeam state and the track contains a ClosestToBeam state, then we move
        // the reference state, a bit like done in VertexResidual tool. (See the latter for the formulas.)
        if ( residuals->m_state.location() != LHCb::State::Location::ClosestToBeam &&
             track.firstState().location() == LHCb::State::Location::ClosestToBeam ) {
          // CHECK: need to check that this also works for backwardvelo tracks
          // We need the transport matrix from the refstate to the first state. We could call an extrapolator, but that
          // is a little exagerated, so let's just compute it here:
          const auto&        newrefstate = track.firstState();
          const auto         dz          = newrefstate.z() - residuals->m_state.z();
          Gaudi::TrackMatrix FT          = ROOT::Math::SMatrixIdentity();
          FT( 2, 0 )                     = dz;
          FT( 3, 1 )                     = dz;
          residuals->m_state             = newrefstate;
          for ( size_t inode = 0; inode < residuals->size(); ++inode )
            residuals->m_residuals[inode].setResidualStateCov( residuals->m_residuals[inode].residualStateCov() * FT );
        }

        // Let's check the result
        if ( !error ) {
          std::string message;
          error = residuals->testHCH( message );
          if ( error ) {
            warnLog << message << endmsg << "Track type = " << track.type() << " " << track.history() << " "
                    << track.flag() << endmsg;
          }
        }
        if ( error ) residuals.reset();
      }
      // let's clean up:
      for ( size_t irow = 0; irow < numnodes; ++irow )
        for ( size_t icol = 0; icol < irow; ++icol ) delete offdiagcov[irow][icol];

      return residuals;
    }
  };

  const TrackResiduals* TrackResidualHelper::get( const LHCb::Track&            track,
                                                  const GetElementsToBeAligned& elementProvider, MsgStream& warnLog ) {
    // is this the most efficient way to create a cache?
    auto entry = m_residuals.emplace( &track, nullptr );
    if ( entry.second ) {
      // here we need to split by track type
      const auto& fr = track.fitResult();
      if ( fr ) {
        const auto tmffr = dynamic_cast<const LHCb::KalmanFitResult*>( fr );
        if ( tmffr )
          entry.first->second = TrackResidualCreator().compute( track, *tmffr, elementProvider, m_projectors, warnLog );
        else {
          const auto prkfr = dynamic_cast<const LHCb::PrKalmanFitResult*>( fr );
          if ( prkfr )
            entry.first->second =
                TrackResidualCreator().compute( track, *prkfr, elementProvider, m_projectors, warnLog );
          else {
            throw GaudiException( "TrackResidualHelper",
                                  format( "Track uses wrong FitResult type. Type = %s. Returning 0.", track.type() ),
                                  StatusCode::FAILURE );
          }
        }
      } else {
        throw GaudiException( "TrackResidualHelper", format( "Track has no fit result. Returning 0.", track.type() ),
                              StatusCode::FAILURE );
      }
    }
    return entry.first->second.get();
  }
} // namespace LHCb::Alignment
