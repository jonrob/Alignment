/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifdef USE_DD4HEP
#else
#  include "AlignKernel/AlEquations.h"
#  include "AlignmentInterfaces/IAlignUpdateTool.h"
#  include "AlignmentMonitoring/CompareConstants.h"
#  include "TAlignment/AlParameters.h"
#  include "TAlignment/AlignmentElement.h"
#  include "TAlignment/GetElementsToBeAligned.h"
#  include "TAlignment/XmlWriter.h"

#  include "Kernel/LHCbID.h"
#  include "TrackInterfaces/ITrackProjector.h"
#  include "TrackKernel/TrackFunctors.h"

#  include "Event/FitNode.h"
#  include "Event/Particle.h"
#  include "Event/TwoProngVertex.h"
#  include "VPDet/DeVP.h"

#  include "Gaudi/Algorithm.h"
#  include "GaudiKernel/IDetDataSvc.h"
#  include "GaudiKernel/IIncidentListener.h"
#  include "GaudiKernel/IIncidentSvc.h"
#  include "GaudiKernel/IMonitorSvc.h"
#  include "GaudiKernel/IRunable.h"
#  include "GaudiKernel/IUpdateable.h"
#  include "GaudiKernel/Service.h"
#  include "GaudiKernel/SmartIF.h"

#  include "TH1D.h"

#  include <algorithm>
#  include <string>
#  include <thread>
#  include <vector>

namespace LHCb::Alignment {

  // helper using to design the base detectorElementClass, as it's different
  // for DDDB and DD4hep

  using GenericDetElem = ::DetectorElementPlus;

  /**
   * Algorithm handling alignment
   *
   * Note that although it looks fully functional, there are a bunch of handles used on top
   * of the inputs of operator() : odin, tracks, vertices and particles.
   * They exist because these inputs may be missing. So declaring them as parameter of the
   * operator() would thus break backward compatibility
   * This should anyway be revisited at a later stage -> FIXME
   */

  class AlignOnlineIterator : public extends2<Service, IRunable, IIncidentListener> {
  public:
    AlignOnlineIterator( const std::string& name, ISvcLocator* sl );
    virtual ~AlignOnlineIterator();

  public:
    /// the execution of the algorithm
    SmartIF<IIncidentSvc>     m_incidentSvc;
    SmartIF<IDataProviderSvc> m_detectorSvc;
    StatusCode                execute( EventContext const& ) const;
    /// Its initialization
    StatusCode         initialize() override;
    StatusCode         stop() override;
    virtual StatusCode run() override;
    StatusCode         pause();
    void               doContinue();
    void               handle( const Incident& inc ) override;
    StatusCode         doIteration();
    virtual StatusCode finalize() override;

    void                                      doStop();
    ToolHandle<IAlignUpdateTool>              m_updatetool{this, "UpdateTool", "AlignUpdateTool"};
    Gaudi::Property<std::vector<std::string>> m_elemsToBeAligned{this, "Elements", {}, "Path to elements"};
    Gaudi::Property<std::vector<XmlWriter>>   m_xmlWriters{
        this, "XmlWriters", {}, "Definitions of the XmlWriters handling the output"};
    Gaudi::Property<std::string> m_derivativeFile{this, "DerivativeFile", "testderivs",
                                                  "path to ASD file with derivatives"};

  private:
    std::thread*                  m_thread    = nullptr;
    int                           m_iteration = 0;
    std::map<std::string, double> m_initAlConsts; // inital alignment constants. Comapred with constants after
                                                  // convergence to see if update is needed
  };

  static AlignOnlineIterator* IteratorInstance;

  extern "C" {
  int AlignOnlineIteratorThreadFunction( void* t ) {
    AlignOnlineIterator* f = (AlignOnlineIterator*)t;
    IteratorInstance       = f;
    f->doIteration().ignore();
    return 1;
  }
  }

  StatusCode AlignOnlineIterator::doIteration() {
    const std::string lhcbDetectorPath = "/dd/Structure/LHCb";
    DataObject*       lhcbObj          = 0;
    m_detectorSvc->retrieveObject( lhcbDetectorPath, lhcbObj ).ignore();
    const GenericDetElem* lhcb = dynamic_cast<const GenericDetElem*>( lhcbObj );
    if ( lhcbObj == nullptr ) error() << "lhcbObj is null!" << endmsg;

    auto elementProvider =
        std::make_unique<GetElementsToBeAligned>( m_elemsToBeAligned.value(), lhcb, true, warning(), m_detectorSvc );

    if ( elementProvider == nullptr ) error() << "elementProvider is null!" << endmsg;
    if ( m_iteration == 0 ) { m_initAlConsts = elementProvider->getAlignmentConstants(); }
    int               iteration         = 0;
    int               maxIteration      = 0;
    ConvergenceStatus convergencestatus = ConvergenceStatus::Unknown;
    Equations         equations( 0 );
    // TODO: read multiple files
    info() << "AlignOnlineIterator::doIteration: reading derivative file " << m_derivativeFile.value() << endmsg;

    const auto [readerSc, readerMsg] = equations.readFromFile( m_derivativeFile.value().c_str() );
    if ( readerSc == StatusCode::SUCCESS ) {
      info() << readerMsg << endmsg;
    } else {
      error() << "AlignOnlineIterator::doIteration: could not read file " << m_derivativeFile.value() << endmsg;
      error() << readerMsg << endmsg;
    }
    StatusCode sc = m_updatetool->process( equations, *elementProvider, iteration, maxIteration, convergencestatus );
    if ( sc.isFailure() ) { return sc; }
    elementProvider->writeAlignmentConditions( m_xmlWriters.value(), warning() );
    // for testing and debugigng, always continue
    if ( convergencestatus == ConvergenceStatus::Converged ) {
      std::map<std::string, double>                      finalAlConsts = elementProvider->getAlignmentConstants();
      ::Alignment::AlignmentMonitoring::CompareConstants cmp( m_initAlConsts, finalAlConsts );
      cmp.SetVerbosity( true );
      cmp.Compare();
      info() << "converged. Stopping" << endmsg;
      doStop();
    } else {
      info() << " not converged. Continue" << endmsg;

      doContinue();
    }
    m_iteration++;
    return StatusCode::SUCCESS;
  }

  DECLARE_COMPONENT_WITH_ID( AlignOnlineIterator, "AlignOnlineIterator" )

  StatusCode AlignOnlineIterator::initialize() {
    StatusCode sc = this->Service::initialize().ignore();
    if ( !sc ) return sc;
    this->m_incidentSvc = this->service( "IncidentSvc", true );
    if ( !this->m_incidentSvc.get() ) {
      error() << "Cannot access incident service of type IncidentSvc." << endmsg;
      return StatusCode::FAILURE;
    }
    this->m_detectorSvc = this->service( "DetectorDataSvc" );
    if ( !this->m_detectorSvc.get() ) {
      error() << "Cannot access incident service of type DetectorDataSvc." << endmsg;
      return StatusCode::FAILURE;
    }
    this->m_incidentSvc->addListener( this, "DAQ_PAUSED" );
    this->m_incidentSvc->addListener( this, "DAQ_CONTINUE" );

    return sc;
  }

  StatusCode AlignOnlineIterator::execute( EventContext const& ) const {
    info() << "iterator executing " << endmsg;
    return StatusCode::SUCCESS;
  }

  StatusCode AlignOnlineIterator::pause() // the execution of the algorithm
  {
    info() << "iterator pausing " << m_iteration << endmsg;
    if ( m_thread != nullptr ) {
      m_thread->join();
      delete m_thread;
    }
    m_thread = new std::thread( AlignOnlineIteratorThreadFunction, this );
    info() << "iterator paused " << endmsg;
    return StatusCode::SUCCESS;
  }

  void AlignOnlineIterator::doContinue() {
    info() << "doContinue" << endmsg;
    this->m_incidentSvc->fireIncident( Incident( this->name(), "DAQ_CONTINUE" ) );
  }

  void AlignOnlineIterator::doStop() { this->m_incidentSvc->fireIncident( Incident( this->name(), "DAQ_STOP" ) ); }

  StatusCode AlignOnlineIterator::stop() {
    info() << "iterator stopping " << endmsg;
    m_iteration = 0;
    if ( m_thread != nullptr ) {
      m_thread->join();
      delete m_thread;
    }
    return Service::stop();
  }

  StatusCode AlignOnlineIterator::finalize() {
    info() << "iterator finalizing " << endmsg;
    this->m_incidentSvc.reset();
    return this->Service::finalize();
  }

  void AlignOnlineIterator::handle( const Incident& inc ) {
    info() << "iterator received DAQ_HANDLE " << endmsg;
    if ( inc.type() == "DAQ_PAUSED" ) {
      info() << "iterator received DAQ_PAUSED " << endmsg;
      this->pause().ignore();
      // m_thread->join();
      // delete(m_thread);
    }
    if ( inc.type() == "DAQ_CONTINUE" ) {
      info() << "iterator received DAQ_CONTINUE " << endmsg;
      // this->pause().ignore();
      // m_thread->join();
      // delete(m_thread);
    }
  }

  AlignOnlineIterator::~AlignOnlineIterator() {}

  StatusCode AlignOnlineIterator::run() {
    info() << "iterator running " << endmsg;
    return StatusCode::SUCCESS;
  }

  AlignOnlineIterator::AlignOnlineIterator( const std::string& name, ISvcLocator* sl ) : base_class( name, sl ) {}

} // namespace LHCb::Alignment
#endif
