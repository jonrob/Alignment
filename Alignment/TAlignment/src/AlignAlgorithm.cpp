/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/KalmanFitResult.h"
#include "Event/PrKalmanFitResult.h"
#include "Event/Track.h"
#include "TAlignment/AlignAlgorithmBase.h"
#include "TAlignment/TrackResidualHelper.h"
#include "TrackKernel/TrackFunctors.h"

#include "TrackInterfaces/ITrackProjector.h"

#include "PrKalmanFilter/KF.h"

#include "fmt/format.h"

namespace {

  Gaudi::Vector6 convert( const Gaudi::Matrix1x6& m ) {
    Gaudi::Vector6 v;
    for ( int i = 0; i < 6; ++i ) v( i ) = m( 0, i );
    return v;
  }

} // namespace

namespace LHCb::Alignment {
  class AlignAlgorithm : public AlignAlgorithmBase {
  public:
    using AlignAlgorithmBase::AlignAlgorithmBase;
    void operator()( const EventContext& evtContext, const GenericDetElem& lhcb ) const override;

  protected:
    bool accumulate( Equations& equations, const Residuals& residuals ) const;
  };

  DECLARE_COMPONENT_WITH_ID( AlignAlgorithm, "AlignAlgorithm" )

  //=============================================================================
  // Main execution
  //=============================================================================
  void AlignAlgorithm::operator()( const EventContext&, const GenericDetElem& lhcb ) const {
    // FIXME XXX : we should check that the ConditionContext does not change
    // If it does, we should throw an exception and stop processing
    // The risk is to mix data from different runs by mistake and screw the alignment procedure
    // Note that the check would have to be thread safe like the initialization of m_elements
    // insert a lock here to ensure thread-safety until the code is rewritten
    initializeGetElementsToBeAligned( lhcb );

    TrackResidualHelper trackResidualHelper{*m_projSelector};

    // Reset histograms if required
    if ( m_resetHistos ) resetHistos();

    // Create the accumulator for this event on the stack
    auto equations = m_data.with_lock( [&]( const Data& data ) {
      return Equations{data.equations.elements().size(), data.equations.initTime()};
    } );

    Gaudi::Time  eventtime;
    unsigned int runnr{0};
    if ( m_odin.exist() ) {
      const LHCb::ODIN* odin = m_odin.get();
      eventtime              = odin->eventTime();
      runnr                  = odin->runNumber();
    }

    // Get tracks. Copy them into a vector, because that's what we need for dealing with vertices.
    LHCb::Track::Range tracks;
    if ( !m_tracks.objKey().empty() ) { tracks = m_tracks.get(); }
    m_nTracks += tracks.size();

    // First just copy the tracks
    std::vector<const LHCb::Track*> selectedtracks;
    for ( auto& track : tracks )
      // just a sanity check
      if ( track->fitStatus() == LHCb::Track::FitStatus::Fitted && !track->checkFlag( LHCb::Track::Flags::Invalid ) &&
           track->nDoF() > 0 ) {
        bool has_res = m_data.with_lock( [&]( const Data& data ) {
          return trackResidualHelper.get( *track, *data.elementProvider, warning() ) != nullptr;
        } );
        if ( has_res ) {
          selectedtracks.push_back( track );
        } else {
          warning() << "Error computing residual cov matrix. Skipping track of type " << track->type()
                    << " with key: " << track->key() << " and chi2 / dof: " << track->chi2() << "/" << track->nDoF()
                    << endmsg;
          ++m_covFailure;
        }
      } else {
        debug() << "Skipping bad track:"
                << " fitstatus = " << track->fitStatus() << " nDoF = " << track->nDoF()
                << " #nodes = " << nodes( *track ).size() << endmsg;
      }
    m_nTracksRejected += int( tracks.size() ) - int( selectedtracks.size() );

    // Now I got a bit worried about overlaps. Sort these tracks in the
    // number of LHCbIDs. Then remove overlapping tracks.
    std::sort( selectedtracks.begin(), selectedtracks.end(),
               []( const LHCb::Track* lhs, const LHCb::Track* rhs ) { return lhs->nLHCbIDs() > rhs->nLHCbIDs(); } );
    std::vector<const LHCb::Track*> nonoverlappingtracks;
    std::set<unsigned int>          selectedlhcbids;
    for ( const auto* track : selectedtracks ) {
      std::set<unsigned int> ids;
      for ( const auto& id : track->lhcbIDs() ) ids.insert( id.lhcbID() );
      // std::set<LHCb::LHCbID> ids( track->lhcbIDs().begin(), track->lhcbIDs().end() ) ;
      if ( ids.size() != track->lhcbIDs().size() ) {
        warning() << "Skipping track with non-unique LHCbIds. Type= " << track->type() << " " << track->history()
                  << endmsg;
      } else {
        std::set<unsigned int> allids = selectedlhcbids;
        allids.insert( ids.begin(), ids.end() );
        if ( allids.size() != selectedlhcbids.size() + ids.size() ) {
          // warning() << "Skipping track of type " << track->type()
          // << "because it overlaps with another selected track" << endmsg ;
        } else {
          nonoverlappingtracks.push_back( track );
          selectedlhcbids = allids;
        }
      }
    }
    if ( selectedtracks.size() != nonoverlappingtracks.size() ) {
      static int count( 0 );
      if ( ++count < 20 )
        info() << "Rejected " << selectedtracks.size() - nonoverlappingtracks.size() << " out of "
               << selectedtracks.size() << " tracks because of overlaps." << endmsg;
    }
    selectedtracks = nonoverlappingtracks;

    // Now deal with dimuons, vertices, if there are any.
    size_t numusedtracks( 0 );
    size_t numusedvertices( 0 );
    size_t numusedparticles( 0 );
    if ( !m_particles.objKey().empty() ) {
      LHCb::Particle::Range particles = m_particles.get();
      for ( const LHCb::Particle* p : particles ) {
        const auto res = m_data.with_lock( [&]( const Data& data ) {
          return m_vertexresidualtool->get( *p, trackResidualHelper, *data.elementProvider, *lhcb.geometry() );
        } );
        if ( res && accumulate( equations, *res ) ) {
          equations.addVertexChi2Summary( res->vertexChi2(), res->vertexNDoF() );
          ++numusedparticles;
          numusedtracks += res->numTracks();
          removeParticleTracks( *p, selectedtracks );
        }
      }
    }

    if ( !m_vertices.objKey().empty() ) {
      LHCb::RecVertex::Range vertices = m_vertices.get();

      for ( const auto& vertex : vertices ) {
        // split this vertex in vertices of the right size
        VertexContainer splitvertices;
        splitVertex( *vertex, selectedtracks, splitvertices );
        for ( auto& subver : splitvertices ) {
          const auto res = m_data.with_lock( [&]( const Data& data ) {
            return m_vertexresidualtool->get( subver, trackResidualHelper, *data.elementProvider, *lhcb.geometry() );
          } );
          if ( res && accumulate( equations, *res ) ) {
            equations.addVertexChi2Summary( res->vertexChi2(), res->vertexNDoF() );
            ++numusedvertices;
            numusedtracks += res->numTracks();
            removeVertexTracks( subver, selectedtracks );
            ++m_numTracksPerPVH1[subver.tracks().size()];
          }
        }
      }
    }

    // Loop over the remaining tracks
    for ( auto& track : selectedtracks ) {
      // this cannot return a zero pointer since we have already checked before
      const Residuals* res = m_data.with_lock(
          [&]( const Data& data ) { return trackResidualHelper.get( *track, *data.elementProvider, warning() ); } );
      assert( res != 0 );
      if ( res && accumulate( equations, *res ) ) ++numusedtracks;
    }

    auto iov = m_data.with_lock( [&]( Data& data ) { return data.elementProvider->iov(); } );

    equations.addEventSummary( numusedtracks, numusedvertices, numusedparticles, eventtime, runnr, iov );

    // Finally, add to the central accumulator
    m_data.with_lock( [&]( Data& data ) { data.equations.add( equations ); } );
  }

  bool AlignAlgorithm::accumulate( Equations& equations, const Residuals& residuals ) const {
    bool accept = false;
    if ( residuals.size() > 0 && ( residuals.nAlignables() > 1 || residuals.nExternalHits() > 0 ) ) {
      accept = true;
      for ( const auto& res : residuals.residuals() ) {
        const auto&  deriv       = res.derivative();
        ElementData& elementdata = equations.element( res.element().index() );
        elementdata.addHitSummary( res.V(), res.R(), res.state().position() );

        // add to the first derivative

        // 'alignment' outliers are not added to the first
        // derivative. However, since they have been used in the track
        // fit, they must be added to the 2nd derivative, otherwise we
        // loose the unconstrained modes. we'll solve the relative
        // normalization when we build the full linear system.
        if ( !( res.chi2() > m_chi2Outlier ) )
          // FIX ME: get rid of minus sign
          elementdata.m_dChi2DAlpha -= 1 / res.V() * res.r() * convert( deriv );
        else
          ++elementdata.m_numOutliers;

        // add V^{-1} ( V - HCH ) V^{-1} to the 2nd derivative
        Gaudi::SymMatrix6x6 tmpsym;
        ROOT::Math::AssignSym::Evaluate( tmpsym, Transpose( deriv ) * deriv );
        elementdata.m_d2Chi2DAlpha2 += ( 1 / res.V() * res.R() * 1 / res.V() ) * tmpsym;

        // compute the derivative of the track parameters, used for one of the
        // canonical constraints:
        //   dx/dalpha = - dchi^2/dalphadx * ( dchi^2/dx^2)^{-1}
        //             = - 2 * dr/dalpha * V^{-1} * H * C
        // This stil needs some work because I actually want the
        // derivative to the first state.
        //
        // Now we actually want x to be the reference state:
        //  dxref/dxi = Cref,i * Ci^{-1} -->
        //  dxref/dalpha = -2 * dr/dalpha * V^{-1} * cov( r_i, x_ref)
        // The last object on the right is already stored in the residual class
        elementdata.m_dStateDAlpha += ROOT::Math::Transpose( deriv ) * 1 / res.V() * res.residualStateCov();
        // Same for the vertex
        elementdata.m_dVertexDAlpha += ROOT::Math::Transpose( deriv ) * 1 / res.V() * res.residualVertexCov();
      }

      // add V^{-1} R V^{-1} to the 2nd derivative for the offdiagonal entries
      if ( m_correlation ) {
        for ( auto& hch : residuals.HCHElements() ) {
          assert( hch.col < hch.row );
          const auto&  rowresidual = residuals.residual( hch.row );
          const auto&  colresidual = residuals.residual( hch.col );
          size_t       rowindex    = rowresidual.element().index();
          size_t       colindex    = colresidual.element().index();
          const auto&  rowderiv    = rowresidual.derivative();
          const auto&  colderiv    = colresidual.derivative();
          const double c           = hch.val / ( rowresidual.V() * colresidual.V() );
          if ( rowindex < colindex ) {
            ElementData& elementdata = equations.element( rowindex );
            elementdata.m_d2Chi2DAlphaDBeta[colindex].add( -c * Transpose( rowderiv ) * colderiv );
          } else if ( rowindex > colindex ) {
            ElementData& elementdata = equations.element( colindex );
            elementdata.m_d2Chi2DAlphaDBeta[rowindex].add( -c * Transpose( colderiv ) * rowderiv );
          } else if ( rowindex == colindex ) {
            ElementData& elementdata = equations.element( rowindex );
            // make sure to symmetrize: add diagonal elements in both orders.
            Gaudi::Matrix6x6    tmpasym = Transpose( rowderiv ) * colderiv;
            Gaudi::SymMatrix6x6 tmpsym;
            ROOT::Math::AssignSym::Evaluate( tmpsym, tmpasym + Transpose( tmpasym ) );
            elementdata.m_d2Chi2DAlpha2 -= c * tmpsym;
          }
        }
      }

      // fill some histograms
      if ( m_fillHistos ) {
        for ( const auto& res : residuals.residuals() ) {
          const auto& deriv = res.derivative();
          double      f     = std::sqrt( res.R() / res.V() );
          double      pull  = res.r() / std::sqrt( res.R() );
          size_t      index = res.element().index();
          double      sign  = deriv( 0, 0 ) > 0 ? 1 : -1;
          ++m_elemHistos[index]->m_nHitsHisto[m_iteration.value()];
          ++m_elemHistos[index]->m_resHisto[{m_iteration.value(), sign * res.r()}];
          ++m_elemHistos[index]->m_unbiasedResHisto[{m_iteration.value(), sign * res.r() / f}];
          ++m_elemHistos[index]->m_pullHisto[{m_iteration.value(), sign * pull}];
          ++m_elemHistos[index]->m_autoCorrHisto[{m_iteration.value(), f}];
          for ( int ipar = 0; ipar < 6; ++ipar ) {
            double weight   = deriv( 0, ipar ) * f;
            double thispull = pull;
            if ( weight < 0 ) {
              weight *= -1;
              thispull *= -1;
            }
            // the last minus sign is because somebody defined our first derivative with the wrong sign
            m_elemHistos[index]->m_residualPullHistos[ipar][-thispull] += weight;
          }
        }
      }

      // keep some information about the tracks that we have seen
      equations.addChi2Summary( residuals.chi2(), residuals.nDoF(), residuals.nExternalHits() );

      std::vector<size_t> elementsOnTrack( residuals.residuals().size() );
      for ( size_t i = 0; i < residuals.residuals().size(); ++i )
        elementsOnTrack[i] = residuals.residuals()[i].element().index();
      // Make sure to remove hits in the same element. These
      // should always be consecutive. When the residuals come
      // from a vertex object, there can still be multiple tracks
      // with hist in the same elements. Hits on different tracks
      // should not be removed. Therefore, we no longer 'sort' the
      // elements before calling 'unique'.
      const auto newend = std::unique( elementsOnTrack.begin(), elementsOnTrack.end() );
      for ( auto icol = elementsOnTrack.begin(); icol != newend; ++icol ) {
        ++equations.element( *icol ).numTracks();
        // For monitoring we also keep track of the correlation
        // between hits in different alignables. We need row and
        // col in the right order here, and skip when they are
        // equal.
        for ( auto irow = elementsOnTrack.begin(); irow != icol; ++irow ) {
          auto [r, c] = std::minmax( *irow, *icol );
          if ( r != c ) ++equations.element( r ).m_d2Chi2DAlphaDBeta[c].numTracks();
        }
      }
    }
    return accept;
  }

} // namespace LHCb::Alignment
