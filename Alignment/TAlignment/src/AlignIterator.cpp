/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "AlignKernel/AlEquations.h"
#include "AlignmentInterfaces/IAlignUpdateTool.h"
#include "DetDesc/IDetectorElement.h"
#include "Kernel/SynchronizedValue.h"
#include "TAlignment/GetElementsToBeAligned.h"

#include "LHCbAlgs/Consumer.h"

#include <string>
#include <vector>

namespace LHCb::Alignment {

  // helper using to design the base detectorElementClass, as it's different
  // for DDDB and DD4hep
#ifdef USE_DD4HEP
  using GenericDetElem = LHCb::Detector::DeIOV;
#else
  using GenericDetElem = ::DetectorElementPlus;
#endif

  class AlignIterator : public Algorithm::Consumer<void( const EventContext&, GenericDetElem const& ),
                                                   LHCb::DetDesc::usesConditions<GenericDetElem>> {

  public:
    AlignIterator( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, {KeyValue{"DELHCbPath", LHCb::standard_geometry_top}} ) {}

    void operator()( const EventContext&, const GenericDetElem& lhcb ) const override {
      // FIXME XXX : we should check that the ConditionContext does not change
      // If it does, we should throw an exception and stop processing
      // The risk is to mix data from different runs by mistake and screw the alignment procedure
      // Note that the check would have to be thread safe like the initialization of m_elements
      // insert a lock here to ensure thread-safety until the code is rewritten
      info() << "starting iterator" << endmsg;
      m_elementProvider.with_lock( [&]( std::unique_ptr<GetElementsToBeAligned>& ep ) {
        if ( ep ) return;
        ep = std::make_unique<GetElementsToBeAligned>( m_elemsToBeAligned.value(), lhcb, true, warning(), detSvc() );
        ep->initAlignmentFrame();
      } );
    }

    StatusCode stop() override {
      return m_elementProvider.with_lock( [&]( std::unique_ptr<GetElementsToBeAligned>& ep ) {
        if ( !ep ) {
          throw GaudiException( "ElementProvider not initialized when AlignIterator::stop was called", "AlignIterator",
                                StatusCode::FAILURE );
        }
        int               iteration         = 0;
        int               maxIteration      = 0;
        ConvergenceStatus convergencestatus = ConvergenceStatus::Unknown;
        Equations         equations( 0 );
        info() << "AlignIterator::stop: reading derivative file " << m_derivativeFile.value() << endmsg;
        const auto [readerSc, readerMsg] = equations.readFromFile( m_derivativeFile.value().c_str() );
        if ( readerSc == StatusCode::SUCCESS ) {
          info() << readerMsg << endmsg;
        } else {
          error() << "AlignIterator::stop: could not read file " << m_derivativeFile.value() << endmsg;
          error() << readerMsg << endmsg;
        }
#ifdef USE_DD4HEP
        m_updatetool->process( equations, *ep, iteration, maxIteration, convergencestatus, getDataSvc() ).ignore();

        getDataSvc().write_alignments( "iteratorymltest" );
        return StatusCode::SUCCESS;
#else
        return m_updatetool->process( equations, *ep, iteration, maxIteration, convergencestatus ).andThen( [&] {
          ep->writeAlignmentConditions( m_xmlWriters.value(), warning() );
        } );
#endif
      } );
    }

  private:
    ToolHandle<IAlignUpdateTool>              m_updatetool{this, "UpdateTool", "AlignUpdateTool"};
    Gaudi::Property<std::vector<std::string>> m_elemsToBeAligned{this, "Elements", {}, "Path to elements"};

    Gaudi::Property<std::string> m_derivativeFile{this, "DerivativeFile", "testderivs",
                                                  "path to ASD file with derivatives"};

    /// The GetElementsToBeAligned instance holding alignment data
    mutable LHCb::cxx::SynchronizedValue<std::unique_ptr<GetElementsToBeAligned>> m_elementProvider;
#ifndef USE_DD4HEP
    Gaudi::Property<std::vector<XmlWriter>> m_xmlWriters{
        this, "XmlWriters", {}, "Definitions of the XmlWriters handling the output"};

#endif
  };

  DECLARE_COMPONENT_WITH_ID( AlignIterator, "AlignIterator" )

} // namespace LHCb::Alignment
