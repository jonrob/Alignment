/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TAlignment/GetElementsToBeAligned.h"
#include "AlignKernel/AlEquations.h"
#include "AlignKernel/DetectorElement.h"
#include "TAlignment/AlignmentElement.h"

#include "FTDet/DeFTDetector.h"
#include "MuonDet/DeMuonDetector.h"
#include "UTDet/DeUTDetector.h"
#include "VPDet/DeVP.h"

#include "DetDesc/CLIDAlignmentCondition.h"
#include "DetDesc/Param.h"
#include "Event/FitNode.h"
#include "Event/Measurement.h"
#include "Event/PrFitNode.h"

#include "boost/regex.hpp"
#include "boost/tokenizer.hpp"

#include "fmt/format.h"

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <list>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

using Separator = boost::char_separator<char>; ///< Char separator
using Tokenizer = boost::tokenizer<Separator>; ///< List of tokens
using RegExs    = std::list<boost::regex>;     ///< List of regular expressions

//-----------------------------------------------------------------------------
// Implementation file for class : GetElementsToBeAligned
//
// 2007-10-08 : Jan Amoraal
//-----------------------------------------------------------------------------

namespace {

  void replace( std::string& conString, std::string in, std::string out ) {
    std::string::size_type pos = 0;
    while ( pos != std::string::npos ) {
      pos = conString.find( in, pos );
      if ( pos != std::string::npos ) { conString.replace( pos, in.size(), out ); }
    } // pos
  }

  void replaceChars( std::string& conString ) {
    std::string blank = " ";
    replace( conString, ",", blank );
    replace( conString, "\"/", "\"" );
  }

#ifdef USE_DD4HEP
  inline std::ostream& operator<<( std::ostream& s, const std::array<double, 3>& v ) {
    return s << v[0] << " " << v[1] << " " << v[2];
  }

  std::string toXMLStr( std::string_view name, int precision, std::array<double, 3> const& val ) {
    std::ostringstream o;
    o.precision( precision );
    o << "<paramVector name=\"" << name << "\" type=\"double\">" << val << "</paramVector>";
    return o.str();
  }

  std::string conditionAsXML( const LHCb::Detector::DeIOV& cond, int precision = 16 ) {
    std::ostringstream xml;
    xml << "<condition classID=\"" << cond.clsID() << "\" name=\"" << cond.detector().name() << "\">";
    auto                  d = cond.delta();
    std::array<double, 3> pparam( {d.pivot.Vect().x(), d.pivot.Vect().y(), d.pivot.Vect().z()} );
    std::array<double, 3> tparam( {d.translation.x(), d.translation.y(), d.translation.z()} );
    std::array<double, 3> rparam( {d.rotation.Psi(), d.rotation.Theta(), d.rotation.Phi()} );
    xml << toXMLStr( "pivotXYZ", precision, pparam ) << toXMLStr( "dPosXYZ", precision, tparam )
        << toXMLStr( "dRotXYZ", precision, rparam ) << "</condition>";
    return xml.str();
  }
#endif

  void applyToAllChildren( const LHCb::Alignment::DetectorElement&                        element,
                           std::function<void( const LHCb::Alignment::DetectorElement& )> func,
                           MsgStream&                                                     warningstream ) {
    const auto cname = element.alignmentConditionName();
    if ( ( cname.find( "Rich" ) == std::string::npos ) && ( cname.find( "Ecal" ) == std::string::npos ) &&
         ( cname.find( "Hcal" ) == std::string::npos ) ) {
      try {
        element.applyToAllChildren( func );
      } catch ( const std::exception& ex ) {
        warningstream << "GetElementsToBeAligned caught exception:" << cname << " : " << ex.what() << endmsg;
      }
    }
  }

  void writeXmlForChildren( const LHCb::Alignment::DetectorElement& parent, std::ofstream& out,
                            const std::vector<unsigned int>& depths, unsigned int depth, unsigned int precision,
                            bool removePivot, MsgStream& warningstream ) {
    const auto* aCon = parent.alignmentCondition();
    if ( aCon ) {
      bool wanted = false;
      for ( unsigned int i = 0; i < depths.size(); i++ ) { // check if current depth is wanted for output
        if ( depth == depths[i] ) wanted = true;
      }
      if ( 0 == depths.size() ) wanted = true; // in case of empty list print all levels
      if ( wanted ) {
#ifdef USE_DD4HEP
        std::string temp = conditionAsXML( *aCon, precision );
#else
        // if we want to remove the pivot point, first set it to zero
        removePivot = removePivot && aCon->exists( "pivotXYZ" );
        if ( removePivot ) ( const_cast<AlignmentCondition*>( aCon ) )->setPivotPoint( Gaudi::XYZPoint() );
        // now get the condition as a single string
        std::string temp = aCon->toXml( "", false, precision );
#endif
        // now remove the pivot point from the output string
        if ( removePivot ) {
          static const std::string stringToRemove =
              "<paramVector name=\"pivotXYZ\" type=\"double\">0 0 0</paramVector>";
          size_t pos = temp.find( stringToRemove );
          if ( pos != std::string::npos ) temp.replace( pos, stringToRemove.size(), "" );
        }
        replaceChars( temp );
        out << temp << std::endl;
      }
    }
    depth++;
    applyToAllChildren(
        parent,
        [&]( const LHCb::Alignment::DetectorElement& child ) {
          writeXmlForChildren( child, out, depths, depth, precision, removePivot, warningstream );
        },
        warningstream );
  }

  /// determine if an AlignmentElement fully overlaps with det elements in a container (usually the mother)
  template <typename DetElementContainer, typename AlignmentElement>
  bool isOffspring( const DetElementContainer& mother_elementsInTree, const AlignmentElement& daughter ) {
    return std::all_of( begin( daughter.detelements() ), end( daughter.detelements() ),
                        [&mother_elementsInTree]( auto& elem ) {
                          return std::find( mother_elementsInTree.begin(), mother_elementsInTree.end(), elem ) !=
                                 mother_elementsInTree.end();
                        } );
  }

  /// create a flat container for an entire detector element tree
  template <typename DetElementContainer>
  void addToElementsInTree( const LHCb::Alignment::DetectorElement& element, DetElementContainer& elements,
                            MsgStream& warningstream ) {
    elements.push_back( element );
    elements.reserve( elements.size() + element.size() );
    applyToAllChildren(
        element,
        [&]( const LHCb::Alignment::DetectorElement& child ) { addToElementsInTree( child, elements, warningstream ); },
        warningstream );
  }

  void getElements( const LHCb::Alignment::DetectorElement detelem, const RegExs& regexs, size_t depth,
                    std::vector<LHCb::Alignment::DetectorElement>& detelements, MsgStream& warningstream ) {
    /// split path on "/" and copy to a temporary list of sub paths. This allows us to determine how deep we are.
    const std::string        path = detelem.name();
    Tokenizer                tokenizer( path, Separator( "/" ) );
    std::vector<std::string> paths;
    paths.reserve( std::count( path.begin(), path.end(), '/' ) + 1 );
    std::copy( tokenizer.begin(), tokenizer.end(), std::back_inserter( paths ) );
    const size_t currentDepth = paths.size();
    /// Can we match a sub path to a regular expression?
    bool match = true;
    /// Loop over list of sub paths and try to match them
    // If it doesn't match break loop. No need to try and match the rest
    for ( auto [iR, iRend, i, iEnd] = std::tuple{regexs.begin(), regexs.end(), paths.begin(), paths.end()};
          i != iEnd && iR != iRend && match; ++i, ++iR ) {
      match = boost::regex_match( *i, *iR );
    }
    /// OK we found a detector element
    if ( match && currentDepth == depth ) { detelements.push_back( detelem ); }
    if ( paths.size() < depth ) {
      /// loop over children
      applyToAllChildren(
          detelem,
          [&]( const LHCb::Alignment::DetectorElement& child ) {
            getElements( child, regexs, depth, detelements, warningstream );
          },
          warningstream );
    }
  }
} // namespace

LHCb::Alignment::GetElementsToBeAligned::GetElementsToBeAligned( LHCb::span<const std::string> elemsToBeAligned,
                                                                 const DetectorElement&        topOfGeometry,
                                                                 bool useLocalFrame, MsgStream& warningStream,
                                                                 SmartIF<IDataProviderSvc>& detSvc )
    : m_detSvc( detSvc ), m_topOfGeometry( topOfGeometry ) {
  size_t index( 0 );
  auto&  alignelements = m_elements;
  for ( const auto& elemToBeAligned : elemsToBeAligned ) {
    /// Split string into path to det elem regex and dofs to align
    bool        groupElems = false;
    std::string groupname, path, dofs;

    // first : second : third
    // tokens of strings
    std::vector<std::string> tokens;
    for ( const auto& j : Tokenizer( elemToBeAligned, Separator( " :" ) ) ) tokens.push_back( j );

    if ( tokens.size() == 2 ) {
      path = tokens.at( 0 );
      dofs = tokens.at( 1 );
    } else if ( tokens.size() == 3 ) {
      groupElems = true;
      path       = tokens.at( 1 );
      dofs       = tokens.at( 2 );
      groupname  = tokens.at( 0 );
      if ( groupname.find( "Group" ) != std::string::npos ) groupname = path;
    } else {
      std::ostringstream s;
      s << "==> There is something wrong with the specified property Elements: " << tokens.size() << "\n"
        << elemToBeAligned;
      throw GaudiException( s.str(), "GetElementsToBeAligned", StatusCode::FAILURE );
    }

    // Traverse LHCb detector in transient store and get alignable elements
    std::vector<DetectorElement> detelements;
    // Break path into a set of regexs. Forward slash is the path separator
    Tokenizer elemtokenizer( path, Separator( "/" ) );
    RegExs    regexs;

    // Create list of regular expressions
    for ( const auto& token : elemtokenizer ) {
      boost::regex ex;
      // Check if token is a valid regular expression, else catch exception and return statuscode failure.
      // Better to stop the program and let the user fix the expression than trying to predict what he/she wants.
      try {
        ex.assign( token, boost::regex_constants::icase );
      } catch ( boost::bad_expression& exprs ) {
        throw GaudiException(
            format( "==> Error: %s is not a valid regular expression: %s", token.c_str(), exprs.what() ),
            "GetElementsToBeAligned", StatusCode::FAILURE );
      }
      // If valid add expression to list of expressions.
      regexs.push_back( ex );
    }

    // Depth is equal to the number regular expressions in the regex list.
    getElements( topOfGeometry, regexs, regexs.size(), detelements, warningStream );

    // Check that we have found elements to align, else exit gracefully
    if ( detelements.empty() ) {
      std::ostringstream s;
      s << "\n ==> Couldn't find any elements that matched the given regular expression! \n"
        << " expression=\'" << elemToBeAligned << "\'\n"
        << " ==> The syntax of the property Elements is a list of : \"Group : Regex representing path of det "
           "elems : dofs\" \n"
        << " ==> where group and dofs are optional.\n"
        << " ==> Check the regular expression and also check if there are no whitespaces.";
      throw GaudiException( s.str(), "GetElementsToBeAligned", StatusCode::FAILURE );
    }

    // Loop over elements and create Alignment::Elements
    if ( groupElems ) {
      // first check that there isn't already a group with this name. if there is, add the elements.
      auto ielem = std::find_if( alignelements.begin(), alignelements.end(),
                                 [&groupname]( const auto* ie ) { return ie->name() == groupname; } );
      if ( ielem != alignelements.end() ) {
        ( *ielem )->addElements( detelements );
        ( *ielem )->addDofs( dofs );
      } else
        alignelements.push_back( new Element( groupname, detelements, index++, dofs, useLocalFrame ) );
    } else {
      for ( auto elem : detelements ) {
        // check that there isn't already a group with this name. if there is, only set the dofs
        std::string name  = Element::stripElementName( elem.name() );
        auto        jelem = std::find_if( alignelements.begin(), alignelements.end(),
                                   [&name]( const auto* je ) { return je->name() == name; } );
        if ( jelem != alignelements.end() ) {
          warningStream << "Multiple specifications of dofs for alignable " << name << ". Using '" << dofs << "'."
                        << endmsg;
          ( *jelem )->setDofs( dofs );
        } else {
          alignelements.push_back( new Element( elem, index++, dofs, useLocalFrame ) );
        }
      }
    }
  }

  // sort the elements by hierarchy. currently, this just follows the
  // name of the first associated detector element.
  std::stable_sort( alignelements.begin(), alignelements.end(), []( const auto& lhs, const auto& rhs ) {
    return ( lhs->basename() < rhs->basename() ) ||
           ( lhs->basename() == rhs->basename() &&
             ( lhs->detelements().front().name() < rhs->detelements().front().name() ) );
  } );

  // make sure to reset the indices after sorting
  for ( auto&& [i, elem] : LHCb::range::enumerate( alignelements ) ) elem->setIndex( i );

  // set up the mother-daughter relations. This only works once the
  // elements are sorted. We start with the lowest level elements, and
  // work our way up the tree.

  // first for every alignment element create a temporary flat container with all elements in the tree
  std::vector<Alignment::Element::ElementContainer> elementsInTree( alignelements.size() );
  for ( auto&& [i, elem] : LHCb::range::enumerate( alignelements ) )
    for ( const auto& detelem : elem->detelements() ) addToElementsInTree( detelem, elementsInTree[i], warningStream );
  // now use that to find the mothers
  for ( auto ielem = alignelements.rbegin(); alignelements.rend() != ielem; ++ielem ) {
    // is 'i' a daughter of 'j'
    auto jelem = std::find_if( std::next( ielem ), alignelements.rend(), [ielem, elementsInTree]( const auto& e ) {
      return isOffspring( elementsInTree[e->index()], **ielem );
    } );
    if ( jelem != alignelements.rend() ) ( *jelem )->addDaughter( **ielem );
  }

  // now fill the element map. this map is used to assign a certain
  // hit to a certain alignment element. again, it is important that
  // alignment elements are sorted: we add the hit only to the lowest
  // level element in the tree.
  for ( const auto* ielem : alignelements ) {
    for ( auto& detelem : elementsInTree[ielem->index()] ) {
      m_elementMap[detelem] = ielem;

      const auto vpsensorobject = detelem.castTo<DeVPSensor>();
      if ( vpsensorobject ) m_vpidmap.at( int( vpsensorobject->sensorNumber() ) ) = ielem;

      const auto utsensorobject = detelem.castTo<DeUTSector>();
      if ( utsensorobject ) m_utidmap[int( utsensorobject->elementID().uniqueSector() )] = ielem;

      const auto ftmoduleobject = detelem.castTo<DeFTModule>();
      if ( ftmoduleobject ) m_ftidmap.at( int( ftmoduleobject->elementID().globalModuleIdx() ) ) = ielem;

      const auto muonpadobject = detelem.castTo<DeMuonChamber>();
      if ( muonpadobject ) m_muonidmap.at( int( muonpadobject->chamberNumber() ) ) = ielem;
    }
  }
}

LHCb::Alignment::GetElementsToBeAligned::~GetElementsToBeAligned() {
  for ( const auto* alignable : m_elements ) { delete alignable; }
}

LHCb::Alignment::Elements LHCb::Alignment::GetElementsToBeAligned::findElements( const std::string& path ) const {
  Elements alignElements;
  std::copy_if( m_elements.begin(), m_elements.end(), std::back_inserter( alignElements ),
                [&, ex = boost::regex{path, boost::regex_constants::icase}]( const auto* ialelem ) {
                  return ialelem->name() == path || boost::regex_match( ialelem->name(), ex ) ||
                         // if any detector element matches, this is also fine
                         std::any_of( ialelem->detelements().begin(), ialelem->detelements().end(),
                                      [&]( auto elem ) { return boost::regex_match( elem.name(), ex ); } );
                } );
  return alignElements;
}

const LHCb::Alignment::Element*
LHCb::Alignment::GetElementsToBeAligned::findElement( const LHCb::FitNode& node ) const {
  return findElement( node.measurement().lhcbID() );
}

const LHCb::Alignment::Element*
LHCb::Alignment::GetElementsToBeAligned::findElement( const LHCb::Pr::Tracks::Fit::Node& node ) const {
  return findElement( node.lhcbID );
}

const LHCb::Alignment::Element* LHCb::Alignment::GetElementsToBeAligned::findElement( const LHCb::LHCbID id ) const {
  switch ( id.detectorType() ) {
  case LHCb::LHCbID::channelIDtype::VP:
    return m_vpidmap.at( int( id.vpID().sensor() ) );
  case LHCb::LHCbID::channelIDtype::UT:
    // FIXME: At some point we need here also the element of the trajectory: see run2_patches
    {
      auto it = m_utidmap.find( int( id.utID().uniqueSector() ) );
      return it != m_utidmap.end() ? it->second : nullptr;
    }
  case LHCb::LHCbID::channelIDtype::FT:
    return m_ftidmap.at( int( id.ftID().globalModuleIdx() ) );
  case LHCb::LHCbID::channelIDtype::Muon:
    return m_muonidmap.at( int( id.muonID() ) );
  default:
    return nullptr;
  }
}

const LHCb::Alignment::Element*
LHCb::Alignment::GetElementsToBeAligned::findElement( const DetectorElement& de ) const {
  auto it = m_elementMap.find( de );
  return it == m_elementMap.end() ? nullptr : it->second;
}

void LHCb::Alignment::GetElementsToBeAligned::initEquations( LHCb::Alignment::Equations& equations ) const {
  equations = Equations( m_elements.size(), equations.initTime() );
  for ( const auto& ielem : m_elements ) {
    // set the current delta (called alpha in Equations)
    auto& elemdata = equations.element( ielem->index() );
    elemdata.setAlpha( ielem->currentDelta().transformParameters() );
    elemdata.setAlignFrame( AlParameters( ielem->alignmentFrame() ).transformParameters() );
  }
}

void LHCb::Alignment::GetElementsToBeAligned::initAlignmentFrame() {
  // update all elements with this time
  for ( auto* elem : m_elements ) { elem->initAlignmentFrame(); }
  m_isInitialized = true;
}

std::map<std::string, double> LHCb::Alignment::GetElementsToBeAligned::getAlignmentConstants() const {
  // this method returns a map of the alignment constant of the kind
  //   - < name.dof, val >
  std::map<std::string, double> alConsts;
  for ( const auto& elem : m_elements ) {
    /// Get translations and rotations
    const auto& translations                      = elem->deltaTranslations();
    const auto& rotations                         = elem->deltaRotations();
    alConsts[elem->name() + std::string( ".Tx" )] = translations[0];
    alConsts[elem->name() + std::string( ".Ty" )] = translations[1];
    alConsts[elem->name() + std::string( ".Tz" )] = translations[2];
    alConsts[elem->name() + std::string( ".Rx" )] = rotations[0];
    alConsts[elem->name() + std::string( ".Ry" )] = rotations[1];
    alConsts[elem->name() + std::string( ".Rz" )] = rotations[2];
  } // i in elements
  return alConsts;
}

LHCb::Alignment::DetectorElement LHCb::Alignment::GetElementsToBeAligned::getDet( std::string const& path ) const {
  if ( path.rfind( m_topOfGeometry.name(), 0 ) != 0 ) {
    throw GaudiException( "WriteAlignmentConditions : unable to find requested element " + path +
                              " as it's not a subpath of top element " + m_topOfGeometry.name(),
                          "GetElementsToBeAligned:", StatusCode::FAILURE );
  }
  // find path in the children of topOfGeometry
  auto det = m_topOfGeometry;
  auto sub = path.substr( m_topOfGeometry.name().size() );
  for ( auto& part : Tokenizer( sub, Separator( "/" ) ) ) { det = det.getChild( part ); }
  return det;
}

void LHCb::Alignment::GetElementsToBeAligned::writeAlignmentConditions( const XmlWriter& writer,
                                                                        MsgStream&       warningStream ) const {
  auto det = getDet( writer.topElement );
  // make output dir if necessary
  size_t pos = writer.outputFileName.find_last_of( '/' );
  if ( pos != std::string::npos ) std::filesystem::create_directories( writer.outputFileName.substr( 0, pos ) );

  std::ofstream outputFile( writer.outputFileName );
  if ( outputFile.fail() ) {
    warningStream << "Failed to open output file" << endmsg;
    return;
  }

  // header
  if ( !writer.online ) {
    outputFile << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"
               << "<!DOCTYPE DDDB SYSTEM \"conddb:/DTD/structure.dtd\">"
               << "<DDDB>";
  }
  writeXmlForChildren( det, outputFile, writer.depths, 0, writer.precision, writer.removePivot, warningStream );
  if ( !writer.online ) outputFile << "</DDDB>";
  outputFile << std::endl;
}

void LHCb::Alignment::GetElementsToBeAligned::writeAlignmentConditions( const std::vector<XmlWriter>& xmlWriters,
                                                                        MsgStream& warningStream ) const {
  for ( auto& w : xmlWriters ) { writeAlignmentConditions( w, warningStream ); }
}
