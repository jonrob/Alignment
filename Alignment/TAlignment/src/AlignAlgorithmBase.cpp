/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TAlignment/AlignAlgorithmBase.h"

#include "AlignKernel/AlEquations.h"
#include "TAlignment/AlParameters.h"
#include "TAlignment/AlignmentElement.h"

#include "Kernel/LHCbID.h"
#include "TrackKernel/TrackFunctors.h"

#include "DetDesc/IDetectorElement.h"
#include "Event/TwoProngVertex.h"
#include "VPDet/DeVP.h"

#include "GaudiKernel/IDetDataSvc.h"

#include <algorithm>

namespace LHCb::Alignment {
  AlignAlgorithmBase::AlignAlgorithmBase( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, {KeyValue{"DELHCbPath", LHCb::standard_geometry_top}} ) {}

  StatusCode AlignAlgorithmBase::initialize() {
    return Consumer::initialize().andThen( [&]() {
      if ( m_histoRefFileName.empty() ) {
        m_histoRefFileName = "/group/online/dataflow/options/" + m_histoPartitionName + "/Alignment_Reference_File.txt";
      }

      // set initial time. From this moment on geometry and conditions are
      // bound to this time
#ifndef USE_DD4HEP
      IDetDataSvc* detDataSvc( 0 );
      StatusCode   sc = service( "DetectorDataSvc", detDataSvc, false );
      if ( sc.isFailure() ) {
        error() << "Could not retrieve DetectorDataSvc" << endmsg;
        return;
      }
      info() << "Updating detector data svc and update manager svc with time: " << m_initialTime.value() << std::endl;
      detDataSvc->setEventTime( m_initialTime.value() );
#endif

      info() << "Use correlations = " << m_correlation << endmsg;
    } );
  }

  void AlignAlgorithmBase::initializeGetElementsToBeAligned( const GenericDetElem& lhcb ) const {
    m_data.with_lock( [&]( Data& data ) {
      if ( !data.elementProvider ) {
        info() << "Use local frame = " << m_useLocalFrame.value() << endmsg;
        info() << "===================== GetElementsToAlign =====================" << endmsg;
        data.elementProvider     = std::make_unique<GetElementsToBeAligned>( m_elemsToBeAligned.value(), lhcb,
                                                                         m_useLocalFrame.value(), warning(), detSvc() );
        const Elements& elements = data.elementProvider->elements();
        info() << "   Going to align " << elements.size() << " detector elements:" << endmsg;
        info() << "==============================================================" << endmsg;
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "==> Got " << elements.size() << " elements to align!" << endmsg;
          for ( auto& element : elements ) {
            const auto& ownDoFMask = element->dofMask();
            debug() << "        " << ( *element ) << endmsg;
            const std::vector<std::string> dofs = {"Tx", "Ty", "Tz", "Rx", "Ry", "Rz"};
            debug() << "DOFs: ";
            for ( auto j = ownDoFMask.begin(), jEnd = ownDoFMask.end(); j != jEnd; ++j ) {
              if ( ( *j ) ) info() << dofs.at( std::distance( ownDoFMask.begin(), j ) );
            }
            debug() << endmsg;
          }
        }
        if ( m_fillHistos ) {
          info() << "booking histograms assuming " << m_nIterations << " iterations " << endmsg;
          for ( auto& element : data.elementProvider->elements() )
            m_elemHistos.emplace_back( std::make_unique<AlElementHistos>( this, *element, m_nIterations ) );
          m_resetHistos = false;
        }
        if ( m_Online ) { m_HistoUpdater.setMonitorService( monitorSvc() ); }
      }
      if ( !data.elementProvider->isInitialized() ) {
        data.elementProvider->initAlignmentFrame();
        data.equations.clear();
        data.elementProvider->initEquations( data.equations );
        info() << "Initializing alignment frames with time: " << data.equations.initTime().ns() << " --> "
               << data.equations.initTime().format( true, "%F %r" ) << endmsg;
      }
    } );
  }

  StatusCode AlignAlgorithmBase::finalize() {
    if ( m_updateInFinalize && !m_Online ) update();

    if ( m_Online ) { m_HistoUpdater.m_MonSvc = 0; }
    return Consumer::finalize();
  }

  StatusCode AlignAlgorithmBase::stop() {
    if ( m_Online ) {
      if ( m_RunList.size() > 0 ) {
        if ( m_RunList[0] != "*" ) { m_HistoUpdater.Update( ::atoi( m_RunList[0].c_str() ), m_histoRefFileName ); }
      }
    }
    if ( !m_outputDataFileName.empty() )
      m_data.with_lock( [&]( const Data& data ) {
        const auto [tmp_numevents, tmp_buffersize] = data.equations.writeToFile( m_outputDataFileName.value().c_str() );
        info() << "Equations::writeToFile: wrote " << tmp_numevents << " events in " << tmp_buffersize
               << " bytes to file " << m_outputDataFileName.value().c_str() << endmsg;
      } );
    return StatusCode::SUCCESS;
  }

  StatusCode AlignAlgorithmBase::start() {
    StatusCode sc;
    if ( m_Online || m_iteration.value() > 1 ) {
      // reset contents of ASD
      reset();
    }
    return StatusCode::SUCCESS;
  }

  //=============================================================================
  //  Update
  //=============================================================================
  void AlignAlgorithmBase::update() {
    info() << "Total number of tracks: " << m_nTracks << endmsg;
    info() << "Number of covariance calculation failures: " << m_covFailure << endmsg;
    if ( !m_inputDataFileNames.empty() ) {
      m_data.with_lock( [&]( Data& data ) {
        auto& equations = data.equations;
        for ( const auto& ifile : m_inputDataFileNames ) {
          Equations tmp( 0 );
          const auto [readerSc, readerMsg] = tmp.readFromFile( ifile.c_str() );
          if ( readerSc == StatusCode::SUCCESS ) {
            info() << readerMsg << endmsg;
          } else {
            error() << "AlignAlgorithmBase::update: could not read file " << ifile << endmsg;
            error() << readerMsg << endmsg;
          }
          if ( equations.numEvents() == 0 )
            equations = tmp;
          else {
            const auto msgMap = equations.add( tmp );
            for ( auto const& msgTpl : msgMap ) {
              auto msgType = msgTpl.first;
              auto tmpMsg  = msgTpl.second;
              if ( msgType == "info" ) info() << tmpMsg << endmsg;
              if ( msgType == "warning" ) warning() << tmpMsg << endmsg;
              if ( msgType == "error" ) error() << tmpMsg << endmsg;
            }
          }
          warning() << "Adding derivatives from input file: " << ifile << " " << tmp.numHits() << " "
                    << tmp.totalChiSquare() << " " << data.equations.totalChiSquare() << endmsg;
        }
      } );
    }
    if ( !m_outputDataFileName.empty() )
      m_data.with_lock( [&]( const Data& data ) {
        const auto [tmp_numevents, tmp_buffersize] = data.equations.writeToFile( m_outputDataFileName.value().c_str() );
        info() << "Equations::writeToFile: wrote " << tmp_numevents << " events in " << tmp_buffersize
               << " bytes to file " << m_outputDataFileName.value().c_str() << endmsg;
      } );
    // Only call update if something was processed or we will fail
    // trying to read the IOV from an empty TES
    if ( m_nTracks.nEntries() > 0 ) { update( m_iteration.value(), m_nIterations ); }
  }

  void AlignAlgorithmBase::updateGeometry( size_t iter, size_t maxiter ) {
    if ( m_data.with_lock( []( const Data& data ) { return data.elementProvider == nullptr; } ) ) {
      throw GaudiException( "ElementProvider not initialized when updateGeometry was called", "AlignAlgorithm",
                            StatusCode::FAILURE );
    }
    update( iter, maxiter );
  }

  void AlignAlgorithmBase::clearEquationsAndReloadGeometry() {
    if ( m_data.with_lock( []( const Data& data ) { return data.elementProvider == nullptr; } ) ) {
      throw GaudiException( "ElementProvider not initialized when clearEquationsAndReloadGeometry was called",
                            "AlignAlgorithm", StatusCode::FAILURE );
    }
    reset();

#ifdef USE_DD4HEP_NOT_USED
    // No longer used but I'd like to keep this example because we may want to move the iov check into the event loop
    //
    // drop current slice from the cache. It will be reloaded when recreating m_elementProvider
    getDataSvc().drop_slice( data.elementProvider->iov() );
    // recreate m_elementProvider based on new geometry
    const auto& ctx  = getDataSvc().get_slice( data.elementProvider->iov() );
    const auto& lhcb = std::get<0>( m_inputs ).get( ctx );
    initializeGetElementsToBeAligned( lhcb );
    m_updatetool->resetLagrangeConstraintHelper();
#endif
  }

#ifdef USE_DD4HEP
  void AlignAlgorithmBase::loadAlignmentConditions( std::string in_dir ) {
    m_data.with_lock( [&]( Data& data ) {
      debug() << "Loading new alignment conditions from directory " << in_dir << endmsg;
      getDataSvc().load_alignments( in_dir );
      getDataSvc().clear_slice_cache();
      data.elementProvider.reset();
    } );
  }

  void AlignAlgorithmBase::writeAlignmentConditions( std::string out_dir ) {
    if ( m_data.with_lock( []( const Data& data ) { return data.elementProvider == nullptr; } ) ) {
      throw GaudiException( "ElementProvider not initialized when writeAlignmentConditions was called",
                            "AlignAlgorithm", StatusCode::FAILURE );
    }

    m_data.with_lock( [&]( Data& ) { getDataSvc().write_alignments( out_dir ); } );
  }

#else

  void AlignAlgorithmBase::writeAlignmentConditions() const {
    m_data.with_lock( [&]( Data const& data ) {
      if ( !data.elementProvider )
        throw GaudiException( "ElementProvider not initialized when writeAlignmentConditions was called",
                              "AlignAlgorithm", StatusCode::FAILURE );
      data.elementProvider->writeAlignmentConditions( m_xmlWriters.value(), warning() );
    } );
  }
#endif

  auto convergencestatus = ConvergenceStatus();
  void AlignAlgorithmBase::update( size_t iter, size_t maxiter ) {
    m_data.with_lock( [&]( Data& data ) {
      m_updatetool
          ->process( data.equations, *data.elementProvider, iter, maxiter, convergencestatus
#ifdef USE_DD4HEP
                     ,
                     getDataSvc()
#endif
                         )
          .ignore();
    } );
  }

  void AlignAlgorithmBase::reset() {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "increasing iteration counter and resetting accumulated data..." << endmsg;
    /// increment iteration counter
    ++m_iteration;
    // set counters to zero
    m_nTracks.reset();
    m_covFailure.reset();
    // clear derivatives. update the parameter vectors used for bookkeeping.
    m_data.with_lock( []( Data& data ) {
      if ( data.elementProvider ) data.elementProvider->resetInitialized();
      data.equations.clear();
    } );
    // clear the histograms on the next execute call
    m_resetHistos = true;
  }

  void AlignAlgorithmBase::resetHistos() const {
    m_resetHistos = false;
    // moved this seperately such that histograms are not reset on last iteration
    if ( m_fillHistos ) {
      std::for_each( m_elemHistos.begin(), m_elemHistos.end(), []( auto& h ) { h.reset(); } );
    }
  }

  namespace {
    void extractTracks( const LHCb::Particle& p, std::vector<const LHCb::Track*>& tracks ) {
      if ( p.proto() && p.proto()->track() ) tracks.push_back( p.proto()->track() );
      for ( const LHCb::Particle* dau : p.daughters() ) extractTracks( *dau, tracks );
    }

    struct CompareLHCbIds {
      bool operator()( const LHCb::LHCbID& lhs, const LHCb::LHCbID& rhs ) const { return lhs.lhcbID() < rhs.lhcbID(); }
    };

    struct TrackClonePredicate {
      const LHCb::Track* lhs;
      TrackClonePredicate( const LHCb::Track* tr ) : lhs( tr ) {}
      bool operator()( const LHCb::Track* rhs ) const {
        // either it is the same tracks, or all LHCbIDs of rhs appear in lhs or vice versa
        return rhs == lhs || lhs->nCommonLhcbIDs( *rhs ) == std::min( lhs->lhcbIDs().size(), rhs->lhcbIDs().size() );
      }
    };

    struct CompareTypeAndSide {
      int direction( const LHCb::Track& track ) const { return track.isVeloBackward() ? -1 : 1; }
      int side( const LHCb::Track& track ) const {
        int txside = track.firstState().tx() > 0 ? 1 : -1;
        return txside * direction( track );
      }
      bool operator()( const LHCb::Track* lhs, const LHCb::Track* rhs ) const {
        return lhs->type() < rhs->type() ||
               ( lhs->type() == rhs->type() &&
                 ( side( *lhs ) < side( *rhs ) ||
                   ( side( *lhs ) == side( *rhs ) && direction( *lhs ) < direction( *rhs ) ) ) );
      }
    };

    template <class TYPE>
    struct IsEqual {
      const TYPE* m_p;
      IsEqual( const TYPE& ref ) : m_p( &ref ) {}
      bool operator()( const SmartRef<TYPE>& lhs ) { return &( *lhs ) == m_p; }
    };
  } // namespace

  void AlignAlgorithmBase::selectVertexTracks( const LHCb::RecVertex& vertex, const TrackContainer& tracks,
                                               TrackContainer& tracksinvertex ) const {
    // loop over the list of vertices, collect tracks in the vertex
    tracksinvertex.reserve( tracks.size() );
    for ( const auto& track : vertex.tracks() )
      if ( track ) {
        // we'll use the track in the provided list, not the track in the vertex
        TrackContainer::const_iterator jtrack =
            std::find_if( tracks.begin(), tracks.end(), TrackClonePredicate( track ) );
        if ( jtrack != tracks.end() ) tracksinvertex.push_back( *jtrack );
      }
  }

  void AlignAlgorithmBase::removeVertexTracks( const LHCb::RecVertex& vertex, TrackContainer& tracks ) const {
    TrackContainer unusedtracks;
    for ( auto& track : tracks )
      if ( std::find_if( vertex.tracks().begin(), vertex.tracks().end(), IsEqual<LHCb::Track>( *track ) ) ==
           vertex.tracks().end() )
        unusedtracks.push_back( track );
    tracks = unusedtracks;
  }

  LHCb::RecVertex* AlignAlgorithmBase::cloneVertex( const LHCb::RecVertex& vertex,
                                                    const TrackContainer&  selectedtracks ) const {
    LHCb::RecVertex* rc( 0 );
    TrackContainer   tracksinvertex;
    selectVertexTracks( vertex, selectedtracks, tracksinvertex );
    if ( tracksinvertex.size() >= 2 ) {
      rc = vertex.clone();
      rc->clearTracks();
      for ( auto& track : tracksinvertex ) rc->addToTracks( track );
    }
    return rc;
  }

  void AlignAlgorithmBase::splitVertex( const LHCb::RecVertex& vertex, const TrackContainer& tracks,
                                        VertexContainer& splitvertices ) const {
    //
    TrackContainer tracksinvertex;
    selectVertexTracks( vertex, tracks, tracksinvertex );

    if ( tracksinvertex.size() >= m_minTracksPerVertex ) {
      // sort the tracks by track type, then by side in the velo
      std::sort( tracksinvertex.begin(), tracksinvertex.end(), CompareTypeAndSide() );
      // the number of vertices after splitting. minimum 2 tracks per vertex.
      size_t numsplit = tracksinvertex.size() / m_maxTracksPerVertex +
                        ( ( tracksinvertex.size() % m_maxTracksPerVertex ) >= m_minTracksPerVertex ? 1 : 0 );
      splitvertices.clear();
      splitvertices.resize( numsplit, LHCb::RecVertex( vertex.position() ) );
      for ( size_t itrack = 0; itrack < tracksinvertex.size(); ++itrack )
        splitvertices[itrack % numsplit].addToTracks( tracksinvertex[itrack] );
    }
  }

  void AlignAlgorithmBase::removeParticleTracks( const LHCb::Particle& particle, TrackContainer& tracks ) const {
    std::vector<const LHCb::Track*> particletracks;
    extractTracks( particle, particletracks );

    TrackContainer unusedtracks;
    for ( auto& track : tracks ) {
      if ( std::find_if( particletracks.begin(), particletracks.end(), TrackClonePredicate( track ) ) ==
           particletracks.end() ) {
        unusedtracks.push_back( track );
      }
    }
    tracks = unusedtracks;
  }
} // namespace LHCb::Alignment
