/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TAlignment/GetElementsToBeAligned.h"
#include "TAlignment/IVertexResidualTool.h"
#include "TAlignment/TrackResidualHelper.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbMath/MatrixInversion.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"

namespace {
  // helper structure to hold nformation needed to apply a mass constraint
  struct MassConstraint {
    std::vector<double> daughtermasses;
    double              mothermass;
    double              motherwidth;
  };
} // namespace

namespace LHCb::Alignment {

  struct TrackContribution {
    typedef std::vector<Gaudi::TrackProjectionMatrix1D> ProjectionMatrix;
    const TrackResiduals*                               trackresiduals;
    State                                               inputstate;  // state at vertex before vertex fit
    Gaudi::TrackSymMatrix                               inputCovInv; // inverse of cov matrix of that state
    ProjectionMatrix residualStateCov; // same as in the residuals, but now extrapolated to vertex state
    ProjectionMatrix dResidualdState;  // residualStateCov * C(vertex-state)^{-1}, just cached for speed
    size_t           offset;
  };

  class VertexResidualTool : public extends<GaudiTool, IVertexResidualTool> {
  public:
    // constructor
    using extends::extends;
    // process a vertex (primary vertex of twoprongvertex)
    ReturnType get( const RecVertex& vertex, TrackResidualHelper& trackResidualHelper,
                    const GetElementsToBeAligned& elementProvider, IGeometryInfo const& geometry ) const override;
    // process a particle. this applies a mass constraint.
    ReturnType get( const Particle& particle, TrackResidualHelper& trackResidualHelper,
                    const GetElementsToBeAligned& elementProvider, IGeometryInfo const& geometry ) const override;

  private:
    // create a new MultiTrackResiduals
    std::unique_ptr<const MultiTrackResiduals> compute( const std::vector<const TrackResiduals*>& tracks,
                                                        const Gaudi::XYZPoint&                    vertexestimate,
                                                        IGeometryInfo const&                      geometry,
                                                        const MassConstraint* massconstraint = nullptr ) const;
    // extrapolate the state in trackresiduals to position z
    StatusCode extrapolate( const TrackResiduals& trackin, double z, TrackContribution& trackout,
                            IGeometryInfo const& geometry ) const;

  private:
    PublicToolHandle<ITrackExtrapolator> m_extrapolator{this, "Extrapolator", "TrackLinearExtrapolator"};
    ServiceHandle<IParticlePropertySvc>  m_propertysvc{this, "ParticlePropertyService", "LHCb::ParticlePropertySvc"};
    Gaudi::Property<double>              m_chiSquarePerDofCut{this, "MaxVertexChi2PerDoF", 10};
    Gaudi::Property<double>              m_maxMassConstraintChi2{this, "MaxMassConstraintChi2", 16};
    Gaudi::Property<int>                 m_maxHitsPerTrackForCorrelations{this, "MaxHitsPerTrackForCorrelations", 9999};
  };

  DECLARE_COMPONENT_WITH_ID( VertexResidualTool, "VertexResidualTool" )

} // namespace LHCb::Alignment

/////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "TrackKernel/TrackStateVertex.h"

#include "Event/Particle.h"
#include "Event/TwoProngVertex.h"
#include "Kernel/ParticleID.h"

#include "GaudiKernel/ParticleProperty.h"

#include <boost/assign/list_of.hpp>

#include <algorithm>
#include <iostream>

namespace LHCb::Alignment {

  std::unique_ptr<const MultiTrackResiduals> VertexResidualTool::get( const RecVertex&              vertex,
                                                                      TrackResidualHelper&          trackResidualHelper,
                                                                      const GetElementsToBeAligned& elementProvider,
                                                                      IGeometryInfo const&          geometry ) const {
    // loop over the list of vertices, collect tracks in the vertex
    std::vector<const TrackResiduals*> trackresiduals;
    for ( const auto& track : vertex.tracks() ) {
      const auto trackres = trackResidualHelper.get( *track, elementProvider, warning() );
      if ( trackres ) {
        trackresiduals.push_back( trackres );
      } else {
        warning() << "No residuals returned by trackresidualhelper!" << endmsg;
        assert( 0 );
      }
    }
    return std::unique_ptr<const MultiTrackResiduals>( compute( trackresiduals, vertex.position(), geometry ) );
  }

  std::unique_ptr<const MultiTrackResiduals> VertexResidualTool::get( const Particle&               p,
                                                                      TrackResidualHelper&          trackResidualHelper,
                                                                      const GetElementsToBeAligned& elementProvider,
                                                                      IGeometryInfo const&          geometry ) const {
    // loop over the list of vertices, collect tracks in the vertex
    std::unique_ptr<const MultiTrackResiduals> rc;
    std::vector<const TrackResiduals*>         trackresiduals;
    MassConstraint                             mconstraint;
    mconstraint.mothermass  = m_propertysvc->find( p.particleID() )->mass();
    mconstraint.motherwidth = m_propertysvc->find( p.particleID() )->width();
    //    info() << "Mothermass: " << mconstraint.mothermass << "\t width: " << mconstraint.motherwidth << endmsg ;

    bool success( true );
    for ( const Particle* daughter : p.daughters() ) {
      if ( daughter->proto() && daughter->proto()->track() ) {
        const auto track    = daughter->proto()->track();
        const auto trackres = trackResidualHelper.get( *track, elementProvider, warning() );
        if ( trackres ) {
          trackresiduals.push_back( trackres );
          const ParticleProperty* pp = m_propertysvc->find( daughter->particleID() );
          if ( !pp ) {
            warning() << "Cannot find property for particle: " << daughter->particleID() << " "
                      << daughter->particleID().pid() << endmsg;
            mconstraint.daughtermasses.push_back( daughter->momentum().M() );
          } else {
            mconstraint.daughtermasses.push_back( pp->mass() );
          }
          // info() << "daughtermass: " << mconstraint.daughtermasses.back() << endmsg ;
        } else {
          warning() << "No residuals returned by trackresidualhelper!" << endmsg;
          success = false;
        }
      } else {
        warning() << "Composite contains daughter that is not a track. Need to adapt code for that." << endmsg;
        success = false;
        assert( 0 );
      }
    }

    if ( success ) rc = compute( trackresiduals, p.referencePoint(), geometry, &mconstraint );
    return rc;
  }

  std::unique_ptr<const MultiTrackResiduals>
  VertexResidualTool::compute( const std::vector<const TrackResiduals*>& tracks, const Gaudi::XYZPoint& vertexestimate,
                               IGeometryInfo const& geometry, const MassConstraint* mconstraint ) const {
    MultiTrackResiduals* rc{nullptr};
    bool                 success      = true;
    bool                 m_debugLevel = msgLevel( MSG::DEBUG );

    // first we need to propagate all tracks to the vertex and keep
    // track of the correlation between the state at the vertex and
    // the reference state inside the TrackResiduals object

    typedef std::vector<TrackContribution> Contributions;
    Contributions                          states;
    double                                 totalchisq( 0 );
    size_t                                 totalndof( 0 );
    size_t                                 nacceptedtracks( 0 );
    size_t                                 numresiduals( 0 );
    size_t                                 numexternalhits( 0 );

    for ( typename std::vector<const TrackResiduals*>::const_iterator itrack = tracks.begin();
          itrack != tracks.end() && success; ++itrack ) {
      TrackContribution tc;
      tc.offset         = numresiduals;
      tc.trackresiduals = *itrack;
      StatusCode sc     = extrapolate( **itrack, vertexestimate.z(), tc, geometry );
      if ( sc.isSuccess() ) {
        states.push_back( tc );
        numresiduals += ( *itrack )->size();
        numexternalhits += ( *itrack )->nExternalHits();
        totalchisq += ( *itrack )->chi2();
        totalndof += ( *itrack )->nDoF();
        ++nacceptedtracks;
      } else {
        warning() << "Extrapolation failed. Rejecting vertex." << endmsg;
        success = false;
      }
    }

    if ( success ) {
      // now vertex the states
      TrackStateVertex vertex;
      for ( typename Contributions::const_iterator i = states.begin(); i != states.end(); ++i )
        vertex.addTrack( i->inputstate );

      TrackStateVertex::FitStatus fitstatus = vertex.fit();
      double                      vchi2orig = vertex.chi2(); // cache it, because I know it is slow
      int                         ndof      = vertex.nDoF();

      if ( fitstatus == TrackStateVertex::FitSuccess && mconstraint ) {
        //	static std::vector<double> masshypos = boost::assign::list_of(m_muonmass)(m_muonmass) ;
        if ( m_debugLevel )
          debug() << "mass before constraint: " << vertex.mass( mconstraint->daughtermasses ) << " +/- "
                  << vertex.massErr( mconstraint->daughtermasses ) << endmsg;
        double qopbefore = std::sqrt( vertex.stateCovariance( 0 )( 4, 4 ) );
        fitstatus =
            vertex.constrainMass( mconstraint->daughtermasses, mconstraint->mothermass, mconstraint->motherwidth );
        // vertex fit updates chi2, but not ndof
        ndof += 1;
        if ( m_debugLevel )
          debug() << "mass afterconstraint: " << vertex.mass( mconstraint->daughtermasses ) << " +/- "
                  << vertex.massErr( mconstraint->daughtermasses ) << std::endl
                  << "error on qop of first track, original, after vertex fit, after mass fit: "
                  << std::sqrt( vertex.inputState( 0 ).covariance()( 4, 4 ) ) << " " << qopbefore << " "
                  << std::sqrt( vertex.stateCovariance( 0 )( 4, 4 ) ) << endmsg;
      }
      // this is the chisquare including an eventual mass constraint
      double vchi2 = vertex.chi2(); // cache it, because I know it is slow

      counter( "vchi2" ) += vchi2;
      counter( "vchi2orig" ) += vchi2orig;
      // counter("niter") += vertex.nIter() ;

      if ( m_debugLevel ) {
        debug() << "Fitted vertex, chi2/dof=" << vchi2 << "/" << ndof << endmsg;
        debug() << "Vertex position orig/new=" << vertexestimate << "/" << vertex.position() << endmsg;
      }

      // note that when we cut on the vertex chisqur2 we exclude the mass constraint
      if ( fitstatus == TrackStateVertex::FitSuccess && vchi2orig / vertex.nDoF() < m_chiSquarePerDofCut &&
           ( !mconstraint || ( vchi2 - vchi2orig ) < m_maxMassConstraintChi2 ) ) {
        // create a vertexresiduals object
        totalchisq += vchi2;
        totalndof += ndof;
        std::vector<const TrackResiduals*> tracks;
        for ( const auto& atrack : states ) tracks.push_back( atrack.trackresiduals );
        rc = new MultiTrackResiduals( totalchisq, totalndof, numexternalhits, tracks, vchi2, ndof );
        rc->m_residuals.reserve( numresiduals );
        if ( m_debugLevel ) debug() << "created the vertex: " << numresiduals << endmsg;

        // calculate all new residuals and all correlations
        bool computationerror( false );
        for ( size_t i = 0; i < states.size() && !computationerror; ++i ) {
          Gaudi::TrackVector    deltaState = vertex.state( i ).stateVector() - vertex.inputState( i ).stateVector();
          Gaudi::TrackSymMatrix deltaCov   = vertex.state( i ).covariance() - vertex.inputState( i ).covariance();

          // These are only needed to compute correlations with the reference state/vertex that we later use for the
          // constraints.
          ROOT::Math::SMatrix<double, 5, 5> dStateOutDStateIn = states[i].inputCovInv * vertex.state( i ).covariance();
          ROOT::Math::SMatrix<double, 5, 3> stateVertexCov    = vertex.matrixA( i ) * vertex.covMatrix();
          stateVertexCov += vertex.matrixB( i ) * vertex.momPosCovMatrix( i );
          ROOT::Math::SMatrix<double, 5, 3> dVertexDStateIn = states[i].inputCovInv * stateVertexCov;

          size_t ioffset = states[i].offset;
          for ( size_t irow = 0; irow < states[i].trackresiduals->size(); ++irow ) {
            // copy the original residual
            assert( rc->m_residuals.size() == irow + ioffset );
            rc->m_residuals.push_back( states[i].trackresiduals->residuals()[irow] );
            Residual1D& res = rc->m_residuals.back();

            // update with vertex info
            double deltar   = ( states[i].dResidualdState[irow] * deltaState )( 0 );
            double deltaHCH = ROOT::Math::Similarity( states[i].dResidualdState[irow], deltaCov )( 0, 0 );
            res.setR( res.r() + deltar );
            res.setHCH( res.HCH() + deltaHCH );
            if ( res.HCH() < 0 ) {
              warning() << "problem computing update of track errors"
                        << states[i].trackresiduals->residuals()[irow].HCH() << deltaHCH << std::endl
                        << deltaCov << endmsg;
              computationerror = true;
            }
            // update also the covariance with reference state and
            // vertex. the math behind this is actually not entirely
            // trivial.
            res.setResidualStateCov( states[i].residualStateCov[irow] * dStateOutDStateIn );
            res.setResidualVertexCov( states[i].residualStateCov[irow] * dVertexDStateIn );
            /*
            for( size_t k=0; k<5; ++k) {
              double s = std::sqrt( res.V() * vertex.state(i).covariance()(k,k) ) ;
              if( std::abs( res.residualStateCov()(0,k) / s) > 1) {
              std::cout << "problem computing residual track covariance: "
                          << k << " "
                          << res.node().z() << " "
                          << res.residualStateCov()(0,k) << " " << s << " "
                          << "track cov: " << vertex.state(i).covariance()(k,k) << std::endl ;
                //assert(0) ;
              }
            }

            bool anerror(false) ;
            for( size_t k=0; k<3; ++k)
              if( std::abs( res.residualVertexCov()(0,k) / std::sqrt( res.V() * vertex.covMatrix()(k,k) ) ) > 1 ) {
                std::cout << "problem computing residual vertex covariance: "
                          << res.node().z() << " "
                          << k << " "
                          << res.residualVertexCov()(0,k) << " "
                          << std::sqrt( res.V() * vertex.covMatrix()(k,k) ) << std::endl ;
                std::cout << "contributions: " ;
                for( size_t l=0; l<5; ++l)
                  std::cout << states[i].residualStateCov[irow](0,l) * dVertexDStateIn(l,k) << " " ;
                std::cout << std::endl ;

                //assert(0) ;
                anerror = true ;
              }
            // let;s chack the track-vertex correlation matrix actually make sense
            if(anerror) {
              std::cout << "delta-z: "
                        << vertex.position().z() - vertexestimate.z() << std::endl ;
              ROOT::Math::SMatrix<double,5,3> statevertexcov = vertex.matrixA(i) * vertex.covMatrix() ;
              for( size_t k=0; k<5; ++k)
                for (size_t l=0; l<3; ++l)
                  statevertexcov(k,l) /= std::sqrt( vertex.covMatrix()(l,l) * vertex.state(i).covariance()(k,k) ) ;
              std::cout << "statevertexcor: " << statevertexcov << std::endl ;
            }
            */
          }

          // loop over all existing entries of the per-track HCH and update
          for ( typename Residuals::CovarianceContainer::const_iterator ielem =
                    states[i].trackresiduals->m_HCHElements.begin();
                ielem != states[i].trackresiduals->m_HCHElements.end(); ++ielem ) {
            double deltaHCH = ( states[i].dResidualdState[ielem->row] * deltaCov *
                                ROOT::Math::Transpose( states[i].dResidualdState[ielem->col] ) )( 0, 0 );
            rc->m_HCHElements.push_back(
                CovarianceElement( ielem->row + ioffset, ielem->col + ioffset, ielem->val + deltaHCH ) );
          }

          // now the correlations between the tracks. this is the very time-consuming part.
          if ( m_maxHitsPerTrackForCorrelations > 0 && !computationerror )
            for ( size_t j = 0; j < i && !computationerror; ++j ) {
              size_t joffset = states[j].offset;
              size_t maxirow = std::min( states[i].trackresiduals->size(), size_t( m_maxHitsPerTrackForCorrelations ) );
              for ( size_t irow = 0; irow < maxirow; ++irow ) {
                // store this intermediate matrix too save some time
                Gaudi::TrackProjectionMatrix1D A = states[i].dResidualdState[irow] * vertex.stateCovariance( i, j );
                size_t                         maxjcol =
                    std::min( states[j].trackresiduals->size(), size_t( m_maxHitsPerTrackForCorrelations ) );
                for ( size_t jcol = 0; jcol < maxjcol; ++jcol ) {
                  double deltaHCH = ( A * ROOT::Math::Transpose( states[j].dResidualdState[jcol] ) )( 0, 0 );
                  rc->m_HCHElements.push_back( CovarianceElement( irow + ioffset, jcol + joffset, deltaHCH ) );
                }
              }
            }

          // Let's check the result Keep track of the roots.
          if ( !computationerror ) {
            std::string message;
            computationerror = rc->testHCH( message );
            if ( computationerror ) warning() << message << endmsg;
          }
        }

        if ( computationerror ) {
          delete rc;
          rc = nullptr;
          warning() << "VertexResidualTool::compute failed" << endmsg;
        }
      } else {
        warning() << "rejected vertex with chisqu/dof: " << vchi2 / vertex.nDoF()
                  << " isConstrained = " << bool( mconstraint != 0 ) << " " << states.size() << " " << tracks.size()
                  << endmsg;
      }
    } else {
      warning() << "not enough tracks for vertex anymore" << endmsg;
    }
    return std::unique_ptr<const MultiTrackResiduals>{rc};
  }

  StatusCode VertexResidualTool::extrapolate( const TrackResiduals& track, double z, TrackContribution& tc,
                                              IGeometryInfo const& geometry ) const {
    // Just to remind you, this is the covariance matrix for a state and its extrapolated state
    //
    //             C1          C1 F^T
    //           F C1       F C1 F^T + Q
    //
    //  where F is the transport matrix and Q is the noise. So, the
    //  covariance in the extrapolated state is the well-known
    //
    //     C2 = F C1 F^T + Q
    //
    // and the correlation is the lesser known
    //
    //     C21 = F C1
    //

    // Now, we have the correlation matrix for C1 and the residuals
    // R. Let's call that 'E' (which is a 5xN matrix). To get the
    // correlation between C' and R, we use the formula
    //
    //  E'  =  C21 * C1^{-1} * E = F E
    //
    // So, we just need to multiply E with the transport matrix,
    // which is kind of obvious.
    //
    // In the end we are not so much interested in E, but in the
    // derivatives of the residuals to the state, which is
    //
    // dRdS = C2^{-1} * E' = C2^{-1} * F * E

    // propagate the reference state and get the transport matrix
    Gaudi::TrackMatrix F;
    tc.inputstate = track.state();
    StatusCode sc = m_extrapolator->propagate( tc.inputstate, z, &F, geometry );
    if ( sc.isSuccess() ) {

      // calculate the correlation
      // stateResidualCorrelation = convertToCLHEP(F) * track.m_stateResCov ;

      // invert the covariance matrix. cache it for later use
      tc.inputCovInv = tc.inputstate.covariance();
      bool OK        = tc.inputCovInv.InvertChol();
      if ( !OK ) {
        warning() << "Inversion error in VertexResidualTool::extrapolate" << endmsg;
        sc = StatusCode::FAILURE;
      } else {
        tc.residualStateCov.resize( track.size() );
        tc.dResidualdState.resize( track.size() );
        for ( size_t inode = 0; inode < track.size(); ++inode ) {
          tc.residualStateCov[inode] = track.residuals()[inode].residualStateCov() * ROOT::Math::Transpose( F );
          tc.dResidualdState[inode]  = tc.residualStateCov[inode] * tc.inputCovInv;
        }
      }
    }
    return sc;
  }

} // namespace LHCb::Alignment
