/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "AlignConstraints.h"

#include "TAlignment/AlignmentElement.h"
#include "TAlignment/GetElementsToBeAligned.h"

#include "LHCbMath/LHCbMath.h"

#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Transform3DTypes.h"

#include "boost/assign/list_of.hpp"
#include "boost/tokenizer.hpp"

#include "fmt/format.h"

namespace {

  std::string removechars( const std::string& s, const std::string& chars ) {
    std::string rc;
    for ( std::string::const_iterator it = s.begin(); it != s.end(); ++it )
      if ( std::find( chars.begin(), chars.end(), *it ) == chars.end() ) rc.push_back( *it );
    return rc;
  }

  std::vector<std::string> tokenize( const std::string& s, const char* separator ) {
    typedef boost::char_separator<char> Separator;
    typedef boost::tokenizer<Separator> Tokenizer;
    Tokenizer                           split( s, Separator( separator ) );
    std::vector<std::string>            rc;
    rc.insert( rc.end(), split.begin(), split.end() );
    return rc;
  }

  LHCb::Alignment::EConstraintDerivatives index( std::string_view s ) {
    LHCb::Alignment::EConstraintDerivatives v;
    parse( v, s ).ignore();
    return v;
  }

  bool check( const std::vector<std::string>& names ) {
    for ( const auto& name : names ) {
      auto constraint = index( name );
      if ( constraint == LHCb::Alignment::EConstraintDerivatives::Unknown ) {
        std::cout << "Warning: unknown constraint " << name << std::endl;
        return false;
      }
    }
    return true;
  }

  size_t numConstraints() {
    return LHCb::Alignment::EConstraintDerivatives_internal_size() - 1; // do not count 'Unknown'
  }

} // namespace

namespace LHCb::Alignment {

  ConstraintDerivatives::ConstraintDerivatives( size_t dim, const std::vector<std::string>& activeconstraints,
                                                const std::string& nameprefix, int offset )
      : m_dofmask( size_t( numConstraints() ) )
      , m_derivatives{size_t( numConstraints() ), dim}
      , m_residuals{size_t( numConstraints() )}
      , m_nameprefix( nameprefix )
      , m_activeParOffset( offset ) {
    std::vector<bool> active( numConstraints(), false );
    if ( check( activeconstraints ) ) {
      for ( const auto& name : activeconstraints ) { active[static_cast<int>( index( name ) )] = true; }
    }
    m_dofmask = AlDofMask( active );
  }

  AlignConstraints::AlignConstraints( const GetElementsToBeAligned& elementProvider,
                                      LHCb::span<const std::string> constraintNames, bool useWeightedAverage,
                                      MsgStream& log )
      : m_useWeightedAverage( useWeightedAverage ) {
    // now we need to decode the string with constraints
    ConstraintDefinition common( "" );
    common.addElements( elementProvider.elements() );
    m_definitions.push_back( common );
    for ( const auto& constraintName : constraintNames ) {
      // tokenize
      std::vector<std::string> tokens = tokenize( constraintName, ":" );
      if ( tokens.size() == 1 ) {
        // this is a single or a set of common constraints
        std::vector<std::string> dofs = tokenize( constraintName, " ," );
        for ( const auto& dof : dofs ) m_definitions.front().addDof( removechars( dof, " ," ) );
      } else if ( tokens.size() == 3 || tokens.size() == 4 ) {
        ConstraintDefinition newconstraint( tokens.at( 0 ) );
        auto                 elements = elementProvider.findElements( removechars( tokens.at( 1 ), " " ) );
        newconstraint.addElements( elements );
        std::vector<std::string> dofs = tokenize( tokens.at( 2 ), " ," );
        for ( const auto& dof : dofs ) newconstraint.addDof( removechars( dof, " ," ) );
        if ( tokens.size() == 4 ) {
          std::string modestr = removechars( tokens.at( 3 ), " " );
          std::transform( modestr.begin(), modestr.end(), modestr.begin(), tolower );
          if ( modestr == "total" )
            newconstraint.mode = ConstraintMode::Total;
          else if ( modestr == "delta" )
            newconstraint.mode = ConstraintMode::Delta;
          else {
            throw GaudiException(
                "AlignConstraints",
                format( "Unknown mode in constraint definition: %s | '%s'", constraintName.c_str(), modestr.c_str() ),
                StatusCode::FAILURE );
          }
        }
        m_definitions.push_back( newconstraint );
      }
    }

    int nactive( 0 );
    for ( const auto& constraint : m_definitions ) {
      if ( !check( constraint.dofs ) ) {
        throw GaudiException( "AlignConstraints", "Constraint list contains unknown constraints!",
                              StatusCode::FAILURE );
      } else {
        nactive += constraint.dofs.size();
        log << "Constraint definition: " << constraint.name << " "
            << "dofs = ";
        for ( const auto& dof : constraint.dofs ) log << dof << " , ";
        log << "num elements = " << constraint.elements.size() << endmsg;
      }
    }
    log << "Number of constraint definitions= " << m_definitions.size()
        << ",  number of active constraints = " << nactive << endmsg;
  }

  ConstraintDerivatives* AlignConstraints::createConstraintDerivatives(
      const std::string& name, const std::vector<std::string>& activeconstraints, const Elements& elements,
      bool addResidual, const Equations& equations, size_t numAlignPars, MsgStream& log ) {
    // This adds lagrange multipliers to constrain the average rotation
    // and translation. Ideally, we could calculate this in any
    // frame. In practise, the average depends on the reference frame in
    // which is is calculated. We will calculate a single 'pivot' point
    // to define the transform to the frame in which we apply the
    // constraint.

    // To make the bookkeeping easy, we add all possible constraints and
    // then 'disable' those we don't need with the 'dofmask'.
    //
    // This is the numbering:
    // 0 -> Tx
    // 1 -> Ty
    // 2 -> Tz
    // 3 -> Rx
    // 4 -> Ry
    // 5 -> Rz
    // 6 -> Sx  (zx shearing)
    // 7 -> Sy  (zy shearing)
    // 8 -> Sz  (zz shearing == z-scale)
    // 9 -> SzRz (twist around z-axis)
    // 10 -> Average track X
    // 11 -> Average track Y
    // 12 -> Average track Tx
    // 13 -> Average track Ty
    // 14 -> Curvature (Curvature constraint)
    // PVx, PVy, PVz -> Average PV position
    // Sz2x, Sz2y, Sz2z, Sz2Rz -> 'shearings' that go with z2

    // If there is only a single element, then we use its alignment
    // frame. In that case we can also not compute shearings, so we
    // leave zmin.max and xmin.max invalid. If there is more than one
    // element, we use the average center as the frame.
    double zmin( 9999999 ), zmax( -999999 ), xmin( 9999999 ), xmax( -999999 ), weight( 1 );
    bool   useWeightedAverage = false;
    if ( elements.size() != 1 ) {
      // FIXME: it should be possible to do this better. we could for example
      // take the frame fo the parent. see Element.
      size_t                numhits( 0 );
      double                weight( 0 );
      ROOT::Math::XYZVector pivot;
      useWeightedAverage = m_useWeightedAverage;
      for ( const auto& element : elements ) {
        if ( element->activeParOffset() >= 0 ) {
          size_t elemindex  = element->index();
          double thisweight = useWeightedAverage ? equations.element( elemindex ).weightV() : 1;
          weight += thisweight;
          ROOT::Math::XYZPoint cog = element->centerOfGravity();
          pivot += thisweight * ROOT::Math::XYZVector( cog );
          zmin = std::min( cog.z(), zmin );
          zmax = std::max( cog.z(), zmax );
          xmin = std::min( cog.x(), xmin );
          xmax = std::max( cog.x(), xmax );
          numhits += equations.element( elemindex ).numHits();
        }
      }
      if ( weight > 0 ) pivot *= 1 / weight;
      log << MSG::DEBUG << "Pivot, z/x-range for canonical constraints: " << name << " " << std::fixed
          << std::setprecision( 3 ) << pivot << ", [" << zmin << "," << zmax << "]"
          << ", [" << xmin << "," << xmax << "]" << endmsg << log.level();
    }

    // create the object that we will return
    ConstraintDerivatives* constraints = new ConstraintDerivatives( numAlignPars, activeconstraints, name );

    for ( const auto& element : elements ) {
      if ( element->activeParOffset() >= 0 ) {
        // calculate the Jacobian for going from the 'alignment' frame to
        // the 'canonical' frame. This is the first place where we could
        // turn things around. It certainly works if the transforms are
        // just translations.
        size_t     elemindex      = element->index();
        const auto canonicalframe = ROOT::Math::Transform3D( ROOT::Math::XYZVector( element->centerOfGravity() ) );
        const auto trans          = canonicalframe.Inverse() * element->alignmentFrame();
        const auto jacobian       = AlParameters::jacobian( trans );

        double thisweight = ( useWeightedAverage ? equations.element( elemindex ).weightV() : 1.0 ) / weight;
        double deltaZ     = element->centerOfGravity().z() - 0.5 * ( zmax + zmin );
        double deltaX     = element->centerOfGravity().x() - 0.5 * ( xmax + xmin );
        double zweight    = zmax > zmin ? deltaZ / ( zmax - zmin ) : 0;
        double xweight    = xmax > xmin ? deltaX / ( xmax - xmin ) : 0;

        // loop over all parameters in this element. skip inactive parameters.
        for ( size_t j = 0; j < 6; ++j ) {
          int jpar = element->activeParIndex( j );
          if ( jpar >= 0 ) {
            // Derivatives for global rotations and translations
            for ( size_t i = 0u; i < 6; ++i )
              // and here comes the 2nd place we could do things entirely
              // wrong, but I think that this is right. if( element->activeParOffset() >= 0 ) { // only take selected
              // elements
              constraints->derivatives()( i, jpar ) = thisweight * jacobian( i, j );

            // Derivatives for shearings
            for ( size_t i = 0u; i < 3u; ++i )
              constraints->derivatives()( i + 6, jpar ) = thisweight * zweight * jacobian( i, j );

            // Derivatives for twist around z-axis
            constraints->derivatives()( static_cast<Eigen::Index>( EConstraintDerivatives::SzRz ), jpar ) =
                thisweight * zweight * jacobian( 5, j );

            // Derivatives for bowing
            for ( size_t i = 0u; i < 3u; ++i )
              constraints->derivatives()( static_cast<Eigen::Index>( EConstraintDerivatives::Sz2x ) + i, jpar ) =
                  thisweight * zweight * zweight * jacobian( i, j );

            // Derivatives for bowing twist around z-axis
            constraints->derivatives()( static_cast<Eigen::Index>( EConstraintDerivatives::Sz2Rz ), jpar ) =
                thisweight * zweight * zweight * jacobian( 5, j );

            // Derivative for scaling in x
            constraints->derivatives()( static_cast<Eigen::Index>( EConstraintDerivatives::Sxx ), jpar ) =
                thisweight * xweight * jacobian( 0, j );

            // Average track parameter constraint
            for ( size_t trkpar = 0; trkpar < 5; ++trkpar )
              constraints->derivatives()( static_cast<Eigen::Index>( EConstraintDerivatives::Trx ) + trkpar, jpar ) =
                  equations.element( elemindex ).dStateDAlpha()( j, trkpar ) / equations.numTracks();

            // Average PV position constraint
            if ( equations.numVertices() > 0 )
              for ( size_t vtxpar = 0; vtxpar < 3; ++vtxpar )
                constraints->derivatives()( static_cast<Eigen::Index>( EConstraintDerivatives::PVx ) + vtxpar, jpar ) =
                    equations.element( elemindex ).dVertexDAlpha()( j, vtxpar ) / equations.numVertices();
          }
        }

        // now we still need to do the residuals, as far as those are
        // meaningful. note the minus signs.
        if ( addResidual ) {
          AlParameters delta           = element->currentDelta( canonicalframe );
          AlVec        deltaparameters = delta.parameters();
          // Residuals for global rotations and translations
          for ( size_t i = 0u; i < 6; ++i ) constraints->residuals()( i ) -= thisweight * deltaparameters( i );
          // Residuals for shearings
          for ( size_t i = 0u; i < 3u; ++i )
            constraints->residuals()( i + 6 ) -= thisweight * zweight * deltaparameters( i );
          // Residuals for twist around z-axis
          constraints->residuals()( static_cast<Eigen::Index>( EConstraintDerivatives::SzRz ) ) -=
              thisweight * zweight * deltaparameters( 5 );
          // Residuals for bowing
          for ( size_t i = 0u; i < 3u; ++i )
            constraints->residuals()( static_cast<Eigen::Index>( EConstraintDerivatives::Sz2x ) + i ) -=
                thisweight * zweight * zweight * deltaparameters( i );
          // Residuals for bowing twist
          constraints->residuals()( static_cast<Eigen::Index>( EConstraintDerivatives::Sz2Rz ) ) -=
              thisweight * zweight * zweight * deltaparameters( 5 );
          // Residuals for scaling in x
          constraints->residuals()( static_cast<Eigen::Index>( EConstraintDerivatives::Sxx ) ) -=
              thisweight * xweight * deltaparameters( 0 );
        }
      }
    }
    // turn off constraints with only zero derivatives
    for ( size_t ipar = 0; ipar < numConstraints(); ++ipar )
      if ( constraints->isActive( ipar ) ) {
        bool hasNonZero( false );
        for ( size_t jpar = 0; jpar < numAlignPars && !hasNonZero; ++jpar )
          hasNonZero = std::abs( constraints->derivatives()( ipar, jpar ) ) > LHCb::Math::lowTolerance;
        if ( !hasNonZero ) {
          log << "removing constraint \'" << constraints->name( static_cast<EConstraintDerivatives>( ipar ) )
              << "\' because it has no non-zero derivatives." << endmsg;
          constraints->setActive( ipar, false );
        }
      }
    return constraints;
  }

  void AlignConstraints::clearConstraintDerivatives() {
    for ( auto* derivative : m_derivatives ) delete derivative;
    m_derivatives.clear();
  }

  size_t AlignConstraints::add( const Elements& /*elements*/, const Equations& equations, AlVec& halfDChi2DAlpha,
                                AlSymMat& halfD2Chi2DAlpha2, MsgStream& log ) {
    // get the total number of alignment parameters
    size_t numpars = halfDChi2DAlpha.size();

    // fill the constraint derivatives
    clearConstraintDerivatives();
    size_t numactive( 0 );
    for ( const auto& constraint : m_definitions ) {
      ConstraintDerivatives* derivative =
          createConstraintDerivatives( constraint.name, constraint.dofs, constraint.elements,
                                       constraint.mode == ConstraintMode::Total, equations, numpars, log );
      derivative->setActiveParOffset( numpars + numactive );
      m_derivatives.push_back( derivative );
      numactive += derivative->nActive();
    }
    log << "Total number of active constraints: " << numactive << endmsg;

    // now add them, if any constraints are active
    if ( numactive > 0 ) {

      // create new matrices
      size_t   dim = numpars + numactive;
      AlVec    halfDChi2DAlphaNew( dim );
      AlSymMat halfD2Chi2DAlpha2New( dim );

      // copy the old matrices in there
      for ( size_t irow = 0; irow < numpars; ++irow ) {
        halfDChi2DAlphaNew( irow ) = halfDChi2DAlpha( irow );
        for ( size_t icol = 0; icol <= irow; ++icol )
          halfD2Chi2DAlpha2New.fast( irow, icol ) = halfD2Chi2DAlpha2.fast( irow, icol );
      }

      // now add the constraints, only to the 2nd derivative (residuals are zero for now)
      for ( const auto& derivative : m_derivatives ) {
        if ( derivative->nActive() > 0 ) {
          for ( size_t irow = 0; irow < numConstraints(); ++irow ) {
            int iactive = derivative->activeParIndex( irow );
            if ( 0 <= iactive ) {
              for ( size_t jrow = 0; jrow < numpars; ++jrow )
                halfD2Chi2DAlpha2New.fast( derivative->activeParOffset() + iactive, jrow ) =
                    derivative->derivatives()( irow, jrow );
            }
          }

          for ( size_t irow = 0; irow < numConstraints(); ++irow ) {
            int iactive = derivative->activeParIndex( irow );
            if ( 0 <= iactive ) {
              halfDChi2DAlphaNew( derivative->activeParOffset() + iactive ) = derivative->residuals()( irow );
            }
          }
        }
      }
      // copy the result back
      halfDChi2DAlpha   = halfDChi2DAlphaNew;
      halfD2Chi2DAlpha2 = halfD2Chi2DAlpha2New;
    }
    return numactive;
  }

  void AlignConstraints::print( const AlVec& parameters, const AlSymMat& covariance, std::ostream& logmessage ) const {
    if ( !m_derivatives.empty() ) {
      // the total number of alignment parameters
      size_t numPars                  = m_derivatives.front()->activeParOffset();
      size_t numConstraintDerivatives = parameters.size() - numPars;

      // ugly: create a map from an index to an alignment parameter in a constraints object
      AlDofMask totalmask;
      for ( const auto& derivative : m_derivatives ) { totalmask.push_back( derivative->dofMask() ); }

      // first extract the part concerning the lagrange multipliers
      AlVec lambda{numConstraintDerivatives};
      AlMat covlambda{numConstraintDerivatives, numConstraintDerivatives};
      for ( size_t i = 0u; i < numConstraintDerivatives; ++i ) {
        lambda( i ) = parameters[numPars + i];
        for ( size_t j = 0u; j < numConstraintDerivatives; ++j )
          covlambda( i, j ) = covariance( numPars + i, numPars + j );
      }

      // For a lagrange constraint defined as delta-chisquare = 2 * lambda * constraint, the solution for lambda is
      //  lambda     =   W * constraint
      //  cov-lambda =   W
      // where W is minus the inverse of the covariance of the constraint. From this we extract
      //   constraint       = W^{-1} * lambda
      //   constraint error = sqrt( W^{-1} )
      //   chisquare = - lambda * constraint
      // Note that this all needs to work for vectors.

      const auto   minusV    = covlambda.inverse();
      const auto   V         = -minusV;
      const auto   x         = V * lambda; // This might need a minus sign ...
      const double chisquare = ( lambda.transpose() * V * lambda );
      logmessage << "Canonical constraint values: " << std::endl;
      for ( size_t iactive = 0u; iactive < numConstraintDerivatives; ++iactive ) {
        size_t     parindex = totalmask.parIndex( iactive );
        const auto thischi2 = x( iactive ) * x( iactive ) / V( iactive, iactive );
        logmessage << std::setw( 15 ) << std::setiosflags( std::ios_base::left )
                   << m_derivatives[parindex / numConstraints()]->name(
                          static_cast<EConstraintDerivatives>( parindex % numConstraints() ) )
                   << " chi2: " << std::setw( 12 ) << thischi2 << " residual: " << std::setw( 12 ) << x( iactive )
                   << " +/- " << std::setw( 12 ) << AlParameters::signedSqrt( V( iactive, iactive ) )
                   << " gcc^2: " << std::setw( 12 ) << 1 + 1 / ( V( iactive, iactive ) * covlambda( iactive, iactive ) )
                   << std::endl;
      }
      logmessage << "Canonical constraint chisquare: " << chisquare << std::endl;

      // Now the inactive constraints. Shrink the parameter and covariance matrix to the parameters only part
      AlVec subparameters{numPars};
      AlMat subcovariance{numPars, numPars};
      for ( size_t irow = 0; irow < numPars; ++irow ) {
        subparameters[irow] = parameters[irow];
        for ( size_t icol = 0; icol < numPars; ++icol ) subcovariance( irow, icol ) = covariance( irow, icol );
      }
      // Calculate the delta
      const auto derivatives     = m_derivatives.front()->derivatives();
      const auto constraintdelta = derivatives * subparameters;
      const auto constraintcov   = derivatives * subcovariance * derivatives.transpose();

      logmessage << "Values of constraint equations after solution (X=active,O=inactive): " << std::endl;
      for ( size_t i = 0; i < numConstraints(); ++i ) {
        logmessage << std::setw( 4 ) << m_derivatives.front()->name( static_cast<EConstraintDerivatives>( i ) ) << " "
                   << std::setw( 2 ) << ( m_derivatives.front()->isActive( i ) ? 'X' : 'O' ) << " " << std::setw( 12 )
                   << constraintdelta( i ) << " +/- " << AlParameters::signedSqrt( constraintcov( i, i ) ) << std::endl;
      }
    }
  }

} // namespace LHCb::Alignment
