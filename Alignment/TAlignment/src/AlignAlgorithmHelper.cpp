/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TAlignment/AlignAlgorithmHelper.h"
#include "TAlignment/AlignAlgorithmBase.h"
#include "TrackInterfaces/IMeasurementProviderProjector.h"

void LHCb::Alignment::Helper::updateGeometry( IAlgorithm* alg, size_t iter, size_t maxiter ) {
  auto* alignAlg = dynamic_cast<LHCb::Alignment::AlignAlgorithmBase*>( alg );
  if ( alignAlg ) { alignAlg->updateGeometry( iter, maxiter ); }
}

void LHCb::Alignment::Helper::clearEquationsAndReloadGeometry( IAlgorithm* alg ) {
  auto* alignAlg = dynamic_cast<LHCb::Alignment::AlignAlgorithmBase*>( alg );
  if ( alignAlg ) { alignAlg->clearEquationsAndReloadGeometry(); }
}

#ifdef USE_DD4HEP
void LHCb::Alignment::Helper::writeAlignmentConditions( IAlgorithm* alg, std::string out_dir ) {
  auto* alignAlg = dynamic_cast<LHCb::Alignment::AlignAlgorithmBase*>( alg );
  if ( alignAlg ) { alignAlg->writeAlignmentConditions( out_dir ); }
}

void LHCb::Alignment::Helper::loadAlignmentConditions( IAlgorithm* alg, std::string in_dir ) {
  auto* alignAlg = dynamic_cast<LHCb::Alignment::AlignAlgorithmBase*>( alg );
  if ( alignAlg ) { alignAlg->loadAlignmentConditions( in_dir ); }
}

#else
void LHCb::Alignment::Helper::writeAlignmentConditions( IAlgorithm* alg ) {
  auto* alignAlg = dynamic_cast<LHCb::Alignment::AlignAlgorithmBase*>( alg );
  if ( alignAlg ) { alignAlg->writeAlignmentConditions(); }
}
#endif
