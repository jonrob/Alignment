/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "AlignConstraints.h"

#include "AlignmentInterfaces/IAlignSolvTool.h"
#include "AlignmentInterfaces/IAlignUpdateTool.h"
#include "TAlignment/AlignmentElement.h"
#include "TAlignment/GetElementsToBeAligned.h"
#include "TAlignment/IAlignChisqConstraintTool.h"

#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiAlg/GaudiHistoTool.h"
#include "GaudiKernel/IDetDataSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "Kernel/SynchronizedValue.h"

#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

#include "TH1D.h"
#include "TMath.h"

#include <fstream>
#include <memory>
#include <sstream>
#include <string>

namespace LHCb::Alignment {

  class AlignUpdateTool : public extends<GaudiHistoTool, IAlignUpdateTool> {
  public:
    /// Typedefs etc.
    using AlignConstants = std::vector<double>;

    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;
    StatusCode process( const Equations& equations, const GetElementsToBeAligned& elementProvider, size_t iter,
                        size_t maxiter, ConvergenceStatus& status
#ifdef USE_DD4HEP
                        ,
                        LHCb::Det::LbDD4hep::IDD4hepSvc& dataSvc
#endif
                        ) const override;
    StatusCode queryInterface( const InterfaceID& riid, void** ppvi ) override;

  private:
    bool       checkGeometry( const Equations& constequations, const GetElementsToBeAligned& elementProvider ) const;
    void       preCondition( const Elements& elements, const Equations& equations, AlVec& dChi2dAlpha,
                             AlSymMat& d2Chi2dAlpha2, AlVec& scale ) const;
    void       postCondition( AlVec& dChi2dAlpha, AlSymMat& d2Chi2dAlpha2, const AlVec& scale ) const;
    void       fillIterProfile( const HistoID& id, const std::string& title, size_t numiter, size_t iter, double val,
                                double err = 0 ) const;
    StatusCode addDaughterDerivatives( Equations& equations, const GetElementsToBeAligned& elementProvider ) const;
    void dumpMostImportantDofs( const Elements& elements, const Equations& equations, std::ostream& logmessage ) const;
    void dumpWorstSurveyOffenders( const Elements& elements, std::ostream& logmessage ) const;
    static double computeLookElsewhereChi2Cut( double chi2, double n );

  private:
    PublicToolHandle<IAlignSolvTool>      m_matrixSolverTool{this, "MatrixSolverTool", "EigenDiagSolvTool",
                                                        "Linear algebra solver tool"};
    ToolHandle<IAlignChisqConstraintTool> m_chisqconstrainttool{this, "SurveyConstraintTool",
                                                                "AlignChisqConstraintTool"};
    Gaudi::Property<size_t>               m_minNumberOfHits{this, "MinNumberOfHits", 100u,
                                              "Minimum number of hits for an Alignable to be aligned"};
    Gaudi::Property<bool>                 m_usePreconditioning{this, "UsePreconditioning", true,
                                               "Precondition the system of equations before calling solver"};
    Gaudi::Property<double>               m_maxDeltaChi2PDofForConvergence{this, "MaxDeltaChi2PDofForConvergence", 4};
    Gaudi::Property<double>               m_maxModeDeltaChi2ForConvergence{this, "MaxModeDeltaChi2ForConvergence", 25};
    Gaudi::Property<std::string>          m_logFileName{this, "LogFile", "alignlog.txt"};
    Gaudi::Property<bool> m_skipGeometryUpdate{this, "SkipGeometryUpdate", false, "Skip the update of the geometry"};

    Gaudi::Property<std::vector<std::string>> m_constraintNames{this, "Constraints"};
    Gaudi::Property<bool>                     m_useWeightedAverage{this, "UseWeightedAverage", false};
  };

  DECLARE_COMPONENT_WITH_ID( AlignUpdateTool, "AlignUpdateTool" )

  StatusCode AlignUpdateTool::initialize() {
    return GaudiHistoTool::initialize().andThen( [&] {
      if ( name() == "ToolSvc.AlignUpdateTool" ) {
        setHistoTopDir( "" );
        setHistoDir( "Alignment" );
      }
      // drop any preexisting log file, as we will then append to it
      if ( !m_logFileName.empty() ) { std::remove( m_logFileName.value().c_str() ); }
    } );
  }

  void AlignUpdateTool::preCondition( const Elements& elements, const Equations& equations, AlVec& halfDChi2DAlpha,
                                      AlSymMat& halfD2Chi2DAlpha2, AlVec& scale ) const {
    // This is just not sufficiently fool proof!
    size_t size = halfDChi2DAlpha.rows();
    scale       = AlVec{size};
    size_t ipar( 0 );
    // initialize all scales with 1
    for ( ipar = 0u; ipar < size; ++ipar ) scale[ipar] = 1;
    // now set the scales for the elements
    int iElem( 0u );
    for ( auto& elem : elements ) {
      int offset = elem->activeParOffset();
      if ( 0 <= offset ) {
        size_t ndof = elem->dofMask().nActive();
        size_t N    = std::max( equations.element( iElem ).numHits(), size_t( 1 ) );
        for ( ipar = offset; ipar < offset + ndof; ++ipar ) {
          assert( ipar < size );
          if ( halfD2Chi2DAlpha2( ipar, ipar ) > 0 ) scale[ipar] = std::sqrt( N / halfD2Chi2DAlpha2( ipar, ipar ) );
        }
      }
      ++iElem;
    }

    // Now determine the scales for the constraint. If the constraint
    // equation fixes an eigenmode with eigenvalue 'w', then the
    // two new eigenvalues become
    //  w_1 = w/2 - sqrt(norm^2 + w^2/4)
    //  w_2 = w/2 + sqrt(norm^2 + w^2/4)
    // where norm is the size of the derivative to the constraint. The
    // first eigenvalue is negative: that's a property of lagrange
    // constraints.
    //
    // Now, to make sure we can still identify which eigenvalue
    // corresponds to which constraints, we scale 'norm' such that it
    // is large and proportional to the index of the constraint:
    //   norm = 1e4 * (constraintindex + 1 )

    const double defaultnorm = 1e4;
    size_t       numParameters( ipar );
    for ( ; ipar < size; ++ipar ) {
      // first compute the length of the derivative vector (taking
      // into account the scales of the parameters)
      double norm2( 0 );
      for ( size_t jpar = 0; jpar < numParameters; ++jpar ) {
        double derivative = halfD2Chi2DAlpha2( ipar, jpar ) * scale( jpar );
        norm2 += derivative * derivative;
      }
      // now set the scale
      if ( norm2 > 0 ) scale( ipar ) = defaultnorm / std::sqrt( norm2 ) * ( ipar - numParameters + 1 );
    }

    // now apply the scales
    for ( size_t i = 0u, iEnd = size; i < iEnd; ++i ) {
      halfDChi2DAlpha( i ) *= scale( i );
      for ( size_t j = 0u; j <= i; ++j ) halfD2Chi2DAlpha2( i, j ) *= scale( i ) * scale( j );
    }
  }

  void AlignUpdateTool::postCondition( AlVec& halfDChi2DAlpha, AlSymMat& halfD2Chi2DAlpha2, const AlVec& scale ) const {
    size_t size = halfDChi2DAlpha.size();
    for ( size_t i = 0u, iEnd = size; i < iEnd; ++i ) {
      halfDChi2DAlpha( i ) *= scale( i );
      for ( size_t j = 0u; j <= i; ++j ) halfD2Chi2DAlpha2( i, j ) *= scale( i ) * scale( j );
    }
  }

  void AlignUpdateTool::fillIterProfile( const HistoID& id, const std::string& title, size_t numiter, size_t iter,
                                         double value, double error ) const {
    AIDA::IHistogram1D* histo = book1D( id, title, -0.5, numiter - 0.5, numiter );
    TH1D*               h1    = Gaudi::Utils::Aida2ROOT::aida2root( histo );
    h1->SetBinContent( iter + 1, value );
    h1->SetBinError( iter + 1, error );
  }

  StatusCode AlignUpdateTool::addDaughterDerivatives( Equations&                    equations,
                                                      const GetElementsToBeAligned& elementProvider ) const {
    // Propagate information accumulated for daughters back to
    // parents.

    // Loop over the elements in reverse order. (Order is very
    // important: We do lowest in hierarchy first, such that it also
    // works for grandparents.)
    Elements sortedelements = elementProvider.elements();
    for ( auto ielem = sortedelements.rbegin(); sortedelements.rend() != ielem; ++ielem ) {
      auto& elem = *ielem;
      if ( !elem->daughters().empty() ) {
        debug() << "Accumulating daughter data for element: " << elem->name() << endmsg;

        // Important note: we accumulate the correlations first inside
        // the parent element. If that means that the correlation gets
        // in the wrong place (e.g. below the diagonal rather than
        // above), then we fix that only in the end.

        ElementData& elementdata = equations.element( elem->index() );
        if ( elementdata.numHits() != 0 ) {
          error()
              << "AlignUpdateTool::addDaughterDerivatives does not yet work if only subset of hits taken by daughters."
              << elem->name() << " " << elem->daughters() << " " << elementdata.numHits() << endmsg;
          return StatusCode::FAILURE;
        }

        // the transpose is because we once made mistake to use a column vector for the derivative.
        Gaudi::Matrix6x6 parentJacobian = ROOT::Math::Transpose( elem->jacobian() );
        // now accumulate information from all daughters. save the jacobians.
        std::vector<Gaudi::Matrix6x6> daughterToMotherJacobians;
        daughterToMotherJacobians.reserve( elem->daughters().size() );
        std::vector<int> daughterIndices( equations.elements().size(), -1 );
        size_t           dauindex( 0 );
        for ( const auto& dau : elem->daughters() ) {
          // 1. create the jacobian
          int              ierr;
          Gaudi::Matrix6x6 totalJacobian = parentJacobian * ROOT::Math::Transpose( dau->jacobian().Inverse( ierr ) );
          if ( ierr ) {
            error() << "Inversion error in AlignUpdateTool::addDaughterDerivatives. Failure." << endmsg;
            return StatusCode::FAILURE;
          }

          // 2. add everything from the daughter, except for its
          // correlations: those we'll do below all at once.
          const ElementData& daudata            = equations.element( dau->index() );
          ElementData        transformeddaudata = daudata;
          transformeddaudata.m_d2Chi2DAlphaDBeta.clear();
          transformeddaudata.transform( totalJacobian );
          elementdata.add( transformeddaudata );

          // 3. add the correlation with the daughter
          elementdata.m_d2Chi2DAlphaDBeta[dau->index()].matrix() += totalJacobian * daudata.d2Chi2DAlpha2();

          // save the jacobian for when we treat correlations
          daughterToMotherJacobians.push_back( totalJacobian );

          // save index such that we can easily navigate to the daughters
          daughterIndices[dau->index()] = dauindex;
          ++dauindex;
        }

        // 4. Add all off-diagonal terms that were not added yet. This is quite tricky.
        size_t leftindex( 0 );
        for ( const auto& data : equations.elements() ) {
          if ( leftindex != elem->index() ) { // skip the mother itself
            for ( const auto& jelem : data.d2Chi2DAlphaDBeta() ) {
              size_t rightindex    = jelem.first;
              int    leftdaughter  = daughterIndices[leftindex];
              int    rightdaughter = daughterIndices[rightindex];
              if ( leftdaughter >= 0 ) {
                elementdata.m_d2Chi2DAlphaDBeta[rightindex].matrix() +=
                    daughterToMotherJacobians[leftdaughter] * jelem.second.matrix();
              }
              if ( rightdaughter >= 0 ) {
                elementdata.m_d2Chi2DAlphaDBeta[leftindex].matrix() +=
                    daughterToMotherJacobians[rightdaughter] * ROOT::Math::Transpose( jelem.second.matrix() );
              }
              // the elements where both are daughters need to be treated seperately
              if ( leftdaughter >= 0 && rightdaughter >= 0 ) {
                // Add this term to the diagonal. Note that we
                // actually have this term twice. Its transpose is
                // hidden because we only keep lowerdiagonal. We
                // therefore symmetrize it as follows:
                Gaudi::Matrix6x6 tmpasym = daughterToMotherJacobians[leftdaughter] * jelem.second.matrix() *
                                           ROOT::Math::Transpose( daughterToMotherJacobians[rightdaughter] );
                Gaudi::SymMatrix6x6 tmpsym;
                ROOT::Math::AssignSym::Evaluate( tmpsym, tmpasym + ROOT::Math::Transpose( tmpasym ) );
                elementdata.m_d2Chi2DAlpha2 += tmpsym;
              }
            }
          }
          ++leftindex;
        }
        // now swap any element that we added with wrongly ordered index.
        ElementData::OffDiagonalContainer keepelements;
        for ( auto& jelem : elementdata.d2Chi2DAlphaDBeta() ) {
          if ( jelem.first < elem->index() ) {
            equations.element( jelem.first ).m_d2Chi2DAlphaDBeta[elem->index()].matrix() +=
                ROOT::Math::Transpose( jelem.second.matrix() );
          } else if ( jelem.first == elem->index() ) {
            error() << "VERY serious problem in addDaughterData. This still needs some development." << jelem.first
                    << " " << elem->index() << endmsg;
            // just symmetrize and add? (will only work if it is
            // actually symmetric :-) First understand again how this
            // could occur.
          } else {
            keepelements.insert( jelem );
          }
        }
        elementdata.m_d2Chi2DAlphaDBeta = keepelements;
      }
    }
    return StatusCode::SUCCESS;
  }

  namespace {
    std::pair<const Element*, int> findElementByParameter( const Elements& elements,
                                                           const size_t    globalparameterindex ) {
      std::pair<const Element*, int> rc( 0, -1 );
      for ( const auto& ielem : elements ) {
        const int di = globalparameterindex - ielem->activeParOffset();
        if ( 0 <= di && di < int( ielem->dofMask().nActive() ) ) {
          int parindex = ielem->dofMask().parIndex( di );
          rc.first     = ielem;
          rc.second    = parindex;
          break;
        }
      }
      return rc;
    }
  } // namespace

  bool AlignUpdateTool::checkGeometry( const Equations&              equations,
                                       const GetElementsToBeAligned& elementProvider ) const {
    // test that the alignment constants in AlEquations actually correspond to what is in the geometry
    const Elements& elements = elementProvider.elements();
    if ( elements.size() != equations.nElem() ) {
      error() << "Number of elements in elementprovider and equations do not match:" << elements.size() << " "
              << equations.nElem() << endmsg;
      return false;
    }

    const double maxmag2     = 1e-10; // could be finetuned a bit
    bool         checkpassed = true;
    for ( auto ielem : elements ) {
      bool thisBad = false;
      // compare the current delta
      const auto&          element    = equations.element( ielem->index() );
      const Gaudi::Vector6 alpha      = ielem->currentDelta().transformParameters();
      const Gaudi::Vector6 dalpha     = alpha - element.alpha();
      const double         dalphamag2 = ROOT::Math::Dot( dalpha, dalpha );
      if ( dalphamag2 > maxmag2 ) thisBad = true;
      // compare the current alignment frame
      const Gaudi::Vector6 alignframe      = AlParameters( ielem->alignmentFrame() ).transformParameters();
      const Gaudi::Vector6 dalignframe     = alignframe - element.alignFrame();
      const double         dalignframemag2 = ROOT::Math::Dot( dalignframe, dalignframe );
      if ( dalignframemag2 > maxmag2 ) { thisBad = true; }
      if ( thisBad ) {
        warning() << "Difference in geometry between frame " << ielem->name() << " too large; mag: " << dalphamag2
                  << ", frame mag: " << dalignframemag2 << endmsg;

        std::stringstream elem;
        ielem->fillStream( elem );

        warning() << "Element:" << endmsg << elem.str() << endmsg;

        checkpassed = false;
      }
    }
    return checkpassed;
  }

  StatusCode AlignUpdateTool::process( const Equations& constequations, const GetElementsToBeAligned& elementProvider,
                                       size_t iteration, size_t maxiteration, ConvergenceStatus& convergencestatus
#ifdef USE_DD4HEP
                                       ,
                                       LHCb::Det::LbDD4hep::IDD4hepSvc& dataSvc
#endif
                                       ) const {
    // Print a warning if the geometry used to compute the derivatives
    // is not the same as the actual one. It is no longer a reason to
    // abort since we allow using derivatives computed with a
    // different geometry.
    convergencestatus = ConvergenceStatus::Converged;
    StatusCode sc     = StatusCode::SUCCESS;
    // copy the equations such that we can manipulate them
    auto equations = constequations;

    if ( !checkGeometry( equations, elementProvider ) ) {
      sc = Warning( "Cannot initialize geometry to that found in AlEquations.", StatusCode::FAILURE );
      if ( !sc.isSuccess() ) return sc;
    }

    typedef Gaudi::Matrix1x6 Derivatives;
    if ( equations.nElem() == 0 ) {
      warning() << "==> No elements to align." << endmsg;
      return sc;
    }

    debug() << "==> iteration " << iteration << " : Initial alignment conditions  : [";
    // Store constants before the update
    info() << "==> Updating constants" << endmsg;
    std::ostringstream logmessage;
    logmessage << "********************* ALIGNMENT LOG ************************" << std::endl
               << "Iteration: " << iteration << std::endl
               << "Total number of events: " << equations.numEvents() << std::endl
               << "Time of first event [ns]: " << equations.firstTime().ns() << " --> "
               << equations.firstTime().format( true, "%F %r" ) << std::endl
               << "Time of last event [ns] : " << equations.lastTime().ns() << " --> "
               << equations.lastTime().format( true, "%F %r" ) << std::endl
               << "Time at initialize [ns] : " << equations.initTime().ns() << " --> "
               << equations.initTime().format( true, "%F %r" ) << std::endl
               << "First/last run number: " << equations.firstRun() << "/" << equations.lastRun() << std::endl
#ifdef USE_DD4HEP
               << "Min/max IOV: " << equations.minIOV() << "/" << equations.maxIOV() << std::endl
#endif
               << "Used " << equations.numVertices() << " vertices for alignment" << std::endl
               << "Used " << equations.numParticles() << " particles for alignment" << std::endl
               << "Used " << equations.numTracks() << " tracks for alignment" << std::endl
               << "Total chisquare/dofs:    " << equations.totalChiSquare() << " / " << equations.totalNumDofs()
               << std::endl
               << "Average track chisquare: " << equations.totalTrackChiSquare() / equations.numTracks() << std::endl
               << "Track chisquare/dof:     " << equations.totalTrackChiSquare() / equations.totalTrackNumDofs()
               << std::endl
               << "Vertex chisquare/dof:    "
               << ( equations.totalVertexNumDofs() > 0
                        ? equations.totalVertexChiSquare() / equations.totalVertexNumDofs()
                        : 0.0 )
               << std::endl
               << "Used " << equations.numHits() << " hits for alignment" << std::endl
               << "Total number of hits in external detectors: " << equations.numExternalHits() << std::endl
               << "Average number of tracks per vertex: "
               << ( equations.totalVertexNumDofs() > 0
                        ? 0.5 * ( equations.totalVertexNumDofs() / double( equations.numVertices() ) + 3 )
                        : 0.0 )
               << std::endl;

    // if there are mother-daughter relations, the information from
    // the daugters must be transformed to the mothers. This is kind
    // of tricky. it should also be done only once!
    info() << "Adding daughter derivatives" << endmsg;
    sc = addDaughterDerivatives( equations, elementProvider );
    if ( !sc.isSuccess() ) return sc;

    if ( msgLevel( MSG::DEBUG ) ) {
      size_t index( 0 );
      for ( const auto& ieq : equations.elements() ) {
        debug() << "\n==> V[" << index << "] = " << ieq.dChi2DAlpha() << endmsg;
        debug() << "\n==> M[" << index << ',' << index << "] = " << ieq.d2Chi2DAlpha2() << endmsg;
        for ( const auto& jeq : ieq.d2Chi2DAlphaDBeta() ) {
          debug() << "==> M[" << index << "," << jeq.first << "] = " << jeq.second.matrix() << endmsg;
        }
        ++index;
      }
    }
    // add the survey constraints
    info() << "Adding survey constraints. " << endmsg;
    const Elements& elements    = elementProvider.elements();
    LHCb::ChiSquare surveychisq = m_chisqconstrainttool->addConstraints( elements, equations, logmessage );
    info() << "End of adding survey constraints. " << endmsg;

    // Create the dof mask and a map from AlignableElements to an
    // offset. The offsets are initialized with '-1', which signals 'not
    // enough hits'.
    size_t numParameters( 0 ), numExcluded( 0 );
    for ( auto elem : elements ) {
      if ( equations.element( elem->index() ).numHits() >= m_minNumberOfHits &&
           equations.element( elem->index() ).d2Chi2DAlpha2().Trace() > 0 && elem->dofMask().nActive() > 0 ) {
        elem->setActiveParOffset( numParameters );
        numParameters += elem->dofMask().nActive();
      } else {
        elem->setActiveParOffset( -1 );
        if ( elem->dofMask().nActive() > 0 ) ++numExcluded;
      }
    }
    info() << "Number of alignment parameters: " << numParameters << endmsg;

    if ( numParameters > 0 ) {

      // now fill the 'big' vector and matrix
      AlVec    halfDChi2dX( numParameters );
      AlSymMat halfD2Chi2dX2( numParameters );
      size_t   index( 0 );
      int      iactive, jactive;
      for ( auto& eq : equations.elements() ) {
        const Element& ielem = *( elements[index] );
        // 1st derivative and diagonal 2nd derivative. note that we correct here for the fraction of outliers
        double outliercorrection = 1. / eq.fracNonOutlier();
        for ( unsigned ipar = 0; ipar < Derivatives::kCols; ++ipar ) {
          if ( 0 <= ( iactive = ielem.activeParIndex( ipar ) ) ) {
            halfDChi2dX( iactive ) = eq.dChi2DAlpha()( ipar ) * outliercorrection;
            for ( unsigned jpar = 0; jpar <= ipar; ++jpar )
              if ( 0 <= ( jactive = ielem.activeParIndex( jpar ) ) )
                halfD2Chi2dX2.fast( iactive, jactive ) = eq.d2Chi2DAlpha2()( ipar, jpar );
          }
        }
        // second derivative. fill only non-zero terms
        for ( const auto& m : eq.d2Chi2DAlphaDBeta() ) {
          // guaranteed: index < jndex .. that's how we fill it. furthermore, row==i, col==j
          if ( !( index < m.first ) ) {
            error() << index << " " << m.first << endmsg;
            assert( index < m.first );
          }
          const Element& jelem = *( elements[m.first] );
          for ( unsigned ipar = 0; ipar < Derivatives::kCols; ++ipar )
            if ( 0 <= ( iactive = ielem.activeParIndex( ipar ) ) )
              for ( unsigned jpar = 0; jpar < Derivatives::kCols; ++jpar )
                if ( 0 <= ( jactive = jelem.activeParIndex( jpar ) ) ) {
                  assert( jactive > iactive );
                  halfD2Chi2dX2.fast( jactive, iactive ) = m.second.matrix()( ipar, jpar );
                }
        }
        ++index;
      }

      // add the lagrange constraints to the linear system
      AlignConstraints constraints{elementProvider, m_constraintNames.value(), m_useWeightedAverage.value(), info()};
      size_t           numConstraints = constraints.add( elements, equations, halfDChi2dX, halfD2Chi2dX2, info() );
      size_t           numDoFs        = numParameters - numConstraints;

      logmessage << "Number of alignables with insufficient statistics: " << numExcluded << std::endl
                 << "Number of lagrange constraints: " << numConstraints << std::endl
                 << "Number of chisq constraints:    " << surveychisq.nDoF() << std::endl
                 << "Number of active parameters:    " << numParameters << std::endl;

      if ( halfDChi2dX.size() < 20 ) {
        info() << "AlVec Vector    = " << halfDChi2dX << endmsg;
        info() << "AlSymMat Matrix = " << halfD2Chi2dX2 << endmsg;
      } else {
        info() << "Matrices too big to dump to std" << endmsg;
        // check that second derivative looks okay:
        // 1. check diagonal elements:
        for ( size_t i = 0; i < numParameters; ++i )
          if ( halfD2Chi2dX2( i, i ) <= 0 )
            info() << "Negative diagonal element: " << i << " " << halfD2Chi2dX2( i, i ) << endmsg;
        for ( size_t i = 0; i < numParameters; ++i )
          for ( size_t j = 0; j < i; ++j ) {
            const auto eta = halfD2Chi2dX2( i, j ) / std::sqrt( halfD2Chi2dX2( i, i ) * halfD2Chi2dX2( j, j ) );
            if ( eta < -1 || eta > 1 )
              info() << "Off-diagonal element out of range: i, j, eta:" << i << "," << j << "," << eta << endmsg;
          }
      }

      // Tool returns H^-1 and alignment constants. Need to copy the 2nd derivative because we still need it.
      AlVec    scale( halfD2Chi2dX2.size() );
      AlSymMat covmatrix = halfD2Chi2dX2;
      AlVec    solution  = halfDChi2dX;
      if ( m_usePreconditioning ) {
        info() << "Applying pre-conditioning" << endmsg;
        preCondition( elements, equations, solution, covmatrix, scale );
      }
      info() << "Calling solver tool" << endmsg;
      IAlignSolvTool::SolutionInfo solinfo;
      using clock   = std::chrono::high_resolution_clock;
      const auto t1 = clock::now();
      sc            = m_matrixSolverTool->compute( covmatrix, solution, solinfo );
      const auto t2 = clock::now();
      logmessage << "Time to compute solution [ms]: "
                 << std::chrono::duration<double, std::nano>( t2 - t1 ).count() * 1.0e-6 << std::endl;
      if ( solinfo.numNegativeEigenvalues != numConstraints ) {
        error() << "Number of negative eigenvalues is not equal to number of constraints: "
                << solinfo.numNegativeEigenvalues << " " << numConstraints << endmsg;
      }
      if ( sc.isSuccess() ) {
        // undo the scaling
        if ( m_usePreconditioning ) {
          info() << "Applying post-conditioning" << endmsg;
          postCondition( solution, covmatrix, scale );
        }

        // test the numerical accuracy of the solution
        const AlVec  errorvec = halfD2Chi2dX2 * solution - halfDChi2dX;
        const double error2   = errorvec.dot( errorvec );
        logmessage << "Error2: " << error2 << std::endl;

        info() << "computing delta-chi2" << endmsg;
        double deltaChi2 = solution.dot( halfDChi2dX ); // somewhere we have been a bit sloppy with two minus signs!
        if ( deltaChi2 < 0 ) {
          error() << "Serious problem in update: delta-chi2 is negative: " << deltaChi2 << endmsg;
          //<< "Will recopute solution without scaling to see what that does! " << endmsg ;
          // covmatrix = halfD2Chi2dX2 ;
          // solution  = halfDChi2dX ;
          // solved = m_matrixSolverTool->compute(covmatrix, solution);
          // deltaChi2 = solution * halfDChi2dX ;
        }

        if ( msgLevel( MSG::DEBUG ) ) {
          info() << "Solution = " << solution << endmsg;
          info() << "Covariance = " << covmatrix << endmsg;
        }
        logmessage << "Alignment delta chisquare/dof: " << deltaChi2 << " / " << numDoFs << std::endl
                   << "Normalised alignment change chisquare: " << deltaChi2 / numDoFs << std::endl
                   << "Alignment delta chisquare/track dof: " << deltaChi2 / equations.totalTrackNumDofs() << std::endl;
        logmessage << "Alternative total chisq: " << solinfo.totalChisq << std::endl
                   << "Maximum chisq contribution of eigenmode: " << solinfo.maxChisqEigenMode << std::endl
                   << "Number of negative eigenvalues: " << solinfo.numNegativeEigenvalues << std::endl
                   << "Number of eigenvalues < 1: " << solinfo.numSmallEigenvalues << std::endl
                   << "Smallest eigenvalues: ";
        for ( int i = 0; i < 10; ++i ) logmessage << solinfo.eigenvalues[i] << " ";
        logmessage << std::endl;
        // print which parameter contributes most to this mode
        {
          std::pair<const Element*, int> elem = findElementByParameter( elements, solinfo.weakestModeMaxPar );
          if ( elem.first ) {
            logmessage << "Parameter contributing most to weakest mode: " << elem.first->name() << " " << elem.second
                       << " " << solinfo.weakestModeMaxParCoef << std::endl;
          }
        }
        // compute which parameter contributes most to total delta chi2
        {
          size_t imaxchi2( 0 );
          double maxchi2( 0 );
          for ( size_t i = 0; i < halfD2Chi2dX2.size(); ++i ) {
            double thisdeltachi2 = solution( i ) * halfDChi2dX( i );
            if ( maxchi2 < thisdeltachi2 ) {
              maxchi2  = thisdeltachi2;
              imaxchi2 = i;
            }
          }
          std::pair<const Element*, int> elem = findElementByParameter( elements, imaxchi2 );
          logmessage << "Alignment parameter with largest contribution to chi2: " << elem.first->name() << " "
                     << elem.second << " " << maxchi2 << std::endl;
        }
        // Did we converge?
        if ( deltaChi2 / numDoFs < m_maxDeltaChi2PDofForConvergence &&
             solinfo.maxChisqEigenMode < computeLookElsewhereChi2Cut( m_maxModeDeltaChi2ForConvergence, numDoFs ) )
          convergencestatus = ConvergenceStatus::Converged;
        else
          convergencestatus = ConvergenceStatus::NotConverged;
        logmessage << "Convergence status: " << toString( convergencestatus ) << std::endl;

        // dump the log message to the output here. save the rest for alignlog.txt
        info() << logmessage.str() << endmsg;

        // print parameters that seem most important
        logmessage << "Most important dofs, including survey: " << std::endl;
        dumpMostImportantDofs( elements, equations, logmessage );

        // print the constraints
        constraints.print( solution, covmatrix, logmessage );

        if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Putting alignment constants" << endmsg;
        size_t             iElem( 0u );
        double             totalLocalDeltaChi2( 0 ); // another figure of merit of the size of the misalignment.
        std::ostringstream modmessage;

        for ( const auto& elem : elements ) {
          const ElementData& elemdata = equations.element( iElem );
          modmessage << "Alignable: " << elem->name() << std::endl
                     << "Global position: " << elem->centerOfGravity()
                     << " Average position of hits: " << elemdata.averageHitPosition() << std::endl
                     << "Number of tracks/hits/outliers seen: " << elemdata.numTracks() << " " << elemdata.numHits()
                     << " " << elemdata.numOutliers() << std::endl;
          modmessage << "Local-to-global diagonal: ";
          for ( int i = 0; i < 6; ++i ) modmessage << std::setprecision( 3 ) << elem->jacobian()( i, i ) << " ";
          modmessage << std::endl;

          LHCb::ChiSquare surveychisqbefore = m_chisqconstrainttool->chiSquare( *elem );

          int offset = elem->activeParOffset();
          if ( offset < 0 && elem->dofMask().nActive() > 0 ) {
            modmessage << "Not enough hits for alignment. Skipping update." << std::endl;
          } else if ( offset >= 0 ) {
            AlParameters delta( solution, covmatrix, halfD2Chi2dX2, elem->dofMask(), offset );
            AlParameters reftotaldelta = elem->currentActiveTotalDelta();
            AlParameters refdelta      = elem->currentActiveDelta();
            // modmessage << delta ;
            for ( unsigned int iactive = 0u; iactive < delta.dim(); ++iactive )
              modmessage << std::setiosflags( std::ios_base::left ) << std::setprecision( 4 ) << std::setw( 3 )
                         << delta.activeParName( iactive ) << " :"
                         << " curtot= " << std::setw( 12 ) << reftotaldelta.parameters()[iactive]
                         << " cur= " << std::setw( 12 ) << refdelta.parameters()[iactive]
                         << " delta= " << std::setw( 12 ) << delta.parameters()[iactive] << " +/- " << std::setw( 12 )
                         << AlParameters::signedSqrt( delta.covariance()( iactive, iactive ) ) << " [ "
                         << std::setw( 12 ) << AlParameters::signedSqrt( 1 / delta.weightMatrix()( iactive, iactive ) )
                         << " ] "
                         << " gcc= " << delta.globalCorrelationCoefficient( iactive ) << std::endl;
            double contributionToCoordinateError = delta.measurementCoordinateSigma( elemdata.weightR() );
            double coordinateError               = std::sqrt( elemdata.numHits() / elemdata.weightV() );
            modmessage << "contribution to hit uncertainty (absolute/relative): " << contributionToCoordinateError
                       << " " << contributionToCoordinateError / coordinateError << std::endl;

            // compute another figure of merit for the change in
            // alignment parameters that does not rely on the
            // correlations. this should also go into either
            // AlParameters or AlEquation
            double thisLocalDeltaChi2 =
                ROOT::Math::Similarity( delta.transformParameters(), equations.element( iElem ).d2Chi2DAlpha2() );
            modmessage << "local delta chi2 / dof: " << thisLocalDeltaChi2 << " / " << delta.dim() << std::endl;
            totalLocalDeltaChi2 += thisLocalDeltaChi2;

            if ( !m_skipGeometryUpdate ) {
              StatusCode sc;
              try {
                sc = elem->updateGeometry( delta
#ifdef USE_DD4HEP
                                           ,
                                           dataSvc
#endif
                );
              } catch ( std::string& errorMsg ) { fatal() << errorMsg; }
              if ( !sc.isSuccess() ) error() << "Failed to set alignment condition for " << elem->name() << endmsg;
            }

            std::string name    = elem->name();
            std::string dirname = "element" + std::to_string( elem->index() ) + "/"; // name + "/"
            fillIterProfile( dirname + std::to_string( 10000u ), "Delta Tx vs iteration for " + name, maxiteration,
                             iteration, delta.translation()[0], delta.errTranslation()[0] );
            fillIterProfile( dirname + std::to_string( 20000u ), "Delta Ty vs iteration for " + name, maxiteration,
                             iteration, delta.translation()[1], delta.errTranslation()[1] );
            fillIterProfile( dirname + std::to_string( 30000u ), "Delta Tz vs iteration for " + name, maxiteration,
                             iteration, delta.translation()[2], delta.errTranslation()[2] );
            fillIterProfile( dirname + std::to_string( 40000u ), "Delta Rx vs iteration for " + name, maxiteration,
                             iteration, delta.rotation()[0], delta.errRotation()[0] );
            fillIterProfile( dirname + std::to_string( 50000u ), "Delta Rx vs iteration for " + name, maxiteration,
                             iteration, delta.rotation()[1], delta.errRotation()[1] );
            fillIterProfile( dirname + std::to_string( 60000u ), "Delta Rx vs iteration for " + name, maxiteration,
                             iteration, delta.rotation()[2], delta.errRotation()[2] );

            // ;
          }

          // print this one after the update
          LHCb::ChiSquare surveychisq = m_chisqconstrainttool->chiSquare( *elem );
          modmessage << "survey chi2 / dof (before/after): " << surveychisqbefore.chi2() << " " << surveychisq.chi2()
                     << " / " << surveychisq.nDoF() << std::endl;

          AlParameters aligndeltaafter = elem->currentDelta();
          modmessage << "align pars: " << aligndeltaafter.transformParameters() << std::endl;
          const AlParameters* survey = m_chisqconstrainttool->surveyParameters( *elem );
          if ( survey != 0 ) {
            modmessage << "survey pars:   " << survey->transformParameters() << std::endl;
            modmessage << "survey uncertainties: " << survey->transformErrors() << std::endl;
          }
          ++iElem;
        }
        // Store constants after the update
        logmessage << "total local delta chi2 / dof: " << totalLocalDeltaChi2 << " / " << numParameters << std::endl;

        // dump information on the worst survey constraints
        dumpWorstSurveyOffenders( elements, logmessage );

        // all the messages from module updates will be at  the end
        logmessage << modmessage.str();

        // fill some histograms
        fillIterProfile( 20, "Total number of used tracks for alignment vs iteration", maxiteration, iteration,
                         equations.numTracks() );
        fillIterProfile( 30, "Total sum of track chi2 vs iteration", maxiteration, iteration,
                         equations.totalChiSquare() );
        fillIterProfile( 31, "Average sum of track chi2 vs iteration", maxiteration, iteration,
                         equations.totalChiSquare() / equations.numTracks() );
        fillIterProfile( 32, "Normalised total sum of track chi2 vs iteration", maxiteration, iteration,
                         equations.totalChiSquare() / equations.totalNumDofs() );
        fillIterProfile( 40, "Delta Alignment chi2 vs iteration", maxiteration, iteration, deltaChi2 );
        fillIterProfile( 41, "Delta Alignment normalised chi2 vs iteration", maxiteration, iteration,
                         deltaChi2 / numParameters );
        AIDA::IHistogram1D* eigenvaluehisto =
            book1D( std::to_string( iteration ) + "/eigenvalues", "10log(eigenvalue)", -4, 6, 100 );
        for ( auto x : solinfo.eigenvalues )
          if ( x > 0 ) eigenvaluehisto->fill( std::log10( x ) );
      } else {
        error() << "Failed to solve system" << endmsg;
      }
    } else {
      logmessage << "No alignable degrees of freedom ... skipping solver tools and update." << std::endl;
    }

    logmessage << "********************* END OF ALIGNMENT LOG ************************";
    debug() << logmessage.str() << endmsg;

    if ( iteration == 0 ) {
      // fill a big 2D matrix with the number of hits
      size_t              num = equations.elements().size();
      AIDA::IHistogram2D* h2 =
          book2D( "HitMatrix", "number of tracks per element", -0.5, num - 0.5, num, -0.5, num - 0.5 );
      size_t index( 0 );
      for ( auto& eq : equations.elements() ) {
        h2->fill( double( index ), double( index ), eq.numTracks() );
        for ( const auto& jeq : eq.d2Chi2DAlphaDBeta() )
          h2->fill( double( index ), double( jeq.first ), jeq.second.numTracks() );
        ++index;
      }
    }

    if ( !m_logFileName.empty() ) {
      std::ofstream logfile( m_logFileName.value().c_str(), std::ios_base::app );
      logfile << logmessage.str() << std::endl;
    }
    return sc;
  }

  StatusCode AlignUpdateTool::queryInterface( const InterfaceID& id, void** ppI ) {
    // check the placeholder:
    if ( 0 == ppI ) return StatusCode::FAILURE;
    // check ID of the interface
    if ( IAlignUpdateTool::interfaceID() == id ) {
      *ppI = static_cast<IAlignUpdateTool*>( this );
      addRef();
    } else {
      return AlgTool::queryInterface( id, ppI );
    }
    return StatusCode::SUCCESS;
  }

  namespace {
    struct DofChisq {
      const Element* element;
      int            dof;
      double         chi2;
      double         par;
      double         sur;
      double         err;
      double         surerr;
    };

    inline bool operator<( const DofChisq& lhs, const DofChisq& rhs ) { return lhs.chi2 > rhs.chi2; }
  } // namespace

  void AlignUpdateTool::dumpMostImportantDofs( const Elements& elements, const Equations& equations,
                                               std::ostream& logmessage ) const {
    std::vector<DofChisq> dofchi2s;
    dofchi2s.reserve( 6 * elements.size() );
    DofChisq dofchi2;
    size_t   index( 0 );
    for ( auto& eq : equations.elements() ) {
      if ( eq.numHits() > 0 ) {
        dofchi2.element = elements[index];
        for ( dofchi2.dof = 0; dofchi2.dof < 6; ++dofchi2.dof ) {
          double dChi2DAlpha   = eq.dChi2DAlpha()( dofchi2.dof );
          double d2Chi2DAlpha2 = eq.d2Chi2DAlpha2()( dofchi2.dof, dofchi2.dof );
          // I could be a factor 2 wrong here
          dofchi2.chi2 = dChi2DAlpha * 1 / d2Chi2DAlpha2 * dChi2DAlpha;
          dofchi2.par  = -1 / d2Chi2DAlpha2 * dChi2DAlpha;
          dofchi2.err  = 1 / std::sqrt( d2Chi2DAlpha2 );
          dofchi2s.push_back( dofchi2 );
        }
      }
      ++index;
    }
    std::sort( dofchi2s.begin(), dofchi2s.end() );

    for ( size_t i = 0; i < 25 && i < dofchi2s.size(); ++i )
      logmessage << "  " << i << " " << dofchi2s[i].element->name() << " dof=" << dofchi2s[i].dof
                 << " active=" << dofchi2s[i].element->dofMask().isActive( dofchi2s[i].dof ) << " "
                 << " chi2= " << dofchi2s[i].chi2 << " delta= " << dofchi2s[i].par << " +/- " << dofchi2s[i].err
                 << std::endl;
  }

  void AlignUpdateTool::dumpWorstSurveyOffenders( const Elements& elements, std::ostream& logmessage ) const {
    std::vector<DofChisq> dofchi2s;
    dofchi2s.reserve( 6 * elements.size() );
    DofChisq dofchi2;
    for ( const auto* element : elements ) {
      const AlParameters* psurvey = m_chisqconstrainttool->surveyParameters( *element );
      if ( psurvey ) {
        AlParameters::TransformParameters surveypars = psurvey->transformParameters();
        AlParameters::TransformCovariance surveycov  = psurvey->transformCovariance();

        AlParameters                      currentdelta = element->currentDelta();
        AlParameters::TransformParameters alignpars    = currentdelta.transformParameters();
        AlParameters::TransformCovariance aligncov     = currentdelta.transformCovariance();

        // this should work for both active and inactive parameters
        AlParameters::TransformParameters residual = alignpars - surveypars;
        AlParameters::TransformCovariance rescov   = surveycov - aligncov;
        dofchi2.element                            = element;
        for ( unsigned int dof = 0; dof < 6; ++dof ) {
          dofchi2.sur    = surveypars( dof );
          dofchi2.par    = alignpars( dof );
          dofchi2.err    = std::sqrt( aligncov( dof, dof ) );
          dofchi2.surerr = std::sqrt( surveycov( dof, dof ) );
          dofchi2.dof    = dof;
          dofchi2.chi2   = residual( dof ) * residual( dof ) / rescov( dof, dof );
          dofchi2s.push_back( dofchi2 );
        }
      }
    }
    std::sort( dofchi2s.begin(), dofchi2s.end() );

    logmessage << "Survey constraints with largest chisquare contribution: " << std::endl;
    for ( size_t i = 0; i < 10 && i < dofchi2s.size(); ++i )
      logmessage << "  " << i << " " << std::setw( 30 ) << std::setiosflags( std::ios_base::left )
                 << dofchi2s[i].element->name() << " dof=" << dofchi2s[i].dof
                 << " active=" << dofchi2s[i].element->dofMask().isActive( dofchi2s[i].dof ) << " "
                 << " chi2=" << std::setprecision( 3 ) << dofchi2s[i].chi2 << " "
                 << " survey=" << std::setprecision( 3 ) << dofchi2s[i].sur << " delta=" << std::setprecision( 3 )
                 << dofchi2s[i].par << " "
                 << " surveyerr=" << std::setprecision( 3 ) << dofchi2s[i].surerr
                 << " deltaerr=" << std::setprecision( 3 ) << dofchi2s[i].err << std::endl;
  }

  double AlignUpdateTool::computeLookElsewhereChi2Cut( double chi2, double n ) {
    // one convergence criterion is the maximum chi2 of any of the
    // alignment 'modes' after diagonalization. of course, due to
    // statistics the maximum that we find depends on the number of
    // modes. the following corrects the chi2 cut for this
    // 'look-elsewhere effect'.
    double chi2prime = chi2;
    if ( n > 0 ) {
      double p      = TMath::Erfc( std::sqrt( chi2 / 2 ) );
      double pprime = 1 - std::pow( ( 1 - p ), 1. / n );
      double x      = TMath::ErfcInverse( pprime );
      chi2prime     = 2 * x * x;
    }
    return chi2prime;
  }

} // namespace LHCb::Alignment
