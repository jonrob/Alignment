###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (ATrackSelector)

ATrackSelector().MinPCut = 0.0
ATrackSelector().MinPtCut = 0.0
ATrackSelector().MinChi2Cut = 0.0
ATrackSelector().MaxChi2Cut = 15.0
ATrackSelector().Charge = 0
ATrackSelector().MaxPCut = -1
ATrackSelector().MaxPtCut = -1
ATrackSelector().MinITHitCut = 0
ATrackSelector().MinOTHitCut = 15
ATrackSelector().MinTTHitCut = 0
ATrackSelector().MinEnergyCut = 0.0
ATrackSelector().OutputLevel = 1
