/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class ParticleToTrackContainer ParticleToTrackContainer.h
 *
 *  Make a subselection of a track list
 *
 *  @author Wouter Hulsbergen
 *  @date   05/01/2010
 */

#include "Event/Particle.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/ITrackSelector.h"
#include <string>

class ParticleToTrackContainer
    : public Gaudi::Functional::Transformer<LHCb::Track::Selection( const LHCb::Particle::Range& )> {

public:
  ParticleToTrackContainer( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, KeyValue{"ParticleLocation", ""}, KeyValue{"Selector", ""} )
      , m_selector( "", this ){};

  LHCb::Track::Selection operator()( const LHCb::Particle::Range& particles ) const override;

private:
  ToolHandle<ITrackSelector> m_selector;
};

DECLARE_COMPONENT( ParticleToTrackContainer )

namespace {
  void extractTracks( const LHCb::Particle& p, std::vector<const LHCb::Track*>& tracks ) {
    if ( p.proto() && p.proto()->track() ) tracks.push_back( p.proto()->track() );
    for ( const LHCb::Particle* dau : p.daughters() ) extractTracks( *dau, tracks );
  }
} // namespace

LHCb::Track::Selection ParticleToTrackContainer::operator()( const LHCb::Particle::Range& particles ) const {
  LHCb::Track::Selection tracks;

  // get all the tracks
  std::vector<const LHCb::Track*> alltracks;
  for ( const auto* p : particles ) extractTracks( *p, alltracks );

  // make sure all tracks are unique
  std::sort( alltracks.begin(), alltracks.end() );
  std::vector<const LHCb::Track*>::iterator it = std::unique( alltracks.begin(), alltracks.end() );
  alltracks.erase( it, alltracks.end() );

  for ( const auto* tr : alltracks ) {
    bool accept = true;
    if ( !m_selector.empty() ) accept = m_selector->accept( *tr );
    if ( accept ) tracks.insert( tr );
  }

  debug() << "candidates, tracks, selected tracks: " << particles.size() << " " << alltracks.size() << " "
          << tracks.size() << endmsg;

  if ( !m_selector.empty() ) m_selector.release().ignore();

  return tracks;
}
