/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackVertexer.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/TwoProngVertex.h"

#include "DetDesc/IDetectorElement.h"

#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IRegistry.h"

class DToKPiTwoProng final : public GaudiTupleAlg {

public:
  DToKPiTwoProng( const std::string& name, ISvcLocator* pSvc );
  StatusCode initialize() override;
  StatusCode execute() override;

private:
  LHCb::Track* track( const LHCb::Particle* part ) const;

  std::unique_ptr<LHCb::TwoProngVertex> refittedMass( const LHCb::Track& track1, const LHCb::Track& track2,
                                                      double zVert, IGeometryInfo const& geometry ) const;

  double PIDK( const LHCb::Particle* part ) const;

  Gaudi::Property<std::string> m_standardGeometry_address{this, "StandardGeometryTop", "/dd/Structure/LHCb"};

  ITrackVertexer*     m_vertexer;
  ITrackExtrapolator* m_trackExtrapolator;

  ITrackFitter* m_trackFit;
  ITrackFitter* m_trackPreFit;

  std::string m_particleLocation;
  std::string m_vertexLocation;
  std::string m_trackOutputLocation;
  std::string m_resonanceName;

  double m_pionMass;
  double m_kaonMass;
  double m_deltaMass;
  double m_minMass;
  double m_maxMass;
};

DECLARE_COMPONENT( DToKPiTwoProng )

DToKPiTwoProng::DToKPiTwoProng( const std::string& name, ISvcLocator* pSvc ) : GaudiTupleAlg( name, pSvc ) {

  declareProperty( "ParticleLocation", m_particleLocation = "/Event/Strip02/SeqD2HH/Phys/SelD2HH/Particles" );
  declareProperty( "VertexLocation", m_vertexLocation = "Rec/Vertex/DKPi" );
  declareProperty( "DaughterTrackLocation", m_trackOutputLocation = "Rec/Track/DKPiDaughters" );
  declareProperty( "dM", m_deltaMass = 24 * Gaudi::Units::MeV );
  declareProperty( "resonanceName", m_resonanceName = "D0" );
}

StatusCode DToKPiTwoProng::initialize() {

  StatusCode sc = GaudiTupleAlg::initialize();
  if ( sc.isFailure() ) { return Warning( "Failed to init base class", StatusCode::FAILURE ); }

  m_vertexer          = tool<ITrackVertexer>( "TrackVertexer" );
  m_trackExtrapolator = tool<ITrackExtrapolator>( "TrackMasterExtrapolator" );

  m_trackPreFit = tool<ITrackFitter>( "TrackMasterFitter", " preFit", this );
  m_trackFit    = tool<ITrackFitter>( "TrackMasterFitter", "fit", this );

  auto                          propertysvc = service<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
  const LHCb::ParticleProperty* prop        = propertysvc->find( m_resonanceName );
  if ( !prop ) return Error( "Failed to find resonance", StatusCode::SUCCESS );
  m_minMass = prop->mass() - m_deltaMass;
  m_maxMass = prop->mass() + m_deltaMass;

  const LHCb::ParticleProperty* prop2 = propertysvc->find( "pi+" );
  m_pionMass                          = prop2->mass();

  const LHCb::ParticleProperty* prop3 = propertysvc->find( "K+" );
  m_kaonMass                          = prop3->mass();

  /*
  using namespace LoKi::Particles;
  double dMass = massFromName(m_resonanceName);
  m_minMass = dMass - m_deltaMass;
  m_maxMass = dMass + m_deltaMass;
  m_pionMass = massFromName("pi+");
  m_kaonMass = massFromName("K+");
  */

  return StatusCode::SUCCESS;
}

StatusCode DToKPiTwoProng::execute() {

  // Create the output container
  typedef KeyedContainer<LHCb::TwoProngVertex, Containers::HashMap> TwoProngVertices;
  TwoProngVertices*                                                 vertContainer = new TwoProngVertices();
  put( vertContainer, m_vertexLocation );

  LHCb::Track::Selection* daughterCont = new LHCb::Track::Selection();
  put( daughterCont, m_trackOutputLocation );

  // Get the default geometry FIXME, use functional framework
  auto lhcb = getDet<IDetectorElement>( m_standardGeometry_address );
  if ( !lhcb ) { throw GaudiException( "Could not load geometry", name(), StatusCode::FAILURE ); }
  auto& geometry = *lhcb->geometry();

  // get the input particles
  const LHCb::Particle::Container* particles = get<LHCb::Particle::Container>( m_particleLocation );
  for ( auto iterP = particles->begin(); iterP != particles->end(); ++iterP ) {

    const SmartRefVector<LHCb::Particle> daughters = ( *iterP )->daughters();
    LHCb::Track*                         track1    = track( daughters.front() );
    LHCb::Track*                         track2    = track( daughters.back() );

    m_trackPreFit->operator()( *track1, geometry ).ignore();
    m_trackFit->   operator()( *track1, geometry ).ignore();

    m_trackPreFit->operator()( *track2, geometry ).ignore();
    m_trackFit->   operator()( *track2, geometry ).ignore();

    double z = ( *iterP )->endVertex()->position().z();

    // make the vertex, make first particle the kaon
    auto tvertex = ( PIDK( daughters[0] ) > PIDK( daughters[1] ) ? refittedMass( *track1, *track2, z, geometry )
                                                                 : refittedMass( *track2, *track1, z, geometry ) );

    double m = tvertex->mass( m_kaonMass, m_pionMass );

    // Tuple tuple = nTuple("testTuple");
    // tuple << Tuples::Column("m", m);
    // tuple->write();

    //   std::cout << " m" << m << std::endl;
    plot( m, "m", 1000., 2000, 200 );
    if ( m > m_minMass && m < m_maxMass ) {
      vertContainer->insert( tvertex.release() );
      daughterCont->insert( track1 );
      daughterCont->insert( track2 );
    }
  } // loop candidates

  return StatusCode::SUCCESS;

} // the end of the Algorihtm

LHCb::Track* DToKPiTwoProng::track( const LHCb::Particle* part ) const {

  const LHCb::ProtoParticle* proto = part->proto();
  if ( !proto || proto->charge() == 0 ) return 0;
  return const_cast<LHCb::Track*>( proto->track() );
}

double DToKPiTwoProng::PIDK( const LHCb::Particle* part ) const {

  const LHCb::ProtoParticle* proto = part->proto();
  return proto == 0 ? -9999 : proto->info( LHCb::ProtoParticle::CombDLLk, -1000 );
}

std::unique_ptr<LHCb::TwoProngVertex> DToKPiTwoProng::refittedMass( const LHCb::Track& track1,
                                                                    const LHCb::Track& track2, double zVert,
                                                                    IGeometryInfo const& geometry ) const {

  LHCb::State state1 = *track1.stateAt( LHCb::State::ClosestToBeam );
  m_trackExtrapolator->propagate( state1, zVert, geometry ).ignore();
  LHCb::State state2 = *track2.stateAt( LHCb::State::ClosestToBeam );
  m_trackExtrapolator->propagate( state2, zVert, geometry ).ignore();

  auto vertex = m_vertexer->fit( state1, state2, geometry );
  vertex->addToTracks( &track1 );
  vertex->addToTracks( &track2 );

  return vertex;
}
