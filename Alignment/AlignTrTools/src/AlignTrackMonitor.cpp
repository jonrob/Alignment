/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
// Implementation file for class : AlignTrackMonitor
//
// Louis Nicolas, Adlene Hicheur (EPFL)
// Started: 06/06
///-----------------------------------------------------------------------------
/** @class AlignTrackMonitor AlignTrackMonitor Align/AlignTrTools/AlignTrackMonitor.h
 *
 *  Provide a monitoring tool for T-Station alignment studies
 *
 *  @author L. Nicolas
 *  @date   13/06/2006
 */

//===========================================================================
// Includes
//===========================================================================
// Gaudi
#include "Event/FitNode.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackKernel/TrackFunctors.h"

namespace {
  //=============================================================================
  // for comparing LHCbIDs
  //=============================================================================
  class lessByID {
  public:
    bool operator()( const LHCb::LHCbID& obj1, const LHCb::LHCbID& obj2 ) const {
      return obj1.lhcbID() < obj2.lhcbID();
    }
  };
  //===========================================================================
  // Check if this hit is shared with (at least) one other track
  //===========================================================================
  std::vector<LHCb::LHCbID> getSharedHits( LHCb::Tracks const& tracks ) {

    std::vector<LHCb::LHCbID> sharedHits;
    sharedHits.reserve( 1000 );

    for ( const auto& aTrack : tracks ) {
      for ( auto aID : aTrack->lhcbIDs() ) {
        if ( std::any_of( tracks.begin(), tracks.end(),
                          [&]( const auto* trk ) { return trk != aTrack && trk->isOnTrack( aID ); } ) ) {
          sharedHits.push_back( aID );
        }
      }
    }

    // sorting and stripping out duplicates
    std::sort( sharedHits.begin(), sharedHits.end(), lessByID() );
    sharedHits.erase( std::unique( sharedHits.begin(), sharedHits.end() ), sharedHits.end() );
    return sharedHits;
  }
} // namespace
//===========================================================================

class AlignTrackMonitor : public GaudiHistoAlg {
public:
  // Constructors and destructor
  AlignTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;

private:
  // Fill the Variables
  void fillVariables( const LHCb::Track* aTrack, const std::vector<LHCb::LHCbID>& );

  //======================================================================
  // Properties
  //======================================================================
  std::string m_tracksPath;

  //=============================================================================
};

//===========================================================================
// Declare
//===========================================================================
// Declaration of the algorithm factory
DECLARE_COMPONENT( AlignTrackMonitor )
//===========================================================================

//===========================================================================
// Constructor
//===========================================================================
AlignTrackMonitor::AlignTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiHistoAlg( name, pSvcLocator ) {

  // Location of the different objects in use
  this->declareProperty( "TracksLocation", m_tracksPath = "Rec/Track/Best" );
}
//===========================================================================

//===========================================================================
// Execute
//===========================================================================
StatusCode AlignTrackMonitor::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "AlignTrackMonitor starting execution" << endmsg;

  // Get the tracks
  const LHCb::Tracks* m_tracks = get<LHCb::Tracks>( m_tracksPath );

  // Get the hits shared by more than one track
  auto sharedHits = getSharedHits( *m_tracks );

  //**********************************************************************
  // Global Variables
  //**********************************************************************
  int m_eventMultiplicity = m_tracks->size();
  plot( m_eventMultiplicity, "Multiplicity", "Multiplicity", 0., 500., 50 );
  //**********************************************************************

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Retrieved " << m_eventMultiplicity << " tracks." << endmsg; }

  // Loop over tracks - select some and make some plots
  for ( auto const& aTrack : *m_tracks ) fillVariables( aTrack, sharedHits );

  return StatusCode::SUCCESS;
}
//===========================================================================

//===========================================================================
// Fill Variables
//===========================================================================
void AlignTrackMonitor::fillVariables( const LHCb::Track* aTrack, std::vector<LHCb::LHCbID> const& sharedHits ) {

  //**********************************************************************
  // Tracks Variables
  //**********************************************************************
  // Various counters
  unsigned int m_nSharedHits  = 0;
  double       m_fSharedHits  = 0;
  unsigned int m_nLadOverlaps = 0;
  unsigned int m_nBoxOverlaps = 0;

  // Track "quality"
  double m_trackChi2PerDoF = aTrack->chi2PerDoF();
  double m_trackChi2Prob   = aTrack->probChi2();

  // Get momentum, transverse momentum and error in GeV
  // Track momentum and more
  double m_trackP    = aTrack->p() / Gaudi::Units::GeV;
  double m_trackPt   = aTrack->pt() / Gaudi::Units::GeV;
  double m_trackErrP = sqrt( aTrack->firstState().errP2() ) / Gaudi::Units::GeV;

  // Track pseudo-rapidity
  double m_trackEta = aTrack->pseudoRapidity();

  // Track entry point in T-station
  // Track Slope and Position at 7500 mm
  LHCb::State aState    = aTrack->closestState( 7500. );
  auto        m_entryTX = aState.tx();
  auto        m_entryTY = aState.ty();
  auto        m_entryX  = aState.x();
  auto        m_entryY  = aState.y();
  //**********************************************************************

  // Loop over the nodes to get the hits variables
  for ( auto const& iNode : nodes( *aTrack ) ) {

    // Only loop on hits with measurement
    if ( !iNode->hasMeasurement() ) continue;

    //**********************************************************************
    // Number of shared hits in the track
    //**********************************************************************
    if ( std::binary_search( sharedHits.begin(), sharedHits.end(), iNode->measurement().lhcbID(), lessByID() ) )
      ++m_nSharedHits;
  }

  //**********************************************************************
  // Printing some informations in debug mode:
  //**********************************************************************
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "This track has " << 0 << " IT hits" << endmsg << "               " << 0 << " IT expected hits" << endmsg
            << "               " << m_nLadOverlaps << " IT ladder overlaps" << endmsg << "               " << 0
            << " IT box overlaps" << endmsg << "               " << 0 << " OT hits" << endmsg << "               " << 0
            << " OT expected hits" << endmsg << "               " << 0 << " holes" << endmsg << "               "
            << m_nSharedHits << " shared hits"
            << " (= " << 100 * m_fSharedHits << " %)" << endmsg << "               " << 0 << " neighbouring hits"
            << endmsg;
  //**********************************************************************

  //===========================================================================
  // Fill the histos
  //===========================================================================

  // Various numbers related to the track
  plot( m_nSharedHits, "NSharedHits", "NSharedHits", -0.5, 30.5, 31 );
  plot( m_fSharedHits, "FSharedHits", "FSharedHits", -0.01, 1.01, 50 );
  plot( m_nLadOverlaps, "NLadOverlaps", "NLadOverlaps", -0.5, 15.5, 16 );
  plot( m_nBoxOverlaps, "NBoxOverlaps", "NBoxOverlaps", -0.5, 3.5, 4 );

  // Track fit "quality"
  plot( m_trackChi2PerDoF, "TrackChi2PerDoF", "TrackChi2PerDoF", -2., 100., 50 );
  plot( m_trackChi2Prob, "TrackChi2Prob", "TrackChi2Prob", -0.01, 1.01, 50 );

  // Track momentum and more
  plot( m_trackP, "TrackP", "TrackP", -5., 205., 50 );
  plot( m_trackPt, "TrackPt", "TrackPt", -0.1, 10.1, 50 );
  plot( m_trackErrP, "TrackErrP", "TrackErrP", -0.1, 10.1, 50 );

  // Track pseudo-rapidity
  plot( m_trackEta, "TrackEta", "TrackEta", 0., 10., 50 );

  // Track State closest to 7500. mm
  plot( m_entryTX, "TXAt7500", "TXAt7500", -1., 1., 50 );
  plot( m_entryTY, "TYAt7500", "TYAt7500", -0.5, 0.5, 50 );
  plot( m_entryX, "XAt7500", "XAt7500", -3000., 3000., 50 );
  plot( m_entryY, "YAt7500", "YAt7500", -3000., 3000., 50 );
  //**********************************************************************
}
//===========================================================================
