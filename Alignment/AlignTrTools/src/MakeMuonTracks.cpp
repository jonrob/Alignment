/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonInterfaces/IMuonTrackRec.h"
#include "MuonInterfaces/MuonHit.h"
#include "MuonInterfaces/MuonTrack.h"

#include "Event/State.h"
#include "Event/StateVector.h"
#include "Event/Track.h"
#include "Kernel/LHCbID.h"
#include "LHCbAlgs/Transformer.h"
#include "TrackInterfaces/ITrackMomentumEstimate.h"
#include <Magnet/DeMagnet.h>

#include "Gaudi/Accumulators.h"

#include <string>
#include <vector>

namespace LHCb {

  /**
   *  Simple algorithm to make "LHCb" tracks from MuonNNet tracks
   *  and to copy them to some location.
   *
   *  @author Stefania Vecchi (alias Jan Amoraal)
   *  @date   2009-12-03
   */

  class MakeMuonTracks
      : public Algorithm::Transformer<Tracks( const RawBank::View&, const DeMuonDetector&, const DeMagnet& ),
                                      LHCb::DetDesc::usesConditions<DeMuonDetector, DeMagnet>> {
  public:
    MakeMuonTracks( const std::string& name, ISvcLocator* pSvcLocator );
    Tracks operator()( const RawBank::View&, const DeMuonDetector&, const DeMagnet& ) const override;

  private:
    Gaudi::Property<bool>         m_skipBigClusters{this, "SkipBigClusters", false};
    Gaudi::Property<bool>         m_Bfield{this, "BField", false};
    Gaudi::Property<unsigned int> m_MaxNTiles{this, "MaxNTiles", 6};

    ToolHandle<IMuonTrackRec>          m_muonRecTool{this, "MuonRecTool", "MuonNNetRec"};
    ToolHandle<ITrackMomentumEstimate> m_fCalcMomentum{this, "TrackPtKick", "TrackPtKick", "momentum tool"};

    mutable Gaudi::Accumulators::AveragingCounter<> m_hitsPerTrack{this, "nb MuonHits per track"};
    mutable Gaudi::Accumulators::AveragingCounter<> m_tilesPerTrack{this, "nb tiles used per track"};
    mutable Gaudi::Accumulators::Counter<>          m_trackRejecteBigCluster{this,
                                                                    "nb track not saved due to BIG cluster rejection"};
    mutable Gaudi::Accumulators::AveragingCounter<> m_nbTracks{this, "nb tracks created"};
    mutable Gaudi::Accumulators::AveragingCounter<> m_nbInputTracks{this, "nb input tracks"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( MakeMuonTracks, "MakeMuonTracks" )

} // namespace LHCb

LHCb::MakeMuonTracks::MakeMuonTracks( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"RawBank", {}}, KeyValue{"DeMuonDetector", DeMuonLocation::Default},
                    KeyValue{"DeMagnet", LHCb::Det::Magnet::det_path}},
                   KeyValue{"TracksOutputLocation", TrackLocation::Muon} ) {}

LHCb::Tracks LHCb::MakeMuonTracks::operator()( const RawBank::View& raw, const DeMuonDetector& muonDetector,
                                               const DeMagnet& magnet ) const {
  // optimize usage of counters
  auto hitsPerTrack           = m_hitsPerTrack.buffer();
  auto tilesPerTrack          = m_tilesPerTrack.buffer();
  auto trackRejecteBigCluster = m_trackRejecteBigCluster.buffer();

  auto [mHits, mTracks, tooManyHits] = m_muonRecTool->tracks( raw, muonDetector );
  m_nbInputTracks += mTracks.size();

  Tracks tracks{};
  for ( const auto& t : mTracks ) {
    auto track = std::make_unique<Track>();
    auto hits  = t.getHits();

    Gaudi::XYZPoint trackPos( t.bx() + t.sx() * muonDetector.getStationZ( 0 ),
                              t.by() + t.sy() * muonDetector.getStationZ( 0 ), muonDetector.getStationZ( 0 ) );
    LHCb::State     state( LHCb::StateVector( trackPos, Gaudi::XYZVector( t.sx(), t.sy(), 1.0 ), 1. / 10000. ) );

    if ( m_Bfield.value() ) {
      double qOverP( 0. ), sigmaQOverP( 0. );
      m_fCalcMomentum->calculate( magnet, &state, qOverP, sigmaQOverP ).ignore();
      state.setQOverP( qOverP );
    }

    state.setLocation( State::Muon );
    track->addToStates( state );

    hitsPerTrack += hits.size();
    int  ntile          = 0;
    bool flagBIGcluster = false; // flag to identify tracks with too big clusters
    for ( auto const* hit : hits ) {
      auto const Tiles = hit->getLogPadTiles();
      if ( Tiles.size() > m_MaxNTiles ) flagBIGcluster = true;
      for ( auto const& tile : Tiles ) { track->addToLhcbIDs( LHCbID( tile ) ); }
      ntile += Tiles.size();
    }
    tilesPerTrack += ntile;

    if ( m_skipBigClusters.value() && flagBIGcluster ) {
      ++trackRejecteBigCluster;
    } else {
      track->setPatRecStatus( Track::PatRecStatus::PatRecIDs );
      track->setType( Track::Types::Muon );
      tracks.insert( track.release() );
    }
  }
  m_nbTracks += tracks.size();
  return tracks;
}
