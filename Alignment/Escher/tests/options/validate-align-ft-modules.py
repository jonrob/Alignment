###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from xml.etree import ElementTree as ET
from pprint import pformat

xml = ET.parse(
    os.path.expandvars('ft-modules/xml/Conditions/FT/Alignment/Modules.xml'))
constants = {(c.attrib['name'], v.attrib['name']): list(
    map(float, v.text.split()))
             for c in xml.iter('condition') for v in c.iter('paramVector')}

result['constants'] = result.Quote(pformat(constants))

for key, vals in constants.items():
    if 'dPosXYZ' in key[1]:
        if abs(vals[0]) > 0.06 or abs(vals[2]) > 0.126:
            causes.append('constant {} not within limit'.format(key))
