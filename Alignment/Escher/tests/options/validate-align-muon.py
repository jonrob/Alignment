###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from xml.etree import ElementTree as ET
from pprint import pformat

xml = ET.parse(
    os.path.expandvars('muon/xml/Conditions/Muon/Alignment/Global.xml'))
constants = {(c.attrib['name'], v.attrib['name']): list(
    map(float, v.text.split()))
             for c in xml.iter('condition') for v in c.iter('paramVector')}

result['constants'] = result.Quote(pformat(constants))

for key, vals in constants.items():
    for v in vals:
        # XXXX Set to 1.3 while it was 0.9 before. Should be reverted once
        # problems are understood and fixed in the align-muon test
        if abs(v) > 1.3:
            causes.append('constant {} not within limit'.format(key))
