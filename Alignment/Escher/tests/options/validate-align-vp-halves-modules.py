###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re

with open('alignlog_vp_halves_modules.txt') as f:
    log = f.read()
result['alignlog'] = result.Quote(log)

res = re.search('Number of negative eigenvalues: (\d+)', log)
if res:
    n = int(res.group(1))
    if n > 6:
        causes.append('large number of negative eigenvalues')
else:
    causes.append(
        'could not find number of negative eigenvalues entry in log file')
