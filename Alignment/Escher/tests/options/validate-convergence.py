###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re

with open('alignlog_muon.txt') as f:
    log = f.read()

status = re.search(r'Convergence status: (\S+)', log).group(1)

result['alignlog'] = result.Quote(log)
if status != 'Converged':
    causes.append('No convergence reached')
