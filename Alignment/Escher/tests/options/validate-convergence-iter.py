###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re

with open('alignlog_vp_halves_modules.txt') as f:
    log = f.read()
result['alignlog'] = result.Quote(log)

res = re.findall(r'Convergence status: (\S+)', log)
if res:
    status = res[-1]
    if (status != 'Converged'):
        causes.append('No convergence reached in second iteration')
else:
    causes.append('Could not find Convergence data in log file')
