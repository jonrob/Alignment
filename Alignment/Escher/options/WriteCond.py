###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (Escher, TAlignment, ApplicationMgr, NTupleSvc)
TAlignment().CondFilePrefix = ""
myfile = 'MC09-it3-evts5100-skpCl-uniTr-dc__lhcbID.root'
Escher().DatasetName = myfile
ApplicationMgr().ExtSvc += ["NTupleSvc"]
tupleFile = "$ESCHERROOT/job/" + myfile
tuple = "FILE1 DATAFILE='" + tupleFile + "' TYP='ROOT' OPT='NEW'"
NTupleSvc().Output = [tuple]
