from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# File for running VP alignment on simulated Data
#
# Syntax is:
#
#   gaudirun.py $ESCHEROPTS/AlignVeloSensors.py $ESCHEROPTS/2008-MC-Files.py
#   gaudirun.py $ESCHEROPTS/AlignVeloSensors.py $ESCHEROPTS/DC06-Files.py
#
#   $ESCHEROPTS/gaudiiter.py -n 3 -e 1000 $ESCHEROPTS/AlignVeloSensors.py $ESCHEROPTS/2008-MC-Files.py
#
##############################################################################
from Configurables import Escher, TrackSys, LHCbApp, DstConf
from GaudiKernel.ProcessJobOptions import importOptions
from Configurables import CondDB
from Configurables import GaudiSequencer, ChargedProtoParticleMaker
from Configurables import CaloDigitConf, CaloProcessor, GlobalRecoConf
from Configurables import TAlignment
from TAlignment.Alignables import *
from TAlignment.TrackSelections import TrackRefiner
from Configurables import AlignAlgorithm
from Configurables import TrackContainerCopy, TrackSelector, TrackMonitor
from Configurables import VPTrackSelector
# from TrackFitter.ConfiguredFitters import *
from Gaudi.Configuration import appendPostConfigAction, INFO, DEBUG, VERBOSE
from TAlignment.SurveyConstraints import *
from GaudiConf import IOHelper
from Configurables import CaloFutureProcessor, GlobalRecoConf

importOptions('$STDOPTS/PreloadUnits.opts')

LHCbApp().Simulation = True
LHCbApp().DataType = 'Upgrade'
CondDB().Upgrade = True
#CondDB().Tags = {
#    "DDDB": 'upgrade/dddb-20190223',#'upgrade/dddb-20160304',#'HEAD',
#    "LHCBCOND": 'default',
#    "SIMCOND": 'upgrade/sim-20180530-vc-mu100',#'upgrade/sim-20150716-vc-mu100',#'HEAD',#'dd4hep',
#    "ONLINE": 'fake'
#}

LHCbApp().DDDBtag = "dddb-20190223"  #"upgrade/dddb-20190223"
LHCbApp().CondDBtag = "sim-20180530-vc-mu100"  #"upgrade/sim-20180530-vc-mu100"

#detectors = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr']
detectors = [
    'VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet',
    'Tr'
]
LHCbApp().Detectors = detectors

#CondDB().LoadCALIBDB = 'HLT1'

Escher().InputType = "DIGI"
Escher().Kalman = True
Escher().Millepede = False
Escher().EvtMax = -1
Escher().MoniSequence = []
#Escher().RecoSequence = ["Decoding", "VP", "VELOPIX","Tr","Vertex","CALO","PROTO"]
Escher().RecoSequence = [
    "Decoding", "TrFast", "TrBest", "VP", "VELOPIX", "CALO", "PROTO"
]
#Escher().RecoSequence = ["Decoding", "TrFast", "TrBest", "RICH", "RICHFUTURE", "CALO", "MUON", "PROTO", "SUMMARY"]
Escher().WithMC = True
Escher().Simulation = True
Escher().DataType = 'Upgrade'
Escher().Upgrade = True

AlignAlgorithm("AlignAlgorithm").ForcedInitialTime = 1

elements = Alignables()
dofs = "TxTyTzRxRyRz"
elements.VPModules(dofs)  # Ty Tz Rx Ry Rz')
#elements.VPLeft("TxTyRz")
#elements.VPRight("TxTyRz")
# constraints = ["Rz","Ry","Rx","Szx","Szy","SzRz"]
constraints = ["Tx", "Ty", "Szx", "Szy", "Rz", "SzRz"]

constraints = []
constraints.append(
    "VPFixModule10 : VP/VPLeft/Module10WithSupport: Tx Ty Tz Rx Ry Rz")
constraints.append(
    "VPFixModule11 : VP/VPRight/Module11WithSupport: Tx Ty Tz Rx Ry Rz")
#constraints.append("VPFixModule40 : VP/VPLeft/Module40WithSupport: Tx Ty Tz Rx Ry Rz")
#constraints.append("VPFixModule41 : VP/VPRight/Module41WithSupport: Tx Ty Tz Rx Ry Rz")
constraints.append(
    "VPFixModule40 : VP/VPLeft/Module40WithSupport: Tx Ty Tz Rx Ry Rz")
constraints.append(
    "VPFixModule41 : VP/VPRight/Module41WithSupport: Tx Ty Tz Rx Ry Rz")
#constraints.append("VPFixModule50 : VP/VPLeft/Module50WithSupport: Tx Ty Tz Rx Ry Rz")
#constraints.append("VPFixModule51 : VP/VPRight/Module51WithSupport: Tx Ty Tz Rx Ry Rz")
# constraints = []
print(("aligning elements", elements))
print("ok 1")

trackRefitSeq = GaudiSequencer("TrackRefitSeq")

# create a track list for tracks with velo hits
velotrackselector = TrackContainerCopy(
    "TracksWithVeloHits",
    inputLocations=["Rec/Track/Best"],
    outputLocation="Rec/Track/TracksWithVeloHits",
    Selector=TrackSelector())

# refit the tracks in that list
#velotrackrefitter = ConfiguredEventFitter(Name="TracksWithVeloHitsFitter",
#                                          TracksInContainer="Rec/Track/TracksWithVeloHits",
#                                          FieldOff=True)
#velotrackrefitter.Fitter.MeasProvider.IgnoreIT = True
#velotrackrefitter.Fitter.MeasProvider.IgnoreOT = True
#velotrackrefitter.Fitter.MeasProvider.IgnoreTT = True
#velotrackrefitter.Fitter.MeasProvider.IgnoreMuon = True
#velotrackrefitter.Fitter.MeasProvider.IgnoreUT = True
#velotrackrefitter.Fitter.MakeNodes = True
#velotrackrefitter.Fitter.MakeMeasurements = True
#
#trackRefitSeq.Members += [velotrackselector, velotrackrefitter]
trackRefitSeq.Members += [velotrackselector]


def redefinefilters():
    GaudiSequencer("RecoTrSeq").Members += [trackRefitSeq]


appendPostConfigAction(redefinefilters)


class myVPTracksOverlap(TrackRefiner):
    def __init__(self,
                 Name="VPOverlapTracks",
                 InputLocation="Rec/Track/Best",
                 Fitted=True):
        TrackRefiner.__init__(
            self, Name=Name, InputLocation=InputLocation, Fitted=Fitted)

    def configureSelector(self, a):
        a.Selector = VPTrackSelector()
        a.Selector.MinHitsASide = 1
        a.Selector.MinHitsCSide = 1
        a.Selector.TrackTypes = ["Velo", "VeloBackward", "Long"]


class myVPTracks(TrackRefiner):
    def __init__(self,
                 Name="VPOverlapTracks",
                 InputLocation="Rec/Track/Best",
                 Fitted=True):
        TrackRefiner.__init__(
            self, Name=Name, InputLocation=InputLocation, Fitted=Fitted)

    def configureSelector(self, a):

        a.Selector = VPTrackSelector()
        a.Selector.TrackTypes = ["Velo", "VeloBackward", "Long"]

    def configureSelector(self, a):
        a.Selector = VPTrackSelector()
        a.Selector.TrackTypes = ["Long"]
        a.Selector.MinHits = 5
        a.Selector.MinPCut = 5000
        a.Selector.MaxPCut = 200000
        a.Selector.MinPtCut = 200

        if self._fitted:
            a.Selector.MaxChi2Cut = 5
            a.Selector.MaxChi2PerDoFMatch = 5
            a.Selector.MaxChi2PerDoFVelo = 5
            a.Selector.MaxChi2PerDoFDownstream = 5


class VPBackwards(TrackRefiner):
    def __init__(self,
                 Name="VPTracks",
                 InputLocation="Rec/Track/Best",
                 Fitted=True):
        TrackRefiner.__init__(
            self, Name=Name, InputLocation=InputLocation, Fitted=Fitted)

    def configureSelector(self, a):
        a.Selector = VPTrackSelector()
        a.Selector.TrackTypes = ["VeloBackward"]

        a.Selector.MinHits = 5
        if self._fitted:
            a.Selector.MaxChi2Cut = 5


TAlignment().ElementsToAlign = list(elements)
TAlignment().TrackLocation = "Rec/Track/Best"
TAlignment().VertexLocation = "Rec/Vertex/Primary"
TAlignment().Constraints = constraints
TAlignment().TrackSelections = [myVPTracks()]  #, VPBackwards()]
TAlignment().WriteCondSubDetList = ['VP']
TAlignment().EigenValueThreshold = 100
TAlignment().UsePreconditioning = True
TAlignment().UseLocalFrame = True
# include survey constraints

surveyconstraints = SurveyConstraints()
surveyconstraints.VP()

# Convert Calo ReadoutStatus to ProcStatus
#caloBanks = GaudiSequencer("CaloBanksHandler")
#caloDetectors = [det for det in ['Spd', 'Prs', 'Ecal', 'Hcal'] if det in detectors]
#CaloDigitConf(ReadoutStatusConvert=True, Sequence=caloBanks, Detectors=caloDetectors)
#GaudiSequencer("EscherSequencer").Members += [caloBanks]

GlobalRecoConf().setProp("NoSpdPrs", True)


def doMyChanges():
    GaudiSequencer("HltFilterSeq").Members = []


appendPostConfigAction(doMyChanges)

IOHelper('ROOT').inputFiles([
    #'/eos/user/b/bimitres/Velo_alignment/Alignment/Boole.xdigi',
    #'/eos/lhcb/user/b/bimitres/SLAngular_Analysis/Upgrade_samples/00052250_00000001_1.xdigi'
    #'../LbDD4hepUpgrade/Gauss/Boole.xdigi'
    #'/eos/user/b/bimitres/Velo_alignment/LbDD4hepUpgrade/Gauss/Boole_v4.xdigi'
    #'/eos/user/b/bimitres/Velo_alignment/LbDD4hepUpgrade/Gauss/Boole.xdigi'
    #'/eos/user/b/bimitres/Velo_alignment/Alignment/Boole.xdigi'
    '/eos/lhcb/user/b/bimitres/SLAngular_Analysis/Upgrade_samples/00091069_00000001_1.digi'
])
