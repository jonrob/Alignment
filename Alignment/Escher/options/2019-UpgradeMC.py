###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Escher
Escher().DataType = "Upgrade"
Escher().Simulation = True
Escher().InputType = "DIGI"
from Configurables import CondDB
CondDB().Upgrade = True

from Configurables import LHCbApp
detectors = [
    'VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet',
    'Tr'
]
LHCbApp().Detectors = detectors

from PRConfig import TestFileDB
TestFileDB.test_file_db['upgrade_minbias_2019'].run(configurable=Escher())
