###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###############################################################################
# File for running VP alignment on simulated Data                             #
#                                                                             #
# Syntax is:                                                                  #
#                                                                             #
#   gaudirun.py $ESCHEROPTS/AlignVPHalves.py                                  #
#                                                                             #
###############################################################################

from TAlignment.TrackSelections import TrackRefiner

from Configurables import TrackSelector
from Gaudi.Configuration import appendPostConfigAction, INFO, DEBUG, VERBOSE
from TAlignment.SurveyConstraints import *
from Configurables import CaloFutureProcessor, GlobalRecoConf, NTupleSvc

importOptions('$STDOPTS/PreloadUnits.opts')

from Configurables import Escher
Escher().InputType = "DIGI"
Escher().EvtMax = -1
Escher().MoniSequence = []
Escher().RecoSequence = ["Decoding", "TrFast", "TrBest", "MUON"]
#Escher().WithMC = True
Escher().Simulation = True
Escher().DataType = 'Upgrade'
Escher().PrintFreq = 1
Escher().DatasetName = 'AlignVPHalves'

# configure the alignable degrees of freedom
from TAlignment.Alignables import *
elements = Alignables()
dofs = "TxTyTzRxRyRz"
elements.VPLeft(dofs)
elements.VPRight(dofs)


# configure the input to the alignment
class myVPOverlapTracks(TrackRefiner):
    def __init__(self,
                 Name="VPOverlapTracks",
                 InputLocation="Rec/Track/Best",
                 Fitted=True):
        TrackRefiner.__init__(
            self, Name=Name, InputLocation=InputLocation, Fitted=Fitted)

    def configureSelector(self, a):
        a.Selector = TrackSelector()
        a.Selector.MinNVeloALayers = 1
        a.Selector.MinNVeloCLayers = 1
        a.Selector.TrackTypes = ["Velo", "VeloBackward", "Long"]


class myVPGoodLongTracks(TrackRefiner):
    def __init__(self,
                 Name="VPGoodLongTracks",
                 InputLocation="Rec/Track/Best",
                 Fitted=True):
        TrackRefiner.__init__(
            self, Name=Name, InputLocation=InputLocation, Fitted=Fitted)

    def configureSelector(self, a):
        a.Selector = TrackSelector()
        a.Selector.TrackTypes = ["Long"]
        a.Selector.MinNVeloLayers = 5
        a.Selector.MinPCut = 5000
        a.Selector.MaxPCut = 200000
        a.Selector.MinPtCut = 200

        if self._fitted:
            a.Selector.MaxChi2Cut = 5
            a.Selector.MaxChi2PerDoFMatch = 5
            a.Selector.MaxChi2PerDoFVelo = 5
            a.Selector.MaxChi2PerDoFDownstream = 5


class myVPBackwardTracks(TrackRefiner):
    def __init__(self,
                 Name="VPBackwardTracks",
                 InputLocation="Rec/Track/Best",
                 Fitted=True):
        TrackRefiner.__init__(
            self, Name=Name, InputLocation=InputLocation, Fitted=Fitted)

    def configureSelector(self, a):
        a.Selector = TrackSelector()
        a.Selector.TrackTypes = ["Velo", "VeloBackward"]
        a.Selector.OnlyBackwardTracks = True

        a.Selector.MinNVeloLayers = 5
        if self._fitted:
            a.Selector.MaxChi2Cut = 5


from Configurables import TAlignment
TAlignment().ElementsToAlign = list(elements)
TAlignment().VertexLocation = "Rec/Vertex/Primary"
from TAlignment.TrackSelections import *
TAlignment().TrackSelections = [
    myVPGoodLongTracks(),
    myVPOverlapTracks(),
    myVPBackwardTracks()
]
TAlignment().WriteCondSubDetList = ['VP']
TAlignment().LogFile = "alignlog_vp_halves.txt"
TAlignment().CondFilePrefix = "vp-halves/xml/"

from Configurables import SurveyConstraints
surveyconstraints = SurveyConstraints()
surveyconstraints.VP()

# Dataset dependent stuff
from Configurables import CondDB
CondDB().Upgrade = True
from Configurables import LHCbApp
LHCbApp().Detectors = [
    'VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet',
    'Tr'
]

#Uncomment this if you need to use VPTrackMonitor or any other monitor that makes an ntuple
#NTupleSvc().Output = ["FILE1 DATAFILE='VPTrackMonitor.root' OPT='NEW' TYP='ROOT'"]

from PRConfig import TestFileDB
TestFileDB.test_file_db['upgrade_minbias_2019'].run()
