###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###############################################################################
# File for running alignment using the default reconstruction sequence.
###############################################################################
# Syntax is:
#   gaudiiter.py -e NUMEVENTS Escher-AlignCollisions.py <someDataFiles>.py
#
###############################################################################

from Configurables import Escher
theApp = Escher()
theApp.DatasetName = 'AlignCollisions'
theApp.PrintFreq = 1
theApp.RecoSequence = ["Decoding", "TrFast", "TrBest", "MUON"]

# specify the input to the alignment
from Configurables import TAlignment
from TAlignment.TrackSelections import *
TAlignment().TrackSelections = [
    GoodLongTracks(),
    VeloOverlapTracks(),
    VeloBackwardTracks()
]

# add the default PV selection
from TAlignment.VertexSelections import configuredPVSelection
TAlignment().PVSelection = configuredPVSelection()

TAlignment().LogFile = "alignlog_collisions.txt"

# specify what we actually align for
from TAlignment.AlignmentScenarios import configureRun3Alignment
configureRun3Alignment()
