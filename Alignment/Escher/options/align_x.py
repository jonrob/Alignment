###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import (TAlignment, AlignTrTools)
AlignTrTools().Degrees_of_Freedom[0] = 1
TAlignment().Constraints += [
    "X_S1_L1_Q0_M0", "X_S1_L2_Q0_M0", "X_S3_L1_Q0_M0", "X_S3_L2_Q0_M0"
]
