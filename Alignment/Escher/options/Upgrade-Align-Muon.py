###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Escher
Escher().DataType = "Upgrade"
Escher().Simulation = True
Escher().InputType = "DIGI"
Escher().PrintFreq = 1
Escher().DatasetName = 'Upgrade-Align-Muon'
Escher().RecoSequence = ["Decoding", "TrFast", "TrBest", "MUON"]
Escher().MoniSequence = []

#configure for muon
from Configurables import TAlignment
from TAlignment.TrackSelections import BestMuonTracks
TAlignment().TrackSelections = [BestMuonTracks("OfflineMuonAlignment")]

from TAlignment.AlignmentScenarios import *
configureMuonAlignment()

TAlignment().LogFile = "alignlog_muon.txt"
TAlignment().CondFilePrefix = "muon/xml/"

# the following is data sample dependent
from Configurables import LHCbApp
detectors = [
    'VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet'
]
LHCbApp().Detectors = detectors
from Configurables import CondDB
CondDB().Upgrade = True

from PRConfig import TestFileDB
TestFileDB.test_file_db['upgrade_alignment_Bs2JpsiPhi'].run()
