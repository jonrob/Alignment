###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###############################################################################
# File for running VP alignment on simulated Data                             #
#                                                                             #
# Syntax is:                                                                  #
#                                                                             #
#   gaudirun.py $ESCHEROPTS/AlignVPHalves.py                                  #
#                                                                             #
###############################################################################
from Configurables import Escher, TrackSys, LHCbApp, DstConf
from GaudiKernel.ProcessJobOptions import importOptions
from Configurables import CondDB
from Configurables import GaudiSequencer
from Configurables import PrLHCbID2MCParticle
from Configurables import TAlignment
from TAlignment.Alignables import *
from TAlignment.TrackSelections import TrackRefiner
from TAlignment.VertexSelections import *
from Configurables import AlignAlgorithm
from Configurables import TrackContainerCopy, TrackSelector, TrackMonitor
from Configurables import VPTrackSelector
from Gaudi.Configuration import appendPostConfigAction, INFO, DEBUG, VERBOSE
from TAlignment.SurveyConstraints import *
from GaudiConf import IOHelper
from Configurables import CaloFutureProcessor, GlobalRecoConf, NTupleSvc

importOptions('$STDOPTS/PreloadUnits.opts')

LHCbApp().Simulation = True
LHCbApp().DataType = 'Upgrade'
CondDB().Upgrade = True

LHCbApp().DDDBtag = "upgrade/dddb-20190223"
LHCbApp().CondDBtag = "sim-20180530-vc-mu100"

detectors = [
    'VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet',
    'Tr'
]
LHCbApp().Detectors = detectors

Escher().InputType = "DIGI"
Escher().EvtMax = -1
Escher().MoniSequence = []

Escher().RecoSequence = ["Decoding", "TrFast", "TrBest", "VP", "VELOPIX"]
Escher().WithMC = True
Escher().Simulation = True
Escher().DataType = 'Upgrade'
Escher().DatasetName = 'AlignVPHalvesModules'

AlignAlgorithm("AlignAlgorithm").ForcedInitialTime = 1

elements = Alignables()
dofs = "TxTyTzRxRyRz"
elements.VPLeft(dofs)
elements.VPRight(dofs)
elements.VPModules(dofs)

constraints = []
constraints.append("VPHalfAverage : VP/VP(Left|Right) : Tx Ty Tz Rx Ry Rz")

trackRefitSeq = GaudiSequencer("TrackRefitSeq")

# create a track list for tracks with velo hits
velotrackselector = TrackContainerCopy(
    "TracksWithVeloHits",
    inputLocations=["Rec/Track/Best"],
    outputLocation="Rec/Track/TracksWithVeloHits",
    Selector=TrackSelector())

trackRefitSeq.Members += [velotrackselector]


def redefinefilters():
    GaudiSequencer("RecoTrSeq").Members += [trackRefitSeq]


appendPostConfigAction(redefinefilters)


class myVPOverlapTracks(TrackRefiner):
    def __init__(self,
                 Name="VPOverlapTracks",
                 InputLocation="Rec/Track/Best",
                 Fitted=True):
        TrackRefiner.__init__(
            self, Name=Name, InputLocation=InputLocation, Fitted=Fitted)

    def configureSelector(self, a):
        a.Selector = TrackSelector()
        a.Selector.MinNVeloALayers = 1
        a.Selector.MinNVeloCLayers = 1
        a.Selector.TrackTypes = ["Velo", "VeloBackward", "Upstream", "Long"]


class myVPGoodLongTracks(TrackRefiner):
    def __init__(self,
                 Name="VPGoodLongTracks",
                 InputLocation="Rec/Track/Best",
                 Fitted=True):
        TrackRefiner.__init__(
            self, Name=Name, InputLocation=InputLocation, Fitted=Fitted)

    def configureSelector(self, a):
        a.Selector = VPTrackSelector()
        a.Selector.TrackTypes = ["Long"]
        a.Selector.MinNVeloLayers = 5
        a.Selector.MinPCut = 5000
        a.Selector.MaxPCut = 200000
        a.Selector.MinPtCut = 200

        if self._fitted:
            a.Selector.MaxChi2Cut = 5
            a.Selector.MaxChi2PerDoFMatch = 5
            a.Selector.MaxChi2PerDoFVelo = 5
            a.Selector.MaxChi2PerDoFDownstream = 5


class myVPBackwardTracks(TrackRefiner):
    def __init__(self,
                 Name="VPBackwardTracks",
                 InputLocation="Rec/Track/Best",
                 Fitted=True):
        TrackRefiner.__init__(
            self, Name=Name, InputLocation=InputLocation, Fitted=Fitted)

    def configureSelector(self, a):
        a.Selector = VPTrackSelector()
        a.Selector.TrackTypes = ["Velo", "VeloBackward"]
        a.Selector.OnlyBackwardTracks = True
        a.Selector.MinNVeloLayers = 5
        if self._fitted:
            a.Selector.MaxChi2Cut = 5


TAlignment().ElementsToAlign = list(elements)
TAlignment().TrackLocation = "Rec/Track/Best"
TAlignment().VertexLocation = "Rec/Vertex/Primary"
TAlignment().PVSelection = configuredPVSelection()
TAlignment().Constraints = constraints
TAlignment().TrackSelections = [
    myVPGoodLongTracks(),
    myVPOverlapTracks(),
    myVPBackwardTracks()
]
TAlignment().WriteCondSubDetList = ['VP']
TAlignment().UsePreconditioning = True
TAlignment().UseLocalFrame = True
TAlignment().LogFile = "alignlog_vp_halves_modules.txt"
TAlignment().CondFilePrefix = "vp-halves-modules/xml/"

surveyconstraints = SurveyConstraints()
surveyconstraints.VP()


def doMyChanges():
    GaudiSequencer("HltFilterSeq").Members = []


appendPostConfigAction(doMyChanges)

#Uncomment this if needed to use the VPTrackMonitor or you need to use any other NTuple Service
#NTupleSvc().Output = ["FILE1 DATAFILE='VPTrackMonitor.root' OPT='NEW' TYP='ROOT'"]

from PRConfig import TestFileDB
TestFileDB.test_file_db['upgrade_minbias_2019'].run()
