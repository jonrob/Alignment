###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###############################################################################
# File for running FT alignment on simulated Data                             #
#                                                                             #
# Syntax is:                                                                  #
#                                                                             #
#   gaudirun.py $ESCHEROPTS/AlignFTStationsLayers.py                          #
#                                                                             #
###############################################################################

from TAlignment.TrackSelections import TrackRefiner

from Gaudi.Configuration import appendPostConfigAction, INFO, DEBUG, VERBOSE
from TAlignment.SurveyConstraints import *
from Configurables import CaloFutureProcessor, GlobalRecoConf

importOptions('$STDOPTS/PreloadUnits.opts')

from Configurables import Escher
Escher().InputType = "DIGI"
Escher().EvtMax = -1
Escher().MoniSequence = []
Escher().RecoSequence = ["Decoding", "TrFast", "TrBest", "MUON"]
#Escher().WithMC = True
Escher().Simulation = True
Escher().DataType = 'Upgrade'
Escher().PrintFreq = 1
Escher().DatasetName = 'AlignFTStationsLayers'

# configure the alignable degrees of freedom
from TAlignment.Alignables import *
elements = Alignables()
dofs = "TxTz"
elements.FTStations(dofs)
elements.FTFrameLayers(dofs)

from Configurables import TAlignment
TAlignment().ElementsToAlign = list(elements)
TAlignment().VertexLocation = "Rec/Vertex/Primary"
from TAlignment.TrackSelections import *
TAlignment().TrackSelections = [GoodLongTracks()]
TAlignment().WriteCondSubDetList = ['FT']
TAlignment().CondFilePrefix = "ft-stations-layers/xml/"

from Configurables import SurveyConstraints
surveyconstraints = SurveyConstraints()
surveyconstraints.FT()

TAlignment().LogFile = "alignlog_ft_stationslayers.txt"

# Dataset dependent stuff
from Configurables import CondDB
CondDB().Upgrade = True
from Configurables import LHCbApp
LHCbApp().Detectors = [
    'VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet',
    'Tr'
]

from PRConfig import TestFileDB
TestFileDB.test_file_db['upgrade_DC19_01_MinBiasMU'].run()
#TestFileDB.test_file_db['upgrade_DC19_01_MinBiasMD'].run()
