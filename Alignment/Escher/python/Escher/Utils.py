###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function


##################################################################################
# Utility to stage files on a local disk
##################################################################################
def stagelocally():
    print("Warning: staging files locally")
    # first create the target directory, if it doesn't exist yet
    import re, os, getpass
    username = getpass.getuser()
    targetdir = '/tmp/%s/stagedfiles/' % username
    if os.path.isdir('/pool/spool/'):
        targetdir = '/pool/spool/%s/stagedfiles/' % username
    if not os.path.isdir(targetdir):
        os.makedirs(targetdir)

    # remove any old files in that directory
    os.system("find %s -atime +7 -exec rm '{}' \;" % targetdir)

    from Configurables import EventSelector
    eventinput = EventSelector().Input
    neweventinput = []
    for i in eventinput:
        # need some pattern matching ...
        x = re.match("DATAFILE='([^']*)'", i)
        if not x: trouble
        #for j in x.groups() : print j
        fullpath = x.group(1)
        #print 'fullfilename:', fullpath
        #extract the filename itself, without the path
        filename = fullpath[fullpath.rfind("/") + 1:]
        targetfilename = targetdir + filename
        issuccess = True
        if not os.path.exists(targetfilename):
            #if filename contains castor, use rfcp
            if fullpath.find('castor') >= 0:
                castorfile = fullpath[fullpath.find("/"):]
                print("copying file %s from castor\n" % castorfile)
                os.system('rfcp %s %s' % (castorfile, targetfilename))
            elif fullpath.find('LFN:') >= 0:
                lfnfile = fullpath.replace("LFN", "lfn")
                print("copying file %s from grid\n" % lfnfile)
                os.system("lcg-cp % s %s " % (lfnfile, targetfilename))
            else:
                print('cannot cache file locally: %s\n' % fullpath)
                issuccess = False

        # now fix the input file
        if issuccess:
            i = i.replace(fullpath, targetfilename)
        neweventinput.append(i)
    EventSelector().Input = neweventinput
