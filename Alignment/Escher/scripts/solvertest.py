#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# first parse all options
from __future__ import print_function
from optparse import OptionParser
parser = OptionParser(usage="%prog [options] <opts_file> ...")
parser.add_option(
    "-n",
    "--numiter",
    type="int",
    dest="numiter",
    help="number of iterations",
    default=1)
parser.add_option(
    "-e",
    "--numevents",
    type="int",
    dest="numevents",
    help="number of events",
    default=-1)
parser.add_option(
    "-d",
    "--aligndb",
    action='append',
    dest="aligndb",
    help="path to file with CALIBOFF/LHCBCOND/SIMCOND database layer")
parser.add_option(
    "--dddb",
    action='append',
    dest="dddb",
    help="path to file with DDDB database layer")
parser.add_option(
    "--skipprintsequence",
    action="store_true",
    help="skip printing the sequence")
parser.add_option(
    "-l",
    "--lhcbcond",
    action="store_true",
    dest="lhcbcondtag",
    help="activate if constants in lhcbcondDB",
    default=False)
(opts, args) = parser.parse_args()

# Prepare the "configuration script" to parse (like this it is easier than
# having a list with files and python commands, with an if statements that
# decides to do importOptions or exec)
options = ["importOptions(%r)" % f for f in args]

# "execute" the configuration script generated (if any)
from Gaudi.Configuration import logging
if options:
    g = {}
    l = {}
    exec("from Gaudi.Configuration import *", g, l)
    for o in options:
        logging.debug(o)
        exec(o, g, l)

# make sure that the algorithms know how many iterations are coming
from Configurables import TAlignment
TAlignment().UpdateInFinalize = False

# set the database layer
if opts.aligndb:
    counter = 1
    for db in opts.aligndb:
        from Configurables import CondDB
        if ".db" in db:
            # this is an sqlite file
            from Configurables import CondDBAccessSvc
            alignCond = CondDBAccessSvc('AlignCond' + str(counter))
            alignCond.ConnectionString = 'sqlite_file:' + db + condtag
            CondDB().addLayer(alignCond)
            counter += 1
        else:
            # this is a git layer
            CondDB().addLayer(db)
        print("adding layer: ", db)

if opts.dddb:
    counter = 1
    for db in opts.dddb:
        from Configurables import (CondDB, CondDBAccessSvc)
        alignCond = CondDBAccessSvc('AlignDDDB' + str(counter))
        alignCond.ConnectionString = 'sqlite_file:' + db + '/DDDB'
        CondDB().addLayer(alignCond)
        counter += 1

## configure all solver tools (before creating the appmgr)
solvers = [
    "GslDiagSolvTool", "EigenDiagSolvTool", "SpmInvTool", "gslSVDsolver",
    "CLHEPSolver", "SparseSolver"
]
from Configurables import AlignUpdateTool, TAlignment
updatetools = []
for solvername in solvers:
    # first configure it, then retrieve it via the toolsvc. does that make sense?
    updatetoolname = "UpdateTool" + solvername
    updatetool = AlignUpdateTool(
        updatetoolname,
        MatrixSolverTool=solvername,
        SkipGeometryUpdate=True,
        LogFile="alignlog_" + solvername + ".txt",
        Constraints=TAlignment().getProp("Constraints"),
        UseWeightedAverage=TAlignment().getProp(
            "UseWeightedAverageConstraint"),
        OutputLevel=3)
    updatetools.append(updatetoolname)

## print the sequence
if not opts.skipprintsequence:
    from Configurables import ApplicationMgr
    ApplicationMgr(PrintAlgsSequence=True)

## Instantiate application manager
from GaudiPython.Bindings import AppMgr
appMgr = AppMgr()

evtSel = appMgr.evtSel()
evtSel.OutputLevel = 1
mainSeq = appMgr.algorithm("EscherSequencer")

print(evtSel.Input)

# event loop
appMgr.run(opts.numevents)

# get the derivatives
import copy
det = appMgr.detsvc()
alignAlg = TAlignment().getAlignAlg()
print("alignderivatives:", alignAlg.equations())
for updatetoolname in updatetools:
    print(["*" * 40])
    print("UpdateTool: ", updatetoolname)
    AlignUpdateTool(updatetoolname).process(alignAlg.equations(),
                                            alignAlg.elementProvider(), 0, 1)
