/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "TVerticalAlignment/STNames.h"

// ClassImp(STNames);

//-----------------------------------------------------------------------------
// Implementation file for class : STNames
//
// 2010-11-22 : Frederic Guillaume Dupertuis
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STNames::STNames( const char* detType ) : m_detType( detType ), m_init( kFALSE ) { Init(); }
//=============================================================================

STNames::Vector* STNames::GetSectors() { return &( m_Sectors ); }

STNames::Vector* STNames::GetLayers() { return &( m_Layers ); }

STNames::Vector* STNames::GetBiLayers() { return &( m_BiLayers ); }

STNames::Vector* STNames::GetBoxes() { return &( m_Boxes ); }

STNames::SectNames* STNames::GetSectNames( string const& sector ) {
  if ( m_MapSectNames.find( sector ) != m_MapSectNames.end() )
    return &( m_MapSectNames[sector] );
  else
    return &( m_MapSectNames[0] );
}

STNames::LayerNames* STNames::GetLayerNames( string const& layer ) {
  if ( m_MapLayerNames.find( layer ) != m_MapLayerNames.end() )
    return &( m_MapLayerNames[layer] );
  else
    return &( m_MapLayerNames[0] );
}

STNames::BiLayerNames* STNames::GetBiLayerNames( string const& bilayer ) {
  if ( m_MapBiLayerNames.find( bilayer ) != m_MapBiLayerNames.end() )
    return &( m_MapBiLayerNames[bilayer] );
  else
    return &( m_MapBiLayerNames[0] );
}

STNames::BoxNames* STNames::GetBoxNames( string const& box ) {
  if ( m_MapBoxNames.find( box ) != m_MapBoxNames.end() )
    return &( m_MapBoxNames[box] );
  else
    return &( m_MapBoxNames[0] );
}

std::string STNames::GetLayerName( std::string const& sector ) {
  TString _sector( sector.c_str() );
  for ( STNames::Vector::iterator It = m_Layers.begin(); It != m_Layers.end(); It++ ) {
    if ( _sector.Contains( *It ) ) return m_MapLayerNames[*It].Nickname.Data();
  }
  return "";
}

std::string STNames::GetBiLayerName( std::string const& sector ) {
  TString _sector( sector.c_str() );
  if ( m_detType == "IT" ) {
    _sector.ReplaceAll( "X1", "U" );
    _sector.ReplaceAll( "U", "X1U" );
    _sector.ReplaceAll( "V", "X2" );
    _sector.ReplaceAll( "X2", "VX2" );
  } else {
    _sector.ReplaceAll( "X", "" );
    _sector.ReplaceAll( "U", "" );
    _sector.ReplaceAll( "V", "" );
  }

  for ( STNames::Vector::iterator It = m_BiLayers.begin(); It != m_BiLayers.end(); It++ ) {
    if ( _sector.Contains( *It ) ) return m_MapBiLayerNames[*It].Nickname.Data();
  }
  cout << "Zut" << endl;

  return "";
}

std::string STNames::GetBoxName( std::string const& sector ) {
  TString _sector( sector.c_str() );
  for ( STNames::Vector::iterator It = m_Boxes.begin(); It != m_Boxes.end(); It++ ) {
    if ( _sector.Contains( *It ) ) return m_MapBoxNames[*It].Nickname.Data();
  }
  return "";
}

std::string STNames::GetGlobalName() { return m_detType.Data(); }

int STNames::GetUniqueSector( std::string const& sector ) {
  if ( m_MapSectNames.find( sector ) != m_MapSectNames.end() )
    return m_MapSectNames[sector].UniqueSector;
  else
    return -1;
}

int STNames::GetUniqueLayer( std::string const& layer ) {
  if ( m_MapLayerNames.find( layer ) != m_MapLayerNames.end() )
    return m_MapLayerNames[layer].UniqueLayer;
  else
    return -1;
}

int STNames::GetUniqueBox( std::string const& box ) {
  if ( m_MapBoxNames.find( box ) != m_MapBoxNames.end() )
    return m_MapBoxNames[box].UniqueBox;
  else
    return -1;
}

int STNames::GetNbSensors( std::string const& sector ) {
  if ( m_MapSectNames.find( sector ) != m_MapSectNames.end() )
    return m_MapSectNames[sector].NbSensors;
  else
    return -1;
}

int STNames::GetYPos( std::string const& sector ) {
  if ( m_MapSectNames.find( sector ) != m_MapSectNames.end() )
    return m_MapSectNames[sector].YPos;
  else
    return 0;
}

void STNames::Init() {
  if ( !m_init ) {
    if ( m_detType == "IT" ) {
      for ( int i( 1 ); i < 4; i++ ) {
        for ( int j( 1 ); j < 8; j++ ) {
          InitITMapNames( i, j, "Bottom", "X1" );
          InitITMapNames( i, j, "Bottom", "U" );
          InitITMapNames( i, j, "Bottom", "V" );
          InitITMapNames( i, j, "Bottom", "X2" );
          InitITMapNames( i, j, "Top", "X1" );
          InitITMapNames( i, j, "Top", "U" );
          InitITMapNames( i, j, "Top", "V" );
          InitITMapNames( i, j, "Top", "X2" );
          InitITMapNames( i, j, "ASide", "X1" );
          InitITMapNames( i, j, "ASide", "U" );
          InitITMapNames( i, j, "ASide", "V" );
          InitITMapNames( i, j, "ASide", "X2" );
          InitITMapNames( i, j, "CSide", "X1" );
          InitITMapNames( i, j, "CSide", "U" );
          InitITMapNames( i, j, "CSide", "V" );
          InitITMapNames( i, j, "CSide", "X2" );
        }
      }
    } else {
      std::vector<TString> TT;
      std::vector<TString> TT_regions;
      std::vector<TString> TT_layers;

      TT.push_back( "a" );
      TT.push_back( "b" );
      TT_regions.push_back( "A" );
      TT_regions.push_back( "B" );
      TT_regions.push_back( "C" );
      TT_layers.push_back( "X" );
      TT_layers.push_back( "S" );

      unsigned int nbSectors;

      for ( unsigned int h = 0; h < TT.size(); h++ ) {
        for ( unsigned int i = 0; i < TT_regions.size(); i++ ) {
          if ( TT_regions[i] == "B" ) {
            if ( TT[h] == "a" )
              nbSectors = 18;
            else
              nbSectors = 26;
          } else {
            nbSectors = 24;
          }
          for ( unsigned int j = 0; j < TT_layers.size(); j++ ) {
            for ( unsigned int k( 1 ); k < nbSectors + 1; k++ ) {
              InitTTMapNames( TT[h], TT_regions[i], TT_layers[j], k );
            }
          }
        }
      }
    }
    m_init = kTRUE;
  }
}

void STNames::InitITMapNames( int const& ITno, int const& Sectno, const char* Box, const char* Layer ) {
  TString sector( "" );
  TString layer( "" );
  TString bilayer( "" );
  TString bilayer_( "" );
  TString box( "" );

  box = "IT";
  box += ITno;
  box += Box;
  layer = box;
  layer += Layer;
  bilayer = box;
  if ( strcmp( Layer, "X1" ) == 0 || strcmp( Layer, "U" ) == 0 ) {
    bilayer_ += "X1U";
  } else {
    bilayer_ += "VX2";
  }
  bilayer += bilayer_;
  sector = layer;
  sector += "Sector";
  sector += Sectno;

  std::string _sector( sector.Data() );
  std::string _layer( layer.Data() );
  std::string _bilayer( bilayer.Data() );
  std::string _box( box.Data() );

  m_MapSectNames[_sector].Nickname = sector;
  m_MapSectNames[_sector].ITno     = ITno;
  m_MapSectNames[_sector].TT       = "";
  m_MapSectNames[_sector].Box      = Box;
  m_MapSectNames[_sector].Layer    = Layer;
  m_MapSectNames[_sector].Region   = "";
  m_MapSectNames[_sector].Sectno   = Sectno;
  m_Sectors.push_back( _sector );

  if ( strcmp( Box, "ASide" ) == 0 || strcmp( Box, "CSide" ) == 0 )
    m_MapSectNames[_sector].NbSensors = 2;
  else
    m_MapSectNames[_sector].NbSensors = 1;

  int uniqueSector( 0 );
  int uniqueSectorTM( 0 );

  if ( ITno == 1 ) {
    uniqueSector += 1000;
    uniqueSectorTM += 100;
  } else if ( ITno == 2 ) {
    uniqueSector += 2000;
    uniqueSectorTM += 200;
  } else if ( ITno == 3 ) {
    uniqueSector += 3000;
    uniqueSectorTM += 300;
  }

  if ( strcmp( Box, "ASide" ) == 0 )
    uniqueSector += 100;
  else if ( strcmp( Box, "CSide" ) == 0 )
    uniqueSector += 200;
  else if ( strcmp( Box, "Top" ) == 0 )
    uniqueSector += 300;
  else if ( strcmp( Box, "Bottom" ) == 0 )
    uniqueSector += 400;

  if ( m_MapBoxNames.find( _box ) == m_MapBoxNames.end() ) {
    m_MapBoxNames[_box].Nickname  = box;
    m_MapBoxNames[_box].ITno      = ITno;
    m_MapBoxNames[_box].Box       = Box;
    m_MapBoxNames[_box].UniqueBox = uniqueSector;
    m_Boxes.push_back( _box );
  }

  if ( strcmp( Layer, "X1" ) == 0 ) {
    uniqueSector += 10;
    uniqueSectorTM += 10;
  } else if ( strcmp( Layer, "U" ) == 0 ) {
    uniqueSector += 20;
    uniqueSectorTM += 20;
  } else if ( strcmp( Layer, "V" ) == 0 ) {
    uniqueSector += 30;
    uniqueSectorTM += 30;
  } else if ( strcmp( Layer, "X2" ) == 0 ) {
    uniqueSector += 40;
    uniqueSectorTM += 40;
  }

  if ( m_MapLayerNames.find( _layer ) == m_MapLayerNames.end() ) {
    m_MapLayerNames[_layer].Nickname    = layer;
    m_MapLayerNames[_layer].ITno        = ITno;
    m_MapLayerNames[_layer].TT          = "";
    m_MapLayerNames[_layer].Box         = Box;
    m_MapLayerNames[_layer].Layer       = Layer;
    m_MapLayerNames[_layer].UniqueLayer = uniqueSector;
    m_Layers.push_back( _layer );
  }

  if ( m_MapBiLayerNames.find( _bilayer ) == m_MapBiLayerNames.end() ) {
    m_MapBiLayerNames[_bilayer].Nickname      = bilayer;
    m_MapBiLayerNames[_bilayer].ITno          = ITno;
    m_MapBiLayerNames[_bilayer].TT            = "";
    m_MapBiLayerNames[_bilayer].Box           = Box;
    m_MapBiLayerNames[_bilayer].BiLayer       = bilayer_;
    m_MapBiLayerNames[_bilayer].UniqueBiLayer = uniqueSector;
    m_BiLayers.push_back( _bilayer );
  }

  uniqueSector += Sectno;
  uniqueSectorTM += Sectno;

  m_MapSectNames[_sector].UniqueSector   = uniqueSector;
  m_MapSectNames[_sector].UniqueSectorTM = uniqueSectorTM;
}

void STNames::InitTTMapNames( const char* TT, const char* Region, const char* Layer, int const& Sectno ) {
  TString sector( "" );
  TString layer( "" );
  TString bilayer( "" );

  bilayer = "TT";
  bilayer += TT;
  layer = bilayer;
  if ( Layer[0] == 'X' )
    layer += "X";
  else if ( TT[0] == 'a' )
    layer += "U";
  else if ( TT[0] == 'b' )
    layer += "V";
  sector = layer;
  sector += "Region";
  sector += Region;
  sector += "Sector";
  sector += Sectno;

  std::string _sector( sector.Data() );
  std::string _layer( layer.Data() );
  std::string _bilayer( bilayer.Data() );

  m_MapSectNames[_sector].Nickname = sector;
  m_MapSectNames[_sector].ITno     = 0;
  m_MapSectNames[_sector].TT       = TT;
  m_MapSectNames[_sector].Box      = "";
  if ( Layer[0] == 'X' )
    m_MapSectNames[_sector].Layer = "X";
  else if ( TT[0] == 'a' )
    m_MapSectNames[_sector].Layer = "U";
  else if ( TT[0] == 'b' )
    m_MapSectNames[_sector].Layer = "V";
  m_MapSectNames[_sector].Region = Region;
  m_MapSectNames[_sector].Sectno = Sectno;
  m_Sectors.push_back( _sector );

  if ( TT[0] == 'a' ) {
    if ( Region[0] == 'A' || Region[0] == 'C' ) {
      if ( ( Sectno - 1 ) % 4 == 0 || ( Sectno - 1 ) % 4 == 3 ) {
        m_MapSectNames[_sector].NbSensors = 4;
      } else {
        m_MapSectNames[_sector].NbSensors = 3;
      }
      if ( ( Sectno - 1 ) % 4 < 2 )
        m_MapSectNames[_sector].YPos = -1;
      else
        m_MapSectNames[_sector].YPos = 1;
    } else {
      if ( ( Sectno - 1 ) % 6 == 0 || ( Sectno - 1 ) % 6 == 5 ) {
        m_MapSectNames[_sector].NbSensors = 4;
      } else if ( ( Sectno - 1 ) % 6 == 1 || ( Sectno - 1 ) % 6 == 4 ) {
        m_MapSectNames[_sector].NbSensors = 2;
      } else {
        m_MapSectNames[_sector].NbSensors = 1;
      }
      if ( ( Sectno - 1 ) % 6 < 3 )
        m_MapSectNames[_sector].YPos = -1;
      else
        m_MapSectNames[_sector].YPos = 1;
    }
  } else {
    if ( Region[0] == 'A' || Region[0] == 'C' ) {
      if ( ( Sectno - 1 ) % 4 == 0 || ( Sectno - 1 ) % 4 == 3 ) {
        m_MapSectNames[_sector].NbSensors = 4;
      } else {
        m_MapSectNames[_sector].NbSensors = 3;
      }
      if ( ( Sectno - 1 ) % 4 < 2 )
        m_MapSectNames[_sector].YPos = -1;
      else
        m_MapSectNames[_sector].YPos = 1;
    } else {
      if ( Sectno - 1 < 4 ) {
        if ( ( Sectno - 1 ) % 4 == 0 || ( Sectno - 1 ) % 4 == 3 ) {
          m_MapSectNames[_sector].NbSensors = 4;
        } else {
          m_MapSectNames[_sector].NbSensors = 3;
        }
        if ( ( Sectno - 1 ) % 4 < 2 )
          m_MapSectNames[_sector].YPos = -1;
        else
          m_MapSectNames[_sector].YPos = 1;
      } else if ( Sectno - 1 > 21 ) {
        if ( ( Sectno - 23 ) % 4 == 0 || ( Sectno - 23 ) % 4 == 3 ) {
          m_MapSectNames[_sector].NbSensors = 4;
        } else {
          m_MapSectNames[_sector].NbSensors = 3;
        }
        if ( ( Sectno - 23 ) % 4 < 2 )
          m_MapSectNames[_sector].YPos = -1;
        else
          m_MapSectNames[_sector].YPos = 1;
      } else {
        if ( ( Sectno - 5 ) % 6 == 0 || ( Sectno - 5 ) % 6 == 5 ) {
          m_MapSectNames[_sector].NbSensors = 4;
        } else if ( ( Sectno - 5 ) % 6 == 1 || ( Sectno - 5 ) % 6 == 4 ) {
          m_MapSectNames[_sector].NbSensors = 2;
        } else {
          m_MapSectNames[_sector].NbSensors = 1;
        }
        if ( ( Sectno - 5 ) % 6 < 3 )
          m_MapSectNames[_sector].YPos = -1;
        else
          m_MapSectNames[_sector].YPos = 1;
      }
    }
  }

  int uniqueSector( 0 );
  int uniqueSectorTM( 0 );

  if ( TT[0] == 'a' && Layer[0] == 'X' ) {
    uniqueSector += 1000;
    uniqueSectorTM += 100;
  } else if ( TT[0] == 'a' && Layer[0] == 'S' ) {
    uniqueSector += 2000;
    uniqueSectorTM += 200;
  } else if ( TT[0] == 'b' && Layer[0] == 'S' ) {
    uniqueSector += 3000;
    uniqueSectorTM += 300;
  } else if ( TT[0] == 'b' && Layer[0] == 'X' ) {
    uniqueSector += 4000;
    uniqueSectorTM += 400;
  }

  if ( m_MapLayerNames.find( _layer ) == m_MapLayerNames.end() ) {
    m_MapLayerNames[_layer].Nickname = layer;
    m_MapLayerNames[_layer].ITno     = 0;
    m_MapLayerNames[_layer].TT       = TT;
    m_MapLayerNames[_layer].Box      = "";
    if ( Layer[0] == 'X' )
      m_MapLayerNames[_layer].Layer = "X";
    else if ( TT[0] == 'a' )
      m_MapLayerNames[_layer].Layer = "U";
    else if ( TT[0] == 'b' )
      m_MapLayerNames[_layer].Layer = "V";
    m_MapLayerNames[_layer].UniqueLayer = uniqueSector;
    m_Layers.push_back( _layer );
  }

  if ( m_MapBiLayerNames.find( _bilayer ) == m_MapBiLayerNames.end() ) {
    m_MapBiLayerNames[_bilayer].Nickname      = bilayer;
    m_MapBiLayerNames[_bilayer].ITno          = 0;
    m_MapBiLayerNames[_bilayer].TT            = TT;
    m_MapBiLayerNames[_bilayer].Box           = "";
    m_MapBiLayerNames[_bilayer].BiLayer       = "";
    m_MapBiLayerNames[_bilayer].UniqueBiLayer = uniqueSector;
    m_BiLayers.push_back( _bilayer );
  }

  if ( Region[0] == 'A' ) {
    uniqueSector += 100;
    uniqueSectorTM += 60;
  } else if ( Region[0] == 'B' ) {
    uniqueSector += 200;
    uniqueSectorTM += 30;
  } else if ( Region[0] == 'C' ) {
    uniqueSector += 300;
    uniqueSectorTM += 0;
  }

  uniqueSector += Sectno;
  uniqueSectorTM += Sectno;

  m_MapSectNames[_sector].UniqueSector   = uniqueSector;
  m_MapSectNames[_sector].UniqueSectorTM = uniqueSectorTM;
}
