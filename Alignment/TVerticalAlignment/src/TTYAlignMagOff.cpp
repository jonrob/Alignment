/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TVerticalAlignment/TTYAlignMagOff.h"
#include "TLine.h"
#include <boost/filesystem.hpp>

using namespace std;
using namespace RooFit;
using namespace Alignment::TVerticalAlignment;
class STNames;

TTYAlignMagOff::TTYAlignMagOff( TString filename, TString dbfilename, TString txtfiledir, bool constraint,
                                bool saveplots )
    : m_filename( filename )
    , m_dbfilename( dbfilename )
    , m_outDirectory( txtfiledir )
    , m_constraint( constraint )
    , m_saveplots( saveplots ) {

  m_va = new TVerticalAlignment();
  m_va->initParameter( std::string( "TT" ) );
  m_param = m_va->getParameter( std::string( "TT" ) );
  m_Names = new STNames( "TT" );

  if ( !boost::filesystem::exists( m_outDirectory.Data() ) )
    boost::filesystem::create_directories( m_outDirectory.Data() );

  boost::filesystem::path s( m_filename.Data() );
  m_outputname = "TTYAlign_" + s.filename().string() + "_eff";
  // m_outputname = "TTYAlign_"+m_filename+"_eff";
  m_outputname.ReplaceAll( ".root", "" );
  if ( m_constraint ) m_outputname += "_constraint";
}

void TTYAlignMagOff::glimpse_data() { m_glimpse = true; }
void TTYAlignMagOff::debug_verbose() { m_verbose = true; }

RooDataSet* TTYAlignMagOff::GetRooDataSetFromTH1( TH1F* histo, TH1F* histo_exp, RooRealVar y, RooRealVar weight,
                                                  RooCategory cat_lr, RooCategory cat_eff, bool add_lr,
                                                  double offset ) {
  if ( m_verbose ) cout << "call function GetRooDataSetFromTH1() " << endl;

  RooDataSet* data = 0;
  double      center;

  if ( add_lr ) {
    data = new RooDataSet( "data", "data", RooArgSet( y, weight, cat_lr, cat_eff ), WeightVar( weight ) );

    for ( int i( 1 ); i <= histo->GetNbinsX(); i++ ) {
      center = histo->GetBinCenter( i );
      y.setVal( center );

      if ( center < offset && offset < 0.001 )
        cat_lr.setLabel( "left" );
      else if ( center > offset && offset > 0.001 )
        cat_lr.setLabel( "left" );
      else if ( center < offset && offset == 0. )
        cat_lr.setLabel( "left" );
      else
        cat_lr.setLabel( "right" );
      cat_eff.setLabel( "found" );
      data->add( RooArgSet( y, cat_lr, cat_eff ), histo->GetBinContent( i ) );

      cat_eff.setLabel( "not found" );
      data->add( RooArgSet( y, cat_lr, cat_eff ), histo_exp->GetBinContent( i ) - histo->GetBinContent( i ) );
    }
  } else {
    data = new RooDataSet( "data", "data", RooArgSet( y, weight, cat_eff ), WeightVar( weight ) );

    for ( int i( 1 ); i <= histo->GetNbinsX(); i++ ) {
      center = histo->GetBinCenter( i );
      y.setVal( center );

      cat_eff.setLabel( "found" );
      data->add( RooArgSet( y, cat_eff ), histo->GetBinContent( i ) );

      cat_eff.setLabel( "not found" );
      data->add( RooArgSet( y, cat_eff ), histo_exp->GetBinContent( i ) - histo->GetBinContent( i ) );
    }
  }

  return data;
}

void TTYAlignMagOff::GetMeanFromHisto( TString SectorName ) {
  auto    m_TT            = boost::get<STParam>( m_param );
  double  ParTol          = m_TT.tolerance();
  double  Scale           = m_TT.scale();
  double  SigmaConstraint = m_TT.sigmaconstraint();
  TString directory( m_TT.directory().first );
  TString directory_exp( m_TT.directory().second );
  double  Edge, Length;

  TH1F*  _yeff     = 0;
  TH1F*  _yeff_exp = 0;
  TFile* datafile  = new TFile( m_filename );
  _yeff            = (TH1F*)datafile->Get( ( TString )( directory + SectorName ) );
  _yeff_exp        = (TH1F*)datafile->Get( ( TString )( directory_exp + SectorName ) );
  if ( !_yeff || !_yeff_exp ) return;

  if ( m_verbose ) {
    cout << " sector " << SectorName << " with number of sensors : " << m_Names->GetNbSensors( SectorName.Data() )
         << endl;
    cout << _yeff->GetNbinsX() << ", " << _yeff->GetXaxis()->GetXmin() << ", " << _yeff->GetXaxis()->GetXmax() << endl;
    cout << SectorName << endl;
  }
  boost::filesystem::path outpath( m_outDirectory.Data() );
  boost::filesystem::path plotpath( ( "plots/" + m_outputname ).Data() );
  if ( m_saveplots ) {
    if ( !boost::filesystem::exists( outpath / plotpath ) ) boost::filesystem::create_directories( outpath / plotpath );
  }

  if ( m_Names->GetNbSensors( SectorName.Data() ) == 4 ) {

    RooRealVar y( "y", "y", -250., 250. );
    y.setBins( 50 );

    if ( m_Names->GetYPos( SectorName.Data() ) > 0 )
      Length = -94.6;
    else
      Length = 94.6;

    // y.setRange("full",-(4.*94.4+3.*0.2)/2.-2.5*Scale,(4.*94.4+3.*0.2)/2.+2.5*Scale);
    y.setRange( "full", -220., 220. );
    y.setRange( "left", Length - Scale, Length + Scale );
    y.setRange( "right", -Scale, Scale );

    RooCategory cat_lr( "cat_lr", "cat_lr" );
    cat_lr.defineType( "left", -1 );
    cat_lr.defineType( "right", 1 );
    RooCategory cat_eff( "cat_eff", "cat_eff" );
    cat_eff.defineType( "found", 1 );
    cat_eff.defineType( "not found", 0 );
    RooRealVar weight( "weight", "weight", 0, 100000000000000 );
    RooRealVar N_l( "N_l", "N", 0.05, 0.8 );
    RooRealVar N_r( "N_r", "N", 0.05, 0.8 );

    RooDataSet* yeff_data = 0; // new RooDataSet("data","data",RooArgSet(y,weight,cat_lr,cat_eff),WeightVar(weight));
    yeff_data             = GetRooDataSetFromTH1( _yeff, _yeff_exp, y, weight, cat_lr, cat_eff, true, Length / 2. );

    RooRealVar    center( "center", "center", 0., -5., 5., "mm" );
    RooRealVar    length( "length", "Length", 100., -150., 150., "mm" );
    RooRealVar    width_l( "width_l", "width_l", 0.2, 0.01, 5., "mm" );
    RooRealVar    width_r( "width_r", "width_r", 0.2, 0.01, 5., "mm" );
    RooFormulaVar mu_l_l( "mu_l_l", "center+length-width_l/2.", RooArgSet( center, length, width_l ) );
    RooFormulaVar mu_l_r( "mu_l_r", "center+length+width_l/2.", RooArgSet( center, length, width_l ) );
    RooFormulaVar mu_r_l( "mu_r_l", "center-width_r/2.", RooArgSet( center, width_r ) );
    RooFormulaVar mu_r_r( "mu_r_r", "center+width_r/2.", RooArgSet( center, width_r ) );
    RooRealVar    sigma_l( "sigma_l", "#sigma_{l}", 2., 1., 8., "mm" );
    RooRealVar    sigma_r( "sigma_r", "#sigma_{r}", 2., 1., 8., "mm" );
    RooFormulaVar y_l_l( "y_l_l", "(y-mu_l_l)/(TMath::Sqrt(2)*sigma_l)", RooArgSet( y, mu_l_l, sigma_l ) );
    RooFormulaVar y_l_r( "y_l_r", "(y-mu_l_r)/(TMath::Sqrt(2)*sigma_l)", RooArgSet( y, mu_l_r, sigma_l ) );
    RooFormulaVar y_r_l( "y_r_l", "(y-mu_r_l)/(TMath::Sqrt(2)*sigma_r)", RooArgSet( y, mu_r_l, sigma_r ) );
    RooFormulaVar y_r_r( "y_r_r", "(y-mu_r_r)/(TMath::Sqrt(2)*sigma_r)", RooArgSet( y, mu_r_r, sigma_r ) );
    RooRealVar    sigslope_l( "sigslope_l", "Sig Slope", 0., -0.005, 0.005, "" );
    RooRealVar    bkgslope_l( "bkgslope_l", "Bkg Slope", 0., -0.001, 0.001, "" );
    RooRealVar    sigfrac_l( "sigfrac_l", "Sig Frac", 0.5, 0.2, 1., "" );
    RooRealVar    sigslope_r( "sigslope_r", "Sig Slope", 0., -0.005, 0.005, "" );
    RooRealVar    bkgslope_r( "bkgslope_r", "Bkg Slope", 0., -0.001, 0.001, "" );
    RooRealVar    sigfrac_r( "sigfrac_r", "Sig Frac", 0.5, 0.2, 1., "" );

    N_l.setVal( 0.2 );
    N_r.setVal( 0.2 );
    center.setVal( 0. );
    center.setRange( 0. - ParTol, 0. + ParTol );
    length.setVal( Length );
    length.setRange( Length - ParTol, Length + ParTol );
    width_l.setVal( 2. );
    sigma_l.setVal( 2. );
    sigslope_l.setVal( 0. );
    bkgslope_l.setVal( 0. );
    sigfrac_l.setVal( 1. );
    width_r.setVal( 2. );
    sigma_r.setVal( 2. );
    sigslope_r.setVal( 0. );
    bkgslope_r.setVal( 0. );
    sigfrac_r.setVal( 1. );
    bkgslope_l.setConstant();
    bkgslope_r.setConstant();
    sigfrac_l.setConstant();
    sigfrac_r.setConstant();

    TString formula_l(
        "N_l*(sigfrac_l*(1.+sigslope_l*(y-(center-length)))*(1.+0.5*TMath::Erf(y_l_r)+0.5*TMath::Erf(-y_l_l))+(1.-"
        "sigfrac_l)*(1.+bkgslope_l*(y-(center-length))))*(TMath::Abs(y)>TMath::Abs(length/2.))" );
    TString formula_r( "N_r*(sigfrac_r*(1.+sigslope_r*(y-center))*(1.+0.5*TMath::Erf(y_r_r)+0.5*TMath::Erf(-y_r_l))+(1."
                       "-sigfrac_r)*(1.+bkgslope_r*(y-center)))*(TMath::Abs(y)<TMath::Abs(length/2.))" );
    TString formula_lr( formula_l );
    formula_lr += "+";
    formula_lr += formula_r;

    RooArgSet arg_commun( y, center, length );
    RooArgSet arg_l( y_l_l, y_l_r, sigfrac_l, sigslope_l, bkgslope_l, N_l );
    RooArgSet arg_r( y_r_l, y_r_r, sigfrac_r, sigslope_r, bkgslope_r, N_r );
    arg_l.add( arg_commun );
    arg_r.add( arg_commun );
    RooArgSet arg_lr( arg_l );
    arg_lr.add( arg_r );

    RooFormulaVar* _model_eff_l = new RooFormulaVar( "_model_eff_l", formula_l, arg_l );
    RooEfficiency* model_eff_l  = new RooEfficiency( "model_eff_l", "model_eff_l", *_model_eff_l, cat_eff, "found" );
    RooFormulaVar* _model_eff_r = new RooFormulaVar( "_model_eff_r", formula_r, arg_r );
    RooEfficiency* model_eff_r  = new RooEfficiency( "model_eff_r", "model_eff_r", *_model_eff_r, cat_eff, "found" );

    RooFormulaVar* _model_eff = new RooFormulaVar( "_model_eff", formula_lr, arg_lr );
    RooEfficiency* model_eff  = new RooEfficiency( "model_eff", "model_eff", *_model_eff, cat_eff, "found" );

    RooDataSet* yeff_l = (RooDataSet*)yeff_data->reduce( "cat_lr == cat_lr::left" );
    RooDataSet* yeff_r = (RooDataSet*)yeff_data->reduce( "cat_lr == cat_lr::right" );

    if ( !m_glimpse ) {
      center.setConstant();
      model_eff_l->fitTo( *yeff_l, Minos( true ), Range( "left" ), PrintLevel( m_verbose ? 1 : -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ) );
      model_eff_r->fitTo( *yeff_r, Minos( true ), Range( "right" ), PrintLevel( m_verbose ? 1 : -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ) );
      center.setConstant( false );
      if ( m_constraint ) {
        RooGaussian* fconstext =
            new RooGaussian( "fconstext", "fconstext", length, RooConst( Length - 0.12 ), RooConst( SigmaConstraint ) );
        // RooProdPdf* model_r = new RooProdPdf("model_r","model with constraint",RooArgSet(*model_r_,*fconstext)) ;
        model_eff->fitTo( *yeff_data, Minos( true ), Range( "left,right" ), PrintLevel( m_verbose ? 1 : -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ),
                          ExternalConstraints( *fconstext ) );
      } else {
        model_eff->fitTo( *yeff_data, Minos( true ), Range( "left,right" ), PrintLevel( m_verbose ? 1 : -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ) );
      }
      if ( m_verbose ) {
        cout << endl;
        cout << SectorName << " " << center.getVal() << " " << center.getError() << endl;
        cout << SectorName << " " << TMath::Abs( length.getVal() ) << " " << length.getError() << endl;
        cout << endl;
      }
      m_YPosFitFile << SectorName << " " << center.getVal() << " " << center.getError() << endl;
      m_LengthFitFile << SectorName << " " << TMath::Abs( length.getVal() ) << " " << length.getError() << endl;
    }

    TCanvas* canvas = new TCanvas( "canvas", "canvas", 1920, 1080 );
    RooPlot* frame  = y.frame( Range( "full" ), Bins( 100 ) );
    frame->SetTitle( SectorName );
    yeff_data->plotOn( frame, Efficiency( cat_eff ) );
    if ( !m_glimpse ) {
      _model_eff_l->plotOn(
          frame, Range( "left" ),
          Normalization( _model_eff_l->createIntegral( y, "left" )->getVal(), RooAbsReal::NumEvent ) );
      _model_eff_r->plotOn(
          frame, Range( "right" ),
          Normalization( _model_eff_r->createIntegral( y, "right" )->getVal(), RooAbsReal::NumEvent ) );
    }
    frame->Draw();
    frame->GetXaxis()->SetTitle( "Local Y [mm]" );
    frame->GetYaxis()->SetTitle( "Efficiency []" );

    if ( m_saveplots ) {
      boost::filesystem::path plotname( ( SectorName + "_eff.eps" ).Data() );
      canvas->SaveAs( ( outpath / plotpath / plotname ).c_str() );
    }
    canvas->Close();
    if ( m_verbose ) cout << "everything finished okay." << endl;

  } else if ( m_Names->GetNbSensors( SectorName.Data() ) == 3 ) {
    RooRealVar y( "y", "y", -200., 200. );
    y.setBins( 30 );

    Length = 94.6;
    Edge   = Length / 2.;

    // y.setRange("full",-(3.*94.4+2.*0.2)/2.-1.5*Scale,(3.*94.4+2.*0.2)/2.+1.5*Scale);
    y.setRange( "full", -170., 170. );
    y.setRange( "left", -Edge - Scale, -Edge + Scale );
    y.setRange( "right", Edge - Scale, Edge + Scale );

    RooCategory cat_lr( "cat_lr", "cat_lr" );
    cat_lr.defineType( "left", -1 );
    cat_lr.defineType( "right", 1 );
    RooCategory cat_eff( "cat_eff", "cat_eff" );
    cat_eff.defineType( "found", 1 );
    cat_eff.defineType( "not found", 0 );
    RooRealVar weight( "weight", "weight", 0, 100000000000000 );
    RooRealVar N_l( "N_l", "N", 0.05, 0.8 );
    RooRealVar N_r( "N_r", "N", 0.05, 0.8 );

    RooDataSet* yeff_data = 0; // new RooDataSet("data","data",RooArgSet(y,weight,cat_lr,cat_eff),WeightVar(weight));
    yeff_data             = GetRooDataSetFromTH1( _yeff, _yeff_exp, y, weight, cat_lr, cat_eff, true, 0. );

    RooRealVar    center( "center", "center", 0., -5., 5., "mm" );
    RooRealVar    length( "length", "Length", 100., 50., 150., "mm" );
    RooRealVar    width_l( "width_l", "width_l", 0.2, 0.01, 5., "mm" );
    RooRealVar    width_r( "width_r", "width_r", 0.2, 0.01, 5., "mm" );
    RooFormulaVar mu_l_l( "mu_l_l", "center-length/2.-width_l/2.", RooArgSet( center, length, width_l ) );
    RooFormulaVar mu_l_r( "mu_l_r", "center-length/2.+width_l/2.", RooArgSet( center, length, width_l ) );
    RooFormulaVar mu_r_l( "mu_r_l", "center+length/2.-width_r/2.", RooArgSet( center, length, width_r ) );
    RooFormulaVar mu_r_r( "mu_r_r", "center+length/2.+width_r/2.", RooArgSet( center, length, width_r ) );
    RooRealVar    sigma_l( "sigma_l", "#sigma_{l}", 2., 1., 8., "mm" );
    RooRealVar    sigma_r( "sigma_r", "#sigma_{r}", 2., 1., 8., "mm" );
    RooFormulaVar y_l_l( "y_l_l", "(y-mu_l_l)/(TMath::Sqrt(2)*sigma_l)", RooArgSet( y, mu_l_l, sigma_l ) );
    RooFormulaVar y_l_r( "y_l_r", "(y-mu_l_r)/(TMath::Sqrt(2)*sigma_l)", RooArgSet( y, mu_l_r, sigma_l ) );
    RooFormulaVar y_r_l( "y_r_l", "(y-mu_r_l)/(TMath::Sqrt(2)*sigma_r)", RooArgSet( y, mu_r_l, sigma_r ) );
    RooFormulaVar y_r_r( "y_r_r", "(y-mu_r_r)/(TMath::Sqrt(2)*sigma_r)", RooArgSet( y, mu_r_r, sigma_r ) );
    RooRealVar    sigslope_l( "sigslope_l", "Sig Slope", 0., -0.005, 0.005, "" );
    RooRealVar    bkgslope_l( "bkgslope_l", "Bkg Slope", 0., -0.001, 0.001, "" );
    RooRealVar    sigfrac_l( "sigfrac_l", "Sig Frac", 0.5, 0.2, 1., "" );
    RooRealVar    sigslope_r( "sigslope_r", "Sig Slope", 0., -0.005, 0.005, "" );
    RooRealVar    bkgslope_r( "bkgslope_r", "Bkg Slope", 0., -0.001, 0.001, "" );
    RooRealVar    sigfrac_r( "sigfrac_r", "Sig Frac", 0.5, 0.2, 1., "" );

    N_l.setVal( 0.5 );
    N_r.setVal( 0.5 );
    center.setVal( 0. );
    center.setRange( 0. - ParTol, 0. + ParTol );
    length.setVal( Length );
    length.setRange( Length - ParTol, Length + ParTol );
    width_l.setVal( 2. );
    sigma_l.setVal( 2. );
    sigslope_l.setVal( 0. );
    bkgslope_l.setVal( 0. );
    sigfrac_l.setVal( 0.99 );
    width_r.setVal( 2. );
    sigma_r.setVal( 2. );
    sigslope_r.setVal( 0. );
    bkgslope_r.setVal( 0. );
    sigfrac_r.setVal( 0.99 );
    bkgslope_l.setConstant();
    bkgslope_r.setConstant();
    // sigfrac_l.setConstant();
    // sigfrac_r.setConstant();

    TString formula_l( "N_l*(sigfrac_l*(1.+sigslope_l*(y-(center-length/"
                       "2.)))*(1.+0.5*TMath::Erf(y_l_r)+0.5*TMath::Erf(-y_l_l))+(1.-sigfrac_l)*(1.+bkgslope_l*(y-("
                       "center-length/2.))))*(y<0.)" );
    TString formula_r( "N_r*(sigfrac_r*(1.+sigslope_r*(y-(center+length/"
                       "2.)))*(1.+0.5*TMath::Erf(y_r_r)+0.5*TMath::Erf(-y_r_l))+(1.-sigfrac_r)*(1.+bkgslope_r*(y-("
                       "center+length/2.))))*(y>0.)" );
    TString formula_lr( formula_l );
    formula_lr += "+";
    formula_lr += formula_r;

    RooArgSet arg_commun( y, center, length );
    RooArgSet arg_l( y_l_l, y_l_r, sigfrac_l, sigslope_l, bkgslope_l, N_l );
    RooArgSet arg_r( y_r_l, y_r_r, sigfrac_r, sigslope_r, bkgslope_r, N_r );
    arg_l.add( arg_commun );
    arg_r.add( arg_commun );
    RooArgSet arg_lr( arg_l );
    arg_lr.add( arg_r );

    RooFormulaVar* _model_eff_l = new RooFormulaVar( "_model_eff_l", formula_l, arg_l );
    RooEfficiency* model_eff_l  = new RooEfficiency( "model_eff_l", "model_eff_l", *_model_eff_l, cat_eff, "found" );
    RooFormulaVar* _model_eff_r = new RooFormulaVar( "_model_eff_r", formula_r, arg_r );
    RooEfficiency* model_eff_r  = new RooEfficiency( "model_eff_r", "model_eff_r", *_model_eff_r, cat_eff, "found" );

    RooFormulaVar* _model_eff = new RooFormulaVar( "_model_eff", formula_lr, arg_lr );
    RooEfficiency* model_eff  = new RooEfficiency( "model_eff", "model_eff", *_model_eff, cat_eff, "found" );

    RooDataSet* yeff_l = (RooDataSet*)yeff_data->reduce( "cat_lr == cat_lr::left" );
    RooDataSet* yeff_r = (RooDataSet*)yeff_data->reduce( "cat_lr == cat_lr::right" );

    if ( !m_glimpse ) {
      center.setConstant();

      model_eff_l->fitTo( *yeff_l, Minos( true ), Range( "left" ), PrintLevel( -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ) );

      model_eff_r->fitTo( *yeff_r, Minos( true ), Range( "right" ), PrintLevel( -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ) );

      center.setConstant( false );

      if ( m_constraint ) {
        RooGaussian* fconstext =
            new RooGaussian( "fconstext", "fconstext", length, RooConst( Length - 0.12 ), RooConst( SigmaConstraint ) );
        // RooProdPdf* model_r = new RooProdPdf("model_r","model with constraint",RooArgSet(*model_r_,*fconstext)) ;
        model_eff->fitTo( *yeff_data, Minos( true ), Range( "left,right" ), PrintLevel( -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ),
                          ExternalConstraints( *fconstext ) );
      } else {
        model_eff->fitTo( *yeff_data, Minos( true ), Range( "left,right" ), PrintLevel( -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ) );
      }
      if ( m_verbose ) {
        cout << endl;
        cout << SectorName << " " << center.getVal() << " " << center.getError() << endl;
        cout << SectorName << " " << TMath::Abs( length.getVal() ) << " " << length.getError() << endl;
        cout << endl;
      }
      m_YPosFitFile << SectorName << " " << center.getVal() << " " << center.getError() << endl;
      m_LengthFitFile << SectorName << " " << length.getVal() << " " << length.getError() << endl;
    }

    TCanvas* canvas = new TCanvas( "canvas", "canvas", 1920, 1080 );

    RooPlot* frame = y.frame( Range( "full" ), Bins( 100 ) );
    frame->SetTitle( SectorName );
    yeff_data->plotOn( frame, Efficiency( cat_eff ) );
    if ( !m_glimpse ) {
      _model_eff_l->plotOn(
          frame, Range( "left" ),
          Normalization( _model_eff_l->createIntegral( y, "left" )->getVal(), RooAbsReal::NumEvent ) );
      _model_eff_r->plotOn(
          frame, Range( "right" ),
          Normalization( _model_eff_r->createIntegral( y, "right" )->getVal(), RooAbsReal::NumEvent ) );
      // model_eff_l->paramOn(frame_l, Layout(0.6,0.9,0.45));
    }
    // frame->SetMinimum(0.);
    // frame->SetMaximum(1.);
    frame->Draw();
    frame->GetXaxis()->SetTitle( "Local Y [mm]" );
    frame->GetYaxis()->SetTitle( "Efficiency []" );

    if ( m_saveplots ) {
      boost::filesystem::path plotname( ( SectorName + "_eff.eps" ).Data() );
      canvas->SaveAs( ( outpath / plotpath / plotname ).c_str() );
    }
    canvas->Close();
    if ( m_verbose ) cout << "everything finished okay." << endl;

  } else if ( m_Names->GetNbSensors( SectorName.Data() ) == 2 ) {
    RooRealVar y( "y", "y", -150., 150. );
    y.setBins( 30 );

    // y.setRange("full",-(2.*94.4+1.*0.2)-Scale,(2.*94.4+1.*0.2)+Scale);
    y.setRange( "full", -140., 140. );
    y.setRange( "center", -Scale, +Scale );

    RooCategory cat_lr( "cat_lr", "cat_lr" );
    cat_lr.defineType( "left", -1 );
    cat_lr.defineType( "right", 1 );
    RooCategory cat_eff( "cat_eff", "cat_eff" );
    cat_eff.defineType( "found", 1 );
    cat_eff.defineType( "not found", 0 );
    RooRealVar weight( "weight", "weight", 0, 100000000000000 );
    RooRealVar N( "N", "N", 0.05, 0.8 );

    RooDataSet* yeff_data = 0; // new RooDataSet("data","data",RooArgSet(y,weight,cat_lr,cat_eff),WeightVar(weight));
    yeff_data             = GetRooDataSetFromTH1( _yeff, _yeff_exp, y, weight, cat_lr, cat_eff, false, 0. );

    RooRealVar    center( "center", "center", 0., -5., 5., "mm" );
    RooRealVar    width( "width", "width", 0.2, 0.1, 10., "mm" );
    RooFormulaVar mu_l( "mu_l", "center-width/2.", RooArgSet( center, width ) );
    RooFormulaVar mu_r( "mu_r", "center+width/2.", RooArgSet( center, width ) );
    RooRealVar    sigma( "sigma", "#sigma", 1., 0.5, 5., "mm" );
    RooFormulaVar y_l( "y_l", "(y-mu_l)/(TMath::Sqrt(2)*sigma)", RooArgSet( y, mu_l, sigma ) );
    RooFormulaVar y_r( "y_r", "(y-mu_r)/(TMath::Sqrt(2)*sigma)", RooArgSet( y, mu_r, sigma ) );
    RooRealVar    sigslope( "sigslope", "Sig Slope", 0., -0.005, 0.005, "" );
    RooRealVar    bkgslope( "bkgslope", "Bkg Slope", 0., -0.001, 0.001, "" );
    RooRealVar    sigfrac( "sigfrac", "Sig Frac", 0.5, 0.2, 1., "" );

    N.setVal( 0.5 );
    center.setVal( 0. );
    center.setRange( 0. - ParTol, 0. + ParTol );
    width.setVal( 1. );
    sigma.setVal( 1. );
    sigslope.setVal( 0. );
    bkgslope.setVal( 0. );
    sigfrac.setVal( 0.99 );
    bkgslope.setConstant();

    TString formula( "N*(sigfrac*(1.+sigslope*(y-center))*(1.+0.5*TMath::Erf(y_r)+0.5*TMath::Erf(-y_l))+(1.-sigfrac)*("
                     "1.+bkgslope*(y-center)))" );

    RooArgSet arg_commun( y, center );
    RooArgSet arg( y_l, y_r, sigfrac, sigslope, bkgslope, N );
    arg.add( arg_commun );

    RooFormulaVar* _model_eff = new RooFormulaVar( "_model_eff", formula, arg );
    RooEfficiency* model_eff  = new RooEfficiency( "model_eff", "model_eff", *_model_eff, cat_eff, "found" );

    RooDataSet* yeff = (RooDataSet*)yeff_data->reduce( "1 == 1" );

    if ( !m_glimpse ) {
      model_eff->fitTo( *yeff, Minos( true ), Range( "center" ), PrintLevel( m_verbose ? 1 : -1 ),
                        /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ) );
      if ( m_verbose ) {
        cout << endl;
        cout << SectorName << " " << center.getVal() << " " << center.getError() << endl;
        cout << endl;
      }

      m_YPosFitFile << SectorName << " " << center.getVal() << " " << center.getError() << endl;
    }

    TCanvas* canvas = new TCanvas( "canvas", "canvas", 1920, 1080 );

    RooPlot* frame = y.frame( Range( "full" ), Bins( 100 ) );
    frame->SetTitle( SectorName );
    yeff->plotOn( frame, Efficiency( cat_eff ) );
    if ( !m_glimpse ) {
      _model_eff->plotOn( frame, Range( "center" ),
                          Normalization( _model_eff->createIntegral( y, "center" )->getVal(), RooAbsReal::NumEvent ) );
      // model_eff->paramOn(frame, Layout(0.6,0.9,0.45));
    }
    frame->Draw();
    frame->GetXaxis()->SetTitle( "Local Y [mm]" );
    frame->GetYaxis()->SetTitle( "Efficiency []" );
    canvas->Update();

    if ( m_saveplots ) {
      boost::filesystem::path plotname( ( SectorName + "_eff.eps" ).Data() );
      canvas->SaveAs( ( outpath / plotpath / plotname ).c_str() );
    }

    canvas->Close();
    if ( m_verbose ) cout << "everything finished okay." << endl;

  } else {
    Scale *= 2.;

    RooRealVar y( "y", "y", -150., 150. );
    y.setBins( 30 );

    Length = 91.676;
    Edge   = Length / 2.;

    // y.setRange("full",-(94.4)-Scale/2.,(94.4)+Scale/2.);
    y.setRange( "full", -80., 80. );
    y.setRange( "left", -Edge - Scale / 2., -Edge + Scale / 2. );
    y.setRange( "right", Edge - Scale / 2., Edge + Scale / 2. );

    RooCategory cat_lr( "cat_lr", "cat_lr" );
    cat_lr.defineType( "left", -1 );
    cat_lr.defineType( "right", 1 );
    RooCategory cat_eff( "cat_eff", "cat_eff" );
    cat_eff.defineType( "found", 1 );
    cat_eff.defineType( "not found", 0 );
    RooRealVar weight( "weight", "weight", 0, 100000000000000 );
    RooRealVar N_l( "N_l", "N", 0.05, 0.99 );
    RooRealVar N_r( "N_r", "N", 0.05, 0.99 );

    RooDataSet* yeff_data = 0; // new RooDataSet("data","data",RooArgSet(y,weight,cat_lr,cat_eff),WeightVar(weight));
    yeff_data             = GetRooDataSetFromTH1( _yeff, _yeff_exp, y, weight, cat_lr, cat_eff, true, 0. );

    RooRealVar    center( "center", "center", 0., -5., 5., "mm" );
    RooRealVar    length( "length", "Length", 100., 50., 250., "mm" );
    RooFormulaVar mu_l( "mu_l", "center-length/2.", RooArgSet( center, length ) );
    RooFormulaVar mu_r( "mu_r", "center+length/2.", RooArgSet( center, length ) );
    RooRealVar    sigma_l_1( "sigma_l_1", "#sigma_{l}", 1., 0.2, 5., "mm" );
    RooRealVar    sigma_l_2( "sigma_l_2", "#sigma_{l}", 3., 1., 20., "mm" );
    RooRealVar    sigma_r_1( "sigma_r_1", "#sigma_{r}", 1., 0.2, 5., "mm" );
    RooRealVar    sigma_r_2( "sigma_r_2", "#sigma_{r}", 3., 1., 20., "mm" );
    RooFormulaVar y_l_1( "y_l_1", "(y-mu_l)/(TMath::Sqrt(2)*sigma_l_1)", RooArgSet( y, mu_l, sigma_l_1 ) );
    RooFormulaVar y_l_2( "y_l_2", "(y-mu_l)/(TMath::Sqrt(2)*sigma_l_2)", RooArgSet( y, mu_l, sigma_l_2 ) );
    RooFormulaVar y_r_1( "y_r_1", "(y-mu_r)/(TMath::Sqrt(2)*sigma_r_1)", RooArgSet( y, mu_r, sigma_r_1 ) );
    RooFormulaVar y_r_2( "y_r_2", "(y-mu_r)/(TMath::Sqrt(2)*sigma_r_2)", RooArgSet( y, mu_r, sigma_r_2 ) );
    RooRealVar    sigslope_l( "sigslope_l", "Sig Slope", 0., -0.012, 0.012, "" );
    RooRealVar    bkgslope_l( "bkgslope_l", "Bkg Slope", 0., -0.008, 0.008, "" );
    RooRealVar    sigfrac_l( "sigfrac_l", "Sig Frac", 0.5, 0.2, 0.9, "" );
    RooRealVar    sig12frac_l( "sig12frac_l", "Sig Frac", 0.5, 0., 1., "" );
    RooRealVar    sigslope_r( "sigslope_r", "Sig Slope", 0., -0.012, 0.012, "" );
    RooRealVar    bkgslope_r( "bkgslope_r", "Bkg Slope", 0., -0.008, 0.008, "" );
    RooRealVar    sigfrac_r( "sigfrac_r", "Sig Frac", 0.5, 0.2, 0.9, "" );
    RooRealVar    sig12frac_r( "sig12frac_r", "Sig Frac", 0.5, 0., 1., "" );

    N_l.setVal( 0.5 );
    N_r.setVal( 0.5 );
    center.setVal( 0. );
    center.setRange( 0. - ParTol, 0. + ParTol );
    length.setVal( Length );
    length.setRange( Length - ParTol, Length + ParTol );
    sigma_l_1.setVal( 1. );
    sigma_l_2.setVal( 3. );
    sigslope_l.setVal( 0. );
    bkgslope_l.setVal( 0. );
    sigfrac_l.setVal( 0.6 );
    sigma_r_1.setVal( 1. );
    sigma_r_2.setVal( 3. );
    sigslope_r.setVal( 0. );
    bkgslope_r.setVal( 0. );
    sigfrac_r.setVal( 0.6 );
    sig12frac_l.setVal( 0.5 );
    sig12frac_r.setVal( 0.5 );
    // bkgslope_l.setConstant();
    // bkgslope_r.setConstant();

    TString formula_l( "N_l*(sigfrac_l*(1.+sigslope_l*(y-(center-length/"
                       "2.)))*(0.5+0.5*sig12frac_l*TMath::Erf(y_l_1)+0.5*(1.-sig12frac_l)*TMath::Erf(y_l_2))+(1.-"
                       "sigfrac_l)*(1.+bkgslope_l*(y-(center-length/2.))))*(y<0.)" );
    TString formula_r( "N_r*(sigfrac_r*(1.+sigslope_r*(y-(center+length/"
                       "2.)))*(0.5+0.5*sig12frac_r*TMath::Erf(-y_r_1)+0.5*(1.-sig12frac_r)*TMath::Erf(-y_r_2))+(1.-"
                       "sigfrac_r)*(1.+bkgslope_r*(y-(center+length/2.))))*(y>0.)" );
    TString formula_lr( formula_l );
    formula_lr += "+";
    formula_lr += formula_r;

    RooArgSet arg_commun( y, center, length );
    RooArgSet arg_l( y_l_1, y_l_2, sigma_l_1, sigma_l_2, sig12frac_l, sigfrac_l, sigslope_l, bkgslope_l, N_l );
    RooArgSet arg_r( y_r_1, y_r_2, sigma_r_1, sigma_r_2, sig12frac_r, sigfrac_r, sigslope_r, bkgslope_r, N_r );
    arg_l.add( arg_commun );
    arg_r.add( arg_commun );
    RooArgSet arg_lr( arg_l );
    arg_lr.add( arg_r );

    RooFormulaVar* _model_eff_l = new RooFormulaVar( "_model_eff_l", formula_l, arg_l );
    RooEfficiency* model_eff_l  = new RooEfficiency( "model_eff_l", "model_eff_l", *_model_eff_l, cat_eff, "found" );
    RooFormulaVar* _model_eff_r = new RooFormulaVar( "_model_eff_r", formula_r, arg_r );
    RooEfficiency* model_eff_r  = new RooEfficiency( "model_eff_r", "model_eff_r", *_model_eff_r, cat_eff, "found" );

    RooFormulaVar* _model_eff = new RooFormulaVar( "_model_eff", formula_lr, arg_lr );
    RooEfficiency* model_eff  = new RooEfficiency( "model_eff", "model_eff", *_model_eff, cat_eff, "found" );

    RooDataSet* yeff_l = (RooDataSet*)yeff_data->reduce( "cat_lr == cat_lr::left" );
    RooDataSet* yeff_r = (RooDataSet*)yeff_data->reduce( "cat_lr == cat_lr::right" );

    if ( !m_glimpse ) {
      center.setConstant();

      model_eff_l->fitTo( *yeff_l, Minos( true ), Range( "left" ), PrintLevel( m_verbose ? 1 : -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ) );

      model_eff_r->fitTo( *yeff_r, Minos( true ), Range( "right" ), PrintLevel( m_verbose ? 1 : -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ) );

      center.setConstant( false );

      if ( m_constraint ) {
        RooGaussian* fconstext =
            new RooGaussian( "fconstext", "fconstext", length, RooConst( Length - 0.05 ), RooConst( SigmaConstraint ) );
        // RooProdPdf* model_r = new RooProdPdf("model_r","model with constraint",RooArgSet(*model_r_,*fconstext)) ;
        model_eff->fitTo( *yeff_data, Minos( true ), Range( "left,right" ), PrintLevel( m_verbose ? 1 : -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ),
                          ExternalConstraints( *fconstext ) );
      } else {
        model_eff->fitTo( *yeff_data, Minos( true ), Range( "left,right" ), PrintLevel( m_verbose ? 1 : -1 ),
                          /*Minimizer("Minuit"),*/ ConditionalObservables( y ), NumCPU( 4 ), Strategy( 1 ) );
      }
      if ( m_verbose ) {
        cout << endl;
        cout << SectorName << " " << center.getVal() << " " << center.getError() << endl;
        cout << SectorName << " " << length.getVal() << " " << length.getError() << endl;
        cout << endl;
      }

      m_YPosFitFile << SectorName << " " << center.getVal() << " " << center.getError() << endl;
      m_LengthFitFile << SectorName << " " << TMath::Abs( length.getVal() ) << " " << length.getError() << endl;
    }

    TCanvas* canvas = new TCanvas( "canvas", "canvas", 1920, 1080 );
    // canvas->Divide(2,1);
    /*
    canvas->cd(1);
    RooPlot* frame_l = y.frame(Range("left"));
    frame_l->SetTitle(SectorName);
    yeff_l->plotOn(frame_l,Efficiency(cat_eff));
    _model_eff_l->plotOn(frame_l);
    model_eff_l->paramOn(frame_l, Layout(0.6,0.9,0.45));
    frame_l->Draw();
    canvas->Update();

    canvas->cd(2);
    RooPlot* frame_r = y.frame(Range("right"));
    frame_r->SetTitle(SectorName);
    yeff_r->plotOn(frame_r,Efficiency(cat_eff));
    _model_eff_r->plotOn(frame_r);
    model_eff_r->paramOn(frame_r, Layout(0.6,0.9,0.45));
    frame_r->Draw();
    canvas->Update();
    */

    RooPlot* frame = y.frame( Range( "full" ), Bins( 100 ) );
    frame->SetTitle( SectorName );
    yeff_data->plotOn( frame, Efficiency( cat_eff ) );
    if ( !m_glimpse ) {
      _model_eff_l->plotOn(
          frame, Range( "left" ),
          Normalization( _model_eff_l->createIntegral( y, "left" )->getVal(), RooAbsReal::NumEvent ) );
      _model_eff_r->plotOn(
          frame, Range( "right" ),
          Normalization( _model_eff_r->createIntegral( y, "right" )->getVal(), RooAbsReal::NumEvent ) );
      // model_eff_l->paramOn(frame_l, Layout(0.6,0.9,0.45));
    }
    // frame->SetMinimum(0.);
    // frame->SetMaximum(1.);
    frame->Draw();
    frame->GetXaxis()->SetTitle( "Local Y [mm]" );
    frame->GetYaxis()->SetTitle( "Efficiency []" );

    if ( m_saveplots ) {
      boost::filesystem::path plotname( ( SectorName + "_eff.eps" ).Data() );
      canvas->SaveAs( ( outpath / plotpath / plotname ).c_str() );
    }

    canvas->Close();
    if ( m_verbose ) cout << "everything finished okay." << endl;

    Scale /= 2.;
  }
}

void TTYAlignMagOff::FitfromROOTFile() {
  STNames::Vector*          sectors( m_Names->GetSectors() );
  STNames::Vector::iterator It, Begin( sectors->begin() ), End( sectors->end() );
  for ( It = Begin; It != End; It++ ) {
    if ( m_verbose ) cout << " call function GetMeanFromHisto() for sector " << ( *It ).c_str() << endl;
    GetMeanFromHisto( ( *It ).c_str() );
  }
  sectors->clear();
}

void TTYAlignMagOff::fit_efficiency() {
  if ( !m_verbose ) {
    RooMsgService::instance().setSilentMode( true );
    RooMsgService::instance().setStreamStatus( 1, false );
    RooFit::Verbose( false );
  }
  TString Titlename( "Local Y Position" );
  gROOT->SetStyle( "Plain" );
  gStyle->SetOptStat( 0 );
  gStyle->SetTitleX( 0.5 );
  gStyle->SetTitleAlign( 23 );
  gStyle->SetTitleBorderSize( 0 );
  gStyle->SetPaintTextFormat( "5.1f" );
  gStyle->SetStatFontSize( 0.15 );
  gStyle->SetTitleFontSize( 0.07 );

  boost::filesystem::path outpath( m_outDirectory.Data() );
  boost::filesystem::path fitpath( "fit" );
  if ( !boost::filesystem::exists( outpath / fitpath ) ) boost::filesystem::create_directories( outpath / fitpath );

  boost::filesystem::path yposfilename( ( m_outputname + ".txt" ).Data() );
  boost::filesystem::path yposfile = outpath / fitpath / yposfilename;
  m_YPosFitFile.open( yposfile.c_str() );

  boost::filesystem::path lengthfilename( ( m_outputname + "_length.txt" ).Data() );
  boost::filesystem::path lengthfile = outpath / fitpath / lengthfilename;
  m_LengthFitFile.open( lengthfile.c_str() );

  FitfromROOTFile();
}

void TTYAlignMagOff::plots() {
  Double_t Length_S34( 94.6 );
  Double_t Length_S1( 91.676 );
  Double_t Scale( 3. );
  RooMsgService::instance().setSilentMode( kTRUE );
  RooMsgService::instance().setStreamStatus( 1, kFALSE );

  boost::filesystem::path outpath( m_outDirectory.Data() );
  boost::filesystem::path plotpath( ( "plots/" + m_outputname ).Data() );
  if ( !boost::filesystem::exists( outpath / plotpath ) ) boost::filesystem::create_directories( outpath / plotpath );

  TString Titlename_YPos( "TT Y Position" );
  TString Titlename_Length( "Two Gaps Distance" );

  gROOT->SetStyle( "Plain" );
  gStyle->SetOptStat( 0 );
  gStyle->SetTitleX( 0.5 );
  gStyle->SetTitleAlign( 23 );
  gStyle->SetTitleBorderSize( 0 );
  gStyle->SetPaintTextFormat( "5.1f" );
  gStyle->SetStatFormat( "5.5f" );
  // gStyle->SetStatFontSize(0.15);
  gStyle->SetTitleFontSize( 0.07 );
  gStyle->SetPadTickY( 1 );
  gStyle->SetPadTickX( 1 );

  unsigned int nColors = 52;
  Int_t        MyPalette[52];
  Double_t     s[3] = {0.00, 0.50, 1.00};
  Double_t     b[3] = {0.80, 1.00, 0.00};
  Double_t     g[3] = {0.00, 1.00, 0.00};
  Double_t     r[3] = {0.00, 1.00, 0.80};
  Int_t        FI   = TColor::CreateGradientColorTable( 3, s, r, g, b, nColors );
  for ( unsigned int k( 0 ); k < nColors; k++ ) { MyPalette[k] = FI + k; }
  gStyle->SetNumberContours( nColors );
  gStyle->SetPalette( nColors, MyPalette );

  boost::filesystem::path   fitpath( "fit" );
  boost::filesystem::path   yposfilename( ( m_outputname + ".txt" ).Data() );
  boost::filesystem::path   yposfile = outpath / fitpath / yposfilename;
  boost::filesystem::path   lengthfilename( ( m_outputname + "_length.txt" ).Data() );
  boost::filesystem::path   lengthfile = outpath / fitpath / lengthfilename;
  Parser*                   m_Parser   = new Parser();
  Parser::Param_Result_Map* YPos       = m_Parser->CreateParamResultMapFromFitFile( yposfile.c_str() );
  Parser::Param_Result_Map* Length     = m_Parser->CreateParamResultMapFromFitFile( lengthfile.c_str() );
  // Parser::XYZ_Pos_Map* XYZPos = m_Parser->CreateXYZPosMapFromFile( m_dbfilename );

  ST2DPlot* h_YPos   = new ST2DPlot( "TT", "h_YPos", Titlename_YPos );
  ST2DPlot* h_Length = new ST2DPlot( "TT", "h_Length", "Measured - Expected Active Length / Two Gaps Distance" );
  ST2DPlot* h_Length_S34 =
      new ST2DPlot( "TT", "h_Length_S34", "Measured - Expected Active Length / Two Gaps Distance" );
  ST2DPlot* h_Length_S1 = new ST2DPlot( "TT", "h_Length_S1", "Measured - Expected Active Length / Two Gaps Distance" );
  gStyle->SetOptStat( 1110 );
  gStyle->SetStatY( 0.87 );
  gStyle->SetStatX( 0.87 );
  gStyle->SetStatH( 0.30 );
  gStyle->SetStatW( 0.25 );
  TH1D* h_YPos_       = new TH1D( "h_YPos_", ';' + Titlename_YPos + " [mm];", 100, -Scale, Scale );
  TH1D* h_Length_S34_ = new TH1D( "h_Length_S34_", "S34 Sectors;" + Titlename_YPos + " [mm];", 100, Length_S34 - Scale,
                                  Length_S34 + Scale );
  TH1D* h_Length_S1_ =
      new TH1D( "h_Length_S1_", "S1 Sectors;" + Titlename_YPos + " [mm];", 100, Length_S1 - Scale, Length_S1 + Scale );

  RooRealVar   y( "y", "Local Y", -Scale, Scale, "mm" );
  RooRealVar   mean( "mean", "Mean", 0., -Scale * 0.9, Scale * 0.9, "mm" );
  RooRealVar   sigma( "sigma", "#sigma", 0.5, 0., 10., "mm" );
  RooGaussian* gauss = new RooGaussian( "gauss", "gauss", y, mean, sigma );

  std::map<std::string, TH1D*> hmap_YPos_Global;
  std::map<std::string, TH1D*> hmap_YPos_Layer;
  std::map<std::string, TH1D*> hmap_YPos_BiLayer;

  std::map<std::string, Gauss_Fit_Result*> gfrmap_YPos_Global;
  std::map<std::string, Gauss_Fit_Result*> gfrmap_YPos_Layer;
  std::map<std::string, Gauss_Fit_Result*> gfrmap_YPos_BiLayer;

  STNames::Vector* LayerNames;
  LayerNames = m_Names->GetLayers();
  STNames::Vector* BiLayerNames;
  BiLayerNames = m_Names->GetBiLayers();
  STNames::SectNames* SectNames;

  string tmp;

  tmp = "h_YPos_";
  tmp += m_Names->GetGlobalName();
  hmap_YPos_Global[m_Names->GetGlobalName()] = new TH1D( tmp.c_str(), "", 100, -Scale, Scale );

  for ( STNames::Vector::iterator It2 = LayerNames->begin(); It2 != LayerNames->end(); It2++ ) {
    tmp = "h_YPos_";
    tmp += ( *It2 );
    hmap_YPos_Layer[*It2] = new TH1D( tmp.c_str(), "", 100, -Scale, Scale );
  }

  for ( STNames::Vector::iterator It2 = BiLayerNames->begin(); It2 != BiLayerNames->end(); It2++ ) {
    tmp = "h_YPos_";
    tmp += ( *It2 );
    hmap_YPos_BiLayer[*It2] = new TH1D( tmp.c_str(), "", 100, -Scale, Scale );
  }

  for ( Parser::Param_Result_Map::const_iterator It = YPos->begin(); It != YPos->end(); It++ ) {
    SectNames = m_Names->GetSectNames( It->first );
    if ( !SectNames ) continue;

    if ( SectNames->Layer == 'U' || SectNames->Layer == 'V' )
      It->second->Value = It->second->Value * TMath::Cos( 5. * TMath::Pi() / 180. );
  }

  for ( Parser::Param_Result_Map::const_iterator It = YPos->begin(); It != YPos->end(); It++ ) {
    SectNames = m_Names->GetSectNames( It->first );
    if ( !SectNames ) continue;
    /*
    if(SectNames->TT == "a" && SectNames->Region == "B" && (SectNames->Sectno)%3 == 0)
      continue;
    if(SectNames->TT == "b" && SectNames->Region == "B" && (SectNames->Sectno-4)%3 == 0 && SectNames->Sectno < 23 &&
    SectNames->Sectno > 4) continue;
    */
    h_YPos->Fill( It->first, It->second->Value );
    h_YPos_->Fill( It->second->Value );
    hmap_YPos_Layer[m_Names->GetLayerName( It->first )]->Fill( It->second->Value );
    hmap_YPos_BiLayer[m_Names->GetBiLayerName( It->first )]->Fill( It->second->Value );
    hmap_YPos_Global[m_Names->GetGlobalName()]->Fill( It->second->Value );
  }

  cout << "######## Y Misalignment ########" << endl;

  if ( hmap_YPos_Global[m_Names->GetGlobalName()]->GetEntries() > 0 ) {
    gfrmap_YPos_Global[m_Names->GetGlobalName()] =
        fit_Gauss( hmap_YPos_Global[m_Names->GetGlobalName()], (RooGaussian*)gauss->Clone( "_gauss" ),
                   m_Names->GetGlobalName().c_str(), 0., Scale );
    cout << *gfrmap_YPos_Global[m_Names->GetGlobalName()] << endl;
  }
  cout << endl;
  for ( STNames::Vector::iterator It2 = BiLayerNames->begin(); It2 != BiLayerNames->end(); It2++ ) {
    if ( hmap_YPos_BiLayer[*It2]->GetEntries() > 0 ) {
      gfrmap_YPos_BiLayer[*It2] =
          fit_Gauss( hmap_YPos_BiLayer[*It2], (RooGaussian*)gauss->Clone( "_gauss" ), ( *It2 ).c_str(),
                     // gfrmap_YPos_Global[Names->GetGlobalName()],
                     0., Scale );
      cout << *gfrmap_YPos_BiLayer[*It2] << endl;
    }
  }
  cout << endl;
  for ( STNames::Vector::iterator It2 = LayerNames->begin(); It2 != LayerNames->end(); It2++ ) {
    if ( hmap_YPos_Layer[*It2]->GetEntries() > 0 ) {
      gfrmap_YPos_Layer[*It2] =
          fit_Gauss( hmap_YPos_Layer[*It2], (RooGaussian*)gauss->Clone( "_gauss" ), ( *It2 ).c_str(),
                     // gfrmap_YPos_Global[Names->GetGlobalName()],
                     0., Scale );
      cout << *gfrmap_YPos_Layer[*It2] << endl;
    }
  }

  // TGraphErrors* g_Y_Z = new TGraphErrors();
  // Int_t g_Y_Z_i(0.);
  for ( Parser::Param_Result_Map::const_iterator It = Length->begin(); It != Length->end(); It++ ) {
    SectNames = m_Names->GetSectNames( It->first );
    if ( !SectNames ) continue;
    /*
    if(SectNames->TT == "a" && SectNames->Region == "B" && (SectNames->Sectno)%3 == 0)
      continue;
    if(SectNames->TT == "b" && SectNames->Region == "B" && (SectNames->Sectno-4)%3 == 0 && SectNames->Sectno < 23 &&
    SectNames->Sectno > 4) continue;
    */

    // if(/*SectNames->Region == 'B' && */SectNames->NbSensors != 1){
    //  g_Y_Z->SetPoint(g_Y_Z_i,(*XYZPos)[It->first]->Z,It->second->Value);
    //  g_Y_Z->SetPointError(g_Y_Z_i,0.,It->second->Error);
    //  g_Y_Z_i++;
    //}

    if ( SectNames->NbSensors == 1 ) {
      h_Length->Fill( It->first, It->second->Value - Length_S1 );
      h_Length_S1->Fill( It->first, It->second->Value );
      h_Length_S1_->Fill( It->second->Value );
      h_Length_S34->Fill( It->first, 0.0001 );
    } else if ( SectNames->NbSensors != 1 ) {
      h_Length->Fill( It->first, It->second->Value - Length_S34 );
      h_Length_S34->Fill( It->first, It->second->Value );
      h_Length_S34_->Fill( It->second->Value );
      h_Length_S1->Fill( It->first, 0.0001 );
    }
  }

  cout << endl;
  cout << "######## Active Length ########" << endl;

  cout << *fit_Gauss( h_Length_S1_, (RooGaussian*)gauss->Clone( "_gauss" ), "S1 Modules", Length_S1, Scale ) << endl;

  cout << endl;
  cout << "######## Two Gaps Distance ########" << endl;

  cout << *fit_Gauss( h_Length_S34_, (RooGaussian*)gauss->Clone( "_gauss" ), "S34 Modules", Length_S34, Scale ) << endl;

  cout << endl;

  TPaveText* lhcb7TeVPrelimR = new TPaveText( 0.17, 0.75, 0.44, 0.88, "BRNDC" );
  lhcb7TeVPrelimR->SetFillColor( 0 );
  lhcb7TeVPrelimR->SetTextAlign( 12 );
  lhcb7TeVPrelimR->SetBorderSize( 0 );
  lhcb7TeVPrelimR->AddText( "LHCb Preliminary 2012" );

  TCanvas* c_YPos = new TCanvas( "c_YPos", "c_YPos", 1200, 1000 );

  h_YPos_->SetTitle( Titlename_YPos );
  h_YPos_->SetFillStyle( 3004 );
  h_YPos_->SetFillColor( 2 );
  h_YPos_->SetLineColor( 2 );
  h_YPos_->Draw();
  // lhcb7TeVPrelimR->Draw();

  // c_YPos->SaveAs("plots/"+outputname+".eps");
  boost::filesystem::path plotname( ( m_outputname + ".eps" ).Data() );
  c_YPos->SaveAs( ( outpath / plotpath / plotname ).c_str() );

  TCanvas* c_YPos_2D = new TCanvas( "c_YPos_2D", "c_YPos_2D", 1200, 600 );

  h_YPos->Histogram()->SetMinimum( -Scale );
  h_YPos->Histogram()->SetMaximum( Scale );
  h_YPos->Draw( "COLZ" );
  h_YPos->Draw( "TEXT,same" );

  // c_YPos_2D->SaveAs("plots/"+outputname+"_2D.eps");
  plotname = boost::filesystem::path( ( m_outputname + "_2D.eps" ).Data() );
  c_YPos_2D->SaveAs( ( outpath / plotpath / plotname ).c_str() );

  TCanvas* c_Length = new TCanvas( "c_Length", "c_Length", 1200, 600 );

  TPad* p_Length = static_cast<TPad*>( gPad );
  p_Length->Divide( 2, 1 );
  p_Length->cd( 1 );
  h_Length_S34_->Draw();
  h_Length_S34_->SetFillStyle( 3004 );
  h_Length_S34_->SetFillColor( 2 );
  h_Length_S34_->SetLineColor( 2 );
  TLine* line_Length_S34 = new TLine( Length_S34, 0, Length_S34, h_Length_S34_->GetMaximum() * 0.95 );
  line_Length_S34->SetLineColor( kBlue );
  line_Length_S34->SetLineWidth( 3. );
  line_Length_S34->Draw();
  // lhcb7TeVPrelimR->Draw();
  p_Length->cd( 2 );
  h_Length_S1_->Draw();
  TLine* line_Length_S1 = new TLine( Length_S1, 0, Length_S1, h_Length_S1_->GetMaximum() * 0.95 );
  line_Length_S1->SetLineColor( kBlue );
  line_Length_S1->SetLineWidth( 3. );
  line_Length_S1->Draw();
  // lhcb7TeVPrelimR->Draw();
  h_Length_S1_->SetFillStyle( 3004 );
  h_Length_S1_->SetFillColor( 2 );
  h_Length_S1_->SetLineColor( 2 );

  // c_Length->SaveAs("plots/"+outputname+"_length.eps");
  plotname = boost::filesystem::path( ( m_outputname + "_length.eps" ).Data() );
  c_Length->SaveAs( ( outpath / plotpath / plotname ).c_str() );

  TCanvas* c_Length_2D = new TCanvas( "c_Length_2D", "c_Length_2D", 1200, 600 );

  h_Length->Histogram()->SetMinimum( -Scale );
  h_Length->Histogram()->SetMaximum( +Scale );
  h_Length->Draw( "COLZ" );
  h_Length->Draw( "TEXT,same" );

  // c_Length_2D->SaveAs("plots/"+outputname+"_length_2D.eps");
  plotname = boost::filesystem::path( ( m_outputname + "_length_2D.eps" ).Data() );
  c_Length_2D->SaveAs( ( outpath / plotpath / plotname ).c_str() );
}
