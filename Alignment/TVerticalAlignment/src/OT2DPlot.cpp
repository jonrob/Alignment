/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "TVerticalAlignment/OT2DPlot.h"
#include "TArrow.h"
#include "TBox.h"
#include "TText.h"

class OTNames;
// ClassImp(OT2DPlot);

//-----------------------------------------------------------------------------
// Implementation file for class : OT2DPlot
//
// 2010-11-23 : Frederic Guillaume Dupertuis
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
OT2DPlot::OT2DPlot( const char* name, const char* title, const char* plotType ) : m_plotType( plotType ) {
  m_names = new OTNames();
  InitMaps();

  m_NBinX = 22;
  m_NBinY = 28;
  m_LowX  = -11.;
  m_UpX   = 11.;
  m_LowY  = -14.;
  m_UpY   = 14.;
  m_hmap  = new TH2D( name, title, m_NBinX, m_LowX, m_UpX, m_NBinY, m_LowY, m_UpY );
}
//=============================================================================

void OT2DPlot::Draw( Option_t* option ) {
  m_hmap->Draw( option );
  PlotLabels();
  PlotBoxes();
}

void OT2DPlot::Fill( string const& module, double const& value, bool const& qcombined ) {
  int uniqueModule;

  uniqueModule = m_names->GetUniqueModule( module );

  if ( uniqueModule < 0 ) {
    cout << "Module not found !" << endl;
    return;
  }
  if ( !m_XMap[uniqueModule] || !m_YMap[uniqueModule] ) {
    cout << "Module not found !" << endl;
    return;
  }

  if ( qcombined ) {
    m_hmap->Fill( m_XMap[uniqueModule], m_YMap[uniqueModule], value );
  } else {
    m_hmap->Fill( m_XMap[uniqueModule], m_YMap[uniqueModule], value / 2. );
  }
}

void OT2DPlot::PlotLabels() {
  m_hmap->GetXaxis()->SetTickLength( 0 );
  m_hmap->GetYaxis()->SetTickLength( 0 );
  m_hmap->GetXaxis()->SetLabelColor( kWhite );
  m_hmap->GetYaxis()->SetLabelColor( kWhite );

  TText* ot1 = new TText( -10.3, -8.5, "OT1" );
  ot1->Draw();

  TText* ot2 = new TText( -10.3, -0.5, "OT2" );
  ot2->Draw();

  TText* ot3 = new TText( -10.3, 7.5, "OT3" );
  ot3->Draw();

  TText* otX1 = new TText( 9.2, -1.8, "X1" );
  otX1->SetTextSize( 0.02 );
  otX1->Draw();

  TText* otU = new TText( 9.2, -0.8, "U" );
  otU->SetTextSize( 0.02 );
  otU->Draw();

  TText* otV = new TText( 9.2, 0.2, "V" );
  otV->SetTextSize( 0.02 );
  otV->Draw();

  TText* otX2 = new TText( 9.2, 1.2, "X2" );
  otX2->SetTextSize( 0.02 );
  otX2->Draw();

  TText* Aside = new TText( -8.7, -12.7, "A" );
  Aside->SetTextSize( 0.07 );
  Aside->Draw();

  TText* Cside = new TText( 8.3, -12.7, "C" );
  Cside->SetTextSize( 0.07 );
  Cside->Draw();

  TArrow* XArrow = new TArrow( 0., -12., -5.5, -12., 0.005, "|-|>" );
  // XArrow->SetLineColor();
  XArrow->Draw();

  TText* X = new TText( -5.9, -13., "X" );
  X->SetTextSize( 0.04 );
  X->Draw();

  TText* Q02 = new TText( 3.8, -4.5, "Q0+Q2" );
  Q02->SetTextSize( 0.05 );
  Q02->Draw();

  TText* Q13 = new TText( -5.2, -4.5, "Q1+Q3" );
  Q13->SetTextSize( 0.05 );
  Q13->Draw();

  TText* otA1 = new TText( 8.5, 2.2, "1" );
  otA1->SetTextSize( 0.02 );
  otA1->Draw();

  TText* otA2 = new TText( 7.5, 2.2, "2" );
  otA2->SetTextSize( 0.02 );
  otA2->Draw();

  TText* otA3 = new TText( 6.5, 2.2, "3" );
  otA3->SetTextSize( 0.02 );
  otA3->Draw();

  TText* otA4 = new TText( 5.5, 2.2, "4" );
  otA4->SetTextSize( 0.02 );
  otA4->Draw();

  TText* otA5 = new TText( 4.5, 2.2, "5" );
  otA5->SetTextSize( 0.02 );
  otA5->Draw();

  TText* otA6 = new TText( 3.5, 2.2, "6" );
  otA6->SetTextSize( 0.02 );
  otA6->Draw();

  TText* otA7 = new TText( 2.5, 2.2, "7" );
  otA7->SetTextSize( 0.02 );
  otA7->Draw();

  TText* otA8 = new TText( 1.5, 2.2, "8" );
  otA8->SetTextSize( 0.02 );
  otA8->Draw();

  TText* otA9 = new TText( 0.5, 2.2, "9" );
  otA9->SetTextSize( 0.02 );
  otA9->Draw();

  TText* otC1 = new TText( -8.5, 2.2, "1" );
  otC1->SetTextSize( 0.02 );
  otC1->Draw();

  TText* otC2 = new TText( -7.5, 2.2, "2" );
  otC2->SetTextSize( 0.02 );
  otC2->Draw();

  TText* otC3 = new TText( -6.5, 2.2, "3" );
  otC3->SetTextSize( 0.02 );
  otC3->Draw();

  TText* otC4 = new TText( -5.5, 2.2, "4" );
  otC4->SetTextSize( 0.02 );
  otC4->Draw();

  TText* otC5 = new TText( -4.5, 2.2, "5" );
  otC5->SetTextSize( 0.02 );
  otC5->Draw();

  TText* otC6 = new TText( -3.5, 2.2, "6" );
  otC6->SetTextSize( 0.02 );
  otC6->Draw();

  TText* otC7 = new TText( -2.5, 2.2, "7" );
  otC7->SetTextSize( 0.02 );
  otC7->Draw();

  TText* otC8 = new TText( -1.5, 2.2, "8" );
  otC8->SetTextSize( 0.02 );
  otC8->Draw();

  TText* otC9 = new TText( -0.5, 2.2, "9" );
  otC9->SetTextSize( 0.02 );
  otC9->Draw();
}

void OT2DPlot::PlotBoxes() {
  TBox* box = new TBox();
  box->SetFillColor( kWhite );
  box->SetFillStyle( 0 );
  box->SetLineStyle( 3 );
  box->SetLineColor( kBlack );
  box->SetLineWidth( box->GetLineWidth() / 10. );

  TBox* box2 = new TBox();
  box2->SetFillColor( kWhite );
  box2->SetFillStyle( 0 );
  box2->SetLineStyle( 1 );
  box2->SetLineColor( kBlack );
  box2->SetLineWidth( box->GetLineWidth() * 2. );

  TBox* boxempty = new TBox();
  boxempty->SetFillColor( 14 );
  boxempty->SetFillStyle( 3254 );
  boxempty->SetLineStyle( 3 );
  boxempty->SetLineColor( 14 );
  boxempty->SetLineWidth( boxempty->GetLineWidth() / 100. );

  OTNames::Vector*          Modules( m_names->GetModules() );
  OTNames::Vector*          Layers( m_names->GetLayers() );
  OTNames::Vector::iterator It, Begin, End;

  int uniqueModule( 0 ), uniqueLayer( 0 );

  if ( m_plotType == "Module" ) {
    Begin = Modules->begin();
    End   = Modules->end();

    for ( It = Begin; It != End; It++ ) {
      uniqueModule = m_names->GetUniqueModule( *It );

      box->DrawBox( m_XMap[uniqueModule] - 0.5, m_YMap[uniqueModule] - 0.5, m_XMap[uniqueModule] + 0.5,
                    m_YMap[uniqueModule] + 0.5 );

      if ( m_hmap->GetBinContent( m_hmap->GetXaxis()->FindBin( m_XMap[uniqueModule] ),
                                  m_hmap->GetYaxis()->FindBin( m_YMap[uniqueModule] ) ) == 0. ) {
        boxempty->DrawBox( m_XMap[uniqueModule] - 0.5, m_YMap[uniqueModule] - 0.5, m_XMap[uniqueModule] + 0.5,
                           m_YMap[uniqueModule] + 0.5 );
      }
    }
  } else if ( m_plotType == "Layer" ) {
    Begin = Layers->begin();
    End   = Layers->end();

    for ( It = Begin; It != End; It++ ) {
      uniqueLayer = m_names->GetUniqueLayer( *It );

      uniqueModule = uniqueLayer;
      uniqueModule += 5;

      box->DrawBox( m_XMap[uniqueModule] - 4.5, m_YMap[uniqueModule] - 0.5, m_XMap[uniqueModule] + 4.5,
                    m_YMap[uniqueModule] + 0.5 );
    }
  }

  box2->DrawBox( -9., -10., 9., -6. );
  box2->DrawBox( -9., -2., 9., 2. );
  box2->DrawBox( -9., 6., 9., 10. );
}

void OT2DPlot::InitMaps() {
  OTNames::Vector*          Modules( m_names->GetModules() );
  OTNames::Vector::iterator It, Begin( Modules->begin() ), End( Modules->end() );

  int uniqueModule;
  int uniqueModule_;

  for ( It = Begin; It != End; It++ ) {
    uniqueModule = m_names->GetUniqueModule( *It );

    if ( uniqueModule == -10000000 ) continue;

    uniqueModule_ = uniqueModule;

    double Otno( uniqueModule_ / 1000 );
    uniqueModule_ = uniqueModule_ - Otno * 1000;
    double Layerno( uniqueModule_ / 100 );
    uniqueModule_ = uniqueModule_ - Layerno * 100;
    double Quadrantno( uniqueModule_ / 10 );
    uniqueModule_ = uniqueModule_ - Quadrantno * 10;
    double Moduleno( uniqueModule_ / 1 );

    if ( Quadrantno == 0. || Quadrantno == 2. ) {
      m_XMap[uniqueModule] = 10. - Moduleno - 0.5;
      m_YMap[uniqueModule] = 8. * ( Otno - 1. ) + 3. + Layerno - 13. - 0.5;
    } else {
      m_XMap[uniqueModule] = -10. + Moduleno + 0.5;
      m_YMap[uniqueModule] = 8. * ( Otno - 1. ) + 3. + Layerno - 13. - 0.5;
    }
  }
}
