/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "TVerticalAlignment/ST2DPlot.h"
#include "TArrow.h"
#include "TBox.h"
#include "TText.h"

class STNames;
// ClassImp(ST2DPlot);

//-----------------------------------------------------------------------------
// Implementation file for class : ST2DPlot
//
// 2010-11-23 : Frederic Guillaume Dupertuis
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ST2DPlot::ST2DPlot( const char* detType, const char* name, const char* title, const char* plotType )
    : m_detType( detType ), m_plotType( plotType ) {
  if ( m_detType == "IT" ) {
    m_NBinX = 25;
    m_NBinY = 52;
    m_LowX  = -12.5;
    m_UpX   = 12.5;
    m_LowY  = -13.;
    m_UpY   = 13.;
  } else if ( m_detType == "TT" ) {
    m_NBinX = 43;
    m_NBinY = 40;
    m_LowX  = -21.5;
    m_UpX   = 21.5;
    m_LowY  = -20.;
    m_UpY   = 20.;
  }
  m_names = new STNames( m_detType );
  m_hmap  = new TH2D( name, title, m_NBinX, m_LowX, m_UpX, m_NBinY, m_LowY, m_UpY );
  InitMaps();
}
//=============================================================================
void ST2DPlot::Draw( Option_t* option ) {
  m_hmap->Draw( option );
  PlotLabels();
  PlotBoxes();
}
void ST2DPlot::Fill( string const& sector, double const& value ) {
  int uniqueSector;

  if ( m_plotType == "Sector" ) {
    uniqueSector = m_names->GetUniqueSector( sector );

    if ( m_detType == "IT" )
      m_hmap->Fill( m_XMap[uniqueSector], m_YMap[uniqueSector], value );
    else
      for ( int i( 0 ); i < m_NbSectMap[uniqueSector]; i++ )
        m_hmap->Fill( m_XMap[uniqueSector], m_YMap[uniqueSector] + i, value );
  } else {
    STNames::Vector*          Sectors( m_names->GetSectors() );
    STNames::Vector::iterator It, Begin( Sectors->begin() ), End( Sectors->end() );

    TString tmp;
    for ( It = Begin; It != End; It++ ) {
      tmp = *It;
      if ( tmp.Contains( sector.c_str() ) ) {
        uniqueSector = m_names->GetUniqueSector( tmp.Data() );

        if ( m_detType == "IT" )
          m_hmap->Fill( m_XMap[uniqueSector], m_YMap[uniqueSector], value );
        else
          for ( int i( 0 ); i < m_NbSectMap[uniqueSector]; i++ )
            m_hmap->Fill( m_XMap[uniqueSector], m_YMap[uniqueSector] + i, value );
      }
    }
  }
}

void ST2DPlot::PlotLabels() {
  m_hmap->GetXaxis()->SetTickLength( 0 );
  m_hmap->GetYaxis()->SetTickLength( 0 );
  m_hmap->GetXaxis()->SetLabelColor( kWhite );
  m_hmap->GetYaxis()->SetLabelColor( kWhite );

  if ( m_detType == "IT" ) {
    TText* it1 = new TText( -0.3, -8.5, "IT1" );
    it1->Draw();

    TText* it2 = new TText( -0.3, -0.5, "IT2" );
    it2->Draw();

    TText* it3 = new TText( -0.3, 7.5, "IT3" );
    it3->Draw();

    TText* itA = new TText( -11.5, -0.5, "A" );
    itA->SetTextSize( 0.07 );
    itA->Draw();

    TText* itC = new TText( 11.1, -0.5, "C" );
    itC->SetTextSize( 0.07 );
    itC->Draw();

    TText* itX1 = new TText( 10.7, -9., "X1" );
    itX1->SetTextSize( 0.02 );
    itX1->Draw();

    TText* itU = new TText( 10.7, -8.5, "U" );
    itU->SetTextSize( 0.02 );
    itU->Draw();

    TText* itV = new TText( 10.7, -8., "V" );
    itV->SetTextSize( 0.02 );
    itV->Draw();

    TText* itX2 = new TText( 10.7, -7.5, "X2" );
    itX2->SetTextSize( 0.02 );
    itX2->Draw();

    TText* itno1 = new TText( 10., 1.2, "1" );
    itno1->SetTextSize( 0.02 );
    itno1->Draw();

    TText* itno2 = new TText( 9., 1.2, "2" );
    itno2->SetTextSize( 0.02 );
    itno2->Draw();

    TText* itno3 = new TText( 8., 1.2, "3" );
    itno3->SetTextSize( 0.02 );
    itno3->Draw();

    TText* itno4 = new TText( 7., 1.2, "4" );
    itno4->SetTextSize( 0.02 );
    itno4->Draw();

    TText* itno5 = new TText( 6., 1.2, "5" );
    itno5->SetTextSize( 0.02 );
    itno5->Draw();

    TText* itno6 = new TText( 5., 1.2, "6" );
    itno6->SetTextSize( 0.02 );
    itno6->Draw();

    TText* itno7 = new TText( 4., 1.2, "7" );
    itno7->SetTextSize( 0.02 );
    itno7->Draw();

    TArrow* XArrow = new TArrow( 0., -12., -5.5, -12., 0.005, "|-|>" );
    XArrow->Draw();

    TText* X = new TText( -5.9, -12.5, "X" );
    X->SetTextSize( 0.04 );
    X->Draw();

  } else {
    TText* tta = new TText( -0.75, -9.8, "TTa" );
    tta->Draw();

    TText* ttb = new TText( -0.75, 8., "TTb" );
    ttb->Draw();

    TText* ttaX = new TText( -19.8, -9., "X" );
    ttaX->Draw();

    TText* ttaU = new TText( 19.3, -9., "U" );
    ttaU->Draw();

    TText* ttbV = new TText( -19.8, 8., "V" );
    ttbV->Draw();

    TText* ttbX = new TText( 19.3, 8., "X" );
    ttbX->Draw();

    TText* ttA = new TText( -20.8, -0.5, "A" );
    ttA->SetTextSize( 0.07 );
    ttA->Draw();

    TText* ttC = new TText( 19.8, -0.5, "C" );
    ttC->SetTextSize( 0.07 );
    ttC->Draw();

    TArrow* XArrow = new TArrow( 0., -18., -8.5, -18., 0.005, "|-|>" );
    XArrow->Draw();

    TText* X = new TText( -9.1, -19., "X" );
    X->SetTextSize( 0.04 );
    X->Draw();
  }
}

void ST2DPlot::PlotBoxes() {
  TBox* box = new TBox();
  box->SetFillColor( kWhite );
  box->SetFillStyle( 0 );
  box->SetLineStyle( 3 );
  box->SetLineColor( kBlack ); // 14
  box->SetLineWidth( box->GetLineWidth() / 10. );

  TBox* box2 = new TBox();
  box2->SetFillColor( kWhite );
  box2->SetFillStyle( 0 );
  box2->SetLineStyle( 1 );
  box2->SetLineColor( kBlack );
  box2->SetLineWidth( box->GetLineWidth() * 2. );

  TBox* boxempty = new TBox();
  boxempty->SetFillColor( 14 );
  boxempty->SetFillStyle( 3254 );
  boxempty->SetLineStyle( 3 );
  boxempty->SetLineColor( 14 );
  boxempty->SetLineWidth( boxempty->GetLineWidth() / 100. );

  STNames::Vector*          Sectors( m_names->GetSectors() );
  STNames::Vector*          Layers( m_names->GetLayers() );
  STNames::Vector*          Boxes( m_names->GetBoxes() );
  STNames::Vector::iterator It, Begin, End;

  int uniqueSector( 0 ), uniqueLayer( 0 ), uniqueBox( 0 );

  if ( m_plotType == "Sector" ) {
    Begin = Sectors->begin();
    End   = Sectors->end();

    for ( It = Begin; It != End; It++ ) {
      uniqueSector = m_names->GetUniqueSector( *It );

      if ( m_detType == "IT" )
        box->DrawBox( m_XMap[uniqueSector] - 0.5, m_YMap[uniqueSector] - 0.25, m_XMap[uniqueSector] + 0.5,
                      m_YMap[uniqueSector] + 0.25 );
      else
        box->DrawBox( m_XMap[uniqueSector] - 0.5, m_YMap[uniqueSector] - 0.5, m_XMap[uniqueSector] + 0.5,
                      m_YMap[uniqueSector] + 0.5 + m_NbSectMap[uniqueSector] - 1. );

      if ( m_hmap->GetBinContent( m_hmap->GetXaxis()->FindBin( m_XMap[uniqueSector] ),
                                  m_hmap->GetYaxis()->FindBin( m_YMap[uniqueSector] ) ) == 0. ) {
        if ( m_detType == "IT" )
          boxempty->DrawBox( m_XMap[uniqueSector] - 0.5, m_YMap[uniqueSector] - 0.25, m_XMap[uniqueSector] + 0.5,
                             m_YMap[uniqueSector] + 0.25 );
        else
          boxempty->DrawBox( m_XMap[uniqueSector] - 0.5, m_YMap[uniqueSector] - 0.5, m_XMap[uniqueSector] + 0.5,
                             m_YMap[uniqueSector] + 0.5 + m_NbSectMap[uniqueSector] - 1. );
      }
    }
  } else if ( m_plotType == "Layer" ) {
    Begin = Layers->begin();
    End   = Layers->end();

    for ( It = Begin; It != End; It++ ) {
      uniqueLayer = m_names->GetUniqueLayer( *It );

      if ( m_detType == "IT" ) {
        uniqueSector = uniqueLayer;
        uniqueSector += 4;
        box->DrawBox( m_XMap[uniqueSector] - 3.5, m_YMap[uniqueSector] - 0.25, m_XMap[uniqueSector] + 3.5,
                      m_YMap[uniqueSector] + 0.25 );
      }
    }
  }

  if ( m_detType == "IT" ) {
    Begin = Boxes->begin();
    End   = Boxes->end();

    for ( It = Begin; It != End; It++ ) {
      uniqueBox    = m_names->GetUniqueBox( *It );
      uniqueSector = uniqueBox;
      uniqueSector += 14;
      box2->DrawBox( m_XMap[uniqueSector] - 3.5, m_YMap[uniqueSector] - 0.25, m_XMap[uniqueSector] + 3.5,
                     m_YMap[uniqueSector] + 1.75 );
    }
  } else {
    Begin = Layers->begin();
    End   = Layers->end();

    for ( It = Begin; It != End; It++ ) {
      uniqueLayer = m_names->GetUniqueLayer( *It );

      double TTlayer( uniqueLayer / 1000 );
      double XOffSet, YOffSet;

      if ( TTlayer == 1 ) {
        XOffSet = -10.;
        YOffSet = -9.;
      } else if ( TTlayer == 2 ) {
        XOffSet = 10.;
        YOffSet = -9.;
      } else if ( TTlayer == 3 ) {
        XOffSet = -10.;
        YOffSet = 9.;
      } else {
        XOffSet = 10.;
        YOffSet = 9.;
      }

      if ( TTlayer < 2.5 )
        box2->DrawBox( XOffSet - 7.5, YOffSet - 7., XOffSet + 7.5, YOffSet + 7. );
      else
        box2->DrawBox( XOffSet - 8.5, YOffSet - 7., XOffSet + 8.5, YOffSet + 7. );
    }
  }
}

void ST2DPlot::InitMaps() {
  STNames::Vector*          Sectors( m_names->GetSectors() );
  STNames::Vector::iterator It, Begin( Sectors->begin() ), End( Sectors->end() );

  int uniqueSector;
  int uniqueSector_;

  for ( It = Begin; It != End; It++ ) {
    uniqueSector = m_names->GetUniqueSector( *It );

    if ( uniqueSector == -10000000 ) continue;

    uniqueSector_ = uniqueSector;

    if ( m_detType == "IT" ) {
      double Itno( uniqueSector_ / 1000 );
      uniqueSector_ = uniqueSector_ - Itno * 1000;
      double Boxno( uniqueSector_ / 100 );
      uniqueSector_ = uniqueSector_ - Boxno * 100;
      double Layerno( uniqueSector_ / 10 );
      uniqueSector_ = uniqueSector_ - Layerno * 10;
      double Sectorno( uniqueSector_ / 1 );

      if ( Boxno == 1 ) {
        m_XMap[uniqueSector] = -3. - Sectorno;
        m_YMap[uniqueSector] = ( 16. * ( Itno - 1. ) + 7. + Layerno ) / 2. - 13. + 0.25;
      } else if ( Boxno == 2 ) {
        m_XMap[uniqueSector] = 11. - Sectorno;
        m_YMap[uniqueSector] = ( 16. * ( Itno - 1. ) + 7. + Layerno ) / 2. - 13. + 0.25;
      } else if ( Boxno == 3 ) {
        m_XMap[uniqueSector] = 4. - Sectorno;
        m_YMap[uniqueSector] = ( 16. * ( Itno - 1. ) + 11. + Layerno ) / 2. - 13. + 0.25;
      } else {
        m_XMap[uniqueSector] = 4. - Sectorno;
        m_YMap[uniqueSector] = ( 16. * ( Itno - 1. ) + 3. + Layerno ) / 2. - 13. + 0.25;
      }
    } else if ( m_detType == "TT" ) {
      double TTlayer( uniqueSector_ / 1000 );
      uniqueSector_ = uniqueSector_ - TTlayer * 1000;
      double Region( uniqueSector_ / 100 );
      uniqueSector_ = uniqueSector_ - Region * 100;
      int Sectorno_1( uniqueSector_ / 1 - 1 );

      double XOffSet, YOffSet;

      if ( TTlayer == 1 ) {
        XOffSet = -10.;
        YOffSet = -9.;
      } else if ( TTlayer == 2 ) {
        XOffSet = 10.;
        YOffSet = -9.;
      } else if ( TTlayer == 3 ) {
        XOffSet = -10.;
        YOffSet = 9.;
      } else {
        XOffSet = 10.;
        YOffSet = 9.;
      }

      const int seq4[4] = {0, 4, 3, 3};
      const int seq6[6] = {0, 4, 2, 1, 1, 2};
      double    compute( 0. );

      if ( TTlayer < 2.5 ) {
        if ( Region == 3 ) {
          if ( Sectorno_1 % 4 == 0 || Sectorno_1 % 4 == 3 )
            m_NbSectMap[uniqueSector] = 4;
          else
            m_NbSectMap[uniqueSector] = 3;
          m_XMap[uniqueSector] = XOffSet + 7. - Sectorno_1 / 4;
          compute              = YOffSet - 7.;
          for ( int i( 0 ); i <= Sectorno_1 % 4; i++ ) compute += seq4[i];
          m_YMap[uniqueSector] = compute + 0.5;
        } else if ( Region == 1 ) {
          if ( Sectorno_1 % 4 == 0 || Sectorno_1 % 4 == 3 )
            m_NbSectMap[uniqueSector] = 4;
          else
            m_NbSectMap[uniqueSector] = 3;
          m_XMap[uniqueSector] = XOffSet - 2. - Sectorno_1 / 4;
          compute              = YOffSet - 7.;
          for ( int i( 0 ); i <= Sectorno_1 % 4; i++ ) compute += seq4[i];
          m_YMap[uniqueSector] = compute + 0.5;
        } else {
          if ( Sectorno_1 % 6 == 0 || Sectorno_1 % 6 == 5 )
            m_NbSectMap[uniqueSector] = 4;
          else if ( Sectorno_1 % 6 == 1 || Sectorno_1 % 6 == 4 )
            m_NbSectMap[uniqueSector] = 2;
          else
            m_NbSectMap[uniqueSector] = 1;
          m_XMap[uniqueSector] = XOffSet + 1. - Sectorno_1 / 6;
          compute              = YOffSet - 7.;
          for ( int i( 0 ); i <= Sectorno_1 % 6; i++ ) compute += seq6[i];
          m_YMap[uniqueSector] = compute + 0.5;
        }
      } else {
        if ( Region == 3 ) {
          if ( Sectorno_1 % 4 == 0 || Sectorno_1 % 4 == 3 )
            m_NbSectMap[uniqueSector] = 4;
          else
            m_NbSectMap[uniqueSector] = 3;
          m_XMap[uniqueSector] = XOffSet + 8. - Sectorno_1 / 4;
          compute              = YOffSet - 7.;
          for ( int i( 0 ); i <= Sectorno_1 % 4; i++ ) compute += seq4[i];
          m_YMap[uniqueSector] = compute + 0.5;
        } else if ( Region == 1 ) {
          if ( Sectorno_1 % 4 == 0 || Sectorno_1 % 4 == 3 )
            m_NbSectMap[uniqueSector] = 4;
          else
            m_NbSectMap[uniqueSector] = 3;
          m_XMap[uniqueSector] = XOffSet - 3. - Sectorno_1 / 4;
          compute              = YOffSet - 7.;
          for ( int i( 0 ); i <= Sectorno_1 % 4; i++ ) compute += seq4[i];
          m_YMap[uniqueSector] = compute + 0.5;
        } else {
          if ( Sectorno_1 < 4 ) {
            if ( Sectorno_1 % 4 == 0 || Sectorno_1 % 4 == 3 )
              m_NbSectMap[uniqueSector] = 4;
            else
              m_NbSectMap[uniqueSector] = 3;
            m_XMap[uniqueSector] = XOffSet + 2. - Sectorno_1 / 4;
            compute              = YOffSet - 7.;
            for ( int i( 0 ); i <= Sectorno_1 % 4; i++ ) compute += seq4[i];
            m_YMap[uniqueSector] = compute + 0.5;
          } else if ( Sectorno_1 > 21 ) {
            Sectorno_1 -= 22;
            if ( Sectorno_1 % 4 == 0 || Sectorno_1 % 4 == 3 )
              m_NbSectMap[uniqueSector] = 4;
            else
              m_NbSectMap[uniqueSector] = 3;
            m_XMap[uniqueSector] = XOffSet - 2. - Sectorno_1 / 4;
            compute              = YOffSet - 7.;
            for ( int i( 0 ); i <= Sectorno_1 % 4; i++ ) compute += seq4[i];
            m_YMap[uniqueSector] = compute + 0.5;
          } else {
            Sectorno_1 -= 4;
            if ( Sectorno_1 % 6 == 0 || Sectorno_1 % 6 == 5 )
              m_NbSectMap[uniqueSector] = 4;
            else if ( Sectorno_1 % 6 == 1 || Sectorno_1 % 6 == 4 )
              m_NbSectMap[uniqueSector] = 2;
            else
              m_NbSectMap[uniqueSector] = 1;
            m_XMap[uniqueSector] = XOffSet + 1. - Sectorno_1 / 6;
            compute              = YOffSet - 7.;
            for ( int i( 0 ); i <= Sectorno_1 % 6; i++ ) compute += seq6[i];
            m_YMap[uniqueSector] = compute + 0.5;
          }
        }
      }
    }
  }
}
