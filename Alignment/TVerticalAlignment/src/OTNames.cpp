/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local

#include "TVerticalAlignment/OTNames.h"

// ClassImp(OTNames);

//-----------------------------------------------------------------------------
// Implementation file for class : OTNames
//
// 2010-05-14 : Frederic Guillaume Dupertuis
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
OTNames::OTNames() : m_init( false ) { Init(); }
//=============================================================================

OTNames::Vector* OTNames::GetModules() { return &( m_Modules ); }

OTNames::Vector* OTNames::GetLayers() { return &( m_Layers ); }

OTNames::Vector* OTNames::GetStations() { return &( m_Stations ); }

OTNames::ModuleNames* OTNames::GetModuleNames( string const& module ) {
  if ( m_MapModuleNames.find( module ) != m_MapModuleNames.end() )
    return &( m_MapModuleNames[module] );
  else
    return 0;
}

std::string OTNames::GetRAWModuleName( std::string const& module ) {
  TString _module( "T" );

  if ( m_MapModuleNames.find( module ) != m_MapModuleNames.end() ) {
    _module += m_MapModuleNames[module].OTno;
    _module += "/";
    _module += m_MapModuleNames[module].Layer;
    _module += "/Q";
    _module += m_MapModuleNames[module].Quadrantno;
    _module += "/M";
    _module += m_MapModuleNames[module].Moduleno;

    return _module.Data();
  } else {
    return "";
  }
}

std::string OTNames::GetLayerName( std::string const& module ) {
  TString _module( module.c_str() );
  for ( OTNames::Vector::iterator It = m_Layers.begin(); It != m_Layers.end(); It++ ) {
    if ( _module.Contains( *It ) ) return m_MapLayerNames[*It].Nickname.Data();
  }
  return "";
}

std::string OTNames::GetStationName( std::string const& module ) {
  TString _module( module.c_str() );
  for ( OTNames::Vector::iterator It = m_Stations.begin(); It != m_Stations.end(); It++ ) {
    if ( _module.Contains( *It ) ) return m_MapStationNames[*It].Nickname.Data();
  }
  return "";
}

std::string OTNames::GetGlobalName() { return "OT"; }

int OTNames::GetUniqueModule( std::string const& module ) {
  if ( m_MapModuleNames.find( module ) != m_MapModuleNames.end() )
    return m_MapModuleNames[module].UniqueModule;
  else
    return -1;
}

int OTNames::GetUniqueLayer( std::string const& layer ) {
  if ( m_MapLayerNames.find( layer ) != m_MapLayerNames.end() )
    return m_MapLayerNames[layer].UniqueLayer;
  else
    return -1;
}

int OTNames::GetUniqueStation( std::string const& station ) {
  if ( m_MapStationNames.find( station ) != m_MapStationNames.end() )
    return m_MapStationNames[station].UniqueStation;
  else
    return -1;
}

void OTNames::Init() {
  if ( !m_init ) {
    for ( int i( 1 ); i < 4; i++ ) {
      for ( int j( 1 ); j < 10; j++ ) {
        for ( int k( 0 ); k < 4; k++ ) {
          InitOTMapNames( i, j, k, "X1" );
          InitOTMapNames( i, j, k, "U" );
          InitOTMapNames( i, j, k, "V" );
          InitOTMapNames( i, j, k, "X2" );
        }
      }
    }
    m_init = true;
  }
}

void OTNames::InitOTMapNames( int const& OTno, int const& Moduleno, int const& Quadrantno, const char* Layer ) {
  TString station( "" );
  TString layer( "" );
  TString module( "" );

  station = "OT";
  station += OTno;
  layer = station;
  layer += Layer;
  module = layer;
  module += "Q";
  module += Quadrantno;
  module += "M";
  module += Moduleno;

  std::string _station( station.Data() );
  std::string _layer( layer.Data() );
  std::string _module( module.Data() );

  m_MapModuleNames[_module].Nickname   = module;
  m_MapModuleNames[_module].OTno       = OTno;
  m_MapModuleNames[_module].Layer      = Layer;
  m_MapModuleNames[_module].Quadrantno = Quadrantno;
  m_MapModuleNames[_module].Moduleno   = Moduleno;
  m_Modules.push_back( _module );

  int uniqueModule( 0 );

  if ( OTno == 1 ) {
    uniqueModule += 1000;
  } else if ( OTno == 2 ) {
    uniqueModule += 2000;
  } else if ( OTno == 3 ) {
    uniqueModule += 3000;
  }

  if ( m_MapStationNames.find( _station ) == m_MapStationNames.end() ) {
    m_MapStationNames[_station].Nickname      = station;
    m_MapStationNames[_station].OTno          = OTno;
    m_MapStationNames[_station].UniqueStation = uniqueModule;
    m_Stations.push_back( _station );
  }
  /*
    if(Layer == "X1"){
      uniqueModule += 100;
    }else if(Layer == "U"){
      uniqueModule += 200;
    }else if(Layer == "V"){
      uniqueModule += 300;
    }else if(Layer == "X2"){
      uniqueModule += 400;
    }
  */
  if ( strcmp( Layer, "X1" ) == 0 ) {
    uniqueModule += 100;
  } else if ( strcmp( Layer, "U" ) == 0 ) {
    uniqueModule += 200;
  } else if ( strcmp( Layer, "V" ) == 0 ) {
    uniqueModule += 300;
  } else if ( strcmp( Layer, "X2" ) == 0 ) {
    uniqueModule += 400;
  }

  if ( m_MapLayerNames.find( _layer ) == m_MapLayerNames.end() ) {
    m_MapLayerNames[_layer].Nickname    = layer;
    m_MapLayerNames[_layer].OTno        = OTno;
    m_MapLayerNames[_layer].Layer       = Layer;
    m_MapLayerNames[_layer].UniqueLayer = uniqueModule;
    m_Layers.push_back( _layer );
  }

  uniqueModule += Quadrantno * 10;

  uniqueModule += Moduleno;

  m_MapModuleNames[_module].UniqueModule = uniqueModule;
}
