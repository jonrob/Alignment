/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _STRUCTS_H_
#define _STRUCTS_H_
#include "TROOT.h"
#include "TString.h"
#include "TStyle.h"
#include "TSystem.h"
#include <iostream>

#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooFit.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "TH1D.h"
#include <cmath>
#include <iomanip>
#include <iostream>
#include <string>

using namespace std;
using namespace RooFit;

struct XYZ_Pos {
  TString  Name;
  Double_t X;
  Double_t Y;
  Double_t Z;
};

struct Param_Result {
  TString  Name;
  Double_t Value;
  Double_t Error;
};

struct Gauss_Fit_Result {
  TString  Name;
  Double_t Mean;
  Double_t Mean_Error;
  Double_t Sigma;
  Double_t Sigma_Error;

  friend std::ostream& operator<<( std::ostream& os, Gauss_Fit_Result const& Result ) {
    Int_t tmp;
    os << setiosflags( ios::fixed );
    tmp = int( floor( log10( Result.Mean_Error ) ) );
    if ( tmp < 0 )
      os << setprecision( abs( tmp ) + 1 );
    else
      os << setprecision( 0 );
    os << Result.Name << " :  Mean = " << Result.Mean << " +- " << Result.Mean_Error << " (";
    os << setprecision( 1 ) << Result.Mean / Result.Mean_Error << ")";
    tmp = int( floor( log10( Result.Sigma_Error ) ) );
    if ( tmp < 0 )
      os << setprecision( abs( tmp ) + 1 );
    else
      os << setprecision( 0 );
    os << " | Sigma = " << Result.Sigma << " +- " << Result.Sigma_Error << " (";
    os << setprecision( 1 ) << Result.Sigma / Result.Sigma_Error << ")";
    os << setprecision( 3 );

    return os;
  }
};

inline Gauss_Fit_Result* fit_Gauss( TH1D* histo, RooGaussian* gauss, TString const& ModuleName, Double_t const& Offset,
                                    Double_t const& Scale ) {
  static_cast<RooRealVar*>( gauss->getVariables()->find( "y" ) )->setRange( Offset - Scale, Offset + Scale );
  static_cast<RooRealVar*>( gauss->getVariables()->find( "y" ) )
      ->setRange( "fitrange", Offset - Scale, Offset + Scale );

  static_cast<RooRealVar*>( gauss->getVariables()->find( "mean" ) )
      ->setRange( Offset - 0.9 * Scale, Offset + 0.9 * Scale );
  static_cast<RooRealVar*>( gauss->getVariables()->find( "mean" ) )->setVal( histo->GetMean() );
  static_cast<RooRealVar*>( gauss->getVariables()->find( "sigma" ) )->setVal( histo->GetRMS() );

  RooDataHist* _histo =
      new RooDataHist( "_histo", "_histo", *static_cast<RooRealVar*>( gauss->getVariables()->find( "y" ) ), histo );

  gauss->fitTo( *_histo, Range( "fitrange" ), PrintLevel( -1 ), PrintEvalErrors( -1 ) );

  Gauss_Fit_Result* Result = new Gauss_Fit_Result();

  Result->Mean        = static_cast<RooRealVar*>( gauss->getVariables()->find( "mean" ) )->getVal();
  Result->Mean_Error  = static_cast<RooRealVar*>( gauss->getVariables()->find( "mean" ) )->getError();
  Result->Sigma       = static_cast<RooRealVar*>( gauss->getVariables()->find( "sigma" ) )->getVal();
  Result->Sigma_Error = static_cast<RooRealVar*>( gauss->getVariables()->find( "sigma" ) )->getError();
  Result->Name        = ModuleName;

  // delete gauss;
  // delete _histo;
  gauss->IsA()->Destructor( gauss );
  _histo->IsA()->Destructor( _histo );
  //  gDirectory->GetList()->Dump();
  return Result;
}

inline Gauss_Fit_Result* fit_Gauss( TH1D* histo, RooGaussian* gauss, TString const& ModuleName,
                                    Gauss_Fit_Result* Offset_Result, Double_t const& Offset, Double_t const& Scale ) {
  static_cast<RooRealVar*>( gauss->getVariables()->find( "y" ) )->setRange( Offset - Scale, Offset + Scale );
  static_cast<RooRealVar*>( gauss->getVariables()->find( "y" ) )
      ->setRange( "fitrange", Offset - Scale, Offset + Scale );

  static_cast<RooRealVar*>( gauss->getVariables()->find( "mean" ) )
      ->setRange( Offset - 0.9 * Scale, Offset + 0.9 * Scale );
  static_cast<RooRealVar*>( gauss->getVariables()->find( "mean" ) )->setVal( histo->GetMean() );
  static_cast<RooRealVar*>( gauss->getVariables()->find( "sigma" ) )->setVal( histo->GetRMS() );

  RooDataHist* _histo =
      new RooDataHist( "_histo", "_histo", *static_cast<RooRealVar*>( gauss->getVariables()->find( "y" ) ), histo );

  gauss->fitTo( *_histo, Range( "fitrange" ), PrintLevel( -1 ), PrintEvalErrors( -1 ) );

  Gauss_Fit_Result* Result = new Gauss_Fit_Result();

  Result->Mean = static_cast<RooRealVar*>( gauss->getVariables()->find( "mean" ) )->getVal() - Offset_Result->Mean;
  Result->Mean_Error = static_cast<RooRealVar*>( gauss->getVariables()->find( "mean" ) )->getError();
  Result->Mean_Error =
      sqrt( Result->Mean_Error * Result->Mean_Error + Offset_Result->Mean_Error * Offset_Result->Mean_Error );
  Result->Sigma       = static_cast<RooRealVar*>( gauss->getVariables()->find( "sigma" ) )->getVal();
  Result->Sigma_Error = static_cast<RooRealVar*>( gauss->getVariables()->find( "sigma" ) )->getError();
  Result->Name        = ModuleName;

  // delete gauss;
  // delete _histo;
  gauss->IsA()->Destructor( gauss );
  _histo->IsA()->Destructor( _histo );
  //  gDirectory->GetList()->Dump();
  return Result;
}

#endif
