/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ALIGNMENT_TVERTICALALIGNMENT_TVERTICALALIGNMENT_H
#define ALIGNMENT_TVERTICALALIGNMENT_TVERTICALALIGNMENT_H

// USER
// STL
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <stdio.h>
#include <string>
#include <utility>
#include <vector>
// boost
#include <boost/variant/get.hpp>
#include <boost/variant/variant.hpp>

using namespace std;

namespace Alignment {
  namespace TVerticalAlignment {

    class OTParam {
    private:
      std::pair<std::string, std::string> m_directory;
      std::map<std::string, double>       m_distance;
      std::map<std::string, double>       m_length;
      std::map<std::string, double>       m_edge;
      std::map<std::string, double>       m_step;
      double                              m_tolerance;
      bool                                m_mc;
      double                              m_sigmaconstraint;

    public:
      OTParam() {}
      OTParam( const std::pair<std::string, std::string>& directory, const std::map<std::string, double>& distance,
               const std::map<std::string, double>& length, const std::map<std::string, double>& edge,
               const std::map<std::string, double>& step, double tolerance, bool mc, double sigmaconstraint )
          : m_directory( directory )
          , m_distance( distance )
          , m_length( length )
          , m_edge( edge )
          , m_step( step )
          , m_tolerance( tolerance )
          , m_mc( mc )
          , m_sigmaconstraint( sigmaconstraint ) {}

      const std::pair<std::string, std::string>& directory() const noexcept { return m_directory; }
      const std::map<std::string, double>&       distance() const noexcept { return m_distance; }
      const std::map<std::string, double>&       length() const noexcept { return m_length; }
      const std::map<std::string, double>&       edge() const noexcept { return m_edge; }
      const std::map<std::string, double>&       step() const noexcept { return m_step; }
      double                                     tolerance() const noexcept { return m_tolerance; }
      bool                                       mc() const noexcept { return m_mc; }
      double                                     sigmaconstraint() const noexcept { return m_sigmaconstraint; }

      void setDirectory( const std::pair<std::string, std::string>& directory ) { m_directory = directory; }
      void setDistance( const std::map<std::string, double>& distance ) { m_distance = distance; }
      void setLength( const std::map<std::string, double>& length ) { m_length = length; }
      void setEdge( const std::map<std::string, double>& edge ) { m_edge = edge; }
      void setStep( const std::map<std::string, double>& step ) { m_step = step; }
      void setTolerance( double tolerance ) noexcept { m_tolerance = tolerance; }
      void setMC( bool mc ) noexcept { m_mc = mc; }
      void setGaussConstraint( double sigmaconstraint ) noexcept { m_sigmaconstraint = sigmaconstraint; }
    };
    class STParam {
    private:
      std::pair<std::string, std::string> m_directory;
      double                              m_scale;
      double                              m_tolerance;
      double                              m_sigmaconstraint;

    public:
      STParam() {}
      STParam( const std::pair<std::string, std::string>& directory, double scale, double tolerance,
               double sigmaconstraint )
          : m_directory( directory )
          , m_scale( scale )
          , m_tolerance( tolerance )
          , m_sigmaconstraint( sigmaconstraint ) {}

      const std::pair<std::string, std::string>& directory() const noexcept { return m_directory; }
      double                                     scale() const noexcept { return m_scale; }
      double                                     tolerance() const noexcept { return m_tolerance; }
      double                                     sigmaconstraint() const noexcept { return m_sigmaconstraint; }

      void setDirectory( const std::pair<std::string, std::string>& directory ) { m_directory = directory; }
      void setScale( double scale ) noexcept { m_scale = scale; }
      void setTolerance( double tolerance ) noexcept { m_tolerance = tolerance; }
      void setGaussConstraint( double sigmaconstraint ) noexcept { m_sigmaconstraint = sigmaconstraint; }
    };

    typedef boost::variant<OTParam, STParam> Param;

    class TVerticalAlignment {
    public:
      // Constructors
      TVerticalAlignment();
      // Destructor
      ~TVerticalAlignment() {}

      // Methods
      void  initParameter( std::string detector );
      Param getParameter( std::string detector );

    private:
      std::map<std::string, Param> m_param;
    };

  } // namespace TVerticalAlignment
} // namespace Alignment

#endif
