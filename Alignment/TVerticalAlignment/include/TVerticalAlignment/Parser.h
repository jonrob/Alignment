/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#ifndef PARSER_H
#define PARSER_H

#include "Structs.h"
#include "TString.h"
#include <map>
#include <string>

using namespace std;

/** @class Parser Parser.h macros/Parser.h
 *   *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2011-07-12
 */

class Parser {
public:
  typedef std::map<std::string, Param_Result*> Param_Result_Map;
  typedef std::map<std::string, XYZ_Pos*>      XYZ_Pos_Map;

  /// Standard constructor
  Parser(){};
  virtual ~Parser(){}; ///< Destructor

  Parser::Param_Result_Map* CreateParamResultMapFromFitFile( TString const& filename );
  Parser::XYZ_Pos_Map*      CreateXYZPosMapFromFile( TString const& filename );

protected:
private:
  // ClassDef(Parser,1);
};

#endif
