/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ALIGNMENT_ITYALIGNMENT_ITYALIGNMENT_H
#define ALIGNMENT_ITYALIGNMENT_ITYALIGNMENT_H

// STL
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <stdio.h>
#include <string>
#include <utility>
#include <vector>
// ROOT
#include "TCanvas.h"
#include "TColor.h"
#include "TCut.h"
#include "TF1.h"
#include "TFile.h"
#include "TFitResult.h"
#include "TGraphErrors.h"
#include "TH1I.h"
#include "TH2I.h"
#include "THStack.h"
#include "TKey.h"
#include "TLegend.h"
#include "TMath.h"
#include "TPad.h"
#include "TPaveStats.h"
#include "TProfile.h"
#include "TROOT.h"
#include "TString.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TTree.h"
// RooFit
#include "RooAbsBinning.h"
#include "RooAbsDataStore.h"
#include "RooAddPdf.h"
#include "RooArgSet.h"
#include "RooBinning.h"
#include "RooCategory.h"
#include "RooChebychev.h"
#include "RooConstVar.h"
#include "RooCurve.h"
#include "RooCustomizer.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooDecay.h"
#include "RooEffProd.h"
#include "RooEfficiency.h"
#include "RooExponential.h"
#include "RooFitResult.h"
#include "RooGaussModel.h"
#include "RooGaussian.h"
#include "RooGenericPdf.h"
#include "RooHist.h"
#include "RooHistPdf.h"
#include "RooPlot.h"
#include "RooProdPdf.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooStats/SPlot.h"
#include "RooTrace.h"
#include "RooTruthModel.h"
#include "RooUnblindUniform.h"
#include "TAxis.h"
#include "TCanvas.h"

#include <boost/variant/get.hpp>
#include <boost/variant/variant.hpp>

#include "TVerticalAlignment/Parser.h"
#include "TVerticalAlignment/ST2DPlot.h"
#include "TVerticalAlignment/TVerticalAlignment.h"

using namespace std;
using namespace RooFit;

namespace Alignment {
  namespace TVerticalAlignment {

    class ITYAlignMagOff {
    public:
      // Constructors
      ITYAlignMagOff( TString filename, TString dbfilename, TString txtfiledir, bool constraint = false,
                      bool saveplots = false );
      // Destructor
      ~ITYAlignMagOff() {}

      // Methods
      void fit_efficiency();
      void plots();
      void glimpse_data();
      void debug_verbose();

    private:
      TVerticalAlignment* m_va;

      Param   m_param;
      TString m_filename;
      TString m_dbfilename;
      TString m_outDirectory;
      bool    m_constraint;
      bool    m_saveplots;

      bool m_glimpse = false;
      bool m_verbose = false;

      STNames* m_Names;
      ofstream m_YPosFitFile;
      ofstream m_LengthFitFile;
      TString  m_outputname;

      void        FitfromROOTFile();
      void        GetMeanFromHisto( TString SectorName );
      RooDataSet* GetRooDataSetFromTH1( TH1F* histo, TH1F* histo_exp, RooRealVar y, RooRealVar weight,
                                        double y_left_min, double y_left_max, double y_right_min, double y_right_max,
                                        RooCategory cal_lr, RooCategory cat_eff );
    };

  } // namespace TVerticalAlignment
} // namespace Alignment

#endif
