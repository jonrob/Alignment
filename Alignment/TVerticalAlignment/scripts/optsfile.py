###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-->always on top
from Gaudi.Configuration import *

#-->If possible, use options provided by Configurables. Or improve Configurables.

#-->CondDB: Conditions DataBase
from Configurables import CondDBAccessSvc, CondDB, UpdateManagerSvc
CondDB(
).UseOracle = False  #Boolean flag to enable the usage of the CondDB from Oracle servers
CondDB().UseLatestTags = ['2015']
#CondDB().IgnoreHeartBeat= True
#Implement local alignment files
localDb = CondDBAccessSvc(
    "myCondLocal",
    ConnectionString=
    "sqlite_file:/afs/cern.ch/user/s/siborghi/public/Alignment2015.db/LHCBCOND",
    DefaultTAG="Velov5_Trackerv8",
)
CondDB().addLayer(localDb)

# Initial IOV time
# http://www.onlineconversion.com/unix_time.htm
# values in ns (so multiply values from above link by 1e9)
#import time
#Date = '2011-06-01 00:00:00'
#Date = '2015-06-06 09:53:44'
#Time = time.mktime(time.strptime(Date, "%Y-%m-%d %H:%M:%S"))
#from Configurables import EventClockSvc
#EventClockSvc( InitialTime = int(Time*1e9) )

#-->Brunel $BRUNELROOT/python/Brunel/Configuration.py
from Configurables import Brunel
Brunel().DataType = "2015"
Brunel().EvtMax = -1
Brunel().PrintFreq = 1000
Brunel().CondDBtag = "cond-20150601"
Brunel().DDDBtag = "dddb-20150526"
Brunel().SpecialData += ["fieldOff"]
Brunel().DatasetName = 'Reco_Collision2015_MagOff_DATA_Velov5_Trackerv8'
#Brunel().Detectors = ['Velo','TT','IT','OT','Tr','Magnet']
Brunel().OutputType = 'None'
#Brunel().Simulation = True
#Brunel().OutputLevel = DEBUG
#Brunel().OnlineMode = True
Brunel().VetoHltErrorEvents = False

#-->TrackSys $TRACKSYSROOT/pythonTrackSys/Configuration.py
from Configurables import (TrackSys)
TrackSys().TrackPatRecAlgorithms = [
    'FastVelo'
]  # List of track pattern recognition algorithms to run


#-->Configurables Framework provides a solution to run some python code after *all* Configurables have been used
def doMyChanges():
    #from Configurables import  CondDBAccessSvc
    #CondDBAccessSvc("ONLINE_2015",ConnectionString="sqlite_file:/afs/cern.ch/user/m/mtobin/w0/ONLINE_fake.db/ONLINE")

    from Configurables import LoKi__HDRFilter
    filterCode = "HLT_PASS_RE('Hlt1(?!ErrorEvent).*Decision')"
    hltErrorFilter = LoKi__HDRFilter('HltErrorFilter')
    hltErrorFilter.Code = filterCode

    for name in ['Vertex', 'CALO', 'RICH', 'PROTO', 'MUON']:
        GaudiSequencer('Reco' + name + 'Seq').Members = []
    from Configurables import TrackContainerCleaner, TrackSelector  #, TrackVeloSelector, TrackVeloIsolated

    GaudiSequencer('RecV0MakeV0Seq').Members = []

    cleaner = TrackContainerCleaner('KeepTrack')
    cleaner.inputLocation = "Rec/Track/Best"
    cleaner.addTool(TrackSelector, name='Selector')
    cleaner.Selector.TrackTypes = ["Velo", "VeloBackward"]

    cleaner2 = TrackContainerCleaner('KeepGoodTrack')
    cleaner2.inputLocation = "Rec/Track/Best"
    cleaner2.addTool(TrackSelector, name='Selector')
    cleaner2.Selector.MaxChi2PerDoFVelo = 3.

    from Configurables import TrackMasterFitter
    from TrackFitter.ConfiguredFitters import *

    eventfitter = ConfiguredEventFitter(
        "MyTrackFitter", TracksInContainer="Rec/Track/Best", FieldOff=True)

    yalignSeq = GaudiSequencer('YAlignSeq')
    yalignSeq.Members += [cleaner, eventfitter, cleaner2]
    from Configurables import TrackHitAdder
    hitadder = TrackHitAdder('TrackHitAdder')
    hitadder.MaxDistVelo = -1.
    hitadder.MaxDistOT = 20.
    hitadder.MaxDistIT = 4.
    hitadder.MaxDistTT = 4.
    hitadder.ForceIT = True
    hitadder.ForceTT = True
    hitadder.ForceOT = True
    hitadder.MaxTolY = 250.

    from Configurables import STYAlignMagOff, OTYAlignMagOff
    itYAlign = STYAlignMagOff("ITYAlign")
    itYAlign.XTol = 4.
    itYAlign.YTol = 50.
    itYAlign.Expected = True
    itYAlign.Extrapolator = "TrackLinearExtrapolator"

    ttYAlign = STYAlignMagOff("TTYAlign")
    ttYAlign.DetType = "TT"
    ttYAlign.XTol = 4.
    ttYAlign.YTol = 50.
    ttYAlign.Expected = True
    ttYAlign.Extrapolator = "TrackLinearExtrapolator"

    otYAlign = OTYAlignMagOff("OTYAlign")
    otYAlign.XTol = 10.
    otYAlign.YTol = 250.
    otYAlign.Expected = True
    otYAlign.Extrapolator = "TrackLinearExtrapolator"

    yalignSeq.Members += [hitadder, itYAlign, ttYAlign, otYAlign]
    GaudiSequencer('RecoTrHLT2Seq').Members += [yalignSeq]


appendPostConfigAction(doMyChanges)
