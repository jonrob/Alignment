/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
// Implementation file for class : SolvExample
//
// 2007-03-07 : Adlene Hicheur
//-----------------------------------------------------------------------------

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

// Solver
#include "AlignmentInterfaces/IAlignSolvTool.h"

class IAlignSolvTool;

class SolvExample : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode execute() override; ///< Algorithm execution
private:
  ToolHandle<IAlignSolvTool> m_solver{this, "SolvMethod", "SpmInvTool"};
};

// Matrix, vector classes
#include "AlignKernel/AlMat.h"
#include "AlignKernel/AlSymMat.h"
#include "AlignKernel/AlVec.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SolvExample )

//=============================================================================
// Main execution
//=============================================================================
StatusCode SolvExample::execute() {

  debug() << "==> Execute" << endmsg;

  // First: examples of vector and matrix handling....

  int   Nvec = 8;
  AlVec b( Nvec );
  AlVec c( Nvec );

  for ( unsigned i = 0; i < b.size(); ++i ) {
    b[i] = i + 2;
    c[i] = i * 3;
    debug() << "AlVec b value, line " << i << " - " << b[i] << endmsg;
    debug() << "AlVec c value, line " << i << " - " << c[i] << endmsg;
  }
  b -= c;

  debug() << "scalar product c*d..." << b.dot( c ) << endmsg;

  AlSymMat md( Nvec );
  AlSymMat ma( Nvec );

  for ( unsigned i = 0; i < md.size(); ++i ) {
    debug() << "AlSymMat md values line " << i << " -- ";
    for ( unsigned j = 0; j <= i; ++j ) {
      md( i, j ) = i - j;
      if ( i == j ) ma( i, j ) = 3;
      debug() << md( i, j ) << " - ";
    }
    debug() << "finish line" << endmsg;
  }

  AlSymMat mdT;
  debug() << "before md-ma" << endmsg;
  md += ma;
  mdT = ( md + ma );
  debug() << "AlSymMat md+ma Size " << mdT.size() << endmsg;

  for ( unsigned i = 0; i < mdT.size(); ++i ) {
    debug() << "AlSymMat mdT values line " << i << " -- ";
    for ( unsigned j = 0; j < mdT.size(); ++j ) { debug() << mdT( i, j ) << " - "; }
    debug() << endmsg;
  }

  AlVec e = md * c;
  debug() << "md*c Nelem..." << e.size() << endmsg;
  debug() << "md*c values..." << endmsg;
  for ( unsigned i = 0; i < e.size(); ++i ) debug() << e[i] << " - ";
  debug() << endmsg;

  AlMat me = md * md;
  AlMat mk = ma * me;
  AlMat mj = me * ma;

  for ( auto i = 0; i < me.rows(); i++ ) {
    debug() << "AlMat me values line " << i << " -- ";
    for ( auto j = 0; j < me.cols(); j++ ) { debug() << me( i, j ) << " - "; }
    debug() << endmsg;
  }

  for ( auto i = 0; i < mk.rows(); i++ ) {
    debug() << "AlSymMat*AlMat mk=ma*me values line " << i << " -- ";
    for ( auto j = 0; j < mk.cols(); j++ ) { debug() << mk( i, j ) << " - "; }
    debug() << endmsg;
  }

  for ( auto i = 0; i < mj.rows(); i++ ) {
    debug() << "AlMat*AlSymMat mj=me*ma values line " << i << " -- ";
    for ( auto j = 0; j < mj.cols(); j++ ) { debug() << mj( i, j ) << " - "; }
    debug() << endmsg;
  }

  me *= 5.;
  AlSymMat mf( me.rows() );

  for ( auto i = 0; i < me.rows(); i++ ) {
    debug() << "AlMat me*5 values line " << i << " -- ";
    for ( auto j = 0; j < me.cols(); j++ ) {
      debug() << me( i, j ) << " - ";
      mf( i, j ) = me( i, j );
    }
    debug() << endmsg;
  }

  // Now examples of invertion and diagonalization

  AlSymMat mfi( mf );
  AlSymMat msp( mf );

  int ierr = 0;

  // mf.invert(ierr);

  if ( ierr == 0 ) {

    debug() << "AlSymMat me^-1 values line " << endmsg;

    for ( unsigned i = 0; i < mf.size(); ++i ) {
      debug() << "AlSymMat mf values line " << i << " -- ";
      for ( unsigned j = 0; j < mf.size(); ++j ) { debug() << mf( i, j ) << " - "; }
      debug() << endmsg;
    }

    debug() << "AlSymMat me*me^-1 values line " << endmsg;

    for ( auto i = 0; i < ( mf * mfi ).rows(); i++ ) {
      debug() << "AlSymMat me*me^-1 values line " << i << " -- ";
      for ( auto j = 0; j < ( mf * mfi ).cols(); j++ ) { debug() << ( mf * mfi )( i, j ) << " - "; }
      debug() << endmsg;
    }
  }

  debug() << "Now using indirect inversion via diagonalization..." << endmsg;

  // declare transition matrix + vector to store eigenvalues
  const size_t dim = mf.size();

  AlMat z( dim, dim );
  AlVec w( dim );

  AlSymMat mfi_bis( mfi );

  const auto info = mfi.diagonalize_GSL( w, z );

  if ( info == 0 ) {
    debug() << "*** successful diagonalization ***" << endmsg;
    AlSymMat invmat( dim );
    debug() << "print eigenvalues: " << endmsg;
    for ( unsigned i = 0; i < invmat.size(); ++i ) {
      debug() << w[i] << "  " << endmsg;
      for ( unsigned j = 0; j <= i; ++j ) {
        for ( size_t k = 0; k < dim; k++ ) invmat( i, j ) = invmat( i, j ) + ( z( k, i ) * z( k, j ) / w[k] );
      }
    }

    debug() << "end print eigenvalues: " << endmsg;

    for ( unsigned i = 0; i < invmat.size(); ++i ) {
      //    debug()<<"AlSymMat me*me^-1 values line "<<i<<" -- ";
      for ( unsigned j = 0; j < invmat.size(); ++j ) { debug() << ( me * invmat )( i, j ) << "   "; }
      debug() << endmsg;
    }
  }

  // Calling the solver

  m_solver->compute( mf, e ).ignore();
  debug() << "Printing solution of mf*X = e..." << endmsg;
  for ( unsigned i = 0; i < e.size(); ++i ) debug() << e[i] << " - ";
  debug() << endmsg;

  return StatusCode::SUCCESS;
}
