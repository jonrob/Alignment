/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: SolvExample.h,v 1.5 2009-08-16 14:16:24 wouter Exp $
#ifndef SOLVEXAMPLE_H
#define SOLVEXAMPLE_H 1

/** @class SolvExample SolvExample.h
 *
 *
 *  @author Adlene Hicheur
 *  @date   2007-03-07
 */

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// Solver
#include "AlignmentInterfaces/IAlignSolvTool.h"

class IAlignSolvTool;

class SolvExample : public GaudiAlgorithm {
public:
  /// Standard constructor
  SolvExample( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~SolvExample(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
private:
  IAlignSolvTool* m_solver;       ///< Interface to solve tool
  std::string     m_solvtoolname; ///< Solver Name
};
#endif // SOLVEXAMPLE_H
