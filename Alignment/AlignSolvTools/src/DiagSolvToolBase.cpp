/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include <iomanip>
#include <iostream>
#include <set>

// local
#include "DiagSolvToolBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DiagSolvTool
//
// 2007-06 : Adlene Hicheur, Wouter Hulsbergen
//
/*

The alignment needs a solution to the equation

  A x = b

where the vector b is one half times the first derivative of the total chi2 to the alignment parameters and the (pos-def
symmetric) matrix A one half times the second derivative. We sometimes call b the ‘big vector’ and ‘A’ the big matrix.

For the eigenvalue analysis (e.g. to put a threshold on the minimum eigenvalue), we perform SVD factorisation  of the
matrix A using either Eigen or GSL: These libraries return a vector with eigenvalues and two matrices U and V such that

   A = U S V^T

where S is diagonal matrix with eigenvalues, and U and V are both orthonormal matricies, e.g. U^{-1} = U^T etc. (Beware:
in the code S is represented by vector.) The matrix U transforms to the diagonal basis. For instance, the ‘big vector’
in the diagonal basis is

  D = U^T b

The solution to the equation above is obtained with

  x = A^{-1} b = V *  S^{-1}  * U^T b
    =  V * S^{-1} * D

The inverse of A is also the covariance matrix for the alignment parameters

  C = V *  S^{-1}  * U^T

A cut on the eigenvalues is implemented by setting S^{-1}(i) to zero if S(i) is too small.

The columns of U represent the eigenvectors. For a symmetric matrix A, we should find V = U. However, both GSL and Eigen
choose all eigenvalues positive and put the sign of negative eigenvalues into a difference U and V. This sign can be
retrieved as

  sign_eigenvalue_i  = sum_k U(k,i) V(k,i)

(The eigenvectors have unit length, so the answer is either +1 or -1.)

With U=V, the Lagrange constraints will have negative eigenvalue. Because we perform an eigenvalue analysis, it is
important to get these signs right. Therefore, in order to ‘correct’ the signs, we perform some manipulation on the
output of GSL and SVD: For every i for which sign_eigenvalue_i<0, we multiply both S(i,i) and V(k,i) by -1.

 */
//-----------------------------------------------------------------------------

//=============================================================================
// Initialize
//=============================================================================
StatusCode DiagSolvToolBase::initialize() {
  StatusCode sc = GaudiTool::initialize();
  info() << "EigenValueThreshold = " << m_eigenValueThreshold << endmsg;
  info() << "MinEigenModeChisquare = " << m_minEigenModeChisquare << endmsg;
  return sc;
}

namespace {
  struct SortByAbsSecond {
    template <class T>
    bool operator()( const T& lhs, const T& rhs ) const {
      return std::abs( lhs.second ) < std::abs( rhs.second );
    }
  };
} // namespace

StatusCode DiagSolvToolBase::compute( AlSymMat& bigmatrix, AlVec& bigvector,
                                      IAlignSolvTool::SolutionInfo& solinfo ) const {
  info() << "Solving with diagonalization method" << endmsg;

  // declare transition matrix + vector to store eigenvalues
  const size_t N = bigmatrix.rows();
  AlMat        U( N, N );
  AlMat        V( N, N );
  AlVec        S( N );

  debug() << "Before diagonalization" << endmsg;

  // defer the diagonalization to whatever tool derives from this.
  // bigmatrix = U S V^T
  StatusCode sc = diagonalize( bigmatrix, U, V, S );
  if ( !sc.isSuccess() ) {
    error() << "Diagonalization failed" << endmsg;
    return sc;
  }

  debug() << "After diagonalization" << endmsg;

  // Correct the eigenvalue signs
  for ( size_t ipar = 0; ipar < N; ++ipar ) {
    const double sign = U.col( ipar ).dot( V.col( ipar ) );
    if ( sign < 0 ) {
      S[ipar] *= -1;
      for ( size_t irow = 0; irow < N; ++irow ) V( irow, ipar ) *= -1;
    }
  }

  //   Compute bigvector in diagonal basis. Here we need the transpose of U, just like it would be in GSL.
  const Eigen::VectorXd D = U.transpose() * bigvector;

  // Perform some analysis of the eigenvalues.
  solinfo.numNegativeEigenvalues = solinfo.numSmallEigenvalues = 0;
  {
    // count the number of negative eigenvalues and number smaller than one
    std::vector<std::pair<size_t, double>> sortedev( N );
    for ( size_t ipar = 0; ipar < N; ++ipar ) {
      sortedev[ipar] = std::make_pair( ipar, S[ipar] );
      if ( std::abs( S[ipar] ) < 1.0 ) ++solinfo.numSmallEigenvalues;

      // Eigen and Gsl put the 'sign' of negative eigenvalues in a
      // difference between U and V. This explains why we do not see
      // negative Eigenvalues and need to use both U and V in the
      // solution.
      if ( !( S[ipar] > 0.0 ) ) {
        ++solinfo.numNegativeEigenvalues;
        // Issue a warning for each negative eigenvalue.
        debug() << "Negative eigenvalue (i,val,chi2)= " << ipar << " " << S( ipar ) << " "
                << D( ipar ) * D( ipar ) / S( ipar ) << endmsg;
      }
    }
    info() << "Number of negative eigenvalues: " << solinfo.numNegativeEigenvalues << endmsg;

    // Dump the 10 smallest eigenvalues. We'll sort these really in
    // abs, ignoring the sign.
    std::sort( sortedev.begin(), sortedev.end(), SortByAbsSecond() );
    std::ostringstream logmessage;
    logmessage << "Smallest eigen values: [ " << std::setprecision( 4 );
    size_t maxpar = std::min( N, m_numberOfPrintedEigenvalues.value() );
    for ( size_t ipar = 0; ipar < maxpar; ++ipar )
      logmessage << sortedev[ipar].second << ( ( ipar < maxpar - 1 ) ? ", " : " ]" );
    solinfo.eigenvalues.resize( N, 0.0 );
    std::transform( sortedev.begin(), sortedev.end(), solinfo.eigenvalues.begin(),
                    []( const auto& x ) { return x.second; } );

    // consider the smallest eigenvalue
    if ( N > 0 ) {
      // dump the corresponding eigenvector
      if ( N <= m_numberOfPrintedEigenvalues ) {
        logmessage << std::endl;
        logmessage << "Eigenvector for smallest eigenvalue: [ ";
        for ( size_t ipar = 0; ipar < N; ++ipar ) {
          logmessage << U( ipar, sortedev[0].first ) << ( ( ipar < N - 1 ) ? ", " : " ]" );
        }
      }
      // figure out which parameter contributes most to this eigenvector
      size_t thepar( 0 );
      for ( size_t ipar = 0; ipar < N; ++ipar ) {
        if ( std::abs( U( sortedev[0].first, thepar ) ) < std::abs( U( sortedev[0].first, ipar ) ) ) thepar = ipar;
      }
      solinfo.minEigenValue         = sortedev[0].second;
      solinfo.weakestModeMaxPar     = thepar;
      solinfo.weakestModeMaxParCoef = U( sortedev[0].first, thepar );
    }
    debug() << logmessage.str() << endmsg;
  }

  // compute alignment corrections and their variance. first flag
  // modes that we cut away by eigenvalue. (constraints have large
  // negative value, so the 'abs' should do)
  std::vector<bool> keepEigenValue( N, true );
  if ( m_eigenValueThreshold > 0 )
    for ( size_t i = 0; i < N; i++ ) {
      keepEigenValue[i] =
          std::abs( S( i ) ) > m_eigenValueThreshold ||
          ( m_minEigenModeChisquare > 0 && D( i ) * D( i ) / std::abs( S( i ) ) > m_minEigenModeChisquare );
    }
  double                sumchisqaccepted( 0 ), sumchisqrejected( 0 ), maxchisq( 0 );
  size_t                numrejected( 0 );
  std::multiset<double> chisqvalues;
  for ( size_t i = 0; i < N; i++ ) {
    // This should actually be "b^T A^-1 b =  b^T V S^-1 U^T b", but now that V=U this should be fine.
    const double thischisq = D( i ) * D( i ) / S( i );
    if ( !keepEigenValue[i] ) {
      info() << "Rejecting eigenvalue: val = " << S( i ) << " chisq = " << thischisq << endmsg;
      ++numrejected;
      sumchisqrejected += thischisq;
    } else {
      chisqvalues.insert( thischisq );
      if ( maxchisq < thischisq ) maxchisq = thischisq;
      sumchisqaccepted += thischisq;
      if ( std::abs( S( i ) ) < m_eigenValueThreshold )
        info() << "Accepting low eigenvalue: val = " << S( i ) << " chisq = " << thischisq << endmsg;
    }
  }

  solinfo.totalChisq = sumchisqaccepted;

  info() << "Number / total chi2 of rejected eigenvalues: " << numrejected << " " << sumchisqrejected << endmsg;
  info() << "Total chi2 of accepted eigenvalues: " << sumchisqaccepted << endmsg;
  info() << "Maximum chi2 of individual mode: " << maxchisq << endmsg;

  // because the lagrange constraints lead to negative ev, they also
  // give negative chi2 contributions. that's all fine for the total
  // chi2, but it leads to problems if you want to identify the
  // 'largest' chi2 contribution: such contributions may be
  // compensated by large negative contributions. Let's solve this
  // pragmatically:
  // - make a multiset of all chisq contributions
  // - while the smallest one is negative, remove the smallest and
  //   largest from the set, and reinsert the sum
  // - the largest remaining eigenvalue is the one we need;
  while ( !chisqvalues.empty() && *( chisqvalues.begin() ) < 0 && *( chisqvalues.rbegin() ) > 0 ) {
    double a = *( chisqvalues.begin() );
    chisqvalues.erase( chisqvalues.begin() );
    double b = *( chisqvalues.rbegin() );
    chisqvalues.erase( --( chisqvalues.rbegin().base() ) );
    chisqvalues.insert( a + b );
  }
  if ( !chisqvalues.empty() ) solinfo.maxChisqEigenMode = *( chisqvalues.rbegin() );

  info() << "Maximum chi2 of individual mode after correcting for lagrange constraints: " << solinfo.maxChisqEigenMode
         << endmsg;

  // reset the input
  for ( size_t i = 0; i < N; i++ ) {
    bigvector( i ) = 0.;
    for ( size_t j = 0; j <= i; j++ ) bigmatrix( i, j ) = 0.;
  }

  // now fill it
  for ( size_t k = 0; k < N; ++k )
    if ( keepEigenValue[k] ) {
      for ( size_t i = 0; i < N; ++i ) {
        bigvector( i ) += V( i, k ) * D( k ) / S( k );
        for ( size_t j = 0; j <= i; ++j ) bigmatrix( i, j ) += V( i, k ) * U( j, k ) / S( k );
      }
    } else {
      warning() << "Rejecting eigenvalue: " << k << " " << S( k ) << endmsg;
    }

  return sc;
}
