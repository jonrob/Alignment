/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include <cmath>

// from GSL
#include "gsl/gsl_linalg.h"

// Interface
#include "DiagSolvToolBase.h"

class GslDiagSolvTool : public DiagSolvToolBase {
public:
  using DiagSolvToolBase::DiagSolvToolBase;

  using IAlignSolvTool::compute; ///< avoids hiding the original function definitions

  StatusCode diagonalize( const AlSymMat& A, AlMat& U, AlMat& V, AlVec& S ) const override;

private:
  Gaudi::Property<bool> m_svdJacobi{this, "SVDJacobi", false}; ///< Use Jacobi SVD?
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( GslDiagSolvTool )

/*
namespace {

  std::ostream& operator<<( std::ostream& lhs, const gsl_vector& rhs ) {
    lhs << std::endl;
    lhs << "[ " << std::endl;
    for ( unsigned i = 0u; i < rhs.size; ++i ) {
      lhs << " " << ( *gsl_vector_const_ptr( &rhs, i ) ) << " ";
      lhs << std::endl;
    }
    lhs << " ]" << std::endl;
    return lhs;
  }

  std::ostream& operator<<( std::ostream& lhs, const gsl_matrix& rhs ) {
    lhs << std::endl;
    lhs << "[ " << std::endl;
    for ( unsigned i = 0u; i < rhs.size1; ++i ) {
      for ( unsigned j = 0u; j < rhs.size2; ++j ) { lhs << " " << ( *gsl_matrix_const_ptr( &rhs, i, j ) ) << " "; }
      lhs << std::endl;
    }
    lhs << " ]" << std::endl;
    return lhs;
  }
} // namespace
*/

StatusCode GslDiagSolvTool::diagonalize( const AlSymMat& A, AlMat& U, AlMat& V, AlVec& S ) const {

  const auto N = A.rows();

  debug() << "Size of AlSymMat A = " << N << endmsg;
  /// Allocate matrix A
  gsl_matrix* matrixA = gsl_matrix_alloc( N, N );
  debug() << "Size of gsl_matrix A = " << matrixA->size1 << endmsg;
  /// Fill matrix A
  for ( unsigned i = 0u; i < N; ++i ) {
    for ( unsigned j = 0u; j < N; ++j ) {
      debug() << "Element (i,j) of AlSymMat A = " << A( i, j ) << endmsg;
      gsl_matrix_set( matrixA, i, j, A( i, j ) );
      debug() << "Element (i,j) of gsl_matrix A = " << gsl_matrix_get( matrixA, i, j ) << endmsg;
    }
  }

  /// Allocate required matrix, vector and workspace
  gsl_matrix* matrixV = gsl_matrix_alloc( N, N );
  gsl_vector* vectorS = gsl_vector_alloc( N );
  gsl_vector* vectorW = gsl_vector_alloc( N );
  // gsl_matrix* matrixU = matrixA ;

  debug() << "Factorising matrix A" << endmsg;
  /// Factorise A into the SVD A = USV^T. Note matrix A is replaced with matrix U.
  /// GSL returns 0 if successful
  int factorised = 1;
  if ( !m_svdJacobi.value() ) {
    info() << "==> SVD standard" << endmsg;
    factorised = gsl_linalg_SV_decomp( matrixA, matrixV, vectorS, vectorW );
  } else {
    info() << "==> SVD Jacobi" << endmsg;
    factorised = gsl_linalg_SV_decomp_jacobi( matrixA, matrixV, vectorS );
  }
  if ( factorised != 0 ) {
    error() << "==> Couldn't factorise  matrix" << endmsg;
    return StatusCode::FAILURE;
  }
  debug() << "Done factorising matrix A" << endmsg;

  //
  for ( size_t irow = 0; irow < N; ++irow ) S( irow ) = gsl_vector_get( vectorS, irow );
  for ( size_t irow = 0; irow < N; ++irow )
    for ( size_t icol = 0; icol < N; ++icol ) {
      U( irow, icol ) = gsl_matrix_get( matrixA, irow, icol );
      V( irow, icol ) = gsl_matrix_get( matrixV, irow, icol );
    }

  gsl_matrix_free( matrixA );
  gsl_matrix_free( matrixV );
  gsl_vector_free( vectorS );
  gsl_vector_free( vectorW );

  return StatusCode::SUCCESS;
}
