###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import re

from glob import glob
logfiles = glob('alignlog.txt') + glob('AlignmentResults/alignlog.txt')

logExists = len(logfiles) > 0
if logExists:
    status = None
    with open(logfiles[-1]) as f:
        log = f.read()
        match = re.search(r'Convergence status: (\S+)', log).group(1)
        if match: status = match
        match = re.search(r'Total number of events: (\S+)', log).group(1)
        if match: nevents = match
    if nevents == '0':
        causes.append('No events processed')
    if not 'Converged' in status:
        causes.append('Alignment not converged')
else:
    causes.append('Alignment log file not produced')
