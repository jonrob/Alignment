###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import re
from DDDB.CheckDD4Hep import UseDD4Hep

logExists = os.path.isfile("alignlog_SciFi.txt")

if UseDD4Hep:
    xmlExists = os.path.isfile(
        "default-yml-dir/Conditions/FT/Alignment/FTSystem.yml")
else:
    xmlExists = os.path.isfile(
        "humb-ft-stations-layers/Conditions/FT/Alignment/FTSystem.xml"
    ) and os.path.isfile(
        "humb-ft-stations-layers/Conditions/FT/Alignment/Modules.xml"
    ) and os.path.isfile(
        "humb-ft-stations-layers/Conditions/FT/Alignment/Mats.xml")

if logExists:
    with open('alignlog_SciFi.txt') as f:
        log = f.read()
        status = re.search(r'Total number of events: (\S+)', log).group(1)
        result['alignlog'] = result.Quote(log)
        if status == '0':
            causes.append('No events processed')
else:
    causes.append('alignment log file not produced')

if not xmlExists:
    causes.append('XML files with constants not produced')
