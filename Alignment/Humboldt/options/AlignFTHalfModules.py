###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.application import configure_input
from PRConfig.FilesFromDirac import get_access_urls
from DDDB.CheckDD4Hep import UseDD4Hep

# set DDDB and CondDB info
options.set_input_and_conds_from_testfiledb(
    'upgrade_Sept2022_minbias_0fb_md_xdigi')

options.evt_max = 100
# options.use_iosvc = True
options.event_store = 'EvtStoreSvc'
options.ntuple_file = "testmonitoring.root"
options.histo_file = "testmonitoringhist.root"

# set options above this line!

configure_input(options)

# only configure data flow after this line !

from Humboldt.utils import runAlignment

from Humboldt.alignment_tracking import make_scifi_tracks_and_particles_prkf
alignmentTracks, alignmentPVs, particles, odin, monitors = make_scifi_tracks_and_particles_prkf(
)

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
elements.FTHalfModules("TxRz")

# add survey constraints
from Configurables import SurveyConstraints
surveyconstraints = SurveyConstraints()
if UseDD4Hep:
    surveyconstraints.FT(ver='data20221115dd4hep', addHalfModuleJoints=True)
else:
    surveyconstraints.FT(ver='MC', addHalfModuleJoints=True)

if UseDD4Hep:
    xml_writer_list = []
else:
    from Humboldt.utils import getXMLWriterList
    xml_writer_list = getXMLWriterList(['FT'])

from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList
with createAlignUpdateTool.bind(
        logFile="alignlog_ft_modules_tracks.txt"), createAlignAlgorithm.bind(
            xmlWriters=xml_writer_list):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=[],
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        odin=odin,
        elementsToAlign=elements,
        monitorList=monitors)
