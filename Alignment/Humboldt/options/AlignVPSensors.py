###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.application import configure_input
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)
options.set_input_and_conds_from_testfiledb('upgrade_Aug2022_minbias_md_xdigi')

options.evt_max = 10
options.ntuple_file = "testmonitoring.root"
options.histo_file = "testmonitoringhist.root"

# set options above this line!

configure_input(options)

# only configure data flow after this line !

from Humboldt.utils import runAlignment


def getAlignmentTracksAndPVs():
    from RecoConf.reconstruction_objects import reconstruction
    from PyConf.Algorithms import VeloClusterTrackingSIMDFull
    from RecoConf.hlt1_tracking import make_reco_pvs, make_PatPV3DFuture_pvs, make_VeloClusterTrackingSIMD
    from RecoConf.hlt2_tracking import make_hlt2_tracks
    from RecoConf.hlt2_global_reco import make_legacy_reconstruction, reconstruction as reconstruction_from_reco
    with reconstruction.bind(from_file=False),\
             reconstruction_from_reco.bind(make_reconstruction = make_legacy_reconstruction),\
    make_hlt2_tracks.bind(light_reco=False),\
    make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
    make_VeloClusterTrackingSIMD.bind(algorithm=VeloClusterTrackingSIMDFull):
        #make tracks and PVs
        reco = reconstruction()
        hlt2_tracks = reco["Tracks"]
        best_tracks = hlt2_tracks
        pvs = reco["PVs_v1"]

        #select tracks and PVs
        from PyConf.Algorithms import TrackSelectionMerger
        from Humboldt.TrackSelections import VPBackwardsTracks, VPLongTracks, VPOverlapTracks
        from Humboldt.VertexSelections import VPPrimaryVertices

        selected_LongTracks = VPLongTracks(best_tracks)
        selected_BackwardTracks = VPBackwardsTracks(best_tracks)
        selected_OverlapTracks = VPOverlapTracks(best_tracks)

        alignmentTracks = TrackSelectionMerger(InputLocations=[
            selected_LongTracks, selected_BackwardTracks,
            selected_OverlapTracks
        ]).OutputLocation

        selected_pvs = VPPrimaryVertices(pvs)

        # add track and vertex monitoring
        from PyConf.Algorithms import TrackMonitor, TrackFitMatchMonitor, TrackVertexMonitor, VPTrackMonitor
        from RecoConf.hlt1_tracking import make_VPClus_hits
        myTrackMonitor = TrackMonitor(
            TracksInContainer=alignmentTracks, PrimaryVertices=pvs)
        myTrackFitMatchMonitor = TrackFitMatchMonitor(
            TrackContainer=alignmentTracks)
        myTrackVertexMonitor = TrackVertexMonitor(
            TrackContainer=alignmentTracks, PVContainer=pvs)
        myVPTrackMonitor = VPTrackMonitor(
            TrackContainer=best_tracks, ClusterContainer=make_VPClus_hits())
        monitorlist = [
            myTrackMonitor, myTrackFitMatchMonitor, myTrackVertexMonitor,
            myVPTrackMonitor
        ]
        return alignmentTracks, selected_pvs, monitorlist


alignmentTracks, alignmentPVs, monitors = getAlignmentTracksAndPVs()

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxTyRz"
elements.VPSensors(dofs)

# add survey constraints
from Configurables import SurveyConstraints
from PyConf.Tools import AlignChisqConstraintTool
surveyconstraints = SurveyConstraints()
surveyconstraints.VP()

# define Lagrange constraints
constraints = []

from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList

with createAlignUpdateTool.bind(
        logFile="alignlog_vp_sensors.txt"), createAlignAlgorithm.bind(
            xmlWriters=getXMLWriterList('VP', prefix='humb-vp-sensors/')):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        elementsToAlign=elements,
        monitorList=monitors)
