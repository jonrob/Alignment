###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file to test running on data.
"""

from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from Hlt2Conf.lines import all_lines
import re
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
    get_UpgradeGhostId_tool_no_UT, make_hlt2_tracks)
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClusterDecoder, PrKalmanFilter, PrKalmanFilter_Downstream, PrKalmanFilter_noUT
from RecoConf.hlt1_tracking import (
    make_VeloClusterTrackingSIMD, get_global_measurement_provider,
    make_velo_full_clusters, make_reco_pvs, make_PatPV3DFuture_pvs)

from RecoConf.hlt1_muonid import make_muon_hits
from RecoConf.calorimeter_reconstruction import make_digits, make_calo
from PyConf.application import configure_input
from PRConfig.FilesFromDirac import get_access_urls
from DDDB.CheckDD4Hep import UseDD4Hep

options.input_type = 'MDF'
options.simulation = False
options.data_type = 'Upgrade'

# set DDDB and CondDB info
options.geometry_version = "trunk"
CONDDBTag = "master"
options.conditions_version = CONDDBTag
from glob import glob
files = glob("/eos/lhcb/hlt2/LHCb/0000256145/Run_*.mdf")
options.input_files = files[0:10]  #50:200

options.event_store = 'EvtStoreSvc'
options.histo_file = "GoodLongTracks_histo.root"
options.use_iosvc = True  # True for data, False for MC
# multithreading not working while creating tuples
options.n_threads = 8
options.evt_max = -1

options.scheduler_legacy_mode = False

configure_input(options)

from Humboldt.utils import runAlignment

from Humboldt.alignment_tracking import make_scifi_tracks_and_particles_prkf
alignmentTracks, alignmentPVs, particles, odin, monitors = make_scifi_tracks_and_particles_prkf(
)

from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxRzTz"
elements.FTHalfLayers(dofs)

# add survey constraints
from Configurables import SurveyConstraints
from PyConf.Tools import AlignChisqConstraintTool
surveyconstraints = SurveyConstraints()
surveyconstraints.FT(ver="data20221115")

# define Lagrange constraints
constraints = []

from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList
with createAlignUpdateTool.bind(
        logFile="alignlog_ft_modules_d0_prkalman.txt"
), createAlignAlgorithm.bind(
        xmlWriters=getXMLWriterList('FT', prefix='humb-ft-modules-d0/')):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        particles=particles,
        odin=odin,
        elementsToAlign=elements,
        monitorList=monitors)
