###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.application import configure_input
from Humboldt.utils import runAlignment
import OnlineEnvBase as OnlineEnv

#TODO: get these from Online
runNumber = 9999
workerNumber = 0
runType = "VELO"
if OnlineEnv.PartitionName == "TEST":
    derivfile = "humb-vp-halves-modules-derivs"
    histofile = "testmonitoringhist.root"
else:
    from pathlib import Path
    onlineprefix = '/group/online/dataflow/cmtuser/alignonlinetest'
    Path(f"{onlineprefix}/{runType}/{runNumber}/analyzerOutput/").mkdir(
        parents=True, exist_ok=True)
    derivfile = f"{onlineprefix}/{runType}/{runNumber}/analyzerOutput/{workerNumber}_humb-vp-halves-modules-derivs"
    histofile = f"{onlineprefix}/{runType}/{runNumber}/analyzerOutput/{workerNumber}_testmonitoringhist.root"

# set options above this line!
options.histo_file = histofile
configure_input(options)

# only configure data flow after this line !


# at the moment, define tracks and PVs by hand. Could be changed to centrally defined selection in the future
def getAlignmentTracksAndPVs():
    from RecoConf.reconstruction_objects import reconstruction
    from PyConf.Algorithms import VeloClusterTrackingSIMDFull
    # note that the PVs reconstructed by TrackBeamLineVertexFinderSoA do not store the list of associated tracks. Use PatPV3DFuture instead
    from RecoConf.hlt1_tracking import make_reco_pvs, make_PatPV3DFuture_pvs, make_VeloClusterTrackingSIMD
    from RecoConf.hlt2_tracking import make_hlt2_tracks
    #, make_reco_pvs.bind( make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)
    #with  reconstruction.bind(from_file=False), make_hlt2_tracks.bind(light_reco=True, use_pr_kf=True, fast_reco=False), make_reco_pvs.bind( make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs):
    from RecoConf.hlt2_global_reco import make_legacy_reconstruction, reconstruction as reconstruction_from_reco
    with reconstruction.bind(from_file=False),\
             reconstruction_from_reco.bind(make_reconstruction = make_legacy_reconstruction),\
             make_hlt2_tracks.bind(light_reco=False),\
             make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
             make_VeloClusterTrackingSIMD.bind(algorithm=VeloClusterTrackingSIMDFull):
        #make tracks and PVs
        # TODO: in principle the PVs should be an optional input for the alignment
        reco = reconstruction()
        #hlt2_tracks = make_hlt2_tracks(light_reco=False)
        #best_tracks = hlt2_tracks["Best"]['v1']
        #pvs = make_pvs()
        hlt2_tracks = reco["Tracks"]
        best_tracks = hlt2_tracks
        pvs = reco["PVs_v1"]

        from PyConf.application import make_odin
        odin = make_odin()

        #select tracks and PVs
        from Humboldt.TrackSelections import VPBackwardsTracks, VPLongTracks, VPOverlapTracks
        from Humboldt.VertexSelections import VPPrimaryVertices
        from PyConf.Algorithms import TrackSelectionMerger
        selected_LongTracks = VPLongTracks(best_tracks)
        selected_BackwardTracks = VPBackwardsTracks(best_tracks)
        selected_OverlapTracks = VPOverlapTracks(best_tracks)

        alignmentTracks = TrackSelectionMerger(InputLocations=[
            selected_LongTracks, selected_BackwardTracks,
            selected_OverlapTracks
        ]).OutputLocation

        selected_pvs = VPPrimaryVertices(pvs)

        # add track and vertex monitoring
        from PyConf.Algorithms import TrackMonitor, TrackVertexMonitor, TrackVPOverlapMonitor
        from RecoConf.hlt1_tracking import make_VPClus_hits
        myTrackMonitor = TrackMonitor(TracksInContainer=alignmentTracks)
        myTrackVertexMonitor = TrackVertexMonitor(
            TrackContainer=alignmentTracks, PVContainer=pvs)
        myTrackVPOverlapMonitor = TrackVPOverlapMonitor(
            TrackContainer=best_tracks, ClusterContainer=make_VPClus_hits())
        monitorlist = [
            myTrackMonitor, myTrackVertexMonitor, myTrackVPOverlapMonitor
        ]
        return alignmentTracks, selected_pvs, odin, monitorlist


alignmentTracks, alignmentPVs, odin, monitors = getAlignmentTracksAndPVs()

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxTyTzRxRyRz"
dofsmodules = "TxTyTzRxRyRz"
elements.VPModules(dofsmodules)

# add survey constraints
from Configurables import SurveyConstraints
surveyconstraints = SurveyConstraints()
surveyconstraints.VP()

# define Lagrange constraints
constraints = []
constraints.append("VPHalfAverage : VP/VP(Left|Right) : Tx Ty Tz Rx Ry Rz")
constraints.append(
    "VPInternalRight : VP/VPRight/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy"
)
constraints.append(
    "VPInternalLeft : VP/VPLeft/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy"
)

from Humboldt.utils import createAlignAlgorithm
with createAlignAlgorithm.bind(
        onlineMode=True,
        histoPrint=True,
        outputDataFile=derivfile,
        updateInFinalize=False):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        odin=odin,
        elementsToAlign=elements,
        monitorList=monitors)
