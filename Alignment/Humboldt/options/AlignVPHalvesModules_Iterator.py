###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *

import Configurables
from Configurables import AlignOnlineIterator, AlignChisqConstraintTool, AlignUpdateTool
import OnlineEnvBase as OnlineEnv
from Moore import options
from PyConf.application import configure_input
options.input_files = []
options.histo_file = "testmonitoringhist.root"
configure_input(options)
# set options above this line!

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxTyTzRxRyRz"
dofsmodules = "TxTyTzRxRyRz"

elements.VPModules(dofsmodules)

# add survey constraints
from Configurables import SurveyConstraints
#from PyConf.Tools import AlignChisqConstraintTool, AlignUpdateTool
surveyconstraints = SurveyConstraints()
surveyconstraints.VP()
myAlignChisqConstraintTool = AlignChisqConstraintTool(
    Constraints=surveyconstraints.Constraints,
    XmlUncertainties=surveyconstraints.XmlUncertainties,
    XmlFiles=surveyconstraints.XmlFiles)

# define Lagrange constraints
constraints = []
constraints.append("VPHalfAverage : VP/VP(Left|Right) : Tx Ty Tz Rx Ry Rz")
constraints.append(
    "VPInternalRight : VP/VPRight/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy"
)
constraints.append(
    "VPInternalLeft : VP/VPLeft/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy"
)

from Humboldt.utils import createAlignUpdateTool, getXMLWriterList

#TODO: get these from Online
runNumber = 9999
workerNumber = 0
runType = "VELO"
if OnlineEnv.PartitionName == "TEST":
    derivfile = '../AlignmentAnalyzer/humb-vp-halves-modules-derivs'
    alignlogFile = 'alignlog_iterator.txt'
    xmlprefix = 'humb-iterator-vp-halves-modules/'
else:
    from pathlib import Path
    onlineprefix = '/group/online/dataflow/cmtuser/alignonlinetest'
    Path(f"{onlineprefix}/{runType}/{runNumber}/iteratorOutput/").mkdir(
        parents=True, exist_ok=True)

    derivfile = f"{onlineprefix}/{runType}/{runNumber}/analyzerOutput/{workerNumber}_humb-vp-halves-modules-derivs"
    alignlogFile = f"{onlineprefix}/{runType}/{runNumber}/iteratorOutput/alignlog_iterator.txt"
    xmlprefix = f"{onlineprefix}/{runType}/{runNumber}/iteratorOutput/humb-iterator-vp-halves-modules/"

myUpdateTool = AlignUpdateTool(
    SurveyConstraintTool=myAlignChisqConstraintTool,
    Constraints=constraints,
    LogFile=alignlogFile,
    SkipGeometryUpdate=False)

myUpdateTool.SurveyConstraintTool = myAlignChisqConstraintTool

myAlignIterator = AlignOnlineIterator(
    Elements=list(elements),
    DerivativeFile=derivfile,
    #    UpdateTool=myUpdateTool,
    UpdateTool=myUpdateTool,
    XmlWriters=[
        f'{xmlprefix}/xml/Conditions/VP/Alignment/Global.xml:/dd/Structure/LHCb/BeforeMagnetRegion/VP:10:0/1:0:1',
        f'{xmlprefix}/xml/Conditions/VP/Alignment/Modules.xml:/dd/Structure/LHCb/BeforeMagnetRegion/VP:10:2:0:1'
    ])
myAlignIterator.UpdateTool.SurveyConstraintTool = myAlignChisqConstraintTool
# print(dir(myUpdateTool))
# myAlignIterator.addTool(myUpdateTool)
# myAlignIterator.UpdateTool=myUpdateTool
#myAlignIterator.OutputLevel = 1
#myconf = myAlignIterator.configuration()
#myconf.apply()
app = ApplicationMgr(EvtMax=0, EvtSel='NONE', TopAlg=[])
app.Runable = myAlignIterator
