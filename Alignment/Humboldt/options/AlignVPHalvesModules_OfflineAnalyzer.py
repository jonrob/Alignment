###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
option file to test DD4HEP
'''

from Moore import options
from PyConf.application import configure_input
from RecoConf.core_algorithms import make_unique_id_generator
from Humboldt.utils import createAlignAlgorithm, createAlignUpdateTool, runAlignment

from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    DD4hepSvc().UseConditionsOverlay = True

options.set_input_and_conds_from_testfiledb(
    'upgrade_Sept2022_minbias_0fb_md_xdigi')
options.evt_max = 100
# options.use_iosvc = True
options.event_store = 'EvtStoreSvc'
options.ntuple_file = "testmonitoring.root"
options.histo_file = "testmonitoringhist.root"
options.data_flow_file = 'dd4hep_dataflownew.pdf'
options.control_flow_file = 'dd4hep_controlflownew.pdf'

# set options above this line!

configure_input(options)

# only configure data flow after this line !

# at the moment, define tracks and PVs by hand. Could be changed to centrally defined selection in the future
from Humboldt.options import usePrKalman
from Humboldt.alignment_tracking import make_align_vp_input
alignmentTracks, alignmentPVs = make_align_vp_input(usePrKalman=usePrKalman)

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxTyTzRxRyRz"
dofsmodules = "TxTyTzRxRyRz"
elements.VPLeft(dofs)
elements.VPRight(dofs)
#elements.VPModules(dofsmodules)

# add survey constraints
from Configurables import SurveyConstraints
surveyconstraints = SurveyConstraints()
surveyconstraints.VP()

# define Lagrange constraints
constraints = []
constraints.append("VPHalfAverage : VP/VP(Left|Right) : Tx Ty Tz Rx Ry Rz")
constraints.append(
    "VPInternal : VP/VPRight/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy")
constraints.append(
    "VPInternal : VP/VPLeft/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy")

with createAlignAlgorithm.bind(
        outputDataFile="humb-vp-halves-modules-derivs",
        updateInFinalize=False,
        onlineMode=True):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        elementsToAlign=elements)
