###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.application import configure_input
options.set_input_and_conds_from_testfiledb('upgrade_DC19_01_MinBiasMD')
#use only a cetain file from testfileDB as some files seem to be corrupt in that sample
options.input_files = list(
    set([
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000030_1.xdigi"
    ]))
options.evt_max = 10
# options.use_iosvc = True
options.event_store = 'EvtStoreSvc'
options.ntuple_file = "testmonitoring.root"
options.histo_file = "testmonitoringhist.root"

# set options above this line!

configure_input(options)

# only configure data flow after this line !

from Humboldt.utils import runAlignment


# at the moment, define tracks and PVs by hand. Could be changed to centrally defined selection in the future
def getAlignmentTracksAndPVs():
    from RecoConf.reconstruction_objects import reconstruction
    from PyConf.Algorithms import VeloClusterTrackingSIMDFull, PrKalmanFilter
    # note that the PVs reconstructed by TrackBeamLineVertexFinderSoA do not store the list of associated tracks. Use PatPV3DFuture instead
    from RecoConf.hlt1_tracking import make_reco_pvs, make_PatPV3DFuture_pvs
    from RecoConf.hlt2_global_reco import make_fastest_reconstruction
    from RecoConf.hlt2_global_reco import reconstruction as reconstruction_hook
    with reconstruction.bind(from_file=False), PrKalmanFilter.bind(
            FillFitResult=True,
            ClassicSmoothing=True), reconstruction_hook.bind(
                make_reconstruction=make_fastest_reconstruction
            ), make_reco_pvs.bind(
                make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs):
        #make tracks and PVs
        # TODO: in principle the PVs should be an optional input for the alignment
        reco = reconstruction()
        hlt2_tracks = reco["Tracks"]
        best_tracks = hlt2_tracks
        pvs = reco["PVs_v1"]

        from PyConf.application import make_odin
        odin = make_odin()
        # create input particles, use dummy for now
        # TODO: in principle these should be composite particles and be an optional input for the alginment
        from Humboldt.ParticleSelections import DummyParticles
        particles = DummyParticles()

        #select tracks and PVs
        from PyConf.Tools import VPTrackSelector
        from PyConf.Algorithms import VertexListRefiner, TrackSelectionMerger
        from PyConf.Algorithms import Deprecated__TrackListRefiner as TrackListRefiner
        myTrackSelector = VPTrackSelector(
            TrackTypes=["Long"], MinNVeloLayers=5, MaxChi2Cut=5)
        selected_tracks = TrackListRefiner(
            inputLocation=best_tracks, Selector=myTrackSelector).outputLocation

        # myBackwardTrackSelector = VPTrackSelector(
        #     TrackTypes=["Velo"],
        #     MinHits=5,
        #     MaxChi2Cut=5,
        #     OnlyBackwardTracks=True,
        #     OutputLevel=1)
        # selected_BackwardTracks = TrackListRefiner(
        #     inputLocation=best_tracks,
        #     Selector=myBackwardTrackSelector).outputLocation

        # FIT VELO TRACKS ------------------------------------------------------------------------------------------------------------
        from RecoConf.hlt1_tracking import make_VeloClusterTrackingSIMD_hits, make_PrStorePrUTHits_hits, make_PrStoreSciFiHits_hits
        from RecoConf.hlt2_tracking import get_global_materiallocator, get_default_hlt2_tracks, get_UpgradeGhostId_tool
        from PyConf.Algorithms import PrKalmanFilter, PrKalmanFilter_Velo, TrackBestTrackCreator
        from PyConf.Tools import TrackLinearExtrapolator
        from functools import partial
        from RecoConf.hlt1_tracking import all_velo_track_types
        from RecoConf.core_algorithms import make_unique_id_generator

        velo_tracks = all_velo_track_types()
        backward_velo_tracks = velo_tracks["Pr::backward"]

        vp_hits = make_VeloClusterTrackingSIMD_hits()

        max_chi2 = 2.8
        kf_template = partial(
            PrKalmanFilter_Velo,
            MaxChi2=max_chi2,
            MaxChi2PreOutlierRemoval=20,
            HitsVP=vp_hits,
            FillFitResult=True,
            ClassicSmoothing=True,
            ReferenceExtrapolator=TrackLinearExtrapolator(),
            InputUniqueIDGenerator=make_unique_id_generator())
        tbtc_template = partial(
            TrackBestTrackCreator,
            MaxChi2DoF=max_chi2,
            GhostIdTool=get_UpgradeGhostId_tool(),
            DoNotRefit=True,
            AddGhostProb=True,
            FitTracks=False)
        fitted_velo = kf_template(
            name="PrKalmanFilter_Velo",
            Input=backward_velo_tracks).OutputTracks
        best_BackwardTracks = tbtc_template(
            name="TBTC_Velo",
            TracksInContainers=[fitted_velo]).TracksOutContainer

        myBackwardTrackSelector = VPTrackSelector(
            TrackTypes=["Velo"],
            MinNVeloLayers=5,
            MaxChi2Cut=5,
            OnlyBackwardTracks=True,
            #OutputLevel=1
        )

        selected_BackwardTracks = TrackListRefiner(
            inputLocation=best_BackwardTracks,
            Selector=myBackwardTrackSelector).outputLocation
        #--------------------------------------------------------------------------------------------------------------------------

        alignmentTracks = TrackSelectionMerger(
            InputLocations=[selected_tracks, selected_BackwardTracks
                            ]).OutputLocation

        selected_pvs = VertexListRefiner(
            MaxChi2PerDoF=5,
            MinNumTracks=15,
            MinNumLongTracks=0,
            InputLocation=pvs).OutputLocation

        # add track and vertex monitoring
        from PyConf.Algorithms import TrackVertexMonitor, VPTrackMonitor_PrKalman
        from RecoConf.mc_checking import vphits_resolution_checker
        from RecoConf.hlt1_tracking import make_VPClus_hits
        # TODO: should only use selected PVs
        # TODO: check vertices
        myTrackVertexMonitor = TrackVertexMonitor(
            TrackContainer=alignmentTracks, PVContainer=pvs)
        myVPTrackMonitor = VPTrackMonitor_PrKalman(
            TrackContainer=best_tracks, ClusterContainer=make_VPClus_hits())
        monitorlist = [
            myTrackVertexMonitor, myVPTrackMonitor,
            vphits_resolution_checker()
        ]
        return alignmentTracks, selected_pvs, particles, odin, monitorlist


alignmentTracks, alignmentPVs, particles, odin, monitors = getAlignmentTracksAndPVs(
)

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxTyTzRxRyRz"
dofsmodules = "TxTyTzRxRyRz"
elements.VPLeft(dofs)
elements.VPRight(dofs)
elements.VPModules(dofsmodules)

# add survey constraints
from Configurables import SurveyConstraints
from PyConf.Tools import AlignChisqConstraintTool
surveyconstraints = SurveyConstraints()
surveyconstraints.VP()
myAlignChisqConstraintTool = AlignChisqConstraintTool(
    Constraints=surveyconstraints.Constraints,
    XmlUncertainties=surveyconstraints.XmlUncertainties,
    XmlFiles=surveyconstraints.XmlFiles)

# define Lagrange constraints
constraints = []
constraints.append("VPHalfAverage : VP/VP(Left|Right) : Tx Ty Tz Rx Ry Rz")
constraints.append(
    "VPInternal : VP/VPRight/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy")
constraints.append(
    "VPInternal : VP/VPLeft/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy")

from Humboldt.utils import createAlignAlgorithm, createAlignUpdateTool
from PyConf.Algorithms import TrackMonitor_PrKalman, TrackFitMatchMonitor_PrKalman

with createAlignUpdateTool.bind(
        logFile="alignlog_vp_halves_modules_prkalman.txt"
), createAlignAlgorithm.bind(xmlWriters=[
        'humb-vp-halves-modules/xml/Conditions/VP/Alignment/Global.xml:/dd/Structure/LHCb/BeforeMagnetRegion/VP:10:0/1:0:1',
        'humb-vp-halves-modules/xml/Conditions/VP/Alignment/Modules.xml:/dd/Structure/LHCb/BeforeMagnetRegion/VP:10:2:0:1'
]):
    runAlignment(
        options,
        chisqConstraintTool=myAlignChisqConstraintTool,
        lagrangeConstrains=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        particles=particles,
        odin=odin,
        elementsToAlign=elements,
        monitorList=monitors,
        usePrKalman=True)
