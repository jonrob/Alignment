###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.application import configure_input
#from PRConfig.FilesFromDirac import get_access_urls
from Humboldt.utils import runAlignment
import OnlineEnvBase as OnlineEnv

# Setup online
# TODO: get inputs from Online
runNumber = 9999
workerNumber = 0
runType = "SCIFI"
if OnlineEnv.PartitionName == "TEST":
    derivfile = "humb-ft-modules-derivs"
    histofile = "testmonitoringhist.root"
else:
    from pathlib import Path
    onlineprefix = '/group/online/dataflow/cmtuser/alignonlinetest'
    Path(f"{onlineprefix}/{runType}/{runNumber}/analyzerOutput/").mkdir(
        parents=True, exist_ok=True)
    derivfile = f"{onlineprefix}/{runType}/{runNumber}/analyzerOutput/{workerNumber}_humb-vp-halves-modules-derivs"
    histofile = f"{onlineprefix}/{runType}/{runNumber}/analyzerOutput/{workerNumber}_testmonitoringhist.root"

# set options above this line!
options.histo_file = histofile
configure_input(options)

# only configure data flow after this line !


# at the moment, define tracks and PVs by hand. Could be changed to centrally defined selection in the future
def getAlignmentTracksAndPVs():

    from RecoConf.reconstruction_objects import reconstruction
    from PyConf.Algorithms import VeloClusterTrackingSIMDFull
    # note that the PVs reconstructed by TrackBeamLineVertexFinderSoA do not store the list of associated tracks. Use PatPV3DFuture instead
    from RecoConf.hlt1_tracking import make_reco_pvs, make_PatPV3DFuture_pvs, make_VeloClusterTrackingSIMD
    from RecoConf.hlt2_tracking import make_hlt2_tracks
    from RecoConf.hlt2_global_reco import make_legacy_reconstruction, reconstruction as reconstruction_from_reco
    with reconstruction.bind(from_file=False),\
    reconstruction_from_reco.bind(make_reconstruction = make_legacy_reconstruction),\
    make_hlt2_tracks.bind(light_reco=False),\
    make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
    make_VeloClusterTrackingSIMD.bind(algorithm=VeloClusterTrackingSIMDFull):
        # TODO: in principle the PVs should be an optional input for the alignment
        reco = reconstruction()
        hlt2_tracks = reco["Tracks"]
        best_tracks = hlt2_tracks
        pvs = reco["PVs_v1"]

        from PyConf.application import make_odin
        odin = make_odin()
        # create input particles
        # TODO: in principle these should be an optional input for the alginment
        from Humboldt.ParticleSelections import DummyParticles

        #select tracks and PVs
        from PyConf.Tools import TrackSelector
        from PyConf.Algorithms import VertexListRefiner, TrackSelectionMerger
        from Humboldt.TrackSelections import GoodLongTracks
        from Humboldt.VertexSelections import VPPrimaryVertices
        selected_tracks = GoodLongTracks(best_tracks)
        track_name = "GoodLongTracks"
        alignmentTracks = TrackSelectionMerger(
            InputLocations=[selected_tracks]).OutputLocation

        selected_pvs = VPPrimaryVertices(pvs)

        # add track and vertex monitoring
        from PyConf.Algorithms import TrackMonitor, TrackFitMatchMonitor, FTTrackMonitor, TrackVertexMonitor
        from GaudiKernel.SystemOfUnits import MeV

        # TODO: should only use selected PVs in monitors
        myTrackMonitor = TrackMonitor(TracksInContainer=alignmentTracks)
        myTrackFitMatchMonitor = TrackFitMatchMonitor(
            TrackContainer=alignmentTracks)

        myFTTrackMonitor = FTTrackMonitor(TracksInContainer=alignmentTracks)

        # TODO: check vertices
        myTrackVertexMonitor = TrackVertexMonitor(
            TrackContainer=alignmentTracks, PVContainer=pvs)

        monitorlist = [
            myTrackMonitor, myTrackFitMatchMonitor, myFTTrackMonitor,
            myTrackVertexMonitor
        ]
        return alignmentTracks, selected_pvs, odin, monitorlist


alignmentTracks, alignmentPVs, odin, monitors = getAlignmentTracksAndPVs()

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxRzTz"
elements.FTModules(dofs)

# add survey constraints
from Configurables import SurveyConstraints
surveyconstraints = SurveyConstraints()
surveyconstraints.FT()

# define Lagrange constraints
constraints = []

#Confine module motion to CFrames
for station in ["T1", "T2", "T3"]:
    for layerlabel, layercode in zip(["X1U", "VX2"],
                                     ["Layer(X1|U)", "Layer(V|X2)"]):
        for sidelabel, sidecode in zip(["CSide", "ASide"],
                                       ["Quarter(0|2)", "Quarter(1|3)"]):
            constraints.append(
                f"Modules{station}{layerlabel}{sidelabel} : FT/{station}{layercode}{sidecode}Module. : Tx Tz Rx Rz"
            )

from Humboldt.utils import createAlignAlgorithm
with createAlignAlgorithm.bind(
        onlineMode=True, histoPrint=True, outputDataFile=derivfile):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        odin=odin,
        elementsToAlign=elements,
        monitorList=monitors)
