###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.application import configure_input
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)

options.dddb_tag = 'velo-open'  #'upgrade/master'
options.dddb_tag = 'upgrade/master'
options.conddb_tag = 'upgrade/master'

#this tag has survey measurements in:
UseSurveyDB = False
if UseSurveyDB: options.conddb_tag = 'upgrade/md_VP_SciFi_macromicrosurvey'

#options.conddb_tag = 'upgrade/VPTrackbasedAlignmentAug2022'
test_files_data = [
    "root://eoslhcb.cern.ch//eos/lhcb/hlt2/VELO/0000248310/Run_0000248310_20221006-140329-403_VAEB10.mdf"
]
test_files_data = [
    "/data/bfys/wouterh/earlyrun3/0000248310/Run_0000248310_20221006-140329-403_VAEB10.mdf"
]

#options.velo_motion_system_yaml = '/user/wouterh/master/velomonstack/Alignment/job/Motion.yml'
#options.velo_motion_system_yaml = '/user/wouterh/master/velomonstack/Alignment/job/yaml/Conditions/VP/Motion.yml'

# the md_VP_SciFi_macromicrosurvey tag seems to have the shim offsets in the geometry.
#if UseSurveyDB: options.velo_motion_system_yaml = '/user/wouterh/master/velomonstack/Alignment/job/yaml/Conditions/VPWithShims/Motion.yml'

options.input_files = test_files_data
options.input_raw_format = 0.5
options.input_type = "MDF"

options.evt_max = 10
options.ntuple_file = "alignmonitoring.root"
options.histo_file = "alignmonitoringhist.root"

#options.event_store = 'EvtStoreSvc'
#options.n_threads = 8

# set options above this line!

configure_input(options)

# only configure data flow after this line !

from Humboldt.utils import runAlignment


def refitVeloOnly(selectedTracks):
    from PyConf.Algorithms import TrackEventFitter
    from PyConf.Tools import TrackMasterFitter, TrackSimpleExtraSelector, TrackMasterExtrapolator, SimplifiedMaterialLocator
    from RecoConf.hlt1_tracking import get_global_measurement_provider
    measprovider = get_global_measurement_provider(
        ignoreUT=True, ignoreFT=True, ignoreMuon=True)
    materiallocator = SimplifiedMaterialLocator()
    extrapolatorselector = TrackSimpleExtraSelector(
        ExtrapolatorName="TrackLinearExtrapolator")
    masterextrapolator = TrackMasterExtrapolator(
        ExtraSelector=extrapolatorselector,
        ApplyEnergyLossCorr=False,
        ApplyElectronEnergyLossCorr=False,
        MaterialLocator=materiallocator)
    trackfitter = TrackMasterFitter(
        MeasProvider=measprovider,
        MaterialLocator=materiallocator,
        Extrapolator=masterextrapolator,
        MaxUpdateTransports=1,
        MinNumVPHitsForOutlierRemoval=8,  # default value it too low
        MakeMeasurements=True,
        MakeNodes=True  # need to force a refit
    )
    veloonlyfitter = TrackEventFitter(
        TracksInContainer=selectedTracks, Fitter=trackfitter)
    return veloonlyfitter.TracksOutContainer


# at the moment, define tracks and PVs by hand. Could be changed to centrally defined selection in the future
def getAlignmentTracksAndPVs():
    #from RecoConf.reconstruction_objects import reconstruction
    from PyConf.Algorithms import VeloClusterTrackingSIMDFull

    from RecoConf.hlt1_tracking import make_reco_pvs, make_PatPV3DFuture_pvs, make_VeloClusterTrackingSIMD, all_velo_track_types
    from RecoConf.hlt1_tracking import make_velo_full_clusters, make_VPClus_location_and_offsets, all_velo_track_types, get_track_master_fitter, default_ft_decoding_version, make_PatPV3DFuture_pvs

    from PyConf.application import default_raw_event
    raw = default_raw_event(["VP"])

    # get two types of clusters
    velo_full_clusters = make_velo_full_clusters()
    velo_light_clusters = make_VPClus_location_and_offsets()["Location"]

    from PyConf.Algorithms import VPTrackMonitor, TrackEventFitter
    from PyConf.Algorithms import TrackPV2HalfMonitor, TrackVertexMonitor, TrackMonitor, TrackVPOverlapMonitor
    from PyConf.application import make_odin

    # the cluster monitors are very slow, so better skip them for now

    monitorlist = []

    # run the parts of the reconstruction that we need
    with make_VeloClusterTrackingSIMD.bind(
            algorithm=VeloClusterTrackingSIMDFull):
        velo_tracks = all_velo_track_types()
    pvs = make_PatPV3DFuture_pvs(velo_tracks, use_beam_spot_cut=False)
    unfittedVeloTracks = velo_tracks['v1']
    fittedVeloTracks = refitVeloOnly(unfittedVeloTracks)

    # split the PVs in left and right such that all the left-right constraint comes from the overlap tracks
    splitPVs = False
    if splitPVs:
        from PyConf.Algorithms import PatPV3DFuture, RecVertexListMerger
        from PyConf.Algorithms import Deprecated__TrackListRefiner as TrackListRefiner
        from PyConf.Tools import TrackSelector
        velo_tracks_aside = TrackListRefiner(
            inputLocation=unfittedVeloTracks,
            Selector=TrackSelector(MinNVeloALayers=3)).outputLocation
        velo_tracks_cside = TrackListRefiner(
            inputLocation=unfittedVeloTracks,
            Selector=TrackSelector(MinNVeloCLayers=3)).outputLocation
        pvs_aside = PatPV3DFuture(
            InputTracks=velo_tracks_aside,
            UseBeamSpotRCut=False,
            minClusterMult=4).OutputVerticesName
        pvs_cside = PatPV3DFuture(
            InputTracks=velo_tracks_cside,
            UseBeamSpotRCut=False,
            minClusterMult=4).OutputVerticesName
        pvs['v1'] = RecVertexListMerger(
            InputLocations=[pvs_aside, pvs_cside]).OutputLocation

    # add more monitoring
    monitorlist.append(TrackMonitor(TracksInContainer=fittedVeloTracks))

    monitorlist.append(
        VPTrackMonitor(
            name="VPTrackMonitorFitted",
            TrackContainer=fittedVeloTracks,
            ClusterContainer=velo_light_clusters))
    TrackVPOverlapMonitor
    #monitorlist.append(  VPTrackMonitor(name="VPTrackMonitorAll",TrackContainer=velo_tracks["v1"],
    #                                            ClusterContainer=velo_light_clusters) )
    monitorlist.append(TrackVPOverlapMonitor(TrackContainer=fittedVeloTracks))

    monitorlist.append(
        TrackPV2HalfMonitor(
            TrackContainer=velo_tracks["v1"],
            ODINLocation=make_odin(),
            limPx=5.0,
            limPy=2.0))  #, limDPx=0.2, limDPy=0.2, limDPz=2) )

    monitorlist.append(
        TrackVertexMonitor(
            MaxXPV=40.0,
            MaxYPV=40.0,
            NumTracksPV=1,
            PVContainer=pvs['v1'],
            TrackContainer=velo_tracks["v1"]))

    #select tracks and PVs
    from PyConf.Algorithms import TrackSelectionMerger
    from Humboldt.TrackSelections import selectAndRefitVeloOnly
    from Humboldt.VertexSelections import VPPrimaryVertices
    from Humboldt.TrackSelections import VPBackwardsTracks, VPLongTracks, VPOverlapTracks
    from Humboldt.TrackSelections import VPGoodTracks, VPTileOverlapTracks, VPBeamHaloTracks

    # I prefer to move the selection merger inside
    vpoverlaptracks = VPOverlapTracks(fittedVeloTracks, MaxChi2Dof=-1)
    alignmentTracks = [
        VPGoodTracks(fittedVeloTracks, MaxChi2Dof=10, MinNVeloLayers=4),
        VPTileOverlapTracks(fittedVeloTracks),
        VPBeamHaloTracks(fittedVeloTracks), vpoverlaptracks
    ]

    monitorlist.append(
        TrackVPOverlapMonitor(
            name="OverlapTrackOverlapMonitor", TrackContainer=vpoverlaptracks))

    from PyConf.Algorithms import PrCheckEmptyTracks
    overlaptrackfilter = PrCheckEmptyTracks(
        inputLocation=VPOverlapTracks(unfittedVeloTracks, MaxChi2Dof=-1))
    filters = [overlaptrackfilter]
    selected_pvs = pvs['v1']

    return alignmentTracks, selected_pvs, monitorlist, filters


alignmentTracks, alignmentPVs, monitors, filters = getAlignmentTracksAndPVs()

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()

import os
scenario = os.environ['ALIGNSCENARIO']
print('alignscenario: %s' % scenario)


def getdofs(name, scenario, useEmpty=False):
    import re
    result = re.search("%s:((Tx|Ty|Tz|Rx|Ry|Rz)*)" % name, scenario)
    if result:
        dofs = filter(None, re.split("(Tx|Ty|Tz|Rx|Ry|Rz)", result.group(1)))
        return " ".join(dofs)
    if useEmpty: return "None"
    return ""


velomoduledofs = getdofs("VeloModules", scenario, True)
velohalfdofs = getdofs("VeloHalves", scenario)
velosensordofs = getdofs("VeloSensors", scenario)
print("Velo module dofs: \'%s\'" % velomoduledofs)
print("Velo half dofs: \'%s\'" % velohalfdofs)
print("Velo sensor dofs: \'%s\'" % velosensordofs)

elements.VP("None")
if velohalfdofs:
    elements.VPLeft(velohalfdofs)
    elements.VPRight(velohalfdofs)
if velomoduledofs:
    elements.VPModules(velomoduledofs)
if velosensordofs:
    elements.VPSensors(velosensordofs)

# add survey constraints
from Configurables import SurveyConstraints
surveyconstraints = SurveyConstraints()
surveyconstraints.VP()
if UseSurveyDB:
    surveyconstraints.Constraints += [
        "VP/VPLeft  :  27. 0. 0. 0. 0. 0. : 1.0 0.200 0.2 0.0001 0.0001 0.0001",
        "VP/VPRight : -27. 0. 0. 0. 0. 0. : 1.0 0.200 0.2 0.0001 0.0001 0.0001"
    ]
else:
    # This is rather ugly, but we have trouble separating survey from motion system. This needs to be fixed, but will only do for dd4hep
    surveyconstraints.Constraints += [
        "VP/VPLeft  :  4.13 0. 0. 0. 0. 0. : 1.0 0.200 0.2 0.0001 0.0001 0.0001",
        "VP/VPRight : -1.86 0. 0. 0. 0. 0. : 1.0 0.200 0.2 0.0001 0.0001 0.0001"
    ]
# adjust the module survey uncertainties

# define Lagrange constraints
constraints = []
if velohalfdofs and velohalfdofs != "None":
    constraints.append("VPHalfAverage : VP/VP(Left|Right) : %s" % velohalfdofs)

# WARNING: If using these Lagrange constraints, then only constrain modes that are actually unconstrained.
if velomoduledofs and velomoduledofs != "None":
    velomoduleconstraints = velomoduledofs
    for dof in ["Tx", "Ty", "Tz", "Rz"]:
        if dof in velomoduledofs:
            dof = dof.replace("T", "")
            velomoduleconstraints += " Sz%s Sz2%s" % (dof, dof)
    constraints.append(
        "VPInternalRight : VP/VPRight/Module..WithSupport: %s : total" %
        velomoduleconstraints)
    constraints.append(
        "VPInternalLeft : VP/VPLeft/Module..WithSupport: %s : total" %
        velomoduleconstraints)

from PyConf.application import make_odin
from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList

with createAlignUpdateTool.bind(
        logFile="alignlog_vp_halves_modules.txt"), createAlignAlgorithm.bind(
            xmlWriters=getXMLWriterList(
                'VP', prefix='humb-vp-halves-modules/')):
    runAlignment(
        options,
        surveryConstraints=surveyconstraints,
        lagrangeConstraints=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        elementsToAlign=elements,
        monitorList=monitors,
        filters=filters,
        odin=make_odin())
