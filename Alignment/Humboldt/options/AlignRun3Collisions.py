###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.application import configure_input
from PRConfig.FilesFromDirac import get_access_urls

# set DDDB and CondDB info
options.set_input_and_conds_from_testfiledb(
    'upgrade_Sept2022_minbias_0fb_md_xdigi')

options.evt_max = 100
# options.use_iosvc = True
options.event_store = 'EvtStoreSvc'
options.ntuple_file = "monitoringtuple.root"
options.histo_file = "monitoringhist.root"

# set options above this line!

configure_input(options)

# only configure data flow after this line !
from Humboldt.options import usePrKalman
#usePrKalman=False
from Humboldt.alignment_tracking import make_align_input_besttracks
aligntracks, alignpvs = make_align_input_besttracks(usePrKalman)

# get the odin bank (not sure why we need it here)
from PyConf.application import make_odin
odin = make_odin()

# choose a standard scenario for the alignables
from Humboldt.AlignmentScenarios import configureRun3Alignment
alignscenario = configureRun3Alignment()

from Humboldt.utils import runAlignment
from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList
with createAlignAlgorithm.bind(
        xmlWriters=getXMLWriterList(alignscenario.SubDetectors)):
    runAlignment(
        options,
        surveyConstraints=alignscenario.SurveyConstraints,
        lagrangeConstraints=alignscenario.LagrangeConstraints,
        elementsToAlign=alignscenario.Elements,
        alignmentTracks=aligntracks,
        alignmentPVs=alignpvs,
        #        particles=particles,
        odin=odin)
