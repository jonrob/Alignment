###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.application import configure_input
options.set_input_and_conds_from_testfiledb('upgrade_DC19_01_MinBiasMD')
options.input_files = list(set(options.input_files))  # remove dups
input_files = options.input_files
input_files = [a for a in input_files if 'eoslhcb.cern.ch' in a]
input_files.sort()
options.input_files = input_files

options.evt_max = 10
options.ntuple_file = "testmonitoring.root"
options.histo_file = "testmonitoringhist.root"

# set options above this line!

configure_input(options)

# only configure data flow after this line !

from Humboldt.utils import runAlignment


# at the moment, define tracks and PVs by hand. Could be changed to centrally defined selection in the future
def getAlignmentTracksAndPVs():
    from RecoConf.reconstruction_objects import reconstruction
    from PyConf.Algorithms import VeloClusterTrackingSIMDFull
    # note that the PVs reconstructed by TrackBeamLineVertexFinderSoA do not store the list of associated tracks. Use PatPV3DFuture instead
    from RecoConf.hlt1_tracking import make_reco_pvs, make_PatPV3DFuture_pvs, make_VeloClusterTrackingSIMD
    #, make_reco_pvs.bind( make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)
    #with  reconstruction.bind(from_file=False), make_hlt2_tracks.bind(light_reco=True, use_pr_kf=True, fast_reco=False), make_reco_pvs.bind( make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs):
    from RecoConf.hlt2_global_reco import make_default_reconstruction
    with reconstruction.bind(from_file=False), make_reco_pvs.bind(
            make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs
    ), make_VeloClusterTrackingSIMD.bind(
            algorithm=VeloClusterTrackingSIMDFull):
        #make tracks and PVs
        # TODO: in principle the PVs should be an optional input for the alignment
        reco = reconstruction()
        #hlt2_tracks = make_hlt2_tracks(light_reco=False)
        #best_tracks = hlt2_tracks["Best"]['v1']
        #pvs = make_pvs()
        hlt2_tracks = reco["Tracks"]
        best_tracks = hlt2_tracks
        pvs = reco["PVs_v1"]

        # create input particles, use dummy particles for now
        # TODO: in principle these should be composite particles and be an optional input for the alginment
        #select tracks and PVs

        from PyConf.Algorithms import TrackSelectionMerger
        from Humboldt.TrackSelections import selectAndRefitVeloOnly
        from Humboldt.VertexSelections import VPPrimaryVertices
        from Humboldt.TrackSelections import VPBackwardsTracks, VPLongTracks, VPOverlapTracks

        veloonlyfittedtracks = selectAndRefitVeloOnly(best_tracks)

        selected_LongTracks = VPLongTracks(veloonlyfittedtracks)
        selected_BackwardTracks = VPBackwardsTracks(veloonlyfittedtracks)
        selected_OverlapTracks = VPOverlapTracks(veloonlyfittedtracks)
        alignmentTracks = TrackSelectionMerger(InputLocations=[
            selected_LongTracks, selected_BackwardTracks,
            selected_OverlapTracks
        ]).OutputLocation

        selected_pvs = VPPrimaryVertices(pvs)

        # add track and vertex monitoring
        from PyConf.Algorithms import TrackMonitor, TrackFitMatchMonitor, TrackVertexMonitor, VPTrackMonitor
        from RecoConf.mc_checking import vphits_resolution_checker
        from RecoConf.hlt1_tracking import make_VPClus_hits
        # TODO: should only use selected PVs
        myTrackMonitor = TrackMonitor(TracksInContainer=alignmentTracks)
        myTrackFitMatchMonitor = TrackFitMatchMonitor(
            TrackContainer=alignmentTracks)
        # TODO: check vertices
        myTrackVertexMonitor = TrackVertexMonitor(
            TrackContainer=alignmentTracks, PVContainer=pvs)
        myVPTrackMonitor = VPTrackMonitor(
            TrackContainer=best_tracks, ClusterContainer=make_VPClus_hits())
        monitorlist = [
            myTrackMonitor, myTrackFitMatchMonitor, myTrackVertexMonitor,
            myVPTrackMonitor,
            vphits_resolution_checker()
        ]
        return alignmentTracks, selected_pvs, monitorlist


alignmentTracks, alignmentPVs, monitors = getAlignmentTracksAndPVs()

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxTyTzRxRyRz"
dofsmodules = "TxTyTzRxRyRz"
elements.VPLeft(dofs)
elements.VPRight(dofs)
elements.VPModules(dofsmodules)

# add survey constraints
from Configurables import SurveyConstraints
from PyConf.Tools import AlignChisqConstraintTool
surveyconstraints = SurveyConstraints()
surveyconstraints.VP()
myAlignChisqConstraintTool = AlignChisqConstraintTool(
    Constraints=surveyconstraints.Constraints,
    XmlUncertainties=surveyconstraints.XmlUncertainties,
    XmlFiles=surveyconstraints.XmlFiles)

# define Lagrange constraints
constraints = []
constraints.append("VPHalfAverage : VP/VP(Left|Right) : Tx Ty Tz Rx Ry Rz")
constraints.append(
    "VPInternalRight : VP/VPRight/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy"
)
constraints.append(
    "VPInternalLeft : VP/VPLeft/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy"
)

from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList

with createAlignUpdateTool.bind(
        logFile="alignlog_vp_halves_modules.txt"), createAlignAlgorithm.bind(
            xmlWriters=getXMLWriterList(
                'VP', prefix='humb-vp-halves-modules/')):
    runAlignment(
        options,
        chisqConstraintTool=myAlignChisqConstraintTool,
        lagrangeConstrains=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        elementsToAlign=elements,
        monitorList=monitors)
