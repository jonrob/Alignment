The new, Moore-like configuration for running alignment jobs is being devloped on this branch (`pyconf`).
The main difference is that the Alignment now depends on Moore instead of Phys and import algorithms from there.

To try it out, the stack setup (https://gitlab.cern.ch/rmatev/lb-stack-setup/) can be used.
When setting it up, modify `utils/config.json` to add
```
"gitBranch": {
        "Alignment": "pyconf"
    }
```

The available options are placed in `Alignment/Humboldt/options/`, while utility functions are defined in `Alignment/Humboldt/python/Humboldt/`.
Currently, example configurations for the Velo and SciFi alignment are present. They can be run with the familiar `gaudiiter` and `gaudisplititer` scripts.
Please note that at the moment `gaudiiter` does not work with multiple iterations.
```
Alignment/run Alignment/Alignment/Escher/scripts/gaudiiter.py Alignment/Alignment/Humboldt/options/AlignFTStationsLayers.py -e 10 --humboldt
```
or
```
Alignment/run Alignment/Alignment/Escher/scripts/gaudisplititer.py Alignment/Humboldt/options/AlignVPHalvesModules.py -e 100 -n 2 --humboldt
```
