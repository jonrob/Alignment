#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# first parse all options
from __future__ import print_function
import sys
from optparse import OptionParser
from DDDB.CheckDD4Hep import UseDD4Hep

# Workaround for ROOT-10769
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import cppyy

parser = OptionParser(usage="%prog [options] <opts_file> ...")
parser.add_option(
    "-n",
    "--numiter",
    type="int",
    dest="numiter",
    help="number of iterations",
    default=1)
parser.add_option(
    "--printiter",
    type="int",
    dest="printiter",
    help=
    "number of iterations printed in alignlog / valid when using gaudisplititer",
    default=0)
parser.add_option(
    "-e",
    "--numevents",
    type="int",
    dest="numevents",
    help="number of events",
    default=-1)
parser.add_option(
    "--nthreads", type="int", dest="nthreads", help="side of the thread pool")
parser.add_option(
    "--skipprintsequence",
    action="store_true",
    help="skip printing the sequence")
parser.add_option(
    "--do_not_rewind",
    action="store_true",
    help="do not rewind after each iteration")

parser.add_option(
    "-r",
    "--roothistofile",
    dest="roothistofile",
    help="name of histogram file",
    default=None)

if UseDD4Hep:
    parser.add_option(
        "--aligncond-indir",
        dest="aligncond_indir",
        help="directory to load yaml conditions",
        default=None)

    parser.add_option(
        "--aligncond-outdir",
        dest="aligncond_outdir",
        help="directory to write yaml conditions",
        default='default-yml-dir')
else:
    parser.add_option(
        "--aligndb",
        action='append',
        dest="aligndb",
        help="path to file with CALIBOFF/LHCBCOND/SIMCOND database layer")

(opts, args) = parser.parse_args()

import datetime
t0 = datetime.datetime.now()

# Prepare the "configuration script" to parse (like this it is easier than
# having a list with files and python commands, with an if statements that
# decides to do importOptions or exec)
options = ["importOptions(%r)" % f for f in args]

# The option lines are inserted into the list of commands using their
# position on the command line
#optlines = list(opts.options)
#optlines.reverse() # this allows to avoid to have to care about corrections of the positions
#for pos, l in optlines:
#   options.insert(pos,l)

# "execute" the configuration script generated (if any)
from Gaudi.Configuration import logging
if options:
    g = {}
    l = {}
    exec("from Gaudi.Configuration import *", g, l)
    for o in options:
        logging.debug(o)
        exec(o, g, l)

#options = [ "importOptions(%r)" % f for f in args ]

# set the output histogram file
if opts.roothistofile:
    from Configurables import HistogramPersistencySvc
    HistogramPersistencySvc().OutputFile = opts.roothistofile

if opts.nthreads and opts.nthreads > 1:
    from Configurables import HLTControlFlowMgr
    HLTControlFlowMgr().ThreadPoolSize = opts.nthreads
    from Gaudi.Configuration import allConfigurables
    evtdatasvc = allConfigurables.get('EventDataSvc')
    if evtdatasvc:
        from math import ceil
        evtdatasvc.EventSlots = ceil(1.2 * opts.nthreads)

if not UseDD4Hep:
    # set the database layer
    if opts.aligndb:
        counter = 1
        for db in opts.aligndb:
            from Configurables import CondDB
            CondDB().addLayer(db)
            print("adding layer: ", db)

## Make sure to empty the output directory
if UseDD4Hep and opts.aligncond_outdir:
    import os
    if os.path.exists(opts.aligncond_outdir):
        import shutil
        shutil.rmtree(opts.aligncond_outdir)

## print the sequence
if not opts.skipprintsequence:
    from Configurables import ApplicationMgr
    ApplicationMgr(PrintAlgsSequence=True)

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    DD4hepSvc().UseConditionsOverlay = True

## Instantiate application manager
from GaudiPython.Bindings import AppMgr
appMgr = AppMgr()
evtSel = appMgr.evtSel()
evtSel.OutputLevel = 1

# Load into cppyy the Helper interface to C++ class AlignAlgorithm
if UseDD4Hep:
    cppyy.gbl.gInterpreter.Declare('#define USE_DD4HEP 1')
cppyy.gbl.gSystem.Load("libTAlignmentLib.so")
cppyy.gbl.gInterpreter.Declare('#include "TAlignment/AlignAlgorithmHelper.h"')

#TODO: there is a problem with calling also calling the AlignmentAlgorithm just "Alignment" in the new configuration, see https://gitlab.cern.ch/lhcb/Alignment/-/issues/17
alignAlg = appMgr.algorithm("AlignAlgorithm")
alignAlg.UpdateInFinalize = False
alignAlg.NumberOfIterations = opts.numiter

if UseDD4Hep:
    # load yaml conditons from directory
    if opts.aligncond_indir:
        print("loading yaml conditions")
        cppyy.gbl.LHCb.Alignment.Helper.loadAlignmentConditions(
            alignAlg._ialg, opts.aligncond_indir)

for i in range(opts.numiter):
    print("Iteration nr: ", i)
    # rewind
    if not opts.do_not_rewind and i > 0:
        if UseDD4Hep:
            # make sure to empty event store, otherwise get stuck in mutex lock
            datasvc = appMgr.service('EventDataSvc', 'IDataManagerSvc')
            if datasvc: datasvc.clearStore()
        # rewind to first event. this is a lazy solution
        try:
            appMgr.service('HLTControlFlowMgr').reinitialize()
        except:
            pass
        try:
            appMgr.evtSel().rewind()
        except:
            pass
        # note that appMgr will be restarted by the run method

    # in dd4hep, load alignment conditions from written files in between iterations
    if UseDD4Hep and i > 0:
        cppyy.gbl.LHCb.Alignment.Helper.loadAlignmentConditions(
            alignAlg._ialg, opts.aligncond_outdir)
        appMgr.service('EventDataSvc').reinitialize()

    # steer the monitor sequence depending on the iteration
    appMgr.algorithm('AlignMonitorSeq').Enable = (i == 0)
    appMgr.algorithm('Moni').Enable = (i == 0)
    if opts.numiter > 1:
        appMgr.algorithm('AlignPostMonitorSeq').Enable = (
            i == opts.numiter - 1)

    # event loop
    sys.stdout.flush()  # avoid mixing the output of python and Gaudi
    appMgr.run(opts.numevents)

    if not UseDD4Hep:
        # make sure to empty event store, otherwise get stuck in mutex lock
        datasvc = appMgr.service('EventDataSvc', 'IDataManagerSvc')
        if datasvc: datasvc.clearStore()

    # perform the update of the geometry. opts.printiter is workaround so we have correctly printed iteration number in gaudisplititer, only used there
    cppyy.gbl.LHCb.Alignment.Helper.updateGeometry(
        alignAlg._ialg, opts.printiter + i, opts.numiter)

    # write alignment conditions back to file(s)
    if UseDD4Hep:
        cppyy.gbl.LHCb.Alignment.Helper.writeAlignmentConditions(
            alignAlg._ialg, opts.aligncond_outdir)
    else:
        cppyy.gbl.LHCb.Alignment.Helper.writeAlignmentConditions(
            alignAlg._ialg)

    # if not last round, clear equations and reload new geometry
    if i < opts.numiter - 1:
        cppyy.gbl.LHCb.Alignment.Helper.clearEquationsAndReloadGeometry(
            alignAlg._ialg)

# make sure to call stop such that AlignAlgorithm writes the output file if configured to do so.
appMgr.stop()

#exit the appmgr for finalize
appMgr.exit()

# print the time it took to execute the script
print("Wall clock time [seconds]:",
      (datetime.datetime.now() - t0).total_seconds())
