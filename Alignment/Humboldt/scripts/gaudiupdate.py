#!/usr/bin/env python3
###############################################################################
# (c) Copyright 2018-2021 CERN for the benefit of the LHCb Collaboration      #
###############################################################################
import os
import sys
import zmq
from Configurables import ApplicationMgr
from Configurables import Gaudi__RootCnvSvc as RootCnvSvc

from PyConf.application import (configure, setup_component, ComponentConfig,
                                ApplicationOptions)
from threading import Thread
from time import sleep
import ctypes
import argparse
from GaudiPython.Bindings import AppMgr, gbl
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import AlignIterator

from Moore import options
from PyConf.application import configure_input
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    DD4hepSvc().UseConditionsOverlay = True

from PyConf.application import default_raw_event

options = ApplicationOptions(_enabled=False)
options.simulation = True
options.data_type = 'Upgrade'
options.input_type = 'MDF'
# same tags as in analyzer
options.dddb_tag = 'upgrade/dddb-20220705'
options.conddb_tag = 'upgrade/sim-20220705-vc-md100'

default_raw_event.global_bind(raw_event_format=0.5)

options.finalize()
config = ComponentConfig()

#
extSvc = ["ToolSvc", "AuditorSvc", "ZeroMQSvc"]

ApplicationMgr().EvtSel = "NONE"
ApplicationMgr().ExtSvc += extSvc

# Copeid from PyConf.application.configure_input
config.add(
    setup_component(
        'DDDBConf', Simulation=options.simulation, DataType=options.data_type))
config.add(
    setup_component(
        'CondDB',
        Upgrade=True,
        Tags={
            'DDDB': options.dddb_tag,
            'SIMCOND': options.conddb_tag,
        }))

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxTyTzRxRyRz"
dofsmodules = "TxTyTzRxRyRz"
elements.VPLeft(dofs)
elements.VPRight(dofs)

# add survey constraints
from Configurables import SurveyConstraints
from PyConf.Tools import AlignChisqConstraintTool
surveyconstraints = SurveyConstraints()
surveyconstraints.VP()

from Humboldt.utils import createAlignUpdateTool
# define Lagrange constraints
constraints = []
constraints.append("VPHalfAverage : VP/VP(Left|Right) : Tx Ty Tz Rx Ry Rz")
constraints.append(
    "VPInternalRight : VP/VPRight/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy"
)
constraints.append(
    "VPInternalLeft : VP/VPLeft/Module..WithSupport: Tx Ty Tz Rx Ry Rz Szx Szy"
)
myUpdateTool = createAlignUpdateTool(
    surveyConstraints=surveyconstraints,
    lagrangeConstraints=constraints,
    logFile='alignlog_iterator.txt')
myAlignIterator = AlignIterator(
    DELHCbPath="/world/BeforeMagnetRegion/VP:DetElement-Info-IOV"
    if UseDD4Hep else '/dd/Structure/LHCb',
    Elements=list(elements),
    DerivativeFile=os.path.join(
        os.getenv("PREREQUISITE_0", ""), "humb-vp-halves-modules-derivs"),
    UpdateTool=myUpdateTool)

control_flow = [myAlignIterator]
cf_node = CompositeNode(
    "aling_iterator",
    control_flow,
    combine_logic=NodeLogic.LAZY_AND,
    force_order=True)

config.update(configure(options, cf_node))

# Start Gaudi and get the AllenUpdater service
gaudi = AppMgr()
sc = gaudi.initialize()
if not sc.isSuccess():
    sys.exit("Failed to initialize AppMgr")

gaudi.start()
gaudi.run(1)
gaudi.stop()
