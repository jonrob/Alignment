#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from optparse import OptionParser
import shutil
import sys

from DDDB.CheckDD4Hep import UseDD4Hep

parser = OptionParser(usage="%prog [options] <opts_file> ...")
parser.add_option(
    "-n",
    "--numiter",
    type="int",
    dest="numiter",
    help="number of iterations",
    default=3)
parser.add_option(
    "-f",
    "--firstiter",
    type="int",
    dest="firstiter",
    help="first iterations",
    default=0)
parser.add_option(
    "-e",
    "--numevents",
    type="int",
    dest="numevents",
    help="number of events",
    default=-1)
parser.add_option(
    "-p",
    "--nthreads",
    type="int",
    dest="nthreads",
    help="size of thread pool")
parser.add_option(
    "-b",
    "--baseDir",
    type='string',
    dest="basedir",
    help="directory to store output",
    default='AlignmentResults')
parser.add_option(
    "--clean",
    action="store_true",
    dest="clean",
    help="clean the output directory if it exists",
    default=False)
parser.add_option(
    "-r",
    "--roothistofile",
    dest="histofile",
    help="name of histogram file",
    default="histograms.root")
parser.add_option("--dryrun", action="store_true", help="dont do anything")

parser.add_option(
    "--humboldt",
    action="store_true",
    help="use Humboldt option files",
    default=False)

if not UseDD4Hep:
    parser.add_option(
        "--aligndb",
        action='append',
        dest="aligndb",
        help=
        "path to file with CALIBOFF/LHCBCOND/SIMCOND database layer that will only be used in first iter"
    )
    parser.add_option(
        "--conddb",
        action='append',
        dest="conddb",
        help=
        "path to file with CALIBOFF/LHCBCOND/SIMCOND database layer that will be used in all iterations"
    )
    parser.add_option(
        "-x",
        "--xmlprefix",
        type='string',
        dest="xmlprefix",
        help="path where constants are written to",
        default='xml')
else:
    parser.add_option(
        "--aligncond-indir",
        dest="aligncond_indir",
        help="directory to load yaml conditions for first iteration",
        default=None)

(opts, args) = parser.parse_args()

import os
rundir = os.getcwd()
outputdir = rundir + "/" + opts.basedir

if not os.path.isdir(outputdir):
    os.mkdir(outputdir)
elif opts.clean:
    shutil.rmtree(outputdir)
    os.mkdir(outputdir)
os.chdir(outputdir)
print("Changing to outputdir")

for i in range(opts.firstiter, opts.numiter):
    print("Iteration nr %d out of %d: " % (i, opts.numiter))
    # make the directory from where to run the job
    iterdir = os.path.join(outputdir, 'Iter' + str(i))
    if os.path.isdir(iterdir):
        print(f"Directory {iterdir} exists. Will skip this iteration.")
        continue
    os.mkdir(iterdir)
    os.chdir(iterdir)

    # create the command line options passed to the lower level script
    theseoptions = ' --numevents ' + str(opts.numevents)
    if opts.nthreads: theseoptions += ' --nthreads ' + str(opts.nthreads)
    if opts.histofile: theseoptions += ' --r ' + opts.histofile
    theseoptions += ' --printiter ' + str(i)

    # beyond the first iteration, add the input database as an option
    if not UseDD4Hep:
        if opts.conddb:
            for db in opts.conddb:
                theseoptions += ' --aligndb ' + db

    if i > 0:
        previterdir = outputdir + '/Iter' + str(i - 1)
        if UseDD4Hep:
            theseoptions += ' --aligncond-indir ' + previterdir + "/yaml"
        else:
            previterdb = os.path.join(previterdir, opts.xmlprefix)
            if not os.path.exists(os.path.join(previterdb, '.git')):
                os.chdir(previterdb)
                os.system("git init -q")
                os.chdir(iterdir)
            theseoptions += ' --aligndb ' + previterdb
    else:
        if UseDD4Hep:
            if opts.aligncond_indir:
                theseoptions += ' --aligncond-indir ' + opts.aligncond_indir
        else:
            if opts.aligndb:
                for db in opts.aligndb:
                    theseoptions += ' --aligndb ' + db

    if UseDD4Hep:
        curiterdir = outputdir + '/Iter' + str(i)
        theseoptions += ' --aligncond-outdir ' + curiterdir + "/yaml"

    if opts.humboldt:
        theseoptions += ' --humboldt '

    # add the remaining options. if it is a python file, make a copy and put it in the directory
    os.chdir(rundir)
    for a in args:
        if os.path.isfile(a):
            # copy the file
            from shutil import copy2
            copy2(a, iterdir)
            # split off the tail and pass that as argument
            (head, tail) = os.path.split(a)
            theseoptions += ' ' + tail
        else:
            # just copy the argument
            theseoptions += ' ' + a

    # run the job
    os.chdir(iterdir)
    thiscommand = 'gaudiiter.py' + theseoptions + ' 2>&1 | tee logfile.txt'
    print('command: %s\n' % thiscommand)
    if not opts.dryrun:
        sys.stdout.flush()
        sys.stderr.flush()
        os.system(thiscommand)
        os.system('gzip logfile.txt')
        # keep only the last version of the derivatives. they take too much space.
        # os.system( 'mv -f myderivatives.dat ..')
    os.chdir(rundir + '/' + opts.basedir)

# create a single alignlog file
os.system('rm -f alignlog.txt')
os.system('cat $(ls -1rt Iter*/alignlog*.txt) > alignlog.txt')
os.chdir(rundir)
