###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def VPPrimaryVertices(input_pvs):
    from PyConf.Algorithms import VertexListRefiner
    selected_pvs = VertexListRefiner(
        name='VertexListRefinerVPPrimaryVertices',
        MaxChi2PerDoF=5,
        MinNumTracks=15,
        MaxNumTracks=50,
        MinNumLongTracks=0,
        InputLocation=input_pvs).OutputLocation
    return selected_pvs


# dummy empty PV container to use for mandatory input of alignment
def DummyPVs():
    from PyConf.Algorithms import RecVertexEmptyProducer
    dummypvs = RecVertexEmptyProducer().Output
    return dummypvs
