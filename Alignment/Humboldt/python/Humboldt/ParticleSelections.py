###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


# dummy empty particle container to use for mandatory input of alignment
def DummyParticles():
    from PyConf.Algorithms import ParticlesEmptyProducer
    dummyparticles = ParticlesEmptyProducer().Output
    return dummyparticles


def defaultHLTD0Selection(vertices_v2):
    #TODO: get particles from HLT SelReports instead of rebuilding
    # NOTE: selections match those in HLT1D2KPi
    import Functors as F
    from Hlt2Conf.standard_particles import make_long_pions, make_long_kaons
    from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
    from GaudiKernel.SystemOfUnits import MeV, mm
    from Functors.math import in_range

    loosepions = make_long_pions()
    loosekaons = make_long_kaons()

    ptmin = 1400 * MeV
    minIP = 0.06 * mm

    pions = ParticleFilter(
        loosepions,
        F.FILTER(
            F.require_all(F.PT > ptmin,
                          F.MINIPCUT(IPCut=minIP, Vertices=vertices_v2))))

    kaons = ParticleFilter(
        loosekaons,
        F.FILTER(
            F.require_all(F.PT > ptmin,
                          F.MINIPCUT(IPCut=minIP, Vertices=vertices_v2))))

    particles = [pions, kaons]

    combination_code = F.require_all(
        in_range(1760 * MeV, F.MASS, 1960 * MeV),
        F.SUM(F.PT) > 1200 * MeV,
        F.MAXDOCACUT(0.1 * mm),
    )
    vertex_code = F.require_all(F.CHI2DOF < 10.,
                                F.BPVETA(vertices_v2) > 2,
                                F.BPVETA(vertices_v2) < 5)

    combinedD0 = ParticleCombiner(
        [pions, kaons],
        name="AlignHLTD0",
        DecayDescriptor="[D0 -> pi+ K-]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
    return combinedD0


def defaultDetatchedJpsi(vertices_v2):
    #TODO: get particles from HLT SelReports instead of rebuilding
    # NOTE: selections match those in HLT... etc

    import Functors as F
    from Hlt2Conf.standard_particles import make_long_muons
    from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter, require_all
    from GaudiKernel.SystemOfUnits import MeV, mm
    from Functors.math import in_range

    loosemuons = make_long_muons()

    ptmin = 500 * MeV
    minIPchi2 = 6
    maxvertexchi2 = 6

    muons = ParticleFilter(
        loosemuons,
        F.FILTER(
            require_all(
                F.PT > ptmin,
                F.MINIPCHI2CUT(IPChi2Cut=minIPchi2, Vertices=vertices_v2))))

    particles = [muons, muons]

    combination_code = require_all(
        in_range(2847 * MeV, F.MASS, 3347 * MeV),
        #        F.Z > -300 * mm
    )
    vertex_code = require_all(
        F.CHI2DOF < maxvertexchi2,

        # Think carefully about whether this is the right functor
        #        F.BPVETA(vertices_v2) > 2,
        #        F.BPVETA(vertices_v2) < 5
    )

    detatchedJpsi = ParticleCombiner([muons, muons],
                                     name="AlignHLTdetatchJPsi",
                                     DecayDescriptor="J/psi(1S) -> mu+ mu-",
                                     CombinationCut=combination_code,
                                     CompositeCut=vertex_code)

    return detatchedJpsi


def defaultHLTZ(vertices_v2):
    #TODO: get particles from HLT SelReports instead of rebuilding
    # NOTE: selections match those in HLT... etc

    import Functors as F
    from Hlt2Conf.standard_particles import make_long_muons
    from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter, require_all
    from GaudiKernel.SystemOfUnits import MeV, mm
    from Functors.math import in_range

    loosemuons = make_long_muons()

    ptmin = 300 * MeV
    pmin = 6000 * MeV
    minIPchi2 = 0
    maxvertexchi2 = 25
    maxdoca = 0.2

    muons = ParticleFilter(
        loosemuons,
        F.FILTER(
            require_all(
                F.PT > ptmin, F.P > pmin,
                F.MINIPCHI2CUT(IPChi2Cut=minIPchi2, Vertices=vertices_v2))))

    particles = [muons, muons]

    combination_code = require_all(
        in_range(78690 * MeV, F.MASS, 103690 * MeV),
        #        F.Z > -300 * mm,
        F.MAXDOCACUT(maxdoca))
    vertex_code = require_all(
        F.CHI2DOF < maxvertexchi2,
        # Think carefully about whether this is the right functor
        #        F.BPVETA(vertices_v2) > 2,
        #        F.BPVETA(vertices_v2) < 5
    )

    defaultZ = ParticleCombiner([muons, muons],
                                name="AlignHLTZ",
                                DecayDescriptor="Z0 -> mu+ mu-",
                                CombinationCut=combination_code,
                                CompositeCut=vertex_code)

    return defaultZ
