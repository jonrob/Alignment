###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from functools import partial
from RecoConf.hlt1_tracking import (
    all_velo_track_types,
    make_VeloClusterTrackingSIMD_hits,
    make_VeloClusterTrackingSIMD,
    make_RetinaClusters,
    get_global_measurement_provider,
    make_velo_full_clusters,
    make_reco_pvs,
    make_PatPV3DFuture_pvs,
)
from PyConf.Algorithms import (
    PrKalmanFilter_Velo,
    TrackSelectionMerger,
    VeloClusterTrackingSIMDFull,
    PatPVLeftRightFinderMerger,
    VeloRetinaClusterTrackingSIMDFull,
    PrKalmanFilter_noUT,
    PrStoreUTHitEmptyProducer,
    VPRetinaFullClusterDecoder,
    VeloRetinaClusterTrackingSIMD,
)

from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from RecoConf.hlt1_muonid import make_muon_hits
from PyConf.Tools import TrackLinearExtrapolator
from RecoConf.core_algorithms import make_unique_id_generator
from Humboldt.TrackSelections import VPBackwardTracks, VPGoodTracks


def make_pvs(inputtracks):
    return PatPVLeftRightFinderMerger(InputTracks=inputtracks).OutputVertices


def make_empty_ut_hit_for_meas_provider():
    """Creates an empty container of UT hits, used for the no UT scenario.
    Necessary to run with DD4hep while Ut is not there yet. Could be moved to Moore

    Returns:
            DataHandle: PrStoreUTHitEmptyProducer' Output.

    """
    return PrStoreUTHitEmptyProducer().Output


def make_fitted_velotracks(usePrKalman=True,
                           MaxOutlierIterations=2,
                           MaxChi2Dof=5,
                           MinNVeloLayers=4):
    with make_VeloClusterTrackingSIMD.bind(
            algorithm=VeloRetinaClusterTrackingSIMDFull):
        vp_hits = make_VeloClusterTrackingSIMD_hits()
        velo_tracks = all_velo_track_types()
    backward_velo_tracks = velo_tracks["Pr::backward"]
    forward_velo_tracks = velo_tracks["Pr"]

    if usePrKalman:
        kf_template = partial(
            PrKalmanFilter_Velo,
            MaxOutlierIterations=MaxOutlierIterations,
            HitsVP=vp_hits,
            FillFitResult=True,
            ClassicSmoothing=True,
            MinNumVPHitsForOutlierRemoval=4,
            ReferenceExtrapolator=TrackLinearExtrapolator(),
            InputUniqueIDGenerator=make_unique_id_generator())

        fitted_velo_bwd = kf_template(
            name="PrKalmanFilter_VeloBwd",
            Input=backward_velo_tracks).OutputTracks
        fitted_velo_fwd = kf_template(
            name="PrKalmanFilter_VeloFwd",
            Input=forward_velo_tracks).OutputTracks

        selected_bwd_tracks = VPBackwardTracks(
            fitted_velo_bwd,
            MaxChi2Cut=MaxChi2Dof,
            MinNVeloLayers=MinNVeloLayers)
        selected_fwd_tracks = VPGoodTracks(
            fitted_velo_fwd,
            MaxChi2Dof=MaxChi2Dof,
            MinNVeloLayers=MinNVeloLayers)
        velotracks = TrackSelectionMerger(
            InputLocations=[selected_bwd_tracks, selected_fwd_tracks
                            ]).OutputLocation
    else:
        from PyConf.Algorithms import TrackEventFitter
        from PyConf.Tools import (
            TrackMasterFitter,
            TrackSimpleExtraSelector,
            TrackMasterExtrapolator,
        )
        measprovider = get_global_measurement_provider(
            ignoreUT=True,
            ignoreFT=True,
            ignoreMuon=True,
            velo_hits=make_RetinaClusters,
            ut_hits=make_empty_ut_hit_for_meas_provider)
        #materiallocator = SimplifiedMaterialLocator() this will not work with dd4hep
        extrapolatorselector = TrackSimpleExtraSelector(
            ExtrapolatorName="TrackLinearExtrapolator")
        masterextrapolator = TrackMasterExtrapolator(
            ExtraSelector=extrapolatorselector,
            ApplyEnergyLossCorr=False,
            ApplyElectronEnergyLossCorr=False)
        trackfitter = TrackMasterFitter(
            MeasProvider=measprovider,
            Extrapolator=masterextrapolator,
            MaxUpdateTransports=1,
            MinNumVPHitsForOutlierRemoval=4,
            MaxNumberOutliers=MaxOutlierIterations,
            MakeMeasurements=True,
            MakeNodes=True,  # need to force a refit
            FastMaterialApproximation=True)
        veloonlyfitter = TrackEventFitter(
            TracksInContainer=velo_tracks["v1"], Fitter=trackfitter)
        velotracks = VPGoodTracks(
            veloonlyfitter.TracksOutContainer,
            MaxChi2Dof=MaxChi2Dof,
            MinNVeloLayers=MinNVeloLayers)

    return velotracks


def make_align_vp_input(usePrKalman=True,
                        MaxOutlierIterations=2,
                        MaxChi2Dof=5,
                        MinNVeloLayers=4):

    align_tracks = make_fitted_velotracks(
        usePrKalman=usePrKalman,
        MaxOutlierIterations=MaxOutlierIterations,
        MaxChi2Dof=MaxChi2Dof,
        MinNVeloLayers=MinNVeloLayers)
    align_pvs = make_pvs(align_tracks)
    return align_tracks, align_pvs


def make_align_input_besttracks(usePrKalman=True):
    # it seems that we have little choice but just to set up the entire reco sequence ourselves. the moore stuff is close to impossible to work with.
    # note that the PVs reconstructed by TrackBeamLineVertexFinderSoA do not store the list of associated tracks. Use PatPV3DFuture instead
    from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT
    with make_VeloClusterTrackingSIMD.bind(algorithm=VeloClusterTrackingSIMDFull),\
        PrKalmanFilter_noUT.bind(FillFitResult=True,ClassicSmoothing=True):
        hlt2_tracks = make_hlt2_tracks_without_UT(use_pr_kf=usePrKalman)

    # this is a bit funny, but we need fitted velo tracks. so we filter from the best track container what we need, then add our own fitted velo tracks
    from Humboldt.TrackSelections import GoodLongTracks
    goodlongtracks = GoodLongTracks(hlt2_tracks['BestLong']['v1'])
    goodvelotracks = make_fitted_velotracks(usePrKalman=usePrKalman)
    # combine using AlignTrackCloneRemover
    from PyConf.Algorithms import AlignTrackCloneRemover
    goodfittedtracks = AlignTrackCloneRemover(
        name="AlignBestCloneRemover",
        InputLocations=[goodlongtracks, goodvelotracks]).OutputLocation

    # now make the PVs with these tracks
    pvs = make_pvs(goodfittedtracks)

    return goodfittedtracks, pvs


def make_scifi_tracks_and_particles_prkf():
    with reconstruction.bind(from_file=False),\
     PrKalmanFilter_noUT.bind(FillFitResult=True,ClassicSmoothing=True),\
     make_light_reco_pr_kf_without_UT.bind(skipRich=False, skipCalo=False, skipMuon=False),\
     make_muon_hits.bind(geometry_version=3),\
     make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClusterDecoder),\
     make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD),\
     make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
     get_global_measurement_provider.bind(velo_hits=make_RetinaClusters),\
     hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT):

        reco = hlt2_reconstruction()
        hlt2_tracks = reco['Tracks']
        best_tracks = hlt2_tracks
        pvs = reco["PVs_v1"]
        particlepvs = reco["PVs"]
        from PyConf.application import default_raw_banks
        from PyConf.application import make_odin
        odin = make_odin()
        from Humboldt.ParticleSelections import defaultHLTD0Selection
        particles = defaultHLTD0Selection(particlepvs)

        from PyConf.Tools import TrackSelector
        from PyConf.Algorithms import VertexListRefiner, TrackSelectionMerger
        from Humboldt.TrackSelections import GoodLongTracks
        from Humboldt.VertexSelections import VPPrimaryVertices
        selected_tracks = GoodLongTracks(best_tracks)
        track_name = "GoodLongTracks"
        alignmentTracks = TrackSelectionMerger(
            InputLocations=[selected_tracks]).OutputLocation
        selected_pvs = VPPrimaryVertices(pvs)
        from PyConf.Algorithms import TrackMonitor_PrKalman, TrackFitMatchMonitor_PrKalman, FTTrackMonitor_PrKalman, TrackVertexMonitor, TrackParticleMonitor, ParticleMassMonitor
        from GaudiKernel.SystemOfUnits import MeV
        myFTTrackMonitor = FTTrackMonitor_PrKalman(
            TracksInContainer=alignmentTracks)
        myTrackVertexMonitor = TrackVertexMonitor(
            TrackContainer=alignmentTracks, PVContainer=pvs)
        myParticleMonitor = ParticleMassMonitor(
            name="defaultD0MassMonitor",
            histoName="D0Mass",
            Particles=particles,
            MassMean=1864.84 * MeV,
            MassSigma=12 * MeV,
            Bins=50)

        monitorlist = [
            myFTTrackMonitor, myTrackVertexMonitor, myParticleMonitor
        ]
        return alignmentTracks, selected_pvs, particles, odin, monitorlist
