###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TAlignment.Alignables import *
from TAlignment.SurveyConstraints import *
from DDDB.CheckDD4Hep import UseDD4Hep


# scenario used for prompt alignment
class AlignmentScenario():
    __slots__ = {
        "Name": ""  # name of the scenario
        ,
        "Elements": []  # list with alignment elements
        ,
        "LagrangeConstraints": []  # list with lagrange constraints
        ,
        "SurveyConstraints": None  # survey constraints
        ,
        "SubDetectors": []  # list of subdetectors written to xml
    }

    def __init__(self, aName="AlignmentScenario", **kwargs):
        for (a, b) in self.__slots__.items():
            setattr(self, a, b)
        self.Name = aName
        for (a, b) in kwargs:
            setattr(self, a, b)
        #self.Elements = Elements
        #self.LagrangeConstraints = lagrangeconstraints
        #self.SurveyConstraints = surveyconstraints
        #self.SubDetectors = subdetectors


def configureRun3Alignment(fixQOverPBias=True):
    # just a first implementation to test some essential things

    setup = AlignmentScenario("Run3Alignment")
    setup.SubDetectors += ['VP', 'UT', 'FT']

    # define the alignment elements
    elements = Alignables()
    elements.VP("None")
    elements.VPRight("Tx")
    elements.VPLeft("Tx")
    elements.VPModules("Tx")
    if UseDD4Hep: elements.FTHalfLayers("Tx")
    elements.FTModules("Tx")
    #elements.UTLayers("Tx")
    setup.Elements = elements

    # add some survey constraints (just fixing to nominal, right now)
    surveyconstraints = SurveyConstraints()
    if UseDD4Hep:
        surveyconstraints.VP(ver='2023_dd4hep')
        surveyconstraints.FT(ver='data20221115dd4hep')
    else:
        surveyconstraints.VP(ver='latest')
        surveyconstraints.FT(ver='MC')
    surveyconstraints.UT()
    setup.SurveyConstraints = surveyconstraints

    # make sure that the velo stays where it was
    constraints = []
    constraints.append("VeloHalfAverage : VP/VP(Left|Right) : Tx")
    # fix the global shearing in the velo
    constraints.append("VeloModuleShearing : VP/VPLeft/Module.{1,2}" +
                       ("WithSupport : Tx" if not UseDD4Hep else " : Tx"))
    # fix the q/p scale by not moving T in X. note that you do not
    # want to do this if you use D0 in the alignment
    if fixQOverPBias:
        constraints.append("FT3X : FT/T3/LayerX2 : Tx")
    # there is still a spectrometer weak mode left, but I cannot find it :-(
    setup.LagrangeConstraints = constraints

    return setup


# define additional scenarios below
def configureVPHalfAlignment():
    setup = AlignmentScenario("VPHalfAlignment")
    setup.WriteCondSubDetList += ['VP']

    # define the alignment elements
    elements = Alignables()
    elements.Velo("None")
    elements.VeloRight("TxTyTzRxRyRz")
    elements.VeloLeft("TxTyTzRxRyRz")
    setup.Elements = elements

    # make sure that the velo stays where it was
    # define Lagrange constraints
    setup.Constraints = [
        "VPHalfAverage : .*?VP(Left|Right) : Tx Ty Tz Rx Ry Rz"
    ]

    # tweak the survey a little bit to fix the z-scale to survey
    surveyconstraints = SurveyConstraints()
    if UseDD4Hep:
        surveyconstraints.VP(ver='2023_dd4hep')
    else:
        surveyconstraints.VP(ver='latest')
    setup.SurveyConstraints = surveyconstraints
    return setup


def configureVPModuleAlignment(halfdofs="TxTyTzRxRyRz", moduledofs="TxTyRz"):
    '''
    This should be the default alignment for the Automatic alignment procedure
    Align 2-halves for all degree of freedom and
    Modues only for only the main degrees of freedom Tx Ty Rz
    Constrain the global Velo position and two modules in each half
    '''
    setup = AlignmentScenario("VPModuleAlignment")
    setup.SubDetectors += ['VP']

    elements = Alignables()
    elements.VP("None")
    elements.VPRight(halfdofs)
    elements.VPLeft(halfdofs)
    elements.VPModules(moduledofs)
    setup.Elements += list(elements)

    # Constraints
    surveyconstraints = SurveyConstraints()
    if UseDD4Hep:
        surveyconstraints.VP(ver='2023_dd4hep')
    else:
        surveyconstraints.VP(ver='latest')
    #surveyconstraints.Constraints += [
    #    "Velo      : 0 0 0 -0.0001 0 -0.0001 : 0.2 0.2 0.2 0.0001 0.0001 0.001",
    #    "Velo/Velo(Right|Left) : 0 0 0 0 0 0 : 10 1 0.4 0.01 0.01 0.001"
    #]
    setup.SurveyConstraints = surveyconstraints

    # make sure that the velo stays where it was. Important note: the
    # dofs here must match the ones that we actually align for. If you
    # specify too many, things will go rather wrong.
    constraints = []
    constraints.append("VPHalfAverage : .*?VP(Left|Right) : Tx Ty Tz Rx Ry Rz")

    if moduledofs and moduledofs != "None":
        velomoduleconstraints = ""
        for dof in ["Tx", "Ty", "Tz", "Rz", "Rx", "Ry"]:
            if dof in moduledofs:
                tmpdof = dof.replace("T", "")
                velomoduleconstraints += " %s Sz%s Sz2%s" % (dof, tmpdof,
                                                             tmpdof)
        for side in ['Right', 'Left']:
            constraints.append(
                "VPInternal%s : .*?%s/Module(..WithSupport|..): %s" %
                (side, side, velomoduleconstraints))
    setup.LagrangeConstraints = constraints

    return setup
