###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.tonic import configurable
from PyConf.Tools import AlignUpdateTool, EigenDiagSolvTool, AlignChisqConstraintTool
from PyConf.application import configure
from RecoConf.event_filters import require_gec
from DDDB.CheckDD4Hep import UseDD4Hep


@configurable
def createAlignAlgorithm(tracks,
                         elements,
                         updatetool,
                         name='AlignAlgorithm',
                         updateInFinalize=False,
                         outputlevel=3,
                         nIterations=1,
                         usecorrelations=True,
                         chi2Outlier=10000,
                         outputDataFile='',
                         onlineMode=False,
                         runList=[],
                         useLocalFrame=True,
                         xmlWriters=[],
                         inputDataFiles=[],
                         pvs=None,
                         particles=None,
                         odin=None):
    from PyConf.Algorithms import AlignAlgorithm

    if particles is None:
        from .ParticleSelections import DummyParticles
        particles = DummyParticles()
    if pvs is None:
        from .VertexSelections import DummyPVs
        pvs = DummyPVs()
    if odin is None:
        from PyConf.application import make_odin
        odin = make_odin()
    return AlignAlgorithm(
        name=name,
        VertexLocation=pvs,
        ODINLocation=odin,
        TracksLocation=tracks,
        ParticleLocation=particles,
        UpdateTool=updatetool,
        Elements=list(elements),
        UpdateInFinalize=updateInFinalize,
        OutputLevel=outputlevel,
        NumberOfIterations=nIterations,
        UseCorrelations=usecorrelations,
        Chi2Outlier=chi2Outlier,
        OutputDataFile=outputDataFile,
        OnlineMode=onlineMode,
        RunList=runList,
        UseLocalFrame=useLocalFrame,
        XmlWriters=xmlWriters,
        InputDataFiles=inputDataFiles)


@configurable
def createAlignUpdateTool(name='updateTool',
                          surveyConstraints=None,
                          matrixSolverTool=EigenDiagSolvTool(public=True),
                          lagrangeConstraints=[],
                          minNumberOfHits=10,
                          usePreconditioning=True,
                          logFile="alignlog.txt",
                          useWeightedAverage=False,
                          isPublic=True):

    from PyConf.Tools import AlignChisqConstraintTool
    surveyconstrainttool = AlignChisqConstraintTool()
    if surveyConstraints:
        surveyconstrainttool = AlignChisqConstraintTool(
            Constraints=surveyConstraints.Constraints,
            XmlUncertainties=surveyConstraints.XmlUncertainties,
            XmlFiles=surveyConstraints.XmlFiles,
            ElementJoints=surveyConstraints.ElementJoints)

    updatetool = AlignUpdateTool(
        name=name,
        MatrixSolverTool=matrixSolverTool,
        SurveyConstraintTool=surveyconstrainttool,
        Constraints=lagrangeConstraints,
        MinNumberOfHits=minNumberOfHits,
        UsePreconditioning=usePreconditioning,
        LogFile=logFile,
        UseWeightedAverage=useWeightedAverage,
        public=isPublic)
    return updatetool


def runAlignment(options,
                 surveyConstraints,
                 lagrangeConstraints,
                 alignmentTracks,
                 elementsToAlign,
                 odin=None,
                 alignmentPVs=None,
                 particles=None,
                 monitorList=[],
                 apply_gec=True,
                 filters=[],
                 updateInFinalize=False,
                 usePrKalman=False):

    # make sure to merge track lists, if multiple lists are provided
    alignTrackLists = alignmentTracks
    if not isinstance(alignTrackLists, list):
        alignTrackLists = [alignmentTracks]
    # make sure to merge them and removes clones
    from PyConf.Algorithms import AlignTrackCloneRemover
    alignmentTracks = AlignTrackCloneRemover(
        InputLocations=alignTrackLists).OutputLocation

    # automatically create monitoring algorithms for each input track list
    from PyConf.Algorithms import TrackFitMatchMonitor, TrackMonitor, TrackVPOverlapMonitor

    for trklist in alignTrackLists:
        monitorList.append(
            TrackMonitor(
                name=trklist.producer.name + "TrackMonitor",
                TracksInContainer=trklist))
        monitorList.append(
            TrackFitMatchMonitor(
                name=trklist.producer.name + "TrackFitMatchMonitor",
                TrackContainer=trklist))
        monitorList.append(
            TrackVPOverlapMonitor(
                name=trklist.producer.name + "TrackVPOverlapMonitor",
                TrackContainer=trklist))

    # create monitor for PVs
    if alignmentPVs:
        from PyConf.Algorithms import TrackVertexMonitor
        monitorList.append(
            TrackVertexMonitor(
                name=alignmentPVs.producer.name + "VertexMonitor",
                PVContainer=alignmentPVs,
                TrackContainer=alignmentTracks))

    # configure an instance of AlignChisqConstraintTool
    updatetool = createAlignUpdateTool(
        surveyConstraints=surveyConstraints,
        lagrangeConstraints=lagrangeConstraints)

    alignment = createAlignAlgorithm(
        pvs=alignmentPVs,
        tracks=alignmentTracks,
        odin=odin,
        particles=particles,
        elements=elementsToAlign,
        updatetool=updatetool,
        updateInFinalize=updateInFinalize)

    alignment_node = CompositeNode(
        "alignmentNode", [alignment] + monitorList,
        combine_logic=NodeLogic.NONLAZY_OR,
        force_order=True)
    gec = []
    if apply_gec:
        filters.append(require_gec())

    top_node = CompositeNode(
        "filterNode",
        filters + [alignment_node],
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True)

    configure(options, top_node, public_tools=[])


def getXMLWriterList(detectors,
                     prefix='xml',
                     precision=10,
                     online=False,
                     removePivot=True):
    from DDDB.CheckDD4Hep import UseDD4Hep
    topLevelElements = {
        'VP':
        "/dd/Structure/LHCb/BeforeMagnetRegion/VP"
        if not UseDD4Hep else "/world/BeforeMagnetRegion/VP",
        'UT':
        "",
        'FT':
        "/dd/Structure/LHCb/AfterMagnetRegion/T/FT",
        'Muon':
        "/dd/Structure/LHCb/DownstreamRegion/Muon",
        'Ecal':
        "/dd/Structure/LHCb/DownstreamRegion/Ecal"
    }
    subElements = {
        'VP': {
            'Global': [0, 1],
            'Modules': [2, 4]
        },
        'UT': {},  #TODO: UT
        'FT': {
            'FTSystem': [0, 1, 2, 3],
            'Modules': [4],
            'Mats': [5]
        },
        'Muon': {
            'Global': [0, 1, 2],
            'Modules': [2]
        },
        'Ecal': {
            'alignment': []
        }
    }
    xmlWriterList = []
    if not isinstance(detectors, list): detectors = [detectors]
    for detector in detectors:
        topLevelElement = topLevelElements[detector]
        for element, depths in subElements[detector].items():
            depth = "/".join([str(d) for d in depths])
            curString = f'{prefix}/Conditions/{detector}/Alignment/{element}.xml:{topLevelElement}:{precision}:{depth}:{int(online)}:{int(removePivot)}'
            xmlWriterList.append(curString)
    return xmlWriterList
