###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting.BaseTest import LineSkipper
from GaudiConf.QMTest.LHCbExclusions import preprocessor as LHCbPreprocessor

preprocessor = LHCbPreprocessor + LineSkipper(
    strings=[
        "MagneticFieldSvc     INFO Map scaled by factor", "AfterMagnetRegion/",
        "MagnetRegion/", "BeforeMagnetRegion/", "DownstreamRegion",
        "There are invalid params and some of them will be ignored.",
        'Parameter {"approx_on_full_history":true} is ignored, because it cannot be parsed.',
        'ToolSvc                                INFO Removing all tools created by ToolSvc',
        'LHCb::Det::LbDD4hep::DD4hepSvc         INFO Field map location: DBASE/FieldMap/vXrYpZ/cdf',
        'DeMagnetConditionCall                  INFO Loading mag field from DBASE/FieldMap/vXrYpZ/cdf',
        'MagneticFieldExtension                 INFO Scale factor: 1.000000',
        'DeFTDetector                           INFO Current FT geometry version =   64'
    ],
    regexps=[
        r"^Error2:",
        r"^Time to compute solution",
        r"^Wall clock time",
        # # https://sft.its.cern.ch/jira/browse/ROOT-10769
        # r"No precompiled header available .* this may impact performance",
        # sometimes we have the following warning
        #   EscherInit.Brun...SUCCESS Exceptions/Errors/Warnings/Infos Statistics : 0/0/1/0
        #   EscherInit.Brun...SUCCESS  #WARNINGS   = 1        Message = 'Delta Memory for the event exceeds 3*sigma'
        r"EscherInit.*Exceptions/Errors/Warnings/Infos",
        r"^velo_motion_system_yaml",
        r"^DetectorPersistencySvc",
        r"^DetectorDataSvc",
        r"^TransportSvc is currently incompatible with DD4HEP. Disabling its use and thus any material corrections.",
        r"^See https://gitlab.cern.ch/lhcb/Rec/-/issues/326 for more details",
        r"^WARNING: found factory loki_functor",
        r"^FunctorFactory",
        r"^ToolSvc.TrackFunctorFactory",
        r"^# loaded from CACHE",
        r"^# loaded from PYTHON",
        r"^ToolSvc.GitOverlay",
        r"^adding layer:",
        r"^command:"
    ])
